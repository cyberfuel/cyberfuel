<?php
namespace App\Http\Traits;

use Illuminate\Support\Facades\Session;

trait FuncionesGenerales {

    function obtener_ambiente(){
        $ambiente = "test";
        if(Session::has("cyberfuel_ambiente")) {
            if(in_array(Session::get("cyberfuel_ambiente"), array("api", "test"))){
                $ambiente = Session::get("cyberfuel_ambiente");
            }
        }
        return $ambiente;
    }

    //------------------------------------------------------------------------------------------------------------------

    function verifica_identificacion_cliente($identificacion = ""){
        $datos_verificacion = array();
        if($identificacion != ""){
            //Ambiente
            $ambiente = $this->obtener_ambiente();

            $parametros = array(
                "usuario" => env("WS_FUNCIONES_PSC_USUARIO_" . strtoupper($ambiente)),
                "clave"   => env("WS_FUNCIONES_PSC_CLAVE_" . strtoupper($ambiente)),
                "identificacion" => $identificacion
            );
            $parametros_api = json_encode($parametros);

            $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/verifica_identificacion_cliente/test";
            if($ambiente == "api") {
                $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/verifica_identificacion_cliente/api";
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros_api);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curl_response = curl_exec($curl);
            if(curl_errno($curl) == 0) {
                $datos_verificacion = json_decode($curl_response, true);
            }
            curl_close($curl);
        }
        return $datos_verificacion;
    }

    //------------------------------------------------------------------------------------------------------------------

    function registrar_nuevo_cliente($datos_cliente = null){
        $resultado_registro = array();
        if($datos_cliente != null){
            //Ambiente
            $ambiente = $this->obtener_ambiente();

            $parametros = array(
                "usuario"    => env("WS_FUNCIONES_PSC_USUARIO_" . strtoupper($ambiente)),
                "clave"      => env("WS_FUNCIONES_PSC_CLAVE_" . strtoupper($ambiente)),
                "tipo"       => $datos_cliente["tipo_cedula"],
                "num_cedula" => $datos_cliente["cedula"],
                "nombre"     => $datos_cliente["nombre"],
                "telefono"   => $datos_cliente["celular"],
                "telefono2"  => $datos_cliente["telefono"],
                "correo"     => $datos_cliente["correo"],
                "direccion"  => $datos_cliente["direccion"]
            );

            //Contacto administrativo
            $parametros["contactos"][0] = array(
                "tipo"     => "administrador",
                "nombre"   => trim($datos_cliente["nombre_admin"]) . " " . trim($datos_cliente["apellido_admin"]),
                "telefono" => $datos_cliente["tel_admin"],
                "correo"   => $datos_cliente["correo_admin"]
            );

            //Contacto tecnico
            $parametros["contactos"][1] = array(
                "tipo"     => "tecnico",
                "nombre"   => trim($datos_cliente["nombre_tecnico"]) . " " . trim($datos_cliente["apellido_tecnico"]),
                "telefono" => $datos_cliente["tel_tecnico"],
                "correo"   => $datos_cliente["correo_tecnico"]
            );

            //Contacto cobros
            $parametros["contactos"][2] = array(
                "tipo"     => "cobros",
                "nombre"   => trim($datos_cliente["nombre_cobro"]) . " " . trim($datos_cliente["apellido_cobro"]),
                "telefono" => $datos_cliente["tel_cobro"],
                "correo"   => $datos_cliente["correo_cobro"]
            );
            $parametros_api = json_encode($parametros);

            $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/registrar_nuevo_cliente/test";
            if($ambiente == "api") {
                $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/registrar_nuevo_cliente/api";
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros_api);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curl_response = curl_exec($curl);
            if(curl_errno($curl) == 0){
                $resultado_registro = json_decode($curl_response, true);
            }
            curl_close($curl);
        }
        return $resultado_registro;
    }

    //------------------------------------------------------------------------------------------------------------------

    function crear_factura($id_cliente = 0){
        $resultado_factura = array();
        if($id_cliente > 0){

        }
        return $resultado_factura;
    }

    //------------------------------------------------------------------------------------------------------------------

    function carrito_compras_obtener_total_productos(){
        $total_productos_carrito_compras = 0;

        //Total productos del carrito de compras
        if(Session::has("cyberfuel_carrito_compras")) {
            $carrito_compras = Session::get("cyberfuel_carrito_compras");
            $total_productos_carrito_compras = count($carrito_compras);
        }
        echo $total_productos_carrito_compras;
    }

    //------------------------------------------------------------------------------------------------------------------

    function obtener_datos_servicios($codigos = ""){
        $datos_servicios = array();
        if($codigos != ""){
            //Ambiente
            $ambiente = $this->obtener_ambiente();

            $parametros = array(
                "usuario" => env("WS_FUNCIONES_PSC_USUARIO_" . strtoupper($ambiente)),
                "clave"   => env("WS_FUNCIONES_PSC_CLAVE_" . strtoupper($ambiente)),
                "codigos" => $codigos
            );
            $parametros_api = json_encode($parametros);

            $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/consultar_servicios_por_codigo/test";
            if($ambiente == "api") {
                $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/consultar_servicios_por_codigo/api";
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros_api);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curl_response = curl_exec($curl);
            if(curl_errno($curl) == 0) {
                $datos_servicios = json_decode($curl_response, true);
            }
            curl_close($curl);
        }
        return $datos_servicios;
    }

    //------------------------------------------------------------------------------------------------------------------

    function consultar_dominio_rrp_proxy($dominio = ""){
        $datos_dominio = array();
        if($dominio != ""){
            //Ambiente
            $ambiente = $this->obtener_ambiente();

            $parametros = array(
                "usuario" => env("WS_FUNCIONES_RRP_PROXY_USUARIO_" . strtoupper($ambiente)),
                "clave"   => env("WS_FUNCIONES_RRP_PROXY_CLAVE_" . strtoupper($ambiente)),
                "dominio" => $dominio
            );
            $parametros_api = json_encode($parametros);

            $url = "https://paybac.cyberfuel.com/rrp_proxy/public/consulta_dominio/test";
            if($ambiente == "api") {
                $url = "https://paybac.cyberfuel.com/rrp_proxy/public/consulta_dominio/api";
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros_api);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curl_response = curl_exec($curl);
            if(curl_errno($curl) == 0) {
                $datos_dominio = json_decode($curl_response, true);
            }
            curl_close($curl);
        }
        return $datos_dominio;
    }

    //------------------------------------------------------------------------------------------------------------------

    function obtener_sugerencias_dominio_rrp_proxy($dominio = ""){
        $datos_dominio = array();
        if($dominio != ""){
            //Ambiente
            $ambiente = $this->obtener_ambiente();

            $parametros = array(
                "usuario" => env("WS_FUNCIONES_RRP_PROXY_USUARIO_" . strtoupper($ambiente)),
                "clave"   => env("WS_FUNCIONES_RRP_PROXY_CLAVE_" . strtoupper($ambiente)),
                "dominio" => $dominio
            );
            $parametros_api = json_encode($parametros);

            $url = "https://paybac.cyberfuel.com/rrp_proxy/public/sugerencias_dominio/test";
            if($ambiente == "api") {
                $url = "https://paybac.cyberfuel.com/rrp_proxy/public/sugerencias_dominio/api";
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros_api);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curl_response = curl_exec($curl);
            if(curl_errno($curl) == 0) {
                $datos_dominio = json_decode($curl_response, true);
            }
            curl_close($curl);
        }
        return $datos_dominio;
    }

    //------------------------------------------------------------------------------------------------------------------

    function obtener_nombre_dominio($dominio = ""){
        $array_nombre_dominio = array();
        if($dominio != ""){
            //Elimina espacios
            $dominio = trim($dominio);
            $dominio = str_replace(" ", "", $dominio);

            //Array del dominio
            $array_dominio = explode(".", $dominio);
            $ultima_posicion = count($array_dominio) - 1;

            //Obtiene extension y elimina ultimo elemento del array
            $solo_extension = $array_dominio[$ultima_posicion];
            unset($array_dominio[$ultima_posicion]);

            //Obtiene solo nombre de dominio
            $solo_nombre = implode("", $array_dominio);

            $array_nombre_dominio = array("dominio" => $solo_nombre. "." . $solo_extension, "nombre" => $solo_nombre, "extension" => $solo_extension);
        }
        return $array_nombre_dominio;
    }

    //------------------------------------------------------------------------------------------------------------------

    function validar_cupon_descuento($cupon_descuento = ""){
        $datos_cupon_descuento = array();
        if($cupon_descuento != ""){
            //Ambiente
            $ambiente = $this->obtener_ambiente();

            $parametros = array(
                "usuario" => env("WS_FUNCIONES_PSC_USUARIO_" . strtoupper($ambiente)),
                "clave"   => env("WS_FUNCIONES_PSC_CLAVE_" . strtoupper($ambiente)),
                "cupon_descuento" => $cupon_descuento,
                "sitio_web" => "cyberfuel"
            );
            $parametros_api = json_encode($parametros);

            $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/validar_cupon_descuento/test";
            if($ambiente == "api") {
                $url = "https://paybac.cyberfuel.com/ws_funciones_psc/public/validar_cupon_descuento/api";
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros_api);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curl_response = curl_exec($curl);
            if(curl_errno($curl) == 0) {
                $datos_cupon_descuento = json_decode($curl_response, true);
            }
            curl_close($curl);
        }
        return $datos_cupon_descuento;
    }

}

?>
