<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactenosRequests extends FormRequest{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool{
        return true;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes(): array{
        return [
            'nombre'=>'Nombre completo',
            'email'=>'Correo electrónico',
            'departamento'=>'Departamento',
            'servicio'=>'Servicio',
            'mensaje'=>'Mensaje',
            //'g-recaptcha-response'=>'Recaptcha'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array{
        return [
            'nombre'=>'required|string',
            'email'=>'required|email',
           // 'departamento'=>[
//                'required',
//                'in:'.implode(',', array_keys(config('departamento')))
//            ],
            'departamento'=>'nullable|string',
            'servicio'=>'nullable|string',
            'mensaje'=>'required|string|max:1000',
            //'g-recaptcha-response'=>'required|captcha'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
//    public function messages(): array{
//        return [
//            'nombre.required'=>'El :attribute es requerido.',
//            'nombre.string'=>'El :attribute debe ser una cadena.',
//            'mensaje.required'=>'El :attribute es requerido.',
//            'mensaje.string'=>'El :attribute debe ser una cadena.',
//            'mensaje.max'=>'El :attribute no debe ser mayor a :max caracteres.',
//            'departamento.required'=>'El :attribute es requerido.',
//            'departamento.in'=>'El :attribute seleccionado es inválido.',
//             'servicio.required'=>'El :attribute es requerido.',
//            'servicio.in'=>'El :attribute seleccionado es inválido.',
//            'codigo.string'=>'El :attribute debe ser una cadena.',
//            'g-recaptcha-response.required'=>'Debe verificar que no sea un robot.',
//            'g-recaptcha-response.captcha'=>'Debe verificar que no sea un robot.',
//        ];
//    }
}