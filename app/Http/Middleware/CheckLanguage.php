<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (session()->has('language')) {
            if (app()->getLocale()!= session()->get('language')) {
              app()->setLocale(session()->get('language'));
            }
          }else {
             session()->put('language', 'es');
              app()->setLocale('es');
         }
          return $next($request);
    }
}