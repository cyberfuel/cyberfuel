<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Traits\FuncionesGenerales;

class MainController extends Controller{

    use FuncionesGenerales;

    public function index(){
        //Ambiente (test o api)
        if(!Session::has('cyberfuel_ambiente')){
            Session::put("cyberfuel_ambiente", "test");
            Session::save();
        }

        //Obtener precio de dominio
        $registro_dominio = $this->obtener_datos_servicios("TLD-REG");

        //Obtener productos del carrito
        $carrito_compras = array();
        if(Session::has('cyberfuel_carrito_compras')){
            $carrito_compras = Session::get('cyberfuel_carrito_compras');
        }
        return view('index')->with('carrito_compras', $carrito_compras)->with('registro_dominio', $registro_dominio);
    }

    public function ver_version_laravel(){
        $laravel = app();
        $version = $laravel::VERSION;
        dd($version);
    }

}
