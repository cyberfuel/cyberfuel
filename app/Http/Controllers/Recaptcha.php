<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;

class Recaptcha
{
    private $site_key = null;
    private $site_secret = null;

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function __construct($site_key, $site_secret){
        $this->site_key     = $site_key;
        $this->site_secret  = $site_secret;
    }

    public function validarRecaptcha($action, $recaptcha_response, $porcentaje = 0.5){
        $respuesta = array();
        try{
            // Build POST request:
            $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
            $recaptcha_secret = $this->site_secret;

            // Make and decode POST request:
            $arrResponse = file_get_contents($recaptcha_url . '?secret=' . $this->site_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($arrResponse, true);

            // verify the response
            if($recaptcha["success"] == '1' && $recaptcha["action"] == $action && $recaptcha["score"] >= $porcentaje) {
                $respuesta = array(
                    'status'    => true,
                    'recaptcha' => $recaptcha
                );
            } else {
                throw new Exception('No se ha verificado el formulario');
            }
        }catch(Exception $e){
            $respuesta = array(
                'status'        => false
                ,'mensaje'      => $e->getMessage()
                ,'recaptcha'    => $recaptcha
            );
        }
        return $respuesta;
    }
}
