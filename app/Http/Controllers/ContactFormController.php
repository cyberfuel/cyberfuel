<?php

namespace App\Http\Controllers;
use App\Http\Requests\ContactenosRequests;
use App\Mail\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Recaptcha;
use Illuminate\Validation\ValidationException;


class ContactFormController extends Controller
{
       public function form(){
        
        return view('contactenos', [
         
            'recaptcha_site' => env('reCAPTCHA_SITE_INV',''),
            'recaptcha_secret' => env('reCAPTCHA_SECRET_INV','')
        ]);
           
           
    }
    
    
 
    
        ///funcion para enviar el correo
    public function send(ContactenosRequests $request)
    {
        
          $data=$request->validated();
        if(!($email_departamento=config(implode('.', ['departamento', $data['departamento'], 'email']), []))){
            throw ValidationException::withMessages([
                'departamento'=>'No se ha encontrado ningún correo electrónico válido para el departamento'
            ]);
        }
        
       
        
        // Se llama al controllador del recaptcha para setear los valores y que se ejecute para la verificacion
        $recaptcha_site = env('MAIL_USERNAME','');
        $recaptcha_secret = env('reCAPTCHA_SECRET_INV','');   
   
        $recaptcha =  new Recaptcha($recaptcha_site, $recaptcha_secret);
        $recaptcha_Answer = $recaptcha->validarRecaptcha('contacto', $request->input('recaptcha_response'));
    // dd($recaptcha_Answer );
        if ($recaptcha_Answer['status'] == false){
            return back()->withErrors([$recaptcha_Answer['mensaje']])->withInput();
        }else if($recaptcha_Answer['status'] == true){
             
            $objCorreo = new \stdClass();
            $objCorreo->nombre = $request[ "nombre" ];
            $objCorreo->email = $request[ "email" ];
            $objCorreo->departamento = $request[ "departamento" ];
            $objCorreo->servicio = $request[ "servicio" ];
            $objCorreo->mensaje = $request[ "mensaje" ];
            $objCorreo->recaptcha_response = $request[ "recaptcha_response" ];
            //Mail::to("gsoto@cyberfuel.com")->send(new ContactForm($objCorreo));
            
            
             
            Mail::mailer('smtp')
            ->to($email_departamento)
            ->cc($data['email'])
            ->send(new ContactForm($objCorreo));

       // if (Mail::failures()) {
//            throw ValidationException::withMessages([
//                'email'=>'Ha ocurrido un error al enviar el correo electrónico'
//            ]);
//        }
        //Toastr()->success('Se ha enviado su solicitud exitosamente', 'Solicitud enviada');
        //return back();
            
//echo "<pre>";
//echo "<hr>";
//print_r($objCorreo); 
//echo "<hr>";
//echo "</pre>";
//die('-');
            
      
           
          return back()->with('message', ['success', 'Mensaje enviado correctamente']);
        };       
    }
    
    
    
}
