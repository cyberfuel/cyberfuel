<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Traits\FuncionesGenerales;

class HospedajeController extends Controller
{

    use FuncionesGenerales;

    public function index($slug)
    {
        //Ambiente (test o api)
        if (!Session::has("cyberfuel_ambiente")) {
            Session::put("cyberfuel_ambiente", "test");
            Session::save();
        }

        /*if(Session::has("cyberfuel_carrito_compras")) {
            $carritos_compras = Session::get("cyberfuel_carrito_compras");
            echo "<br><br><br><br><br><br><br><br>";
            echo "<pre>";
            echo print_r($carritos_compras);
            echo "</pre>";
            //die;
        }else{
            echo "NO SETEADA";
        }*/

        //Obtener productos del carrito
        //Session::forget('cyberfuel_carrito_compras');
        $carrito_compras = array();
        if (Session::has("cyberfuel_carrito_compras")) {
            $carrito_compras = Session::get("cyberfuel_carrito_compras");
        }

        //Cupones de descuento
        $cupon_descuento_aplicado = array();
        if (Session::has("cupon_descuento_cyberfuel")) {
            $cupon_descuento_aplicado = Session::get("cupon_descuento_cyberfuel");
        }

        //Datos en sesion de cliente temporal
        $datos_cliente_temp = array();
        if (Session::has('cliente_temp_cyberfuel')) {
            $datos_cliente_temp = Session::get('cliente_temp_cyberfuel');
        }

        switch ($slug) {
                //Hospedaje web
            case 'web-hosting':
            case 'hospedaje-web':
                $planes_hospedaje = $this->obtener_datos_planes_hospedaje();
                return view('hospedaje-web')->with('planes_hospedaje', $planes_hospedaje)
                    ->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Comercio electronico
            case 'electronic-commerce':
            case 'comercio-electronico':
                return view('comercio-electronico')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Nombres-de-dominios
            case 'domain-names':
            case 'nombres-de-dominios':
                return view('nombres-de-dominios')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Filtro de correo
            case 'spam-filter':
            case 'filtro-de-correo-no-deseado':
                return view('filtro-de-correo-no-deseado')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Seguridad
            case 'security-and-protection':
            case 'seguridad-y-proteccion':
                return view('seguridad-y-proteccion')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);

                break;

                //Almacenamiento
            case 'storage':
            case 'almacenamiento':
                return view('almacenamiento')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);

                break;

                //Mercado
            case 'marketplace':
            case 'mercado':
                return view('mercado')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);

                break;



                //Servidores
            case 'servers':
            case 'servidores':
                return view('servidores')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Servicios en la nube
            case 'cloud-servers':
            case 'servicios-nube':
                return view('servicios_nube')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Baas
            case 'baas':
                return view('baas')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Draas
            case 'draas':
                return view('draas')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;




                //Centro de datos
            case 'data-center':
            case 'centro-de-datos':
                return view('centro-de-datos')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Contactenos
            case 'contact-us':
            case 'contactenos':
                return view('contactenos')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;


                //Detalle pago
            case 'payment-detail':
            case 'detalle-de-pago':
                if ((isset($carrito_compras)) && ($carrito_compras != null) && (isset($datos_cliente_temp)) && ($datos_cliente_temp != null)) {
                    return view('detalle-de-pago')->with('carrito_compras', $carrito_compras)
                        ->with('datos_cliente_temp', $datos_cliente_temp)
                        ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                } else {
                    //return redirect()->route('main');
                    return view('detalle-de-pago')->with('carrito_compras', $carrito_compras)
                        ->with('datos_cliente_temp', $datos_cliente_temp)
                        ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                }
                break;

                //Nuevo-cliente
            case 'new-client':
            case 'nuevo-cliente':
                return view('nuevo-cliente')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Compra
            case 'buy':
            case 'compra':
                $datos_seccion = $this->datos_seccion_compra();
                return view('compra')->with('carrito_compras', $carrito_compras)
                    ->with('dominio', $datos_seccion["dominio"])
                    ->with('array_dominio', $datos_seccion["array_dominio"])
                    ->with('consulta_dominio', $datos_seccion["consulta_dominio"])
                    ->with("registro_dominio", $datos_seccion["registro_dominio"])
                    ->with('planes_hospedaje', $datos_seccion["planes_hospedaje"])
                    ->with('complementos', $datos_seccion["complementos"])
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Terminos y condiciones
            case 'terms-conditions':
            case 'terminos-condiciones':
                return view('footer/terminos-condiciones')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Acerca de cyberfuel
            case 'about-cyberfuel':
            case 'acerca-de-cyberfuel':
                return view('footer/acerca-de-cyberfuel')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //comparacion hospedajes
            case 'hosting-compare':
            case 'comparacion-hospedajes':
                return view('includes/hospedajes_comparacion')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Politicas privacidad
            case 'privacy-policy':
            case 'politica-de-privacidad':
                return view('footer/politica-de-privacidad')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Politica anti spam
            case 'anti-spam-policy':
            case 'politica-anti-spam':
                return view('footer/politica-anti-spam')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Carrito de compra
            case 'shopping-cart':
            case 'carrito-de-compra':
                return view('footer/carrito-de-compra')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Herramientas
            case 'tools':
            case 'herramientas':
                return view('footer/herramientas')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Mapa-sitio
            case 'site-map':
            case 'mapa-del-sitio':
                return view('footer/mapa-del-sitio')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //hospedajes

            case 'hospedajes':
                return view('includes/hospedajes')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //Producto
            case 'product':
            case 'producto':
                return view('productos/producto')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //orden completa
            case 'order-complete':
            case 'orden-completa':
                return view('orden-completa')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

                //orden fallida
            case 'order-fail':
            case 'orden-fallida':
                return view('orden-fallida')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;

            case 'auth':
                return view('auth')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;



                //Default
            default:
                return view('index')->with('carrito_compras', $carrito_compras)
                    ->with('datos_cliente_temp', $datos_cliente_temp)
                    ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
                break;
        }
    }

    //--------------------------------------------------------------------------------------------------------------------

    function guardar_datos_temporales_cliente(Request $request)
    {
        $array_resultado = array();
        $total_productos = 0;
        $cliente_ya_registrado = "no";

        //Verifica si hay productos en el carrito
        $array_resultado = array("estado" => "error", "mensaje" => __('compra.error_carrito_vacio'));
        if (Session::has("cyberfuel_carrito_compras")) {
            $carrito_compras = Session::get("cyberfuel_carrito_compras");
            if ((isset($carrito_compras)) && ($carrito_compras != null)) {
                $total_productos = count($carrito_compras);
                if ($total_productos > 0) {
                    $array_resultado = array("estado" => "ok", "mensaje" => "");
                }
            }
        }

        //Verifica si la cedula ya esta registrada en el sistema
        if ($total_productos > 0) {
            $verifica_cliente_registrado = $this->verifica_identificacion_cliente($request->input("cedula"));
            if ((isset($verifica_cliente_registrado["estado"])) && ($verifica_cliente_registrado["estado"] == "ok")) {
                if ((isset($verifica_cliente_registrado["info"])) && ($verifica_cliente_registrado["info"] != null)) {
                    $cliente_ya_registrado = "si";
                    $array_resultado = array("estado" => "error", "mensaje" => __('compra.error_ya_registrado'));
                }
            }

            //Si el cliente no se encuentra registrado
            if ($cliente_ya_registrado == "no") {
                $datos_cliente_temp = array(
                    "paises" => $request->input("paises"),
                    "tipo_cedula" => $request->input("tipo_cedula"),
                    "nombre" => $request->input("nombre"),
                    "cedula" => $request->input("cedula"),
                    "correo" => $request->input("correo"),
                    "celular" => $request->input("celular"),
                    "telefono" => $request->input("telefono"),
                    "telefono2" => $request->input("telefono2"),
                    "direccion" => $request->input("direccion"),
                    "nombre_admin" => $request->input("nombre_admin"),
                    "apellido_admin" => $request->input("apellido_admin"),
                    "correo_admin" => $request->input("correo_admin"),
                    "tel_admin" => $request->input("tel_admin"),
                    "chk_tecnico" => $request->input("chk_tecnico"),
                    "nombre_tecnico" => $request->input("nombre_tecnico"),
                    "apellido_tecnico" => $request->input("apellido_tecnico"),
                    "correo_tecnico" => $request->input("correo_tecnico"),
                    "tel_tecnico" => $request->input("tel_tecnico"),
                    "chk_cobro" => $request->input("chk_cobro"),
                    "nombre_cobro" => $request->input("nombre_cobro"),
                    "apellido_cobro" => $request->input("apellido_cobro"),
                    "correo_cobro" => $request->input("correo_cobro"),
                    "tel_cobro" => $request->input("tel_cobro")
                );
                Session::forget('cliente_temp_cyberfuel');
                Session::put("cliente_temp_cyberfuel", $datos_cliente_temp);
                Session::save();
                $array_resultado = array("estado" => "ok", "mensaje" => "");
            }
        }

        echo json_encode(
            array(
                "estado"  => $array_resultado["estado"],
                "titulo"  => __('compra.titulo_registro_cliente'),
                "mensaje" => $array_resultado["mensaje"],
                "boton_aceptar" => __('compra.boton_aceptar')
            )
        );
        die;
    }

    //--------------------------------------------------------------------------------------------------------------------

    function procesar_registro_con_compra()
    {
        $resultado = array("estado" => "error", "mensaje" => __('compra.error_registro_general'));

        if ((isset($_POST["registro"])) && ($_POST["registro"] == "cliente")) {
            //Verifica datos de cliente en sesion
            if (Session::has("cliente_temp_cyberfuel")) {
                $datos_cliente_temp = Session::get("cliente_temp_cyberfuel");
                if ((isset($datos_cliente_temp)) && ($datos_cliente_temp != null)) {

                    //Registrar cliente
                    $registro_cliente = $this->registrar_nuevo_cliente($datos_cliente_temp);
                    if ((isset($registro_cliente["estado"])) && ($registro_cliente["estado"] == "ok")) {
                        if ((isset($registro_cliente["id_cliente"])) && ($registro_cliente["id_cliente"] > 0)) {
                            $id_cliente = $registro_cliente["id_cliente"];
                            $resultado = array("estado" => "ok", "mensaje" => __('compra.cliente_registrado'));

                            //Carrito de compras
                            /*$carrito_compras = array();
                            if(Session::has("cyberfuel_carrito_compras")){
                                $carrito_compras = Session::get("cyberfuel_carrito_compras");
                            }*/

                            //Crear factura
                            //$registro_factura = $this->crear_factura($id_cliente, $carrito_compras);
                        }
                    } else {
                        if ((isset($registro_cliente["mensaje"])) && ($registro_cliente["mensaje"] == "Cliente ya registrado anteriormente")) {
                            $resultado = array("estado" => "error", "mensaje" => __('compra.error_ya_registrado'));
                        } else {
                            $resultado = array("estado" => "error", "mensaje" => __('compra.error_registro_general'));
                        }
                    }
                }
            }
        }

        $resultado_registro_compra = array(
            "estado"  => $resultado["estado"],
            "titulo"  => __('compra.titulo_registro_cliente'),
            "mensaje" => $resultado["mensaje"],
            "boton_aceptar" => __('compra.boton_aceptar')
        );
        echo json_encode($resultado_registro_compra);
        die;
    }

    //--------------------------------------------------------------------------------------------------------------------

    function aplicar_cupon_descuento()
    {
        $aplicar_cupon = "error";
        if ((isset($_POST["cupon_descuento"])) && ($_POST["cupon_descuento"] != "")) {
            $cupon_descuento = trim($_POST["cupon_descuento"]);

            $consulta_cupon_descuento = $this->validar_cupon_descuento($cupon_descuento);
            if ((isset($consulta_cupon_descuento["estado"])) && ($consulta_cupon_descuento["estado"] == "ok")) {
                if ((isset($consulta_cupon_descuento["info"][0]["codigo"])) && ($consulta_cupon_descuento["info"][0]["codigo"] != null)) {
                    $aplicar_cupon = "ok";
                    $info_cupon_descuento = array();

                    //Informacion del cupon de descuento (porcentaje)
                    if (($consulta_cupon_descuento["info"][0]["porcentaje"] > 0) && ($consulta_cupon_descuento["info"][0]["monto"] == 0)) {
                        $info_cupon_descuento = array(
                            "tipo_descuento"  => "porcentaje",
                            "cupon_descuento" => $consulta_cupon_descuento["info"][0]["codigo"],
                            "valor_descuento" => $consulta_cupon_descuento["info"][0]["porcentaje"]
                        );
                    }

                    //Informacion del cupon de descuento (monto)
                    if (($consulta_cupon_descuento["info"][0]["monto"] > 0) && ($consulta_cupon_descuento["info"][0]["porcentaje"] == 0)) {
                        $info_cupon_descuento = array(
                            "tipo_descuento"  => "monto",
                            "cupon_descuento" => $consulta_cupon_descuento["info"][0]["codigo"],
                            "valor_descuento" => $consulta_cupon_descuento["info"][0]["monto"]
                        );
                    }

                    //Guarda en sesion el cupon de descuento
                    Session::forget("cupon_descuento_cyberfuel");
                    Session::put("cupon_descuento_cyberfuel", $info_cupon_descuento);
                    Session::save();
                }
            }
        }
        echo $aplicar_cupon;
        die;
    }

    //--------------------------------------------------------------------------------------------------------------------

    function eliminar_cupon_descuento()
    {
        if ((isset($_POST["eliminar_cupon_descuento"])) && ($_POST["eliminar_cupon_descuento"] == "eliminar")) {
            Session::forget("cupon_descuento_cyberfuel");
            Session::save();
        }
        die;
    }

    //--------------------------------------------------------------------------------------------------------------------

    function datos_seccion_compra()
    {
        $dominio = "";
        $array_dominio = array();
        $consulta_dominio = array();
        if (Session::has('cyberfuel_dominio')) {
            $array_dominio = Session::get('cyberfuel_dominio');
            $dominio = $array_dominio["dominio"];

            //Consulta disponibilidad del dominio en RRP PROXY
            $consulta_dominio = $this->consultar_dominio_rrp_proxy($dominio);
        }

        //Obtener precio de dominio
        $registro_dominio = $this->obtener_datos_servicios("TLD-REG");

        //Planes de hospedaje
        $planes_hospedaje = $this->obtener_datos_planes_hospedaje();

        //Complementos
        $complementos = $this->obtener_datos_complementos();

        return array(
            "dominio"          => $dominio,
            "array_dominio"    => $array_dominio,
            "consulta_dominio" => $consulta_dominio,
            "registro_dominio" => $registro_dominio,
            "planes_hospedaje" => $planes_hospedaje,
            "complementos"     => $complementos
        );
    }

    //--------------------------------------------------------------------------------------------------------------------

    function consultar_dominio()
    {
        if (isset($_POST["dominio"])) {
            $dominio = trim($_POST["dominio"]);
            if ($dominio != "") {
                $array_dominio = $this->obtener_nombre_dominio($dominio);

                Session::put("cyberfuel_dominio", $array_dominio);
                Session::save();
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------------------

    function cargar_sugerencias_dominios()
    {
        $contador = 1;
        $registro_dominio = array();
        $dominios_sugeridos = array();

        if (Session::has('cyberfuel_dominio')) {
            //Obtener precio de dominio
            $registro_dominio = $this->obtener_datos_servicios("TLD-REG");

            //Sugerencias de dominios
            $dominio = Session::get('cyberfuel_dominio');
            $resultado_dominios_sugeridos = $this->obtener_sugerencias_dominio_rrp_proxy($dominio["dominio"]);
            if ((isset($resultado_dominios_sugeridos["CODE"])) && ($resultado_dominios_sugeridos["CODE"] == 200)) {
                if ((isset($resultado_dominios_sugeridos["PROPERTY"]["NAME"])) && isset($resultado_dominios_sugeridos["PROPERTY"]["AVAILABILITY"])) {
                    foreach ($resultado_dominios_sugeridos["PROPERTY"]["NAME"] as $index => $nombre_dominio_sugerido) {
                        $agregado_carrito = "no";

                        if ($resultado_dominios_sugeridos["PROPERTY"]["AVAILABILITY"][$index] == "available") {
                            if ($nombre_dominio_sugerido != $dominio["dominio"]) {
                                //Obtener por separado el dominio y la extension (TLD)
                                $array_dominio = $this->obtener_nombre_dominio($nombre_dominio_sugerido);

                                //Verifica si ya fue agregado anteriormente al carrito
                                if (Session::has('cyberfuel_dominio')) {
                                    $carrito_compras = Session::get('cyberfuel_carrito_compras');
                                    if ((isset($carrito_compras)) && ($carrito_compras != null)) {
                                        foreach ($carrito_compras as $info_carrito) {
                                            if (($info_carrito["codigo"] == "TLD-REG") && ($info_carrito["titulo"] == $nombre_dominio_sugerido)) {
                                                $agregado_carrito = "si";
                                            }
                                        }
                                    }
                                }

                                //Datos del dominio sugerido
                                $dominios_sugeridos[$contador]["id_campo"]  = $contador;
                                $dominios_sugeridos[$contador]["dominio"]   = $nombre_dominio_sugerido;
                                $dominios_sugeridos[$contador]["nombre"]    = $array_dominio["nombre"];
                                $dominios_sugeridos[$contador]["extension"] = $array_dominio["extension"];
                                $dominios_sugeridos[$contador]["agregado"]  = $agregado_carrito;
                                $contador++;
                            }
                        }
                    }
                }
            }
        }
        return view('dominios_sugerencias')->with('dominios_sugeridos', $dominios_sugeridos)->with('registro_dominio', $registro_dominio);
    }

    //--------------------------------------------------------------------------------------------------------------------

    function obtener_datos_planes_hospedaje()
    {
        $planes_hospedaje = array();

        //Codigos de los planes de hospedaje
        $codigos_plan_personal    = array("WH-LPPE", "WH-WPPE");   //Plan Personal
        $codigos_plan_pymes       = array("WH-LPPY", "WH-WPPY");   //Plan Pymes
        $codigos_plan_empresarial = array("WH-LPEM", "WH-WPEM");   //Plan Empresarial
        $codigos_comercio_electronico = array("WH-LPEC");          //Comercio Electronico
        $array_codigos = array_merge($codigos_plan_personal, $codigos_plan_pymes, $codigos_plan_empresarial, $codigos_comercio_electronico);
        $codigos = implode(",", $array_codigos);

        //Obtener datos de planes de hospedaje
        $info_planes_hospedaje = $this->obtener_datos_servicios($codigos);
        if ((isset($info_planes_hospedaje["estado"])) && ($info_planes_hospedaje["estado"] == "ok")) {
            if ((isset($info_planes_hospedaje["info"])) && ($info_planes_hospedaje["info"] != null)) {
                foreach ($info_planes_hospedaje["info"] as $info_servicio) {
                    $codigo_servicio = $info_servicio["codigo"];

                    //Plan personal
                    if (in_array($info_servicio["codigo"], $codigos_plan_personal) !== false) {
                        $planes_hospedaje["personal"][$codigo_servicio] = $info_servicio;
                    }

                    //Plan pymes
                    if (in_array($info_servicio["codigo"], $codigos_plan_pymes) !== false) {
                        $planes_hospedaje["pymes"][$codigo_servicio] = $info_servicio;
                    }

                    //Plan empresarial
                    if (in_array($info_servicio["codigo"], $codigos_plan_empresarial) !== false) {
                        $planes_hospedaje["empresarial"][$codigo_servicio] = $info_servicio;
                    }

                    //Comercio electronico
                    if (in_array($info_servicio["codigo"], $codigos_comercio_electronico) !== false) {
                        $planes_hospedaje["comercio_electronico"][$codigo_servicio] = $info_servicio;
                    }
                }
            }
        }
        return $planes_hospedaje;
    }

    //--------------------------------------------------------------------------------------------------------------------

    function obtener_datos_complementos()
    {
        $planes_complementos = array();

        //Obtener datos de complementos
        $codigos_complementos = array("WH-EMB", "AD-SSL");
        $str_codigos_complementos = implode(",", $codigos_complementos);
        $info_planes_complementos = $this->obtener_datos_servicios($str_codigos_complementos);
        if ((isset($info_planes_complementos["estado"])) && ($info_planes_complementos["estado"] == "ok")) {
            if ((isset($info_planes_complementos["info"])) && ($info_planes_complementos["info"] != null)) {
                foreach ($info_planes_complementos["info"] as $info_servicio) {
                    $codigo_servicio = $info_servicio["codigo"];
                    $planes_complementos[$codigo_servicio] = $info_servicio;
                }
            }
        }
        return $planes_complementos;
    }

    //--------------------------------------------------------------------------------------------------------------------

    public function carrito_compras_agregar_producto()
    {
        $codigo_servicio = trim($_POST["codigo_servicio"]);
        $familia  = trim($_POST["familia"]);
        $titulo   = trim($_POST["titulo"]);
        $precio   = trim($_POST["precio"]);
        $periodo  = trim($_POST["periodo"]);
        $cantidad = trim($_POST["cantidad"]);

        //Periodos
        switch ($periodo) {
            case "mensual":
                if ($cantidad == 1) {
                    $familia .= sprintf(" - %s mes", $cantidad);
                } else {
                    $familia .= sprintf(" - %s meses", $cantidad);
                }
                break;

            case "anual":
                if ($cantidad == 1) {
                    $familia .= sprintf(" - %s año", $cantidad);
                } else {
                    $familia .= sprintf(" - %s años", $cantidad);
                }
                break;
        }

        //Datos
        $datos_servicio = array(
            "codigo"   => $codigo_servicio,
            "periodo"  => $periodo,
            "cantidad" => $cantidad,
            "familia"  => $familia,
            "titulo"   => $titulo,
            "precio"   => $precio
        );

        $carrito_compras = array();
        if ((Session::has("cyberfuel_carrito_compras")) && (Session::get("cyberfuel_carrito_compras") != null)) {
            $carrito_compras = Session::get("cyberfuel_carrito_compras");

            if ((isset($carrito_compras)) && ($carrito_compras != null) && is_array($carrito_compras)) {
                $agregar_al_carrito = true;

                //Si es registro de dominio verifica si ya fue agregado el dominio previamente al carrito
                if ($codigo_servicio == "TLD-REG") {
                    foreach ($carrito_compras as $info_carrito) {
                        if (($info_carrito["codigo"] == "TLD-REG") && ($info_carrito["titulo"] == $titulo)) {
                            $agregar_al_carrito = false;
                            break;
                        }
                    }
                }

                if ($agregar_al_carrito == true) {
                    $indice_carrito_compras = array_key_last($carrito_compras) + 1;
                    $carrito_compras[$indice_carrito_compras] = $datos_servicio;
                }
            }
        } else {
            $carrito_compras[0] = $datos_servicio;
        }

        //Guarda productos del carrito de compras
        Session::forget("cyberfuel_carrito_compras");
        Session::put("cyberfuel_carrito_compras", $carrito_compras);
        Session::save();

        die();
    }

    //------------------------------------------------------------------------------------------------------------------

    function carrito_compras_eliminar_producto()
    {
        if ((isset($_POST["index_carrito"])) && ($_POST["index_carrito"] != "")) {
            if (Session::has("cyberfuel_carrito_compras")) {
                $carrito_compras = Session::get("cyberfuel_carrito_compras");

                //Elimina servicio del carrito de compras
                $index_carrito = (int)$_POST["index_carrito"];
                unset($carrito_compras[$index_carrito]);

                //Modifica sesion del carrito de compras
                Session::forget("cyberfuel_carrito_compras");
                Session::put("cyberfuel_carrito_compras", $carrito_compras);
                Session::save();
            }
        }
        die();
    }

    //------------------------------------------------------------------------------------------------------------------

    function carrito_compras_actualizar_lista_productos()
    {
        //Carrito compras
        $carrito_compras = array();
        if (Session::has('cyberfuel_carrito_compras')) {
            $carrito_compras = Session::get('cyberfuel_carrito_compras');
        }

        //Cupones de descuento
        $cupon_descuento_aplicado = array();
        if (Session::has("cupon_descuento_cyberfuel")) {
            $cupon_descuento_aplicado = Session::get("cupon_descuento_cyberfuel");
        }

        //Datos en sesion de cliente temporal
        $datos_cliente_temp = array();
        if (Session::has('cliente_temp_cyberfuel')) {
            $datos_cliente_temp = Session::get('cliente_temp_cyberfuel');
        }
        return view('carrito_compras_actualizar')->with('carrito_compras', $carrito_compras)
            ->with('datos_cliente_temp', $datos_cliente_temp)
            ->with('cupon_descuento_aplicado', $cupon_descuento_aplicado);
    }
}
