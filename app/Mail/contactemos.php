<?php

namespace App\Mail;

use App\Http\Requests\ContactenosRequests;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class contactenos extends Mailable{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var RepuestosRequest
     */
    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactenosRequests $request){
        $this->request=$request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
		return $this->subject('Formulario Contactenos')->view('emails.template-contactenos');
    }
}
