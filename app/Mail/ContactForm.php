<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;   
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('frm-notificaciones@cyberfuel.com')->subject('Formulario de Contáctenos')->view('emails.contactenos', ['data' => $this->data]);
        //return $this->subject('Formulario para - '.$this->request->get('departamento'))->view('emails.contactenos');

               
    }  
     }