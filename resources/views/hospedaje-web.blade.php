@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | {{ __('hospedaje.titulo') }}</title>
<meta name="keywords"
  content="cyberfuel, hospedaje web, alojamiento web, web hosting, webhosting, linux, windows, costa rica, correo electronico, sitio" />
<meta name="description" content="{{ __('hospedaje.descripcion') }}">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="{{ __('hospedaje.titulo') }}">
<meta property="og:title" content="Cyberfuel | {{ __('hospedaje.hospedaje_web') }}">
@endsection


@section('content')
<!--aca empieza el contenido del HTML-->






<section class="mt-4">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <form class="card p-0 mx-1">
          <div class="card-body pt-2 ">
            <div class="tab d-none">
              @include('includes.elegir-hosting')
            </div>
            <div class="tab d-none">
              <div class="accordion" id="carrodecompras">
                <div class="card border-0">
                  @include('includes.elegir-dominio-hospedaje')
                </div>


              </div>
              <div class="tab d-none">
                <div id="opcion_dominio"
                  class="d-inline-flex align-items-center  font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter appear-animation-visible w-100 h-100 text-left"
                  data-appear-animation=" fadeInUpShorter" data-appear-animation-delay="550"
                  style="animation-delay: 550ms;">
                  <div class="row w-100">
                    <div class="col-12">

                      <div class="alert alert-primary mt-4">
                        <strong>Necesita un Dominio, o ya cuenta con uno
                          registrado?</strong>
                      </div>
                    </div>

                    <div class="col-12">
                      <div class="tabs tabs-bottom tabs-center tabs-simple">
                        <ul class="nav nav-tabs nav-justified flex-column flex-md-row">
                          <li class="nav-item">
                            <a class="nav-link active" href="#no_tengo" data-bs-toggle="tab" class="text-center">Lo
                              necesito</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#tengo" data-bs-toggle="tab" class="text-center">Cuento con
                              uno</a>
                          </li>
                        </ul>
                        <div class="tab-content">
                          <div id="no_tengo" class="tab-pane active">
                            <div class="row justify-content-center align-items-center mt-4">
                              <div class="col-lg-3 d-none d-lg-block">
                                <div class=""><img class="m-auto d-block w-75"
                                    src="{{URL::asset('assets/imagenes/dominio.webp')}}" alt="Cyberfuel"></div>
                              </div>
                              <div class="col-lg-9  justify-content-center ">
                                <div class="input-group ">
                                  <input type="text" id="nombre_dominio_pc" name="nombre_dominio_pc"
                                    class="form-control  text-center border-color-success w-50"
                                    placeholder="{{ __('compra.encuentra') }}" required="">
                                  <button onclick="consultar_dominio('{{ __('rutas.url_compra') }}', 'pc');"
                                    type="button" class="btn btn-success mostrardominio text-3 py-1"><i
                                      class="fas fa-search ms-1 "></i>
                                  </button>
                                </div>
                              </div>
                            </div>

                            {{-- Rigistro Privado --}}

                            <div class="row shop">
                              <div class="alert alert-primary mt-4">
                                <strong>Puedes elegir si quieres tener un registro privado.</strong>
                              </div>
                              <div class="col-12 ">
                                <div
                                  class="d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter appear-animation-visible w-100 h-100"
                                  data-appear-animation="fadeInUpShorter" data-appear-animation-delay="550"
                                  style="animation-delay: 550ms;">
                                  <div class="row align-items-center w-100">
                                    <div class="col-10 align-self-start"> <span
                                        class="text-start text-2 float-start"><span
                                          class="text-primary text-3-4">Registro
                                          Privado</span>&nbsp;<i class="fas fa-info-circle ml-2"
                                          data-bs-toggle="tooltip" data-bs-animation="false" data-bs-placement="top"
                                          data-bs-original-title="Tooltip on top"></i></span>
                                    </div>

                                    <div class="col-2 px-2 ">
                                      <p class="price text-4 m-0 float-end d-md-none"> <span
                                          class="sale text-color-dark font-weight-semi-bold">$1.80/yr</span>
                                        <br>
                                      </p>
                                    </div>

                                    <div class="col-10 px-2">
                                      <span class="float-start align-items-center row w-100 row-cols-auto">
                                        <div class="col-auto">
                                          <span class="small lista text-start mt-2">
                                            <strong>Mailboxes Deseados</strong>
                                          </span>
                                        </div>
                                        <div class="col-auto">


                                          <div class="quantity float-none float-md-end m-2">
                                            <input type="button"
                                              class="minus text-color-hover-light bg-color-hover-primary border-color-hover-primary"
                                              value="-">
                                            <input type="text" class="input-text qty text" title="Qty" value="1"
                                              name="quantity" min="1" max="10" step="1">
                                            <input type="button"
                                              class="plus text-color-hover-light bg-color-hover-primary border-color-hover-primary"
                                              value="+">
                                          </div>


                                        </div>
                                        <div class="col-auto flex-md-fill">
                                          <p class="price text-4 m-0 float-end d-none d-md-block"> <span
                                              class="sale text-color-dark font-weight-semi-bold">$1.80/yr</span>
                                            <br>
                                          </p>
                                        </div>
                                      </span>
                                    </div>

                                    <div class="col-2 px-0 ">
                                      <button onclick="mostrar_dominio(); next()" type="button" href="#"
                                        class="mb-1 mt-1 btn btn-outline btn-primary float-end float-md-start btn">
                                        <span> <i class="fas fa-cart-plus fa-2x"></i>
                                        </span></button>

                                      {{-- <button onclick="back()" type="button" href="#"
                                        class="mb-1 mt-1 btn btn-outline btn-primary float-end btn">
                                        <span> <i class="fas fa-cart-plus fa-2x"></i>
                                        </span></button> --}}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div id="tengo" class="tab-pane">
                            <div class="row w-100 align-items-center">
                              <form>
                                <div class="form-group col-12 col-md-auto">
                                  <div class="input-group">
                                    <label class="form-label w-100">Nombre de su dominio <span
                                        class="text-color-danger">*</span></label>
                                    <input type="text" class="form-control border-color-primary h-auto py-2" name=""
                                      value="" required="" placeholder="midominio.com">
                                  </div>
                                  <p class="text-2 text-danger text-left">Escriba su dominio completo como el ejemplo
                                    para
                                    poder ser
                                    registrado</p>
                                </div>
                                <div class="col-auto">
                                  <button type="submit" class="btn btn-primary mb-3">Registrar dominio</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>

      </div>
      <!--  <div class="col-xxl-2 d-none d-xxl-block">
        @include('includes.side-compra')
      </div>-->
    </div>

  </div>
</section>

<!--fin el contenido del HTML-->
@endsection

@section('fix-footer')
<section id="footer-fix" class="section bg-primary border-0 m-0 fixed-bottom py-0">
  <div class="container-fluid">
    <div class="row justify-content-around justify-content-md-start align-items-center d-flex  py-3">
      <div class="col-md-6">
        <div class="px-4 mb-2 mb-md-0">
          <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><span
              class="position-relative">
              @if((Session::has("cyberfuel_carrito_compras")) && (Session::get("cyberfuel_carrito_compras") !=
              null)) <span id="span_total_carrito_compras"
                class="position-absolute top-50 start-0 translate-middle badge rounded-pill bg-danger"> @php
                $carrito_compras = Session::get("cyberfuel_carrito_compras");
                if((isset($carrito_compras)) && ($carrito_compras != null) && is_array($carrito_compras)){
                $total_productos_carrito_compras = count($carrito_compras);
                if($total_productos_carrito_compras > 0){
                echo $total_productos_carrito_compras;
                }
                }
                @endphp </span> @endif &nbsp; &nbsp; <span id="span_total_carrito_compras"
                class="cart-qty "></span></span>
          </h3>
          <p class="price text-5 m-0">

            <span class="subtotal text-light">
              <h4 class="font-weight-bold text-uppercase text-3 mb-3">
                Cantidad de Artículos
                <span class="pl-4">
                  <a href="" class="header-nav-features-toggle" aria-label="">
                    &nbsp; <i class="fas fa-shopping-basket text-white"></i>&nbsp;
                    <span class="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1">5 </span>
                  </a>
                </span>
              </h4>
            </span>
          </p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="float-md-end lista-carrito">
          <a href="{{ __('rutas.url_carrito-de-compra') }}"
            class="btn btn-modern btn-light box-shadow-2  rounded-0 sidecar">
            <i class="fas fa-shopping-basket  text-5"></i> &nbsp; Ver mi compra <span id="span_total_carrito_compras"
              class="cart-qty"></span> <span>&nbsp;</span> </a>
          </a>
          <a data-bs-toggle="modal" data-bs-target="#usuario" data-bs-toggle="tooltip" data-bs-placement="bottom"
            type="button" class="btn btn-md btn-modern btn-quaternary box-shadow-2 rounded-0 "> <i
              class="fas fa-arrow-right text-2 me-2"></i>&nbsp;Continuar con la compra</a>
        </div>

      </div>
    </div>
  </div>
</section>

<!--  modal hopedajes-->
<div class="modal fade" id="hospedaje" tabindex="-1" role="dialog" aria-labelledby="modalHospedajes" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header alert-primary">
        <h4 class="modal-title text-white" id="modalHospedajes">{{ __('hospedaje.planes') }}</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body bg-color-grey-scale-1">
        @include('includes.hospedajes')
      </div>
      <div class="modal-footer alert-primary">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('comercio.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>
@endsection



@section('scripts')

<script src="{{ URL::asset('assets/js/view.shop.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap-input-spinner.js') }}"></script>
<script src="{{ URL::asset('assets/js/dominios.js') }}"></script>

<script>
  var current = 0;
var tabs = $(".tab");
var tabs_pill = $(".tab-pills");

loadFormData(current);

function loadFormData(n) {
  $(tabs_pill[n]).addClass("active");
  $(tabs[n]).removeClass("d-none");
  $("#back_button").attr("disabled", n == 0 ? true : false);
  n == tabs.length - 1
    ? $("#next_button").text("Submit").removeAttr("onclick")
    : $("#next_button")
        .attr("type", "button")
        .text("Next")
        .attr("onclick", "next()");
}

function next() {
  $(tabs[current]).addClass("d-none");
  $(tabs_pill[current]).removeClass("active");

  current++;
  loadFormData(current);

}

function back() {
  $(tabs[current]).addClass("d-none");
  $(tabs_pill[current]).removeClass("active");

  current--;
  loadFormData(current);
}
    

      
       $(document).ready(function  () {  
           if ($(".nav-link.active")) {
                $(this).trigger('shown.bs.tab');
               // $(".esconder-hospedaje").hide();
                $(".esconder-hospedaje[data-container='container1']").show();
               escogerHost();
        }
               
           
  $('#myTab').on('shown.bs.tab', function (e) {
      


  var enlace = $(e.target).attr("href");
  switch (enlace) {
  case "#tabsCorreo":
        $(".esconder-hospedaje").hide();
        $(".esconder-hospedaje[data-container='container1']").show();
         escogerHost();
    break;
  case "#tabsPersonal":
    
        $(".esconder-hospedaje").hide();
        $(".esconder-hospedaje[data-container='container2']").show();
         escogerHost();
    break;
  case "#tabsPymes":
   
        $(".esconder-hospedaje").hide();
        $(".esconder-hospedaje[data-container='container3']").show();
         escogerHost();
    break;
  case "#tabsEmpresarial":
    
        $(".esconder-hospedaje").hide();
        $(".esconder-hospedaje[data-container='container4']").show();
         escogerHost();
    break;
  case "#tabsComercio":
 
        $(".esconder-hospedaje").hide();
        $(".esconder-hospedaje[data-container='container5']").show();
         escogerHost();
    break;
  default:
    $(".esconder-hospedaje").hide();
        $(".esconder-hospedaje[data-container='container1']").show();
         escogerHost();
    break;
}
  
});
    
   });     
    

</script>
<script>
  window.addEventListener("load", function () {
  var barra = document.querySelector("#footer-fix");
  var contenidoPrincipal = document.querySelector("#footer");
  var alturaBarra = barra.offsetHeight;

  contenidoPrincipal.style.marginBottom = alturaBarra + "px";
});
</script>

<script>
  $(document).ready(function() {
    // Cuando el modal se muestra
    $('#spamfilter').on('shown.bs.modal', function (e) {
      // Selecciona el input de tipo número
  $("#inputTransfer").on("input", function(){
    // Obtiene el valor del input de tipo número
    var valor = $(this).val();
    // Imprime el valor en el campo de texto
    $("#textTransfer").val(valor);
  });
    });
         
          $(".cerrarRegistro").click(function() {
        $(this).toggleClass('btn-quaternary btn-outline btn-primary');
        $(this).children().toggleClass('fa-cart-plus fa-2x');
        $(this).children().toggleClass('fa-times');
        });
  });
</script>

@endsection