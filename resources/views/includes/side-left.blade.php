

   <div class="sticky-top2">
    <div class="row pb-4 ">
		  <div class="col-12 ">
    <form class="w-100 " action="#" method="get">
      <div class="input-group ">
        <div class="search-form-wrapper input-group">
          <input class="form-control text-1 w-50" id="headerSearch" name="q" type="search" value="" placeholder="{{ __('compra.buscar') }}...">
          <button type="submit" class="btn btn-primary text-1 p-2"><i class="fas fa-search m-2"></i></button>
        </div>
      </div>
    </form>
	  </div>
		</div>
     
 <!-- destock menu-->
		<div class="d-none d-lg-block">
         
        <div class="tabs tabs-vertical tabs-left tabs-navigation border-0">
           
                <ul class="nav nav-tabs sort-source sort-source-style-1 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
                <li class="nav-item all active" data-option-value="*"> <a class="nav-link lateral " href="#" data-bs-toggle="tab">{{ __('compra.todo') }}</a> </li>
                <li class="nav-item" data-option-value=".servidores"> <a class="nav-link lateral" href="#" data-bs-toggle="tab">{{ __('compra.servidores') }}</a> </li>
                <li class="nav-item" data-option-value=".admin"> <a class="nav-link lateral" href="#" data-bs-toggle="tab">{{ __('compra.servicios') }}</a> </li>
                <li class="nav-item seg" data-option-value=".seguridad"> <a class="nav-link lateral" href="#" data-bs-toggle="tab">{{ __('compra.seguridad') }}</a> </li>
              </ul>
            
       
            
          
        </div>
             
      </div>
  </div>
<!--mobile menu-->
 <div class="d-lg-none">
        <nav class="navbar navbar-dark bg-primary">  
          <div class="container-fluid py-2">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
            &nbsp; <span class="text-4 text-white">{{ __('compra.fitro_cate') }}</span> </div>
        </nav>
        <div class="collapse" id="navbarToggleExternalContent">
          <div class="p-4">
           
            <div class="tabs tabs-vertical tabs-left tabs-navigation border-0">
            
                <ul class="nav nav-tabs sort-source sort-source-style-1 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
                      
             <li class="nav-item all active" data-option-value="*"> <a class="nav-link lateral " href="#" data-bs-toggle="tab">{{ __('compra.todo') }}</a> </li>
                <li class="nav-item" data-option-value=".servidores"> <a class="nav-link lateral" href="#" data-bs-toggle="tab">{{ __('compra.servidores') }}</a> </li>
                <li class="nav-item" data-option-value=".admin"> <a class="nav-link lateral" href="#" data-bs-toggle="tab">{{ __('compra.servicios') }}</a> </li>
                <li class="nav-item seg" data-option-value=".seguridad"> <a class="nav-link lateral" href="#" data-bs-toggle="tab">{{ __('compra.seguridad') }}</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>