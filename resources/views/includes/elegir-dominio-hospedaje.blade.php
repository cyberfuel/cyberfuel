<!--
<div class="row py-5"><div class="col-6 col-md-2 position-relative"> <div class="position-absolute top-100 start-100 translate-middle"><img class="m-auto d-block" src="{{URL::asset('assets/imagenes/dominio.png')}}" alt="Cyberfuel"></div></div></div>-->




<div class="row justify-content-center align-items-center mt-4">

  <div class="col-lg-3 d-none d-lg-block">
    <div class=""><img class="m-auto d-block w-75" src="{{URL::asset('assets/imagenes/dominio.webp')}}" alt="Cyberfuel">
    </div>
  </div>
  <div class="col-lg-9  justify-content-center ">
    <div class="row">
      <div class="col-12">
      
          
             @if (Session::get('language') == 'es')
                 <h4 class="font-weight-normal line-height-1 mb-2">Puedes <strong
            class="font-weight-extra-bold">Registrar</strong> un
          dominio nuevo, <strong class="font-weight-extra-bold">Transferir</strong> o <strong
            class="font-weight-extra-bold">Utilizar</strong> un dominio propio</h4>
             @else
           
          <h4 class="font-weight-normal line-height-1 mb-2">You can <strong
            class="font-weight-extra-bold">Register</strong> a
          new domain, <strong class="font-weight-extra-bold">Transfer</strong> or<strong
            class="font-weight-extra-bold">Use</strong> your own domain</h4>
             @endif
      </div>
      <div class="col-12 p-0">
        <div class="input-group ">
          <input type="text" id="nombre_dominio_pc" name="nombre_dominio_pc"
            class="form-control  text-center border-color-success w-50" placeholder="{{ __('compra.encuentra') }}"
            required="">
          <button onclick="consultar_dominio('{{ __('rutas.url_compra') }}', 'pc');" type="button"
            class="btn btn-success mostrardominio text-3 py-1"><i class="fas fa-search ms-1 "></i>
          </button>
        </div>
      </div>
      <div class="col-12 p-0 pt-2 text-left">
        <div class="btn-group bg-white" role="group" aria-label="">
          <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
          <label class="btn btn-outline-success rounded-0" for="btnradio1">{{__('master.regis') }}</label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
          <label class="btn btn-outline-success rounded-0" for="btnradio2">{{__('master.transferir') }}</label>

          <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
          <label class="btn btn-outline-success rounded-0" for="btnradio3">{{__('master.utilizar') }}</label>
        </div>
      </div>

    </div>

  </div>
</div>


{{-- aqui van las opciones que se despliegan despues de escoger el dominio --}}
@include('includes.opcionesDominio')
<div class="row">
  <div class="col px-0">
    <div class="form-group">
      <div class="row"> @if((isset($consulta_dominio)) && ($consulta_dominio != null))
        @if((isset($consulta_dominio["CODE"])) && ($consulta_dominio["CODE"] == 210))
        <div class="col-12">
          <div class="row pt-4">
            <div class="col-md-6">
              <h1 class="mb-0 font-weight-bold text-5 "> {{ __('compra.este_dominio_suyo') }} </h1>
              <p> <span class="text-primary font-weight-bold m-0 text-center text-7"> {{ $dominio }} </span> </p>
              <p class=" mb-3"> <span class="d-flex flex-column align-items-md-start font-weight-bold"> <span> <s
                      class="d-inline-block text-4"> $19.99 </s>&nbsp; <span
                      class="d-inline-block text-5 ml-2 mr-2 text-success"> @if((isset($registro_dominio["estado"]))
                      && ($registro_dominio["estado"] == "ok"))
                      @if((isset($registro_dominio["info"][0])) && ($registro_dominio["info"][0] != null))
                      &dollar;{{ number_format($registro_dominio["info"][0]["precio"], 2) }}
                      <input type="hidden" id="nombre_registro_dominio" name="nombre_registro_dominio"
                        value="{{ $dominio }}">
                      <input type="hidden" id="codigo_registro_dominio" name="codigo_registro_dominio"
                        value="{{ $registro_dominio[" info"][0]["codigo"] }}">
                      <input type="hidden" id="familia_registro_dominio" name="familia_registro_dominio"
                        value="{{ $registro_dominio[" info"][0]["familia"] }}">
                      <input type="hidden" id="titulo_registro_dominio" name="titulo_registro_dominio"
                        value="{{ $dominio }}">
                      <input type="hidden" id="precio_registro_dominio" name="precio_registro_dominio"
                        value="{{ $registro_dominio[" info"][0]["precio"] }}">
                      @endif
                      @endif </span> <span class="d-inline-block align-items-md-start text-muted">&nbsp;
                      {{ __('compra.durante_primer_anio') }} </span> </span> </span> </p>
            </div>
            <div class="col-md-6">
              <div class="text-3 mb-3">
                <div class="text-3"> {{ __('compra.es_genial') }} </div>
                <ul class="list list-icons list-primary">
                  @if(strlen($dominio) <= 15) <li> <i class="fas fa-check"></i> <b>{{ $dominio }}</b> {{
                    __('compra.menos_caracteres') }} </li>
                    @endif
                    <li> <i class="fas fa-check"></i> {{ __('compra.palabras_populares') }} </li>
                    <li> <i class="fas fa-check"></i> {{ __('compra.proteccion_privacidad') }} </li>
                </ul>
              </div>
              <button onclick="agregar_registro_dominio('{{ $dominio }}', '1');" type="button"
                class="btn btn-outline btn-rounded btn-primary btn-with-arrow mb-2"> {{ __('compra.agregar') }} <span>
                  <i class="fas fa-shopping-cart"></i> </span> </button>
            </div>
          </div>
        </div>
        @endif
        @else
        <div class="row pt-4"></div>
        @endif
        <div class="col-12"> @if((isset($consulta_dominio)) && ($consulta_dominio != null))
          @if((isset($consulta_dominio["CODE"])) && ($consulta_dominio["CODE"] == 211))
          <div class="row">
            <div class="col-auto my-2">
              <p> {{ __('compra.disculpas') }} <span class="text-primary font-weight-bold m-0 text-center h4"> {{
                  $dominio }} </span> {{ __('compra.no_se_encuentra') }} </p>
            </div>
          </div>
          @endif
          @endif
          <div class="row align-items-center">
            <div class="col pt-0">
              <div id="div_cargando_dominios" class="text-center" style="display:none;"> <img
                  src="{{URL::asset('assets/imagenes/cargando.gif')}}" width="100" height="100" /> </div>

              <!-- Cargar sugerencias de dominios -->
              <div id="div_sugerencias_dominios" role="main" class="main shop"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>