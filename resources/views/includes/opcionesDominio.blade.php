<div class="row pt-2 shop">
    <div class="col  px-0">
        <div class="card card-default">
            <div class="card-header">
                <h4 class="card-title m-0">
                    <a class="accordion-toggle collapsed domain border-gradiente">
                        <div class="row align-items-center">
                            <div class="col-12 col-lg-3 col-xl-3 no-disponible">
                                <i class="fas fa-heart-broken text-danger"></i><span class="text-4">gsoto.com</span><br>
                                <span class="badge badge-danger mx-4 py-1 text-uppercase">
                                    @if (Session::get('language') == 'es')
                                    Dominio no disponible
                                    @else
                                    Domain not available
                                    @endif
                                </span>
                            </div>
                            <div class="col-12 col-lg-3 col-xxl-3 disponible d-none">
                                <i class="fas fa-trophy text-dorado"></i><span class="text-4">gsoto.com</span><br>
                                <span class="badge bg-dorado mx-4 py-1 text-uppercase">
                                    @if (Session::get('language') == 'es')
                                    Dominio Transferido
                                    @else
                                    Transferred Domain
                                    @endif
                                </span>
                            </div>
                            <div class="col-12 col-lg-5 col-xxl-5">
                                <p class="text-3 text-lg-4 pt-4">
                                    @if (Session::get('language') == 'es')
                                    Si el dominio es tuyo puedes utilizarlo o realizar transferencia.
                                    @else
                                    If the domain is yours you can use it or transfer it.
                                    @endif
                                </p>

                            </div>


                            <div class="col col-lg-4 col-xxl-4">
                                <div class="m-0 align-items-center justify-content-end  row mt-2 mt-md-0">
                                    <div class="col-6">

                                        <button data-bs-toggle="collapse" data-bs-target="#collapseUtilizar"
                                            aria-expanded="false" aria-controls="collapseUtilizar" type="button"
                                            class="boton_toggle_agregar utili mb-1 mt-1 me-1 btn btn-primary float-end w-100">
                                            <span class="txt-agregado">{{__('master.utilizar') }} </span>
                                            <i id="check" class="fas fa-shopping-cart"></i>
                                        </button>

                                    </div>
                                    <div class="col-auto px-2 d-none">
                                        <button type="button"
                                            class="btn btn-quaternary btn-circle float-end cerrarAddUtilizar mb-1 mt-1 me-1 w-100"><i
                                                class="fa fa-times"></i>
                                        </button>
                                    </div>
                                    <div class="col-6 px-0 px-lg-2">
                                        <button data-bs-toggle="collapse" data-bs-target="#collapseTrans"
                                            aria-expanded="false" aria-controls="collapseTrans" type="button"
                                            class="boton_toggleTrans trans mb-1 mt-1 me-1 btn  btn-primary float-end w-100">
                                            <span class="txt-agregado">{{__('master.transferir') }}</span>
                                            <i id="check" class="fas fa-exchange-alt"></i>
                                        </button>

                                    </div>

                                    <div class="col-auto px-2 d-none">
                                        <button type="button"
                                            class="btn btn-quaternary  btn-circle float-end cerrarAddTrans mb-1 mt-1 me-1 w-100"><i
                                                class="fa fa-times"></i>
                                        </button>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </a>
                </h4>
            </div>

          
        </div>
          <div id="collapseUtilizar" class="collapse">
                <div class="card-body desplegado p-2">
                        <div class="row g-0">
                            {{-- Compra Filro antispam Utilizar--}}
                            <div class="col-12 ">
                                <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3  w-100 h-100">
                                    <div class="row d-flex justify-content-around align-items-center w-100">
                                        <div class="col-lg-6">
                                            <div class="row align-items-center">
                                                <div class="col-6   flex-lg-fill"> <span
                                                        class="text-start text-2 float-start"><span
                                                            class="text-primary text-3-4">
                                                            Filtro AntiSpam
                                                        </span></span>
                                                </div>
                                                <div class="col-6  px-0 py-3 py-lg-0 selectorSpam">
                                                    <input type="number" id="inputUtilizar" value="1" min="1"
                                                        max="200" />
                                                    <p class="text-start mb-0 d-none d-lg-block">
                                                        Escoge la cantidad que necesites de buzones de correo
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 px-2  flex-fill">
                                           <div class="row align-items-center d-flex justify-content-end">
                                                <div class="col-auto standar_precio">
                                                    <div class="input-group float-start float-lg-end">
                                                        <span class="input-group-text sin_estilo">$</span>
                                                        <input value="1.00"
                                                            class="price addon text-4 m-0  border border-white form-control text-color-dark font-weight-semi-bold utilizar"
                                                            type="text" id="textUtilizar" readonly="">
                                                        <span class="mt-2 mt-md-0"> <sup>+IVA</sup>
                                                            <span
                                                                class="text-primary font-weight-black">Mensual</span></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 cerrarFiltro">
                                                    <div class="row justify-content-end">
                                                        <div class="col-6 col-md-5 col-xl-auto ms-auto">
                                                            <span class="price text-4 m-0  mostrar-precio d-none"> <span
                                                                    class="text-success small font-weight-black text-end">Comprado</span>
                                                                <br>
                                                            </span>
                                                        </div>


                                                        <div class="col-3 col-md-7 col-xl-auto">
                                                            <button type="button"
                                                                class="btn btn-outline btn-primary btn-circle float-end toggleButton"><i
                                                                    class="fa fa-cart-plus fa-2x"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Aqui van las cajas de Hospedajes que se despliegan en la seccion de dominios --}}
                        @include('includes.box-hospedajes')
                </div>
            </div>
        <div id="collapseTrans" class="collapse">
            <div class="card-body desplegado p-2">
            
                        <div class="row shop g-0">
                            {{-- Compra años Dominio --}}
                            <div class="col-12 ">
                                <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3 w-100 h-100"
                                    style="animation-delay: 550ms;">
                                    <div class="row d-flex justify-content-around align-items-center w-100">
                                        <div class="col-lg-6">
                                            <div class="row align-items-center">
                                                <div class="col-6   flex-lg-fill"> <span
                                                        class="text-start text-2 float-start"><span
                                                            class="text-primary text-3-4">
                                                            @if (Session::get('language') == 'es')
                                                            Años de Registro
                                                            @else
                                                            Years of Registration
                                                            @endif
                                                        </span></span>
                                                </div>
                                                <div class="col-6  px-0 py-3 py-lg-0">
                                                    <input type="number" id="inputNet2" value="1" min="1" max="10" />

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 px-2  flex-fill">
                                        <div class="row align-items-center d-flex justify-content-end">
                                            <div class="col-auto standar_precio">
                                            <div class="input-group float-start float-lg-end"> <span class="input-group-text">$</span>
                                                <input
                                                            class="price addon text-4 m-0  border border-white form-control text-color-dark font-weight-semi-bold"
                                                            type="text" id="inputGross2" readonly />
                                                <span class="mt-2 mt-md-0"> <sup>+IVA</sup> <span
                                                                class="text-primary font-weight-black">{{__('master.anual')
                                                }}</span></span> </div>
                                          </div>
                                            <div class="col-lg-5 cerrarFiltro">
                                            <div class="row justify-content-end">
                                                <div class="col-6 col-md-5 col-xl-auto ms-auto"> <span class="price text-4 m-0  mostrar-precio d-none"> <span
                                                                    class="text-success small font-weight-black text-end">Comprado</span> <br>
                                                  </span> </div>
                                                <div class="col-3 col-md-7 col-xl-auto">
                                                <button type="button"
                                                                class="btn btn-outline btn-primary btn-circle float-end toggleButton"><i
                                                                    class="fa fa-cart-plus fa-2x"></i> </button>
                                              </div>
                                              </div>
                                          </div>
                                          </div>
                                      </div>

                                    </div>
                                </div>
                            </div>

                            {{-- compra registro Privado --}}

                            <div class="col-12 ">
                                <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3 w-100 h-100">
                                    <div class="row justify-content-between align-items-center w-100">
                                        <div class="col-lg-6">
                                            <div class="row align-items-center">
                                                <div class="col-6   flex-lg-fill"> <span
                                                        class="text-start text-2 float-start"><span
                                                            class="text-primary text-3-4">
                                                            {{__('master.regis_priva') }}
                                                        </span>
                                                </div>
                                                <div class="col-6  px-0 py-3 py-lg-0">


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 px-2  flex-fill">
                                        <div class="row align-items-center d-flex justify-content-end">
                                            <div class="col-auto standar_precio">
                                            <div class="input-group float-start float-lg-end"> <span class="input-group-text">$</span>
                                                <input
                                                            class="price addon text-4 m-0  border border-white form-control text-color-dark font-weight-semi-bold"
                                                            type="text" value="10.00"  readonly />
                                                <span class="mt-2 mt-md-0"> <sup>+IVA</sup> <span
                                                                class="text-primary font-weight-black">{{__('master.anual')
                                                }}</span></span> </div>
                                          </div>
                                            <div class="col-lg-5 cerrarFiltro">
                                            <div class="row justify-content-end">
                                                <div class="col-6 col-md-5 col-xl-auto ms-auto"> <span class="price text-4 m-0  mostrar-precio d-none"> <span
                                                                    class="text-success small font-weight-black text-end">Comprado</span> <br>
                                                  </span> </div>
                                                <div class="col-3 col-md-7 col-xl-auto">
                                                <button type="button"
                                                                class="btn btn-outline btn-primary btn-circle float-end toggleButton"><i
                                                                    class="fa fa-cart-plus fa-2x"></i> </button>
                                              </div>
                                              </div>
                                          </div>
                                          </div>
                                      </div>

                                    </div>
                                </div>
                            </div>

                            {{-- Compra Filro antispam transferencia --}}
                              <div class="col-12 ">
                                <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3  w-100 h-100">
                                    <div class="row d-flex justify-content-around align-items-center w-100">
                                        <div class="col-lg-6">
                                            <div class="row align-items-center">
                                                <div class="col-6   flex-lg-fill"> <span
                                                        class="text-start text-2 float-start"><span
                                                            class="text-primary text-3-4">
                                                            Filtro AntiSpam
                                                        </span></span>
                                                </div>
                                                <div class="col-6  px-0 py-3 py-lg-0 selectorSpam">
                                                    <input type="number" id="inputTransfer" value="1" min="1"
                                                        max="200" />
                                                    <p class="text-start mb-0 d-none d-lg-block">
                                                        Escoge la cantidad que necesites de buzones de correo
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 px-2  flex-fill">
                                           <div class="row align-items-center d-flex justify-content-end">
                                                <div class="col-auto standar_precio">
                                                    <div class="input-group float-start float-lg-end">
                                                        <span class="input-group-text sin_estilo">$</span>
                                                        <input value="1.00"
                                                            class="price addon text-4 m-0  border border-white form-control text-color-dark font-weight-semi-bold transferir"
                                                            type="text" id="textTransfer" readonly="">
                                                        <span class="mt-2 mt-md-0"> <sup>+IVA</sup>
                                                            <span
                                                                class="text-primary font-weight-black">Mensual</span></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 cerrarFiltro">
                                                    <div class="row justify-content-end">
                                                        <div class="col-6 col-md-5 col-xl-auto ms-auto">
                                                            <span class="price text-4 m-0  mostrar-precio d-none"> <span
                                                                    class="text-success small font-weight-black text-end">Comprado</span>
                                                                <br>
                                                            </span>
                                                        </div>


                                                        <div class="col-3 col-md-7 col-xl-auto">
                                                            <button type="button"
                                                                class="btn btn-outline btn-primary btn-circle float-end toggleButton"><i
                                                                    class="fa fa-cart-plus fa-2x"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Aqui van las cajas de Hospedajes que se despliegan en la seccion de dominios --}}
                        @include('includes.box-hospedajes')
                 
            </div>
        </div>
    </div>
</div>

<!--tabla comparativa-->
<div class="row">
    <div class="col text-center">
        <div class="overflow-hidden">
            <h2 class="font-weight-semibold text-5 mb-0 appear-animation animated maskUp appear-animation-visible"
                data-appear-animation="maskUp" data-appear-animation-delay="0" data-appear-animation-duration="1s"
                style="animation-duration: 1s; animation-delay: 0ms;">

                @if (Session::get('language') == 'es')
                Otras excelentes opciones para ti
                @else
                Other great options for you
                @endif
            </h2>
        </div>
        <div class="divider divider-primary my-4">
            <i class="fas fa-chevron-down"></i>
        </div>
    </div>
</div>
    
   <!-- registrar-->
<div class="row pt-2 shop">
    <div class="col  px-0">
        <div class="card card-default">
            <div class="card-header">
                <h4 class="card-title m-0">

                    <a class="accordion-toggle collapsed domain border-gradiente">
                        <div class="row align-items-center">
                            <div class="col-md">
                                <i class="fas fa-check-circle text-success"></i> <span class="text-4">gilsoto.org</span>

                                <!-- esto queda oculto para mas adelante-->
                                <!--       <span
                                    class="badge badge-primary mx-4 text-uppercase">
               @if (Session::get('language') == 'es')
             5% AHORRA AL COMPRAR POR 3 AñOS
                @else
                5% SAVE WHEN YOU BUY FOR 3 YEARS
                @endif
                                </span>-->

                            </div>
                            <div class="col-6 col-md-auto">
                                <div class="row align-items-center ">

                                    <div class="col-12">

                                    </div>
                                    <div class="col mt-2 mt-md-0"></div>
                                    <div class="col-12 col-md-auto">
                                        <span
                                            class="sale text-color-dark font-weight-semi-bold d-block text-4 float-lg-start">
                                            <p class="price text-4 mb-0">
                                                <span
                                                    class="sale  font-weight-semi-bold text-primary">$15.99&nbsp;&nbsp;</span>
                                                <span class="amount d-none">$18.00</span>
                                            </p>
                                        </span>

                                    </div>
                                </div>
                            </div>


                            <div class="col-6 col-md-auto">
                                <div class="m-0 row-cols-auto float-end  row mt-2 mt-md-0 bot-registrar">

                                    <div class="col-auto ">
                                        <button data-bs-toggle="collapse" data-bs-target="#collapseRegistrar"
                                            aria-expanded="false" aria-controls="collapseRegistrar" type="button"
                                            href="#"
                                            class="boton_toggle_registrar d-block mb-1 mt-1 me-1 btn  btn-success float-end w-100">
                                            <span class="txt-agregado">{{__('master.regis') }}</span>
                                            <i id="check" class="fas fa-shopping-cart"></i>
                                        </button>
                                    </div>

                                    <div class="col-auto px-2">
                                        <button type="button"
                                            class="btn btn-quaternary btn-circle float-end cerrarAddRegistrar mb-1 mt-1 me-1 w-100 d-none"><i
                                                class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </a>
                </h4>
            </div>
            <div id="collapseRegistrar" class="collapse">
                <div class="card-body desplegado p-2">

                    {{-- @include('includes.elegir-addOns') --}}

                    <div class="row shop g-0">
                        {{-- Compra años Dominio --}}
                        <div class="col-12 ">
                            <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3 w-100 h-100">
                                <div class="row d-flex justify-content-around align-items-center w-100">
                              <div class="col-lg-6">
                                            <div class="row align-items-center">
                                                <div class="col-6   flex-lg-fill"> <span
                                                        class="text-start text-2 float-start"><span
                                                            class="text-primary text-3-4">
                                                            @if (Session::get('language') == 'es')
                                                            Años de Registro
                                                            @else
                                                            Years of Registration
                                                            @endif
                                                        </span></span>
                                                </div>
                                                <div class="col-6  px-0 py-3 py-lg-0">
                                                    <input type="number" id="inputNet" value="1" min="1" max="10" />

                                                </div>
                                            </div>
                                        </div>
                             
                                        <div class="col-lg-6 px-2  flex-fill">
                                        <div class="row align-items-center d-flex justify-content-end">
                                            <div class="col-auto standar_precio">
                                            <div class="input-group float-start float-lg-end"> <span class="input-group-text">$</span>
                                                <input
                                                            class="price addon text-4 m-0  border border-white form-control text-color-dark font-weight-semi-bold"
                                                            type="text" id="inputGross" readonly />
                                                <span class="mt-2 mt-md-0"> <sup>+IVA</sup> <span
                                                                class="text-primary font-weight-black">{{__('master.anual')
                                                }}</span></span> </div>
                                          </div>
                                            <div class="col-lg-5 cerrarFiltro">
                                            <div class="row justify-content-end">
                                                <div class="col-6 col-md-5 col-xl-auto ms-auto"> <span class="price text-4 m-0  mostrar-precio d-none"> <span
                                                                    class="text-success small font-weight-black text-end">Comprado</span> <br>
                                                  </span> </div>
                                                <div class="col-3 col-md-7 col-xl-auto">
                                                <button type="button"
                                                                class="btn btn-outline btn-primary btn-circle float-end toggleButton"><i
                                                                    class="fa fa-cart-plus fa-2x"></i> </button>
                                              </div>
                                              </div>
                                          </div>
                                          </div>
                                      </div>

                                </div>
                            </div>
                        </div>

                        {{-- compra registro Privado --}}

                        <div class="col-12 ">
                            <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3 w-100 h-100">
                                <div class="row justify-content-between align-items-center w-100">
                                    <div class="col-lg-6">
                                        <div class="row align-items-center">
                                            <div class="col-6   flex-lg-fill"> <span
                                                    class="text-start text-2 float-start"><span
                                                        class="text-primary text-3-4">
                                                        {{__('master.regis_priva') }}
                                                    </span>
                                            </div>
                                            <div class="col-6  px-0 py-3 py-lg-0">


                                            </div>
                                        </div>
                                    </div>
                                      <div class="col-lg-6 px-2  flex-fill">
                                        <div class="row align-items-center d-flex justify-content-end">
                                            <div class="col-auto standar_precio">
                                            <div class="input-group float-start float-lg-end"> <span class="input-group-text">$</span>
                                                <input
                                                            class="price addon text-4 m-0  border border-white form-control text-color-dark font-weight-semi-bold"
                                                            type="text" value="10.00"  readonly />
                                                <span class="mt-2 mt-md-0"> <sup>+IVA</sup> <span
                                                                class="text-primary font-weight-black">{{__('master.anual')
                                                }}</span></span> </div>
                                          </div>
                                            <div class="col-lg-5 cerrarFiltro">
                                            <div class="row justify-content-end">
                                                <div class="col-6 col-md-5 col-xl-auto ms-auto"> <span class="price text-4 m-0  mostrar-precio d-none"> <span
                                                                    class="text-success small font-weight-black text-end">Comprado</span> <br>
                                                  </span> </div>
                                                <div class="col-3 col-md-7 col-xl-auto">
                                                <button type="button"
                                                                class="btn btn-outline btn-primary btn-circle float-end toggleButton"><i
                                                                    class="fa fa-cart-plus fa-2x"></i> </button>
                                              </div>
                                              </div>
                                          </div>
                                          </div>
                                      </div>

                                </div>
                            </div>
                        </div>

                        {{-- Compra Filro antispam Registrar --}}
                        <div class=" col-12  altura-plan">
                                <div class=" d-inline-flex align-items-center btn btn-3d btn-light
                       rounded-0 font-weight-semibold btn-py-3 text-3 w-100 h-100">

                                    <div class="row d-flex justify-content-around align-items-center w-100">
                                        <div class="col-lg-6">
                                            <div class="row align-items-center">
                                                <div class="col-6   flex-lg-fill"> <span
                                                        class="text-start text-2 float-start"><span
                                                            class="text-primary text-3-4">
                                                            Filtro AntiSpam
                                                        </span></span>
                                                </div>
                                                <div class="col-6  px-0 py-3 py-lg-0 selectorSpam">
                                                    <input type="number" id="inputRegistrar" value="1" min="1"
                                                        max="200" />
                                                    <p class="text-start mb-0 d-none d-lg-block">
                                                        Escoge la cantidad que necesites de buzones de correo
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 px-2  flex-fill">
                                            <div class="row align-items-center d-flex justify-content-end">
                                                <div class="col-auto standar_precio">
                                                    <div class="input-group float-start float-lg-end">
                                                        <span class="input-group-text sin_estilo">$</span>
                                                        <input value="1.00"
                                                            class="price addon text-4 m-0  border border-white form-control text-color-dark font-weight-semi-bold registrar"
                                                            type="text" id="textRegistrar" readonly="">
                                                        <span class="mt-2 mt-md-0"> <sup>+IVA</sup>
                                                            <span
                                                                class="text-primary font-weight-black">Mensual</span></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 cerrarFiltro">
                                                    <div class="row justify-content-end">
                                                        <div class="col-6 col-md-5 col-xl-auto ms-auto">
                                                            <span class="price text-4 m-0  mostrar-precio d-none"> <span
                                                                    class="text-success small font-weight-black text-end">Comprado</span>
                                                                <br>
                                                            </span>
                                                        </div>


                                                        <div class="col-3 col-md-7 col-xl-auto">
                                                            <button type="button"
                                                                class="btn btn-outline btn-primary btn-circle float-end toggleButton"><i
                                                                    class="fa fa-cart-plus fa-2x"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="row g-0">
                        {{-- Aqui van las cajas de Hospedajes que se despliegan en la seccion de dominios --}}
                        @include('includes.box-hospedajes')

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>