<div class="container">
    <div class="featured-boxes featured-boxes-style-3 featured-boxes-flat">
        <div class="row mb-4">
            <div class="col text-center appear-animation animated fadeInUpShorter appear-animation-visible"
                data-appear-animation="fadeInUpShorter" style="animation-delay: 100ms;">
                <h2 class="font-weight-bold mb-1">{{ __('inicio.por_cyber') }}</h2>
            </div>
        </div>
        <div class="row d-md-none justify-content-center">
            <div class="col-12">
                <h2 class="word-rotator clip is-full-width mb-2 h3 text-center"> <span>Cyberfuel {{ __('inicio.es') }}
                    </span> <span class="word-rotator-words"> <b class="is-visible text-color-primary">{{
                            __('inicio.expe') }}</b> <b class="text-color-primary"><strong
                                class="font-weight-extra-bold">{{ __('inicio.clientes') }}</strong> {{
                            __('inicio.satisfechos') }}</b> <b class="text-color-primary"><strong
                                class="font-weight-extra-bold">{{ __('inicio.servicio') }}</strong> 24/7</b> </span>
                </h2>
            </div>
        </div>
        <div class="d-none d-md-block">
            <div class="row ">
                <div class="col-lg-3 col-sm-6">
                    <div class="featured-box featured-box-primary featured-box-effect-3">
                        <div class="box-content box-content-border-0 px-1"> <i class="icon-featured fas fa-trophy"></i>
                            <span>
                                <div class="altura-caja">
                                    <h4 class="font-weight-normal text-5 mt-3">
                                        <strong class="font-weight-extra-bold text-quaternary">{{ __('inicio.expe')
                                            }}</strong>
                                    </h4>
                                    <p class="mb-2 mt-2 text-3">
                                        @if (Session::get('language') == 'es')
                                        Más de 
                                        <?php
                                        $fecha_actual = date('Y'); 
                                        $edad =  $fecha_actual-1997;
                                        echo  $edad;
                                        ?> años de experiencia que nos respaldan
                                        @else
                                        More than 
                                        <?php
                                        $fecha_actual = date('Y'); 
                                        $edad =  $fecha_actual-1997;
                                        echo  $edad;
                                        ?> years of experience
                                        @endif
                                    </p>

                                </div>
                            </span>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="featured-box featured-box-quaternary-verde featured-box-effect-3">
                        <div class="box-content box-content-border-0 px-1"> <i
                                class="icon-featured fas fa-handshake verde"></i>
                            <span>
                                <div class="altura-caja">
                                    <h4 class="font-weight-normal text-5 mt-3"><strong
                                            class="font-weight-extra-bold text-quaternary">{{ __('inicio.clientes') }}
                                            {{
                                            __('inicio.satisfechos') }}</strong></h4>
                                    <p class="mb-2 mt-2 text-3">{{ __('inicio.clien_satis') }}</p>
                                </div>
                            </span>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="featured-box featured-box-quaternary featured-box-effect-3">
                        <div class="box-content box-content-border-0 px-1"> <i
                                class="icon-featured fas fa-award dorado"></i>
                            <span>
                                <div class="altura-caja">
                                    <h4 class="font-weight-normal text-5 mt-3"><strong
                                            class="font-weight-extra-bold text-quaternary">
                                            @if (Session::get('language') == 'es')
                                            Certificados nos respaldan
                                            @else
                                            Our certifications support us
                                            @endif
                                        </strong></h4>
                                    <p class="mb-2 mt-2 text-3">{{ __('inicio.empresa_certi') }}</p>
                                </div>
                            </span>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="featured-box featured-box-tertiary featured-box-effect-3">
                        <div class="box-content box-content-border-0 px-1"> <i class="icon-featured fas fa-headset"></i>
                            <span>
                                <div class="altura-caja">
                                    <h4 class="font-weight-normal text-5 mt-3"><strong
                                            class="font-weight-extra-bold text-quaternary">{{ __('inicio.servicio') }}
                                            24/7</strong></h4>
                                    <p class="mb-2 mt-2 text-3">{{ __('inicio.nuestro_equi') }}</p>
                                </div>
                            </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col">

            <div class="owl-carousel owl-theme carousel-center-active-item"
                data-plugin-options="{'responsive': {'0': {'items': 3}, '480': {'items': 3}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/logos/certificado_l.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/azul.webp')}}" alt="Certificados de Cyberfuel">
                </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/cloud_very.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/cr.webp')}}" alt="Certificados de Cyberfuel">
                </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/comodo-secure-icon.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/dcos.webp')}}" alt="Certificados de Cyberfuel">
                </div>
                   <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/rate3.webp')}}" alt="Certificados de Cyberfuel">
                </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/ipv6-ready.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/logo-certificado.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/Microsoft-Partner.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/Odin_Partner-level_bronze2.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/positivessl-icon.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>
             
                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/Vmware-Partner.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>

                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/veeam-logo.webp')}}"
                        alt="Certificados de Cyberfuel"> </div>

                <div> <img class="img-fluid lazyload" src="{{URL::asset('assets/imagenes/lazy.png')}}"
                        data-src="{{URL::asset('assets/imagenes/logos/PYME.webp')}}" alt="Certificados de Cyberfuel">
                </div>


            </div>
        </div>
    </div>
</div>