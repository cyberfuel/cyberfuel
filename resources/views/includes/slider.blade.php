<!--Banner web-->
<div id="carouselCyber" class="carousel slide" data-ride="carousel">
  <div class="carousel-indicators ">
    <button type="button" data-bs-target="#carouselCyber" data-bs-slide-to="0" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselCyber" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselCyber" data-bs-slide-to="2" aria-label="Slide 3"></button>
    <button type="button" data-bs-target="#carouselCyber" data-bs-slide-to="3" aria-label="Slide 4"></button>
    <button type="button" data-bs-target="#carouselCyber" data-bs-slide-to="4" aria-label="Slide 5"></button>
    <button type="button" data-bs-target="#carouselCyber" data-bs-slide-to="5" aria-label="Slide 6"></button>
    <button type="button" data-bs-target="#carouselCyber" data-bs-slide-to="6" aria-label="Slide 7"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item"> <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/arte-carrusel-wh.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel"> <img width="600"
        src="{{URL::asset('assets/imagenes/slider/mobile/arte-carrusel-wh-600x600.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
      <div class="carousel-caption">
        <div class="text-white"> @if (Session::get('language') == 'es')
          <h4 name="texto" class="text-white text-slider">Su sitio WEB en manos de expertos, conozca nuestros planes de
            Hospedaje WEB</h4>
          @else
          <h4 name="texto" class="text-white text-slider">Your website in the hands of experts, learn about our WEB
            Hosting plans</h4>
          @endif
        </div>
        <a href="{{ __('rutas.url_hospedaje') }}" type="button"
          class="btn btn-modern btn-sm btn- btn-success rounded-0 mb-1 m-auto">{{ __('inicio.ver_mas') }}</a>
      </div>
    </div>
    <div class="carousel-item"> <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/arte-carrusel-fp.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel"> <img width="600"
        src="{{URL::asset('assets/imagenes/slider/mobile/arte-carrusel-fp-600x600.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
      <div class="carousel-caption">
        <div class="text-white"> @if (Session::get('language') == 'es')
          <h4 name="texto" class="text-white text-slider">Software de Facturación Electrónica en la nube para
            Profesionales y Empresas en Costa Rica…</h4>
          @else
          <h4 name="texto" class="text-white text-slider">Electronic Invoice Software in the cloud for Professionals and
            Companies in Costa Rica…</h4>
          @endif
        </div>
        <a href="https://www.facturaprofesional.com" type="button"
          class="btn btn-modern btn-sm btn- btn-success rounded-0 mb-1 m-auto">{{ __('inicio.ver_mas') }}</a>
      </div>
    </div>
    <div class="carousel-item"> <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/arte-carrusel-sf.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel"> <img width="600"
        src="{{URL::asset('assets/imagenes/slider/mobile/arte-carrusel-sf-600x600.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
      <div class="carousel-caption">
        <div class="text-white"> @if (Session::get('language') == 'es')
          <h4 name="texto" class="text-white text-slider">Configure su servidor virtual con lo que usted solo necesita…
          </h4>
          @else
          <h4 name="texto" class="text-white text-slider">Configure your virtual server with everything you need...</h4>
          @endif
        </div>
        <a href="{{ __('rutas.url_servidores text-slider') }}" type="button"
          class="btn btn-modern btn-sm btn- btn-success rounded-0 mb-1 m-auto">{{ __('inicio.ver_mas') }}</a>
      </div>
    </div>
    <div class="carousel-item"> <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/arte-carrusel-sl.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel"> <img width="600"
        src="{{URL::asset('assets/imagenes/slider/mobile/arte-carrusel-sl-600x600.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
      <div class="carousel-caption">
        <div class="text-white"> @if (Session::get('language') == 'es')
          <h4 name="texto" class="text-white text-slider">Coloque su equipo en el Centro de Datos borde de Costa Rica,
            certificado normativa Rated 3 de ANSI/TIA-942-B…</h4>
          @else
          <h4 name="texto" class="text-white text-slider">Your server colocation in Costa Rica Edge Data Center,
            certified regulations Rated 3 de ANSI/TIA-942-B…</h4>
          @endif
        </div>
        <a href="{{ __('rutas.url_centro') }}" type="button"
          class="btn btn-modern btn-sm btn- btn-success rounded-0 mb-1 m-auto">{{ __('inicio.ver_mas') }}</a>
      </div>
    </div>
    <div class="carousel-item"> <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/arte-carrusel-sms.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel">
      <img width="600" src="{{URL::asset('assets/imagenes/slider/mobile/arte-carrusel-sms-600x600.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
      <div class="carousel-caption">
        <div class="text-white"> @if (Session::get('language') == 'es')
          <h4 name="texto" class="text-white text-slider">Comuníquese directamente con sus clientes, por medio de un
            mensaje de texto SMS al celular…</h4>
          @else
          <h4 name="texto" class="text-white text-slider">Communicate directly with your customers, through an SMS text
            message to the cell phone…</h4>
          @endif
        </div>
        <a href="https://www.smsempresarial.com" type="button"
          class="btn btn-modern btn-sm btn- btn-success rounded-0 mb-1 m-auto">{{ __('inicio.ver_mas') }}</a>
      </div>
    </div>
    <div class="carousel-item"> <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/arte-carrusel-ce.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel"> <img width="600"
        src="{{URL::asset('assets/imagenes/slider/mobile/arte-carrusel-ce-600x600.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
      <div class="carousel-caption">
        <div class="text-white"> @if (Session::get('language') == 'es')
          <h4 name="texto" class="text-white text-slider">Integración con su software de Facturación o ERP para utilizar
            la Factura Electrónica en Costa Rica…</h4>
          @else
          <h4 name="texto" class="text-white text-slider">Integration with your Invoice or ERP software to use the
            Electronic Invoice in Costa Rica</h4>
          @endif
        </div>
        <a href="https://www.comprobanteselectronicoscr.com/" type="button"
          class="btn btn-modern btn-sm btn- btn-success rounded-0 mb-1 m-auto">{{ __('inicio.ver_mas') }}</a>
      </div>
    </div>
    {{-- anuncio --}}
    <div class="carousel-item"> 
        
        @if (Session::get('language') == 'es')
          <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/anuncio_diciembre_2023.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel"> <img width="600"
        src="{{URL::asset('assets/imagenes/slider/mobile/anuncio_diciembre_2023_m.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
          @else
         <img width="1500"
        src="{{URL::asset('assets/imagenes/slider/web/anuncio-diciembre-ingles.webp')}}" class="w-100 d-none d-md-block"
        alt="Cyberfuel"> <img width="600"
        src="{{URL::asset('assets/imagenes/slider/mobile/anuncio-diciembre-mobile-ingles.webp')}}"
        class="w-100 d-block d-md-none" alt="Cyberfuel">
          @endif
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselCyber" data-bs-slide="prev">
    <div class="fondo-flecha"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span
        class="visually-hidden">Previous</span> </div>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselCyber" data-bs-slide="next">
    <div class="fondo-flecha"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span
        class="visually-hidden">Next</span> </div>
  </button>
</div>
@section('scripts')
<script>
  $(document).ready(function() {
  // Activar el carrusel manualmente
  $("#carouselCyber").carousel();

  var $item = $('.carousel-item');
//var $wHeight = $(window).height();

//$item.height($wHeight); 
$item.addClass('full-screen');

var $numberofSlides = $('.carousel-item').length;
var $currentSlide = Math.floor((Math.random() * $numberofSlides));

$('.carousel-indicators button').each(function(){
  var $slideValue = $(this).attr('data-bs-slide-to');
  if($currentSlide == $slideValue) {
    $(this).addClass('active');
    $item.eq($slideValue).addClass('active');
   
  } else {
    $(this).removeClass('active');
    $item.eq($slideValue).removeClass('active');
  
  }
});

$('.carousel img').each(function() {
  var $src = $(this).attr('src');
  $(this).parent().css({
    //'background-image' : 'url(' + $src + ')',
   'background-image' : $src,
  });
 
});
});
 

//$(window).on('resize', function (){
//  $wHeight = $(window).height();
//  $item.height($wHeight);
//});


</script>
@endsection