<div id="div_carrito_compras" class="sidecart bg-dark overflow-auto">
    <div class="row justify-content-end pt-2">
        <div class="col-auto text-light  m-0 px-4 ">
            <h4 class="font-weight-bold text-uppercase text-2 mb-3 text-white">
                <a onclick="toggleCart()" class="header-nav-features-toggle text-white" style="cursor:pointer;">
                    {{ __('comercio.cerrar') }} &nbsp;
                    <span class="pl-4">
                        <i class="far text-success  float-right fa-arrow-alt-circle-right mt-1"></i>
                    </span>
                </a>
            </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12 position-relative px-4">
            <h4 class="font-weight-bold text-uppercase text-4 mb-3 text-white">
                {{ __('compra.tu_compra') }}
                <span class="pl-4">
                    <a href="" class="header-nav-features-toggle">
                        <i class="fas fa-shopping-basket text-success"></i>
                        <span class="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1">
                            @if((isset($detalle_carrito_compras)) && ($detalle_carrito_compras != null))
                                @if(count($detalle_carrito_compras) > 0)
                                    {{ count($detalle_carrito_compras) }}
                                @endif
                            @endif
                        </span>
                    </a>
                </span>
            </h4>

            <table class="w-100 mb-3 " align="left">
              
                <tbody>
                    <!--<tr>
                        <td>
                            <strong class="d-block text-white line-height-1 font-weight-semibold">gbeart.com</strong>
                            <span class="text-1">Dominio</span>
                        </td>
                        <td class="text-end align-top"><span class="amount font-weight-medium text-color-grey">$15 &nbsp;</span></td>
                        <td valign="top">
                              <span class="rounded-pill bg-danger">
                                  <a href="#" class="p-1" title="Quitar Producto">
                                      <i class="fas fa-times"></i>
                                  </a>
                              </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong class="d-block text-white line-height-1 font-weight-semibold">Correos Extra</strong>
                            <span class="text-1">Complementos</span>
                        </td>
                        <td class="text-end align-top">
                            <span class="amount font-weight-medium text-color-grey">$25 &nbsp;</span>
                        </td>
                        <td valign="top">
                            <span class="rounded-pill bg-danger">
                                <a href="#" class="p-1" title="Quitar Producto"> <i class="fas fa-times"></i> </a>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong class="d-block text-white line-height-1 font-weight-semibold">Dominio Privado</strong>
                            <span class="text-1">Seguridad contra spam</span>
                        </td>
                        <td class="text-end align-top">
                            <span class="amount font-weight-medium text-color-grey">
                                <input type="checkbox" name="" class="gdpe-input custom-checkbox-switch mb-1" value="">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="border-top-0 pt-0">
                            <strong class="d-block text-white line-height-1 font-weight-semibold">SSL
                                <span class="d-inline-block mt-1">Gratis</span>
                            </strong>
                        </td>
                        <td class="border-top-0 text-end align-top pt-0">
                            <span class="amount font-weight-medium text-color-grey">$0</span>
                        </td>
                    </tr>-->
                    
             
                        
                       
                    @php
                        $total_descuento = 0;
                        $total_impuesto  = 0;
                        $total_final     = 0;

                        //Aplica descuento
                        $porcentaje_descuento = 0;
                        if((isset($cupon_descuento_aplicado)) && ($cupon_descuento_aplicado != null)){
                            //Porcentaje
                            if($cupon_descuento_aplicado["tipo_descuento"] == "porcentaje"){
                                $porcentaje_descuento = $cupon_descuento_aplicado["valor_descuento"]/100;
                            }

                            //Monto
                            if($cupon_descuento_aplicado["tipo_descuento"] == "monto"){
                                $monto_descuento = $cupon_descuento_aplicado["valor_descuento"];
                                if((isset($carrito_compras)) && ($carrito_compras != null)){
                                    $total_compra = 0;
                                    foreach($carrito_compras as $info_carrito_compras){
                                        $total_compra = $total_compra + ($info_carrito_compras["precio"] * $info_carrito_compras["cantidad"]);
                                    }

                                    //Calcula porcentaje de descuento
                                    if($total_compra > $monto_descuento){
                                        $total_temp = $total_compra - $monto_descuento;
                                        $porcentaje_descuento = (($total_temp * 100 ) / $total_compra) / 100;
                                    }
                                }
                            }
                        }

                        //Aplica impuesto
                        $porcentaje_impuesto = 0;
                        if((isset($datos_cliente_temp)) && ($datos_cliente_temp != null)){
                            $tipo_cedula = $datos_cliente_temp["tipo_cedula"];
                            if(($tipo_cedula == "fisico") || ($tipo_cedula == "juridico")){
                                $porcentaje_impuesto = 0.13;
                            }
                        }
                    @endphp
{{-- 
					
					 @if((isset($carrito_compras)) && ($carrito_compras != null))
                        @foreach($carrito_compras as $index => $info_carrito_compras)
                            @php
                                $descuento = 0;
                                $impuesto  = 0;
                                $subtotal  = 0;

                                //Calcular monto
                                $monto = $info_carrito_compras["cantidad"] * $info_carrito_compras["precio"];

					//Aplica descuentos
					
                                if($porcentaje_descuento > 0){
                                    $descuento = $monto * $porcentaje_descuento;
                                }
                                $total_descuento = $total_descuento + $descuento;

                                //Aplica impuesto
                                if($porcentaje_impuesto > 0){
                                    $impuesto = ($monto - $descuento) * $porcentaje_impuesto;
                                }
                                $total_impuesto  = $total_impuesto + $impuesto;

                                $subtotal = ($monto - $descuento) + $impuesto;
                                $total_final = $total_final + $subtotal;
                            @endphp

                            <tr>
                                <td>
                                    <strong class="d-block text-white line-height-1 font-weight-semibold text-2 text-overflow">
                                        {{ $info_carrito_compras["titulo"] }}
                                    </strong>
                                    <span class="text-1">{{ $info_carrito_compras["familia"] }}</span>
                                </td>
                                <td class="text-end align-top">
                                    <span class="amount font-weight-medium text-color-grey">
                                        &dollar;{{ number_format($monto, 2) }} &nbsp;
                                    </span>
                                </td>
                                <td valign="top">
                                    <span class="rounded-pill bg-danger">
                                        <a onclick="carrito_compras_eliminar_producto('{{ $index }}');" class="p-1" title="Quitar Producto" style="cursor: pointer;">
                                            <i class="fas fa-times"></i>
                                        </a>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @endif

					
					
					--}}
                  
            
                    <tr class="cart-subtotal">
                        <td class="border-top-0">
                            <strong class="text-white">
                                {{ __('master.descuento_promo') }}
                            </strong>
                        </td>
                        <td class="border-top-0 text-end">
                            <strong>
                                <span class="amount font-weight-medium">
                                    &dollar;{{ number_format($total_descuento, 2) }}
                                </span>
                            </strong>
                        </td>
                    </tr>

                    <tr class="cart-subtotal">
                        <td class="border-top-0">
                            <strong class="text-white">
                                {{ __('master.impuesto') }}
                            </strong>
                        </td>
                        <td class="border-top-0 text-end">
                            <strong>
                                <span class="amount font-weight-medium">
                                    &dollar;{{ number_format($total_impuesto, 2) }}
                                </span>
                            </strong>
                        </td>
                    </tr>

                    <tr class="cart-subtotal">
                        <td class="border-top-0">
                            <strong class="text-white">Subtotal</strong>
                        </td>
                        <td class="border-top-0 text-end">
                            <strong>
                                <span class="amount font-weight-medium">&dollar;{{ number_format($total_final, 2) }}</span>
                            </strong>
                        </td>
                    </tr>

                    <tr class="total">
                        <td>
                            <strong class="text-white text-3-5">Total</strong>
                        </td>
                        <td class="text-end">
                            <strong class="text-white">
                                <span class="amount text-white text-5">&dollar;{{ number_format($total_final, 2) }}</span>
                            </strong>
                        </td>
                    </tr>
                 

                    <tr></tr>
                </tbody>
              
            </table>
        </div>
    </div>

    <div class="row justify-content-center mt-4">
        @if($total_final > 0)
            <div class="col-auto ">
                <a  href="{{ __('rutas.url_nuevo_cliente') }}" type="submit" class="btn btn-dark btn-modern  text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-2 py-3  mx-4">
                    Aprovar servicios <br> y continuar
                    <i class="fas fa-arrow-right ms-2"></i>
                </a>
            </div>
        @endif
        <div class="col-auto mt-2 ">
            <button onClick="toggleCart();" type="submit" class="btn btn-dark btn-modern  text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-2 py-3">
                Seguir Comprando
                <i class="fas fa-arrow-right ms-2"></i>
            </button>
        </div>
    </div>
</div>
