
 
    


       
          
         
<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center mb-4" data-sort-id="portfolio"
  data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
  <li class="nav-item active" data-option-value="*"><a class="nav-link text-2-5 text-uppercase active" href="#">{{__('compra.todo') }}</a>
  </li>
  <li class="nav-item" data-option-value=".servidores"><a class="nav-link text-2-5 text-uppercase" href="#">{{__('master.servidores') }}</a></li>
  <li class="nav-item" data-option-value=".servicios"><a class="nav-link text-2-5 text-uppercase" href="#">{{__('compra.servicios') }}</a>
  </li>
  <li class="nav-item" data-option-value=".seguridad"><a class="nav-link text-2-5 text-uppercase" href="#">{{__('master.seguridad') }}</a>
  </li>
     <li class="nav-item" data-option-value=".respaldo"><a class="nav-link text-2-5 text-uppercase" href="#">{{__('master.respaldo') }}</a>
  </li>
</ul>
          <div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
              <div class="masonry-loader masonry-loader-showing">
  <div class="row portfolio-list sort-destination products product-thumb-info-list" data-sort-id="portfolio" data-plugin-masonry
        data-plugin-options="{'layoutMode': 'fitRows'}">

 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item servidores" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/servidor.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
          {{ __('comercio.servidor_dinamico_nube') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$32.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item servidores" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor2" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor2" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/plesk.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
        {{ __('comercio.servidor_dinamico_plesk') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$47.31*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

<!-- <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item servidores" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor3" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor3" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/plesk_w.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
         {{ __('comercio.servidor_dinamico_plesk_wordpress') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$32.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>-->

 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item servidores" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor4" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#servidor4" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/cpanel.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
  {{ __('comercio.servidor_dinamico_cpanel') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$52.99*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item servicios" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#plan" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#plan" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/plan1.png')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
        {{ __('comercio.planes_administracion') }} #1
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$45.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>
      
 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item servicios" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#plan" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#plan" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/plan2.png')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
        {{ __('comercio.planes_administracion') }} #2
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$95.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>
      
 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item servicios" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#plan" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#plan" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/plan3.png')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
        {{ __('comercio.planes_administracion') }} #3
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$140.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item respaldo" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#respaldo" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#respaldo" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/baas.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
      {{ __('comercio.respaldp_basas') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$32.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item seguridad" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#ninja" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#ninja" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/ninja.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
       {{ __('comercio.bitninja') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$50.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

 <div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item seguridad" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#spamfilter" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#spamfilter" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/spam.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
 {{ __('comercio.spamfilter') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$1.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

<div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item seguridad" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#certificado" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#certificado" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/ssl.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
         {{ __('comercio.certificado') }}
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$88.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>

<div class="col-12 col-sm-6 col-lg-6 col-xl-4 isotope-item seguridad" >
  <div class="product mb-0">
    <div class="product-thumb-info border-0 mb-3">
      <div class="product-thumb-info-badges-wrapper"> <span class="badge badge-ecommerce badge-success">NUEVO</span>
        <span class="badge badge-ecommerce badge-danger">27% DESC</span>
      </div>
      <div class="addtocart-btn-wrapper">  </div>
      <a type="button" data-bs-toggle="modal" data-bs-target="#pci" data-bs-whatever="@mdo"  class="quick-view text-uppercase font-weight-semibold text-2"> {{ __('inicio.ver_mas') }} </a>
      <a type="button" data-bs-toggle="modal" data-bs-target="#pci" data-bs-whatever="@mdo">
        <div class="product-thumb-info-image"> <img class="lazyload m-auto w-75 d-block"
            src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/market/pci.webp')}}" alt="Cyberfuel"> </div>
      </a>
    </div>
    <div class="d-flex justify-content-between">
      <div>
        <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><a
            href="shop-product-sidebar-right.html" class="text-color-dark text-color-hover-primary">
          PCI-DSS
            </a>
        </h3>
      </div>
     <a href="{{ __('rutas.url_carrito-de-compra') }}" class="text-decoration-none addtocart-btn"
          title="{{ __('hospedaje.agregar_carrito') }}"> <i class="fas fa-shopping-cart fa-2x pad_25"></i> </a>
    </div>
    <div title="Rated 5 out of 5">
      <input type="text" class="d-none" value="5" title="" data-plugin-star-rating=""
        data-plugin-options="{'displayOnly': true, 'color': 'default', 'size':'xs'}">
    </div>
    <p class="price text-5 mb-3"><span class="text-3 text-primary">{{ __('master.desde') }}: &nbsp;</span> <span class="sale text-color-dark font-weight-semi-bold">$100.00*</span> <span
        class="amount d-none">69,00</span> </p>
  </div>
</div>
          
  </div>
</div>
  </div>      


@include('includes.modal-market')
