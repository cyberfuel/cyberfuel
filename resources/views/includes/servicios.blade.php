<div id="servicios" class="container-fluid mt-4">
	<div class="row">
		<div class="col col-md-10 offset-md-1">

			<div class="row">
				<div class="col">
					<h4 class="text-center text-6">{{ __('inicio.nuestros') }} {{ __('inicio.servicio') }}s</h4>
				</div>
			</div>
			<div class="divider divider-primary">
				<i class="fas fa-chevron-down"></i>
			</div>
			<!-- <div class="owl-carousel owl-theme show-nav-title " data-plugin-options="{'responsive': {'0': {'items': 1}, '480': {'items': 1}, '768': {'items': 4}, '992': {'items': 4}, '1200': {'items': 4}}, 'autoplay': true, 'autoplayTimeout': 8000, 'dots': true}">-->

			<div class="row">
				<div class="col-12 col-md-6 col-xl-4">
					<div class="featured-box featured-box-primary border-radius-0 featured-box-effect-1">
						<div class="box-content border-gradiente border-radius-0 p-1">
							<a class="link-offset-2 link-underline link-underline-opacity-0"
								href="{{ __('rutas.url_dominio') }}">
								<div class="row align-items-center">
									<div class="col-3 "> <img width="100" height="83"
											class="card-img-top  lazyload m-auto p-2"
											src="{{URL::asset('assets/imagenes/servicios/dominios.svg')}}"
											alt="Registro de dominios">
									</div>
									<div class="col-9">
										<h4 class="font-weight-normal text-5 text-dark">{{ __('inicio.regi_dominios') }}
											<i class="fas fa-angle-right position-relative top-1 ms-1"></i>
										</h4>
										<p class="text-4 font-weight-bold text-primary">{{ __('master.desde') }} $15.99*
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>


				<div class="col-12 col-md-6 col-xl-4">
					<div class="featured-box featured-box-primary border-radius-0 featured-box-effect-1">
						<div class="box-content border-gradiente border-radius-0 p-1">
							<a class="link-offset-2 link-underline link-underline-opacity-0"
								href="{{ __('rutas.url_hospedaje') }}">
								<div class="row align-items-center">
									<div class="col-3 "> <img width="100" height="83"
											class="card-img-top  lazyload m-auto p-2"
											src="{{URL::asset('assets/imagenes/servicios/hospedaje.svg')}}"
											alt="Hospedaje web">
									</div>
									<div class="col-9">
										<h4 class="font-weight-normal text-5 text-dark">{{ __('inicio.hospedaje_web')
											}}<i class="fas fa-angle-right position-relative top-1 ms-1"></i>
										</h4>
										<p class="text-4 font-weight-bold text-primary">{{ __('master.desde') }} $30.00*
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>


				<div class="col-12 col-md-6 col-xl-4">
					<div class="featured-box featured-box-primary border-radius-0 featured-box-effect-1">
						<div class="box-content border-gradiente border-radius-0 p-1">
							<a class="link-offset-2 link-underline link-underline-opacity-0"
								href="{{ __('rutas.url_hospedaje') }}#tabsComercio">
								<div class="row align-items-center">
									<div class="col-3 "><img width="100" height="83"
											class="card-img-top  lazyload m-auto p-2"
											src="{{URL::asset('assets/imagenes/servicios/comercio.svg')}}"
											alt="Tienda en linea">
									</div>
									<div class="col-9">
										<h4 class="font-weight-normal text-5 text-dark">{{ __('inicio.tienda') }} <i
												class="fas fa-angle-right position-relative top-1 ms-1"></i>
										</h4>
										<p class="text-4 font-weight-bold text-primary">{{ __('master.desde') }} $64.99*
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>


				<div class="col-12 col-md-6 col-xl-4">
					<div class="featured-box featured-box-primary border-radius-0 featured-box-effect-1">
						<div class="box-content border-gradiente border-radius-0 p-1">
							<a class="link-offset-2 link-underline link-underline-opacity-0"
								href="{{ __('rutas.url_seguridad') }}">
								<div class="row align-items-center">
									<div class="col-3 "> <img width="100" height="83"
											class="card-img-top  lazyload m-auto p-2"
											src="{{URL::asset('assets/imagenes/servicios/seguridad.svg')}}"
											alt="Seguridad">
									</div>
									<div class="col-9">
										<h4 class="font-weight-normal text-5 text-dark">{{ __('inicio.seguridad') }} <i
												class="fas fa-angle-right position-relative top-1 ms-1"></i>
										</h4>
										<p class="text-4 font-weight-bold text-primary">{{ __('master.desde') }} $88.00*
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6 col-xl-4">
					<div class="featured-box featured-box-primary border-radius-0 featured-box-effect-1">
						<div class="box-content border-gradiente border-radius-0 p-1">
							<a class="link-offset-2 link-underline link-underline-opacity-0"
								href="{{ __('rutas.url_servidores') }}">
								<div class="row align-items-center">
									<div class="col-3 "> <img width="100" height="83"
											class="card-img-top  lazyload m-auto p-2"
											src="{{URL::asset('assets/imagenes/servicios/servidores.svg')}}"
											alt="Servidores en la nube">
									</div>
									<div class="col-9">
										<h4 class="font-weight-normal text-5 text-dark">{{ __('inicio.servidor_nube') }}
											<i class="fas fa-angle-right position-relative top-1 ms-1"></i>
										</h4>
										<p class="text-4 font-weight-bold text-primary">{{ __('master.desde') }} $32.00*
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6 col-xl-4">
					<div class="featured-box featured-box-primary border-radius-0 featured-box-effect-1">
						<div class="box-content border-gradiente border-radius-0 p-1">
							<a class="link-offset-2 link-underline link-underline-opacity-0"
								href="{{ __('rutas.url_centro') }}">
								<div class="row align-items-center">
									<div class="col-3 "> <img width="100" height="83"
											class="card-img-top  lazyload m-auto p-2"
											src="{{URL::asset('assets/imagenes/servicios/centro_datos.svg')}}"
											alt="Centro de datos">
									</div>
									<div class="col-9">
										<h4 class="font-weight-normal text-5 text-dark">{{ __('inicio.centro') }} <i
												class="fas fa-angle-right position-relative top-1 ms-1"></i>
										</h4>
										<p class="text-4 font-weight-bold text-primary">{{ __('master.desde') }} $149.99
											*</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>

			</div>


		</div>
	</div>
</div>