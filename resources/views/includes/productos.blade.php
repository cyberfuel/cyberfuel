@php
$rutaActual = $_SERVER['REQUEST_URI'];
@endphp
<div class="row">
    <div class="col">

        <div class="row justify-content-start g-4">
            @if($rutaActual == "/")

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/factura.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Sistema WEB Factura Electrónica
                    @else
                    WEB Electronic Invoice System
                    @endif
                </x-slot>
                <x-slot name="link">https://facturaprofesional.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Solución de facturación electrónica en la nube para empresas y profesionales
                    @else
                    Electronic invoicing solution in the cloud for companies and professionals
                    @endif
                </x-slot>
                <x-slot name="producto">www.FacturaProfesional.com</x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/comprobantes.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    API de Factura Electrónica con otros sistemas
                    @else
                    Electronic Invoice API with other systems
                    @endif
                </x-slot>
                <x-slot name="link">https://www.comprobanteselectronicoscr.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Conexión API para integrar su sistema actual de facturación o ERP y facturar
                    electrónicamente
                    @else
                    API connection to integrate your current invoice or ERP system and allows you to invoice
                    electronically.
                    @endif
                </x-slot>
                <x-slot name="producto">www.ComprobantesElectronicosCR.com</x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/atencion.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Atención en Línea
                    @else
                    Online Service
                    @endif
                </x-slot>
                <x-slot name="link">https://www.atencionprofesional.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Permite a empresas una comunicación de diferentes soluciones omnicanal entre
                    agente y cliente
                    @else
                    It allows companies to communicate different omnichanel solutions between
                    agent and client
                    @endif
                </x-slot>
                <x-slot name="producto">www.AtencionProfesional.com</x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/sms.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Mensajes de Texto SMS y WhatsApp
                    @else
                    SMS and WhatsApp messages
                    @endif
                </x-slot>
                <x-slot name="link">http://www.smsempresarial.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Sistema que permite el envío de mensajes de texto SMS desde Internet a
                    teléfonos celulares
                    @else
                    System that allows the sending of SMS text messages from the Internet to
                    cell phones
                    @endif
                </x-slot>
                <x-slot name="producto">www.SMSEmpresarial.com</x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/data.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Sus servidores en un lugar seguro y con alta disponibilidad
                    @else
                    Your servers in a safe place with high availability
                    @endif
                </x-slot>
                <x-slot name="link">{{ __('rutas.url_centro') }}</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Alojamiento de servidores físicos en Centro de Datos cumpliendo estándares
                    internacionales
                    @else
                    Hosting of physical servers in a Data Center complying with international
                    standards
                    @endif
                </x-slot>
                <x-slot name="producto">

                    @if (Session::get('language') == 'es')
                    Centro de Datos de Cyberfuel
                    @else
                    Cyberfuel Data Center
                    @endif
                </x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/factoraje.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Sistema para descontar Facturas
                    @else
                    Factoring software
                    @endif
                </x-slot>
                <x-slot name="link">https://www.factorajeelectronico.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Permite revolucionar la forma tradicional del proceso de comunicación para
                    el financiamiento a corto plazo de actividades empresariales.
                    @else
                    It allows revolutionizing the traditional form of the communication process
                    for the short-term financing of business activities.
                    @endif
                </x-slot>
                <x-slot name="producto">www.FactorajeElectronico.com</x-slot>
            </x-productos>
            @elseif($rutaActual == "/landings/servicios-productos")

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/factura.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Sistema WEB Factura Electrónica
                    @else
                    WEB Electronic Invoice System
                    @endif
                </x-slot>
                <x-slot name="link">https://www.facturaprofesional.com/landings/ventajas-facturacion-electronica
                </x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Solución de facturación electrónica en la nube para empresas y profesionales
                    @else
                    Electronic invoicing solution in the cloud for companies and professionals
                    @endif
                </x-slot>
                <x-slot name="producto">www.FacturaProfesional.com</x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/comprobantes.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    API de Factura Electrónica con otros sistemas
                    @else
                    Electronic Invoice API with other systems
                    @endif
                </x-slot>
                <x-slot name="link">https://www.comprobanteselectronicoscr.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Conexión API para integrar su sistema actual de facturación o ERP y facturar
                    electrónicamente
                    @else
                    API connection to integrate your current invoice or ERP system and allows you to invoice
                    electronically.
                    @endif
                </x-slot>
                <x-slot name="producto">www.ComprobantesElectronicosCR.com</x-slot>
            </x-productos>

            {{-- <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/atencion.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Atención en Línea
                    @else
                    Online Service
                    @endif
                </x-slot>
                <x-slot name="link">https://www.atencionprofesional.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Permite a empresas una comunicación de diferentes soluciones omnicanal entre
                    agente y cliente
                    @else
                    It allows companies to communicate different omnichanel solutions between
                    agent and client
                    @endif
                </x-slot>
                <x-slot name="producto">www.AtencionProfesional.com</x-slot>
            </x-productos> --}}

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/sms.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Mensajes de Texto SMS y WhatsApp
                    @else
                    SMS and WhatsApp messages
                    @endif
                </x-slot>
                <x-slot name="link">https://www.cyberfuel.com/landings/sms.html</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Sistema que permite el envío de mensajes de texto SMS desde Internet a
                    teléfonos celulares
                    @else
                    System that allows the sending of SMS text messages from the Internet to
                    cell phones
                    @endif
                </x-slot>
                <x-slot name="producto">www.SMSEmpresarial.com</x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/data.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Sus servidores en un lugar seguro y con alta disponibilidad
                    @else
                    Your servers in a safe place with high availability
                    @endif
                </x-slot>
                <x-slot name="link">https://www.cyberfuel.com/landings/data-center.html</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Alojamiento de servidores físicos en Centro de Datos cumpliendo estándares
                    internacionales
                    @else
                    Hosting of physical servers in a Data Center complying with international
                    standards
                    @endif
                </x-slot>
                <x-slot name="producto">

                    @if (Session::get('language') == 'es')
                    Centro de Datos de Cyberfuel
                    @else
                    Cyberfuel Data Center
                    @endif
                </x-slot>
            </x-productos>

            <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/hospedaje.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Elige el Plan de Hospedaje adecuado para usted.
                    @else
                    Choose the appropriate Hosting Plan for you.
                    @endif
                </x-slot>
                <x-slot name="link">https://www.cyberfuel.com/landings/hospedaje_web.html</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')

                    @else

                    @endif
                </x-slot>
                <x-slot name="producto">

                    @if (Session::get('language') == 'es')
                    Hospedaje Web
                    @else
                    Web Hosting
                    @endif
                </x-slot>
            </x-productos>

            {{-- <x-productos>
                <x-slot name="imagen">{{URL::asset('assets/imagenes/productos/factoraje.webp')}}</x-slot>
                <x-slot name="titulo">
                    @if (Session::get('language') == 'es')
                    Sistema para descontar Facturas
                    @else
                    Factoring software
                    @endif
                </x-slot>
                <x-slot name="link">https://www.factorajeelectronico.com/</x-slot>
                <x-slot name="contenido">
                    @if (Session::get('language') == 'es')
                    Permite revolucionar la forma tradicional del proceso de comunicación para
                    el financiamiento a corto plazo de actividades empresariales.
                    @else
                    It allows revolutionizing the traditional form of the communication process
                    for the short-term financing of business activities.
                    @endif
                </x-slot>
                <x-slot name="producto">www.FactorajeElectronico.com</x-slot>
            </x-productos> --}}
            @endif
        </div>
    </div>
</div>