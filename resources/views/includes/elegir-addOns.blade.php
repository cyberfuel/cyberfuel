<div class="row shop g-0">
    {{-- Compra años Dominio --}}
    <div class="col-12 ">
        <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter appear-animation-visible w-100 h-100"
            data-appear-animation="fadeInUpShorter" data-appear-animation-delay="550" style="animation-delay: 550ms;">
            <div class="row align-items-center  w-100">
                <div class="col-8 col-md-auto flex-md-fill"> <span class="text-start text-2 float-start"><span
                            class="text-primary text-3-4">Años de Registro gil</span></span>
                </div>
                <div class="col-4 col-md-auto flex-md-fill px-0">


                    <div class="quantity float-end my-2 mx-0">
                        <input type="button"
                            class="minus text-color-hover-light bg-color-hover-primary border-color-hover-primary"
                            value="-">
                        <input type="text" class="input-text qty text" title="Qty" value="1" name="quantity" min="1"
                            max="10" step="1">
                        <input type="button"
                            class="plus text-color-hover-light bg-color-hover-primary border-color-hover-primary"
                            value="+">
                    </div>

                    {{-- <div class="input-group">

                        <p>
                            <label for="inputNet">Net</label>
                            <input type="number" id="inputNet" value="100" min="0" max="10000" step="0.01"
                                data-decimals="2" />
                        </p>
                        <p>
                            <label for="inputGross">Gross (+19%)</label>
                            <input type="number" id="inputGross" value="100" min="0" max="11900" step="0.01"
                                data-decimals="2" />
                        </p>
                    </div> --}}
                </div>
                <div class="col-auto px-2 flex-fill">
                    <p class="price text-4 m-0 float-start float-md-end"> <span id="valueOnChange"
                            class="sale text-color-dark font-weight-semi-bold">$1.80/yr</span>
                        <br>
                    </p>
                </div>
                <div class="col-auto col-md-auto px-2">
                    <button type="button" class="btn btn-quaternary btn-sm btn-circle float-end cerrarAdd"><i
                            class="fa fa-times"></i>
                    </button>
                </div>


            </div>
        </div>
    </div>

    {{-- compra registro Privado --}}

    <div class="col-12 ">
        <div class="cerrado_add d-inline-flex align-items-center btn btn-3d btn-light rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter appear-animation-visible w-100 h-100"
            data-appear-animation="fadeInUpShorter" data-appear-animation-delay="550" style="animation-delay: 550ms;">
            <div class="row align-items-center  w-100">
                <div class="col-8 col-md-auto flex-md-fill"> <span class="text-start text-2 float-start"><span
                            class="text-primary text-3-4">Registro Privado</span></span>
                </div>
                <div class="col-4 col-md-auto flex-md-fill px-0">

                </div>
                <div class="col-auto px-2 flex-fill">
                    <p class="price text-4 m-0 float-start float-md-end"> <span
                            class="sale text-color-dark font-weight-semi-bold">$1.80/yr</span>
                        <br>
                    </p>
                </div>

                <div class="col-auto col-md-auto px-2">
                    <button type="button" class="btn btn-quaternary btn-sm btn-circle float-end cerrarAdd"><i
                            class="fa fa-times"></i>
                    </button>
                </div>


            </div>
        </div>
    </div>
</div>