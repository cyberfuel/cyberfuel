
<div class="col-12"><div class="row g-0">
    <div class="home-intro light  border border-0 mb-0 py-4">
					<div class="container">

						<div class="row align-items-center">
							<div class="col-lg-8">
								<p class="font-weight-bold text-color-dark">
									Tabla comparativa de  <span class="highlighted-word highlighted-word-animation-1 text-color-primary font-weight-semibold text-5">hospedajes</span>
									<span>Si tienes dudas aqui puedes ver las caracteristicas de cada uno.</span>
								</p>
							</div>
							<div class="col-lg-4">
								<div class="get-started text-start text-lg-end">
									<button data-bs-toggle="modal"
                        data-bs-target="#hospedaje" class="btn btn-primary btn-lg text-3 font-weight-semibold btn-py-2 px-4">  {{ __('compra.ver_mas_planes') }}</button>
								
								</div>
							</div>
						</div>

					</div>
</div>
    </div></div>

<div data-container="container1" class="esconder-hospedaje col-12 altura-plan">
    <div class=" d-inline-flex align-items-center btn btn-3d btn-light
                       rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter
                       appear-animation-visible w-100 h-100" data-appear-animation="fadeInUpShorter"
        data-appear-animation-delay="550" style="animation-delay: 550ms;">
        <div class="row align-items-center w-100">
            <div class="col-10 align-self-start"> <span class="text-start text-2 float-start"><span
                        class="text-primary text-3-4">Plan de Hospedaje
                        Correo</span></span>
            </div>

            <div class="col-2 px-2 ocultar-precio">
                <p class="price text-4 m-0 float-end"> <span
                        class="sale text-color-dark font-weight-semi-bold">$30.00<sup>+IVA</sup>
                        <span class="text-primary font-weight-black">{{__('master.anual') }}</span>
                    </span>
                    <br>
                </p>
            </div>

            <div class="col-8 px-2"> <span class="float-start align-items-center row w-100">

                    <span class="col-10 offset-1">
                        <span class="small lista text-start mt-2">
                            <ul class="px-2">
                                <li> <strong>1</strong> Buzon de Correo.</li>
                                <li><strong>1 GB</strong> de espacio.</li>
                            </ul>
                        </span>
                    </span> </span> </div>
            <div class="col-auto px-2 flex-fill">
                <p class="price text-4 m-0 float-end mostrar-precio hospedajes d-none"> <span class="text-success small font-weight-black">Comprado</span>
                    <br>
                </p>
            </div>


            <div class="col-auto flex-fill px-2 quitar_fill">
                <button data-target="container1" onclick="mostrarBoton(this)" type="button"
                    class="mb-1 mt-1 btn btn-outline btn-primary float-end btn mostrar">
                    <span> <i class="fas fa-cart-plus fa-2x"></i>
                </button>
                <a type="button" class="btn btn-quaternary  btn-circle float-end d-none btn-cerrar-host"
                    aria-label="Cyberfuel"><i class="fa fa-times"></i>
                </a>
            </div>

        </div>

    </div>

</div>

<div data-container="container2" class="esconder-hospedaje col-12 altura-plan">
    <div class=" d-inline-flex align-items-center btn btn-3d btn-light
                       rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter
                       appear-animation-visible w-100 h-100" data-appear-animation="fadeInUpShorter"
        data-appear-animation-delay="550" style="animation-delay: 550ms;">
        <div class="row align-items-center w-100">
            <div class="col-10 align-self-start"> <span class="text-start text-2 float-start"><span
                        class="text-secondary text-3-4">Plan de Hospedaje
                        Personal</span></span>
            </div>

            <div class="col-2 px-2 ocultar-precio">
                <p class="price text-4 m-0 float-end"> <span
                        class="sale text-color-dark font-weight-semi-bold">$64.99<sup>+IVA</sup>
                        <span class="text-primary font-weight-black">{{__('master.anual') }}</span>
                    </span>
                    <br>
                </p>
            </div>

            <div class="col-8 px-2"> <span class="float-start align-items-center row w-100">

                    <span class="col-10 offset-1">
                        <span class="small lista text-start mt-2">
                            <ul class="px-2">
                                <li> <strong>10</strong> Buzones Correo.</li>
                                <li><strong>5 GB</strong> de espacio.</li>
                                <li><strong>1 DB</strong> MySQL</li>
                            </ul>
                        </span>
                    </span> </span> </div>
            <div class="col-auto px-2 flex-fill">
                <p class="price text-4 m-0 float-end mostrar-precio hospedajes d-none"> <span class="text-success small font-weight-black">Comprado</span>
                    <br>
                </p>
            </div>


            <div class="col-auto flex-fill px-2 quitar_fill">
                <button data-target="container2" onclick="mostrarBoton(this)" type="button"
                    class="mb-1 mt-1 btn btn-outline btn-primary float-end btn mostrar ">
                    <span> <i class="fas fa-cart-plus fa-2x"></i>
                </button>
                <a type="button" class="btn btn-quaternary  btn-circle float-end d-none btn-cerrar-host"
                    aria-label="Cyberfuel"><i class="fa fa-times"></i>
                </a>
            </div>

        </div>

    </div>

</div>

<div data-container="container3" class="esconder-hospedaje col-12 altura-plan">
    <div class=" d-inline-flex align-items-center btn btn-3d btn-light
                       rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter
                       appear-animation-visible w-100 h-100" data-appear-animation="fadeInUpShorter"
        data-appear-animation-delay="550" style="animation-delay: 550ms;">
        <div class="row align-items-center w-100">
            <div class="col-10 align-self-start"> <span class="text-start text-2 float-start"><span
                        class="text-info text-3-4">Plan
                        de Hospedaje
                        Pymes</span></span>
            </div>

            <div class="col-2 px-2 ocultar-precio">
                <p class="price text-4 m-0 float-end"> <span
                        class="sale text-color-dark font-weight-semi-bold">$129.99<sup>+IVA</sup>
                        <span class="text-primary font-weight-black">{{__('master.anual') }}</span></span>
                    <br>
                </p>
            </div>

            <div class="col-8 px-2"> <span class="float-start align-items-center row w-100">

                    <span class="col-10 offset-1">
                        <span class="small lista text-start mt-2">
                            <ul class="px-2">
                                <li> <strong>60</strong> Buzones Correo.</li>
                                <li><strong>10 GB</strong> de espacio.</li>
                                <li><strong>10 DB</strong> MySQL</li>
                            </ul>
                        </span>
                    </span> </span> </div>
             <div class="col-auto px-2 flex-fill">
                <p class="price text-4 m-0 float-end mostrar-precio hospedajes d-none"> <span class="text-success small font-weight-black">Comprado</span>
                    <br>
                </p>
            </div>
            <div class="col-auto flex-fill px-2 quitar_fill">
                <button data-target="container3" onclick="mostrarBoton(this)" type="button"
                    class="mb-1 mt-1 btn btn-outline btn-primary float-end btn mostrar">
                    <span> <i class="fas fa-cart-plus fa-2x"></i>
                </button>
                <a type="button" class="btn btn-quaternary  btn-circle float-end d-none btn-cerrar-host"
                    aria-label="Cyberfuel"><i class="fa fa-times"></i>
                </a>
            </div>

        </div>

    </div>

</div>
<div data-container="container4" class="esconder-hospedaje col-12 altura-plan">
    <div class=" d-inline-flex align-items-center btn btn-3d btn-light
                       rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter
                       appear-animation-visible w-100 h-100" data-appear-animation="fadeInUpShorter"
        data-appear-animation-delay="550" style="animation-delay: 550ms;">
        <div class="row align-items-center w-100">
            <div class="col-10 align-self-start"> <span class="text-start text-2 float-start"><span
                        class="text-success text-3-4">Plan de Hospedaje
                        Empresarial</span></span>
            </div>

            <div class="col-2 px-2 ocultar-precio">
                <p class="price text-4 m-0 float-end"> <span
                        class="sale text-color-dark font-weight-semi-bold">$299.99<sup>+IVA</sup>
                        <span class="text-primary font-weight-black">{{__('master.anual') }}</span></span>
                    <br>
                </p>
            </div>

            <div class="col-8 px-2"> <span class="float-start align-items-center row w-100">

                    <span class="col-10 offset-1">
                        <span class="small lista text-start mt-2">
                            <ul class="px-2">
                                <li> <strong>120</strong> Buzones Correo.</li>
                                <li><strong>20 GB</strong> de espacio.</li>
                                <li><strong>20 DB</strong> MySQL</li>
                            </ul>
                        </span>
                    </span> </span> </div>
          <div class="col-auto px-2 flex-fill">
                <p class="price text-4 m-0 float-end mostrar-precio hospedajes d-none"> <span class="text-success small font-weight-black">Comprado</span>
                    <br>
                </p>
            </div>
            <div class="col-auto flex-fill px-2 quitar_fill">
                <button data-target="container4" onclick="mostrarBoton(this)" type="button"
                    class="mb-1 mt-1 btn btn-outline btn-primary float-end btn mostrar">
                    <span> <i class="fas fa-cart-plus fa-2x"></i>
                </button>
                <a type="button" class="btn btn-quaternary btn-circle float-end d-none btn-cerrar-host"
                    aria-label="Cyberfuel"><i class="fa fa-times"></i>
                </a>
            </div>

        </div>

    </div>

</div>
<div data-container="container5" class="esconder-hospedaje col-12 altura-plan">
    <div class=" d-inline-flex align-items-center btn btn-3d btn-light
                       rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter
                       appear-animation-visible w-100 h-100" data-appear-animation="fadeInUpShorter"
        data-appear-animation-delay="550" style="animation-delay: 550ms;">
        <div class="row align-items-center w-100">
            <div class="col-10 align-self-start"> <span class="text-start text-2 float-start"><span
                        class="text-primary text-3-4">Plan de Hospedaje
                        Comercio Electrónico</span></span>
            </div>

            <div class="col-2 px-2 ocultar-precio">
                <p class="price text-4 m-0 float-end"> <span
                        class="sale text-color-dark font-weight-semi-bold">$64.99<sup>+IVA</sup>
                        <span class="text-primary font-weight-black">Mes</span></span>
                    <br>
                </p>
            </div>

            <div class="col-8 px-2"> <span class="float-start align-items-center row w-100">

                    <span class="col-10 offset-1">
                        <span class="small lista text-start mt-2">
                            <ul class="px-2">
                                <li> <strong>20</strong> Buzones Correo.</li>
                                <li><strong>120 GB</strong> de espacio.</li>
                                <li><strong>5 DB</strong> MySQL y Certificado SSL</li>
                            </ul>
                        </span>
                    </span> </span> </div>
           <div class="col-auto px-2 flex-fill">
                <p class="price text-4 m-0 float-end mostrar-precio hospedajes d-none"> <span class="text-success small font-weight-black">Comprado</span>
                    <br>    
                </p>
            </div>
      
            <div class="col-auto flex-fill px-2 quitar_fill">
                <button data-target="container5" onclick="mostrarBoton(this)" type="button"
                    class="mb-1 mt-1 btn btn-outline btn-primary float-end btn mostrar">
                    <span> <i class="fas fa-cart-plus fa-2x"></i>
                </button>
                <a type="button" class="btn btn-quaternary btn-circle float-end d-none btn-cerrar-host"
                    aria-label="Cyberfuel"><i class="fa fa-times"></i>
                </a>
            </div>

        </div>

    </div>

</div>

    

   
                   
    
