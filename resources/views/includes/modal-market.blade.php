<!--servidor-->

<div class="modal fade" id="servidor" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel"> {{ __('comercio.servidor_dinamico_nube') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
       Funciona como un servidor tradicional con los beneficios de la virtualización y sus
              características son escalables.
            @else
          It works like a traditional server with the benefits of virtualization and its features
              are scalable.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<!--servidor2-->
<div class="modal fade" id="servidor2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel"> {{ __('comercio.servidor_dinamico_plesk') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
       Servidor Dinámico en la Nube, permite administrar vía WEB su servidor mediante el panel
              de control de hospedaje web Plesk.
            @else
       Dynamic Server in the Cloud, allows you to manage your server via WEB through the Plesk
              web hosting control panel.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<!--servidor3-->
<div class="modal fade" id="servidor3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel"> {{ __('comercio.servidor_dinamico_plesk_wordpress') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
    Servidor Dinámico en la Nube con Plesk adaptado para WordPress, permite utilizar un
              sistema gestor de contenidos para páginas web.
            @else
     Dynamic Server in the Cloud with Plesk adapted for WordPress, allows you to use a
              content management system for web pages.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<!--servidor4-->
<div class="modal fade" id="servidor4" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel"> {{ __('comercio.servidor_dinamico_cpanel') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
   Servidor Dinámico en la Nube, permite administrar vía WEB su servidor mediante el panel
              de control de hospedaje web Cpanel.
            @else
    Dynamic Server in the Cloud, allows you to manage your server via WEB through the Cpanel
              web hosting control panel.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<!--plan 1-->
<div class="modal fade" id="plan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{ __('comercio.planes_administracion') }} #1</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
 Servicio con el cual puede contar con asesoría de nuestro personal especializado para la
              revisión de sus servidores virtuales.
          <br>
          <strong>Incluye:</strong>
          <ul>
          <li> Configuración de Usuarios, y servicios básicos en Panel de Administración Plesk (SMTP-POP-IMAP).</li>
          <li> Soporte en la configuración de usuarios en los distintos clientes de Correo.</li>
          <li> Verificación de colas de correo.</li>
          <li> Hasta 2 horas de soporte por mes.</li>
          </ul>
            @else
 Service where you can get advice from our specialized staff to review your virtual
              servers.
           <br>
          <strong>Incluye:</strong>
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<!--plan 2-->
<div class="modal fade" id="plan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{ __('comercio.planes_administracion') }} #2</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
           <br>
          <strong>Incluye:</strong>
           <ul>
            <li> Todo lo especificado en el Plan de Administración Nivel 1</li>
            <li> Servicios relacionados al sistema (HTTP, HTTPS, SMTP, IMAP, FTP, SPAM)</li>
            <li> Verificación de logs de Mail Server.</li>
            <li> Verificación de logs de Web Server.</li>
            <li> Hasta 4 horas de Soporte al Mes.</li>
          </ul>
            @else
          
 Service where you can get advice from our specialized staff to review your virtual
              servers.
           <br>
          <strong>Incluye:</strong>
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>


<!--plan 3-->
<div class="modal fade" id="plan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{ __('comercio.planes_administracion') }} #3</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
 Servicio con el cual puede contar con asesoría de nuestro personal especializado para la
              revisión de sus servidores virtuales.
           <br>
          <strong>Incluye:</strong>
     <ul>
                <li> Todo lo especificado en los Planes de Administración Nivel 1 y 2</li>
                <li> Actualización/Instalación de nuevas librerías y/o aplicativos en el servidor.</li>
                <li> Aplicación de actualizaciones en el servidor.</li>
                <li> Inconvenientes a nivel de Panel de Administración Plesk.</li>
                <li> Hasta 6 horas de soporte al Mes.</li>
          </ul>
            @else
 Service where you can get advice from our specialized staff to review your virtual
              servers.
           <br>
          <strong>Incluye:</strong>
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>


<!--respaldo-->
<div class="modal fade" id="respaldo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{ __('comercio.respaldp_basas') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
Servicio que ofrece protección de datos para máquinas virtuales (VM), cargas de trabajo
              basadas en la nube, endpoints, entre otras.
            @else
Service that offers data protection for virtual machines (VMs), cloud-based workloads,
              endpoints, and more.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<!--ninja-->
<div class="modal fade" id="ninja" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{ __('comercio.bitninja') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
Herramienta que ofrece protección proactiva contra todo tipo de ciberamenzas mediante
              varias capas.
            @else
Tool that offers proactive protection against all types of cyber threats through several
              layers.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>


<!--spamfilter-->
<div class="modal fade" id="spamfilter" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">{{ __('comercio.spamfilter') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body ">
              @if (Session::get('language') == 'es')
Servicio de seguridad y protección del correo electrónico contra 6 tipos de amenazas.
            @else
Security service and email protection against 6 types of threats.
            @endif
          
             <div class="row">
					
                 
                  <div class=" col-12  altura-plan">
    <div class=" d-inline-flex align-items-center btn btn-3d btn-light
                       rounded-0 font-weight-semibold btn-py-3 text-3 appear-animation animated fadeInUpShorter
                       appear-animation-visible w-100 h-100" data-appear-animation="fadeInUpShorter"
        data-appear-animation-delay="550" style="animation-delay: 550ms;">
              <div class="row d-flex justify-content-around align-items-center w-100">
                                            <div class="col-lg-6">
                                                <div class="row align-items-center">
                                                    <div class="col-6   flex-lg-fill"> <span
                                                            class="text-start text-2 float-start"><span
                                                                class="text-primary text-3-4">
                                    {{__('master.filtro_anti') }}
                                                        </span></span>
                                                    </div>
                                                    <div class="col-6  px-0 py-3 py-lg-0 selectorSpam">
                                                                <input type="number" id="numeroInput" value="1" min="1" max="200" />
                                <p class="text-start mb-0 d-none dlg-block">
                                                          @if (Session::get('language') == 'es')
                                    Escoge la cantidad que necesites de buzones de correo
                                    @else
                                   Choose the number of mailboxes you need
                                    @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 px-2  flex-fill">
                                                      <div class="row align-items-center d-flex justify-content-end">
                                                    <div class="col-auto">
                                                        <div class="input-group float-start float-lg-end">
                                                            <span class="input-group-text">$</span>
                                                           <input class="price addon text-4 m-0  border border-white form-control" type="text"
                                id="valorImpreso" readonly />
                                                            <span class="mt-2 mt-md-0"> <sup>*</sup>
                                                                <span
                                                                    class="text-primary font-weight-black">mes</span></span>
                                                        </div>
                                                    </div>
                                                     <div class="col-3 cerrarFiltro">
                <button type="button" class="btn btn-outline btn-primary btn-circle float-end cerrarRegistro"><i
                        class="fa fa-cart-plus fa-2x"></i>
                </button>
                
            </div>
                                                </div>

                                            </div>

                                        </div>
    </div>
</div>
                 
					</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
  
       
    </div>
  </div>
</div>

<!--certificado--->

<div class="modal fade" id="certificado" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel"> {{ __('comercio.certificado') }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
Siendo muy utilizados en los sitios de Internet que permiten comprar por medio de
              tarjeta de crédito o bien desean reforzar la seguridad entre el usuario y el sitio WEB.
            @else
Being widely used in Internet sites that allow you to buy by credit card or wish to
              reinforce the security between the user and the WEB site.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<!--pci--->

<div class="modal fade" id="pci" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">New message</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
              @if (Session::get('language') == 'es')
El servicio consiste en realizar escaneos al sitio web y/o servidor solicitado mostrando
              la lista de vulnerabilidades para que puedan ser corregidas. El escaneo tiene una validez de 3 meses.
            @else
The service consists of scanning the requested website and/or server, showing the list
              of vulnerabilities so that they can be corrected. The scan is valid for 3 months.
            @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary">{{ __('compra.comprar_ahora') }}</button>
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">{{ __('master.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

@section('scripts') 
 <script src="{{ URL::asset('assets/js/bootstrap-input-spinner.js') }}"></script>
 <script src="{{ URL::asset('assets/js/dominios.js') }}"></script>
<script>
     $(document).ready(function() {
    // Cuando el modal se muestra
    $('#spamfilter').on('shown.bs.modal', function (e) {
      // Selecciona el input de tipo número
  $("#numeroInput").on("input", function(){
    // Obtiene el valor del input de tipo número
    var valor = $(this).val();
    // Imprime el valor en el campo de texto
    $("#valorImpreso").val(valor);
  });
    });
         
          $(".cerrarRegistro").click(function() {
        $(this).toggleClass('btn-quaternary btn-outline btn-primary');


        $(this).children().toggleClass('fa-cart-plus fa-2x');
        $(this).children().toggleClass('fa-times');
        
        

    });
  });
</script>

@endsection