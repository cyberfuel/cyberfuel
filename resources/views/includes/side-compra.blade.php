<div class="sticky-lg-top padd-side-carrito pt-lg-5 m-auto">
	<div class="card card-border card-border-bottom bg-color-light">
		<div class="card-body">

			<table class="shop_table cart-totals mb-4">
				<thead>
					<tr class="border-bottom-2 border-color-primary ">
						<th colspan="2">
							<h4 class="font-weight-bold text-uppercase text-4 ">{{ __('compra.tu_compra')
								}}</h4>
						</th>
					</tr>
				</thead>
				<tbody>

					<tr class="cart-subtotal">
						<td class="border-top-0 py-0">
							<strong class="text-color-dark"><span class="text-primary"><i
										class="fas fa-info-circle"></i></span> <span class="text-primary"
									data-bs-toggle="tooltip" data-bs-placement="top" title=""
									data-bs-original-title="Cértificado de seguridad"><strong>SSL &nbsp; <span><input
												class="form-check-input" type="checkbox" role="switch"
												id="flexSwitchCheckChecked" checked=""></span></strong></span></strong>
						</td>
						<td class="border-top-0 py-0 text-end">
							<strong><span class="amount font-weight-medium">$0</span></strong>
						</td>
					</tr>



					<tr class="cart-subtotal">
						<td class="border-top-0 py-0">
							<strong class="text-color-dark">Subtotal</strong>
						</td>
						<td class="border-top-0 py-0 text-end">
							<strong><span class="amount font-weight-medium">$431</span></strong>
						</td>
					</tr>

					<tr class="total">
						<td>
							<strong class="text-color-dark text-3-5">Total</strong>
						</td>
						<td class="text-end">
							<strong class="text-color-dark"><span
									class="amount text-color-dark text-5">$432</span></strong>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">
							<a data-bs-toggle="modal" data-bs-target="#usuario" data-bs-toggle="tooltip"
								data-bs-placement="bottom" type="button"
								class="btn btn-success btn-sm  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-2 p-2 w-100">{{__('master.conti_carrito') }}<i class="fas fa-arrow-right ms-2"></i></a>
							{{-- @php
							$rutaDominio = request()->routeIs('nombres-de-dominios');
							$rutaHost = request()->routeIs('hospedaje-web');
							@endphp
							@if ($rutaDominio)
							<button id="btnCH" type="button"
								class="btn btn-success btn-sm  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-2 p-2 w-100">Seguir
								con la compra<i class="fas fa-arrow-right ms-2"></i></button>
							<button id="btnCH" type="button"
								class="btn btn-success btn-sm  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-2 p-2 w-100 mt-5">Comprar
								Hospedaje<i class="fas fa-arrow-right ms-2"></i></button>
							@elseif($rutaHost)
							<button href="{{ __('rutas.hospedaje-web') }}" id="btnCH" type="button"
								class="btn btn-success btn-sm  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-2 p-2 w-100">Seguir
								con la compra<i class="fas fa-arrow-right ms-2"></i></button>
							@else
							<button id="btnCH" type="button"
								class="btn btn-success btn-sm  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-2 p-2 w-100">no
								seguir
								con la compra<i class="fas fa-arrow-right ms-2"></i></button>
							@endif --}}

							{{-- @php
							$rutaDominio = request()->routeIs('nombres-de-dominios');
							$rutaHost = request()->routeIs('hospedaje-web');
							@endphp
							@if($rutaDominio)
							<h5>dominio</h5>
							@elseif($rutaHost)
							<h5>hospedaje</h5>
							@else
							<h5>nada</h5>
							@endif --}}


							{{-- <a id="btnAprobar" href="nuevo-cliente"
								class="btn btn-success btn-modern w-100 text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-3 py-3 d-none">{{
								__('compra.aprobar_servicios') }} <i class="fas fa-arrow-right ms-2"></i></a> --}}

						</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>
</div>