<!--
<div class="row py-5"><div class="col-6 col-md-2 position-relative"> <div class="position-absolute top-100 start-100 translate-middle"><img class="m-auto d-block" src="{{URL::asset('assets/imagenes/dominio.png')}}" alt="Cyberfuel"></div></div></div>-->


<div class="card-body">
  <div class="row justify-content-center align-items-center mt-4">
     
    <div class="col-lg-3 d-none d-lg-block">
      <div class=""><img class="m-auto d-block w-75" src="{{URL::asset('assets/imagenes/dominio.webp')}}"
          alt="Cyberfuel"></div>
    </div>
    <div class="col-lg-9">
        <div class="row justify-content-start">
        
         <div class="col-12 p-0">
              <div class="input-group ">
        <input type="text" id="nombre_dominio_pc" name="nombre_dominio_pc"
          class="form-control  text-center border-color-success w-50" placeholder="{{ __('compra.encuentra') }}"
          required="">
        <button onclick="consultar_dominio('{{ __('rutas.url_compra') }}', 'pc');" type="button"
          class="btn btn-success mostrardominio text-3 py-1"><i class="fas fa-search ms-1 "></i>
        </button>
      </div>
            </div>
                 <div class="col-12 p-0 pt-2 text-left">
<div class="btn-group bg-white" role="group" aria-label="">
  <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
  <label class="btn btn-outline-success rounded-0" for="btnradio1">{{__('master.regis') }}</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
  <label class="btn btn-outline-success rounded-0" for="btnradio2">{{__('master.transferir') }}</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
  <label class="btn btn-outline-success rounded-0" for="btnradio3">{{__('master.utilizar') }}</label>
</div>  
                </div>
               
        </div>
    
    </div>
  </div>
</div>

<div class="card-body py-0">
  {{-- aqui van las opciones que se despliegan despues de escoger el dominio --}}
  @include('includes.opcionesDominio')
  <div class="row">
    <div class="col px-0">
      <div class="form-group">
        <div class="row"> @if((isset($consulta_dominio)) && ($consulta_dominio != null))
          @if((isset($consulta_dominio["CODE"])) && ($consulta_dominio["CODE"] == 210))
          <div class="col-12">
            <div class="row pt-4">
              <div class="col-md-6">
                <h1 class="mb-0 font-weight-bold text-5 "> {{ __('compra.este_dominio_suyo') }} </h1>
                <p> <span class="text-primary font-weight-bold m-0 text-center text-7"> {{ $dominio }} </span> </p>
                <p class=" mb-3"> <span class="d-flex flex-column align-items-md-start font-weight-bold"> <span> <s
                        class="d-inline-block text-4"> $19.99 </s>&nbsp; <span
                        class="d-inline-block text-5 ml-2 mr-2 text-success"> @if((isset($registro_dominio["estado"]))
                        && ($registro_dominio["estado"] == "ok"))
                        @if((isset($registro_dominio["info"][0])) && ($registro_dominio["info"][0] != null))
                        &dollar;{{ number_format($registro_dominio["info"][0]["precio"], 2) }}
                        <input type="hidden" id="nombre_registro_dominio" name="nombre_registro_dominio"
                          value="{{ $dominio }}">
                        <input type="hidden" id="codigo_registro_dominio" name="codigo_registro_dominio"
                          value="{{ $registro_dominio[" info"][0]["codigo"] }}">
                        <input type="hidden" id="familia_registro_dominio" name="familia_registro_dominio"
                          value="{{ $registro_dominio[" info"][0]["familia"] }}">
                        <input type="hidden" id="titulo_registro_dominio" name="titulo_registro_dominio"
                          value="{{ $dominio }}">
                        <input type="hidden" id="precio_registro_dominio" name="precio_registro_dominio"
                          value="{{ $registro_dominio[" info"][0]["precio"] }}">
                        @endif
                        @endif </span> <span class="d-inline-block align-items-md-start text-muted">&nbsp;
                        {{ __('compra.durante_primer_anio') }} </span> </span> </span> </p>
              </div>
              <div class="col-md-6">
                <div class="text-3 mb-3">
                  <div class="text-3"> {{ __('compra.es_genial') }} </div>
                  <ul class="list list-icons list-primary">
                    @if(strlen($dominio) <= 15) <li> <i class="fas fa-check"></i> <b>{{ $dominio }}</b> {{
                      __('compra.menos_caracteres') }} </li>
                      @endif
                      <li> <i class="fas fa-check"></i> {{ __('compra.palabras_populares') }} </li>
                      <li> <i class="fas fa-check"></i> {{ __('compra.proteccion_privacidad') }} </li>
                  </ul>
                </div>
                <button onclick="agregar_registro_dominio('{{ $dominio }}', '1');" type="button"
                  class="btn btn-outline btn-rounded btn-primary btn-with-arrow mb-2"> {{ __('compra.agregar') }} <span>
                    <i class="fas fa-shopping-cart"></i> </span> </button>
              </div>
            </div>
          </div>
          @endif
          @else
          <div class="row pt-4"></div>
          @endif
          <div class="col-12"> @if((isset($consulta_dominio)) && ($consulta_dominio != null))
            @if((isset($consulta_dominio["CODE"])) && ($consulta_dominio["CODE"] == 211))
            <div class="row">
              <div class="col-auto my-2">
                <p> {{ __('compra.disculpas') }} <span class="text-primary font-weight-bold m-0 text-center h4"> {{
                    $dominio }} </span> {{ __('compra.no_se_encuentra') }} </p>
              </div>
            </div>
            @endif
            @endif
            <div class="row align-items-center">
              <div class="col pt-0">
                <div id="div_cargando_dominios" class="text-center" style="display:none;"> <img
                    src="{{URL::asset('assets/imagenes/cargando.gif')}}" width="100" height="100" /> </div>

                <!-- Cargar sugerencias de dominios -->
                <div id="div_sugerencias_dominios" role="main" class="main shop"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>





<div class="card-body">

  <div class="row mt-2">
    <div class="col">
      <div class="accordion" id="accordionDom">
        <div class="accordion-item">
          <h2 class="accordion-header" id="headingOne">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
              aria-expanded="true" aria-controls="collapseOne">
           @if (Session::get('language') == 'es')
            Información de como seleccionar el nombre de dominio :
                @else
            Information on how to select the domain name:
                @endif
            </button>
          </h2>
          <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
            data-bs-parent="#accordionDom" style="">
            <div class="accordion-body">
              @if (Session::get('language') == 'es')
              <div class="row">
                <div class="row">
                  <div class="col-12 col-md-6 offset-md-3">
                    <div class="row">
                      <div class="col">
                        <div class="call-to-action-content">
                          <h3><strong><a type="button" data-bs-toggle="popover" data-bs-trigger="hover focus"
                                title="{{__('compra.dominio')}} .com" data-bs-content="{{__('compra.com')}}"><img
                                  class="img-fluid" src="{{URL::asset('assets/imagenes/dominios/com.webp')}}"
                                  alt=""></a></strong></h3>
                        </div>
                      </div>
                      <div class="col">
                        <div class="call-to-action-content">
                          <h3><strong><a type="button" data-bs-toggle="popover" data-bs-trigger="hover focus"
                                title="{{__('compra.dominio')}} .net" data-bs-content="{{__('compra.net')}}"><img
                                  class="img-fluid" src="{{URL::asset('assets/imagenes/dominios/net.webp')}}"
                                  alt=""></a></strong></h3>
                        </div>
                      </div>
                      <div class="col">
                        <div class="call-to-action-content">
                          <h3><strong><a type="button" data-bs-toggle="popover" data-bs-trigger="hover focus"
                                title="{{__('compra.dominio')}} .org" data-bs-content="{{__('compra.org')}}"><img
                                  class="img-fluid" src="{{URL::asset('assets/imagenes/dominios/org.webp')}}"
                                  alt=""></a></strong></h3>
                        </div>
                      </div>
                      <div class="col">
                        <div class="call-to-action-content">
                          <h3><strong><a type="button" data-bs-toggle="popover" data-bs-trigger="hover focus"
                                title="{{__('compra.dominio')}} .biz" data-bs-content="{{__('compra.biz')}}"><img
                                  class="img-fluid" src="{{URL::asset('assets/imagenes/dominios/biz.webp')}}"
                                  alt=""></a></strong></h3>
                        </div>
                      </div>
                      <div class="col">
                        <div class="call-to-action-content">
                          <h3><strong><a type="button" data-bs-toggle="popover" data-bs-trigger="hover focus"
                                title="{{__('compra.dominio')}} .info" data-bs-content="{{__('compra.info')}}"><img
                                  class="img-fluid" src="{{URL::asset('assets/imagenes/dominios/info.webp')}}"
                                  alt=""></a></strong></h3>
                        </div>
                      </div>
                      <div class="col">
                        <div class="call-to-action-content">
                          <h3><strong><a type="button" data-bs-toggle="popover" data-bs-trigger="hover focus"
                                title="{{__('compra.dominio')}} .name" data-bs-content="{{__('compra.name')}}"><img
                                  class="img-fluid" src="{{URL::asset('assets/imagenes/dominios/name.webp')}}"
                                  alt=""></a></strong></h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <ul class="list list-icons list-icons-style-2 text-justify">
                    <li><i class="fas fa-check"></i>1. Lo ideal es que refleje la marca o el servicio/producto
                      que brinda.</li>
                    <li><i class="fas fa-check"></i> 2. Fácil de decirlo y recordarlo</li>
                    <li><i class="fas fa-check"></i> 3. Tiene un largo de 1 a 63 caracteres, intente no utilizar
                      caracteres especiales como guiones “-”, que son difíciles de decir verbalmente</li>
                    <li><i class="fas fa-check"></i> 4. Puede hacer todas las combinaciones posibles y si está
                      localizado en un país específico puede utilizar el dominio del país o bien agregarlo antes de la
                      extensión. Ejemplo si estuviera en Costa Rica, podría usar midominiocr.com</li>


                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list list-icons list-icons-style-2 text-justify">
                    <li><i class="fas fa-check"></i> 5. Seleccione la extensión (TLD) que más se adapte a su negocio y
                      vaya de acuerdo con su idea. Ahora si es de un país específico también puedes utilizar una del
                      país. Como ejemplo .CR para Costa Rica o .PA para Panamá.</li>
                    <li><i class="fas fa-check"></i> 6. Recuerde que necesita de un <a class="text-success"
                        href="{{ __('rutas.url_hospedaje') }}">hospedaje WEB </a>p para tener su sitio web y correos
                      electrónicos.</li>
                  </ul>
                </div>
              </div>
              @else
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list list-icons list-icons-style-2 text-justify">


                    <li><i class="fas fa-check"></i> 1. Ideally, it should reflect the brand or the service/product it
                      provides.</li>
                    <li><i class="fas fa-check"></i> 2. Easy to say and remember</li>
                    <li><i class="fas fa-check"></i> 3. It is between 1 and 63 characters long, try not to use special
                      characters like hyphens “-”, which are difficult to say verbally</li>
                    <li><i class="fas fa-check"></i> 4. You can do all possible combinations and if you are located in a
                      specific country you can use the country's domain or add it before the extension. Example if you
                      were in Costa Rica, you could use midominiocr.com</li>

                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list list-icons list-icons-style-2 text-justify">
                    <li><i class="fas fa-check"></i> 5. Select the extension (TLD) that best suits your business and
                      goes according to your idea. Now if it is from a specific country you can also use one from the
                      country. As an example .CR for Costa Rica or .PA for Panama.</li>
                    <li><i class="fas fa-check"></i>6. Remember that you need a <a class="text-success"
                        href="{{ __('rutas.url_hospedaje') }}">WEB hosting </a> to have your website and emails.</li>

                  </ul>
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</div>
@section('scripts')

<script src="{{ URL::asset('assets/js/view.shop.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap-input-spinner.js') }}"></script>
<script src="{{ URL::asset('assets/js/dominios.js') }}"></script>
<script>
window.addEventListener("load", function () {
  var barra = document.querySelector("#footer-fix");
  var contenidoPrincipal = document.querySelector("#footer");
  var alturaBarra = barra.offsetHeight;

  contenidoPrincipal.style.marginBottom = alturaBarra + "px";
});
</script>


@endsection