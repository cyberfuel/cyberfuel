

   <div class="row align-items-center">

     	  <div class="col-4">
    <form class="w-100 " action="#" method="get">
      <div class="input-group ">
        <div class="search-form-wrapper input-group">
          <input class="form-control text-1 w-50" id="headerSearch" name="q" type="search" value="" placeholder="{{ __('compra.buscar') }}...">
          <button type="submit" class="btn btn-primary text-1 p-2"><i class="fas fa-search m-2"></i></button>
        </div>
      </div>
    </form>
	  </div>
 <!-- destock menu-->
		<div class="col-8 d-none d-lg-block">
         
       	<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
						<li class="nav-item active" data-option-value="*"><a class="nav-link text-2-5 text-uppercase active" href="#">{{ __('compra.todo') }}</a></li>
						<li class="nav-item" data-option-value=".servidores"><a class="nav-link text-2-5 text-uppercase" href="#">{{ __('compra.servidores') }}</a></li>
						<li class="nav-item" data-option-value=".admin"><a class="nav-link text-2-5 text-uppercase" href="#">{{ __('compra.servicios') }}</a></li>
						<li class="nav-item" data-option-value=".seguridad"><a class="nav-link text-2-5 text-uppercase" href="#">{{ __('compra.seguridad') }}</a></li>
						
					</ul>
             
      </div>
  </div>
