<header id="header" class="header-effect-shrink bg-color-white custom-header"
  data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true,   'stickyStartAt': 100, 'stickyHeaderContainerHeight': 83}">
  <div class="header-body border-0 box-shadow-none">
    <div class="header-top header-top-borders d-lg-none">
      <div class="container h-100">
        <div class="header-row h-100">
          <div class="header-column justify-content-end">
            <div class="header-row">
              <nav class="header-nav-top">
                <ul class="nav nav-pills">
                  <li class="list-inline-item me-0 mb-0 mt-3">
                    <a data-bs-toggle="modal" data-bs-target="#usuario" data-bs-toggle="tooltip"
                      data-bs-placement="bottom" data-bs-original-title="Ingreso | Registro" href="#"
                      class="text-quaternary text-color-hover-primary text-decoration-none"> <svg
                        xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                        class="bi bi-person-bounding-box" viewBox="0 0 20 20">
                        <path
                          d="M1.5 1a.5.5 0 0 0-.5.5v3a.5.5 0 0 1-1 0v-3A1.5 1.5 0 0 1 1.5 0h3a.5.5 0 0 1 0 1zM11 .5a.5.5 0 0 1 .5-.5h3A1.5 1.5 0 0 1 16 1.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 1-.5-.5M.5 11a.5.5 0 0 1 .5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 1 0 1h-3A1.5 1.5 0 0 1 0 14.5v-3a.5.5 0 0 1 .5-.5m15 0a.5.5 0 0 1 .5.5v3a1.5 1.5 0 0 1-1.5 1.5h-3a.5.5 0 0 1 0-1h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 1 .5-.5" />
                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0" />
                      </svg> {{ __('compra.ini_o_regis') }}</a>
                  </li>
                  <li class="nav-item"></li>
                </ul>
                <ul class="lista-carrito">
                  <li class="pt-4 mr-4"> <a href="{{ __('rutas.url_carrito-de-compra') }}" class=" sidecar">
                  <?php /*?>    @if((Session::has("cyberfuel_carrito_compras")) && (Session::get("cyberfuel_carrito_compras") !=
                      null)) <span id="span_total_carrito_compras"
                        class="position-absolute top-50 start-100 translate-middle badge rounded-pill bg-danger"> @php
                        $carrito_compras = Session::get("cyberfuel_carrito_compras");
                        if((isset($carrito_compras)) && ($carrito_compras != null) && is_array($carrito_compras)){
                        $total_productos_carrito_compras = count($carrito_compras);
                        if($total_productos_carrito_compras > 0){
                        echo $total_productos_carrito_compras;
                        }
                        }
                        @endphp </span> @endif<?php */?>

                      <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                        class="bi bi-basket3-fill" viewBox="0 0 20 20">
                        <path
                          d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15.5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1A.5.5 0 0 1 .5 6h1.717L5.07 1.243a.5.5 0 0 1 .686-.172zM2.468 15.426.943 9h14.114l-1.525 6.426a.75.75 0 0 1-.729.574H3.197a.75.75 0 0 1-.73-.574z" />
                      </svg>

                      <span id="span_total_carrito_compras" class="cart-qty badge badge-dark badge-md rounded-pill text-uppercase px-2 py-1">4</span> <span>&nbsp;</span> </a></li>
                </ul>
              </nav>
            </div>
          </div>

        </div>
      </div>
    </div>

    {{-- desctock --}}
    <div class="header-container container">
      <div class="header-row py-2">
        <div class="header-column">
          <div class="header-row">
            <div class="header-logo "> <a href="{{ url('/') }}"> <img alt="Cyberfuel" width="230" height="55"
                  data-sticky-width="0" data-sticky-height="0" data-sticky-top="0"
                  src="{{URL::asset('assets/imagenes/logo-cyber.webp')}}"> </a> </div>
          </div>
        </div>
        <div class="header-column justify-content-end">
          <div class="header-row">
            <ul class="nav nav-pills d-none d-lg-block">
              <li class="list-inline-item me-0 mb-0 mt-3">
                <a data-bs-toggle="modal" data-bs-target="#usuario" data-bs-toggle="tooltip" data-bs-placement="bottom"
                  data-bs-original-title="Ingreso | Registro" href="#"
                  class="text-quaternary text-color-hover-primary text-decoration-none">

                  <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                    class="bi bi-person-bounding-box" viewBox="0 0 20 20">
                    <path
                      d="M1.5 1a.5.5 0 0 0-.5.5v3a.5.5 0 0 1-1 0v-3A1.5 1.5 0 0 1 1.5 0h3a.5.5 0 0 1 0 1zM11 .5a.5.5 0 0 1 .5-.5h3A1.5 1.5 0 0 1 16 1.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 1-.5-.5M.5 11a.5.5 0 0 1 .5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 1 0 1h-3A1.5 1.5 0 0 1 0 14.5v-3a.5.5 0 0 1 .5-.5m15 0a.5.5 0 0 1 .5.5v3a1.5 1.5 0 0 1-1.5 1.5h-3a.5.5 0 0 1 0-1h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 1 .5-.5" />
                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0" />
                  </svg>

                  {{ __('compra.ini_o_regis') }}</a>
              </li>
              <li class="nav-item"></li>
            </ul>
            <ul class="lista-carrito d-none d-lg-block">
              <li class="pt-4 mr-4"> <a href="{{ __('rutas.url_carrito-de-compra') }}" class=" sidecar">
                <?php /*?>  @if((Session::has("cyberfuel_carrito_compras")) && (Session::get("cyberfuel_carrito_compras") !=
                  null)) <span id="span_total_carrito_compras"
                    class="position-absolute top-50 start-100 translate-middle badge rounded-pill bg-danger"> @php
                    $carrito_compras = Session::get("cyberfuel_carrito_compras");
                    if((isset($carrito_compras)) && ($carrito_compras != null) && is_array($carrito_compras)){
                    $total_productos_carrito_compras = count($carrito_compras);
                    if($total_productos_carrito_compras > 0){
                    echo $total_productos_carrito_compras;
                    }
                    }
                    @endphp </span> @endif<?php */?> <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                    fill="currentColor" class="bi bi-basket3-fill" viewBox="0 0 20 20">
                    <path
                      d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15.5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H.5a.5.5 0 0 1-.5-.5v-1A.5.5 0 0 1 .5 6h1.717L5.07 1.243a.5.5 0 0 1 .686-.172zM2.468 15.426.943 9h14.114l-1.525 6.426a.75.75 0 0 1-.729.574H3.197a.75.75 0 0 1-.73-.574z" />
                  </svg> <span id="span_total_carrito_compras" class="cart-qty badge badge-dark badge-md rounded-pill text-uppercase px-2 py-1">4</span> <span>&nbsp;</span> </a></li>
            </ul>
            <ul class="header-extra-info d-flex align-items-center">  
              <li>
                <button class="btn header-btn-collapse-nav" data-bs-toggle="collapse"
                  data-bs-target=".header-nav-main nav"> Menú <i class="fas fa-bars"></i> </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav-bar bg-color-light-scale-1 z-index-0" data-sticky-header-style="{'minResolution': 991}"
      data-sticky-header-style-active="{'background-color': 'transparent'}"
      data-sticky-header-style-deactive="{'background-color': '#f7f7f7'}">
      <div class="container">
        <div class="header-row">
          <div class="header-column">
            <div class="header-row ">
              <div class="header-nav header-nav-stripe justify-content-start">
                <div
                  class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
                  <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav" data-sticky-header-style="{'minResolution': 991}"
                      data-sticky-header-style-active="{'margin-left': '0px'}"
                      data-sticky-header-style-deactive="{'margin-left': '0'}">
                      <li class="dropdown"> <a class=" dropdown-item px-2 " href="{{ url('/') }}"> {{
                          __('master.inicio') }} </a> </li>
                      <li class="dropdown"> <a class="dropdown-item px-2" href="{{ __('rutas.url_dominio') }}"> {{
                          __('master.nombre_dominio') }} </a> </li>
                      <li class="dropdown"> <a class=" dropdown-item px-2 " href="{{ __('rutas.url_hospedaje') }}"> {{
                          __('master.hospedaje_web') }} </a> </li>
                      {{-- <li class="dropdown"> <a class="dropdown-item px-2"
                          href="{{ __('rutas.url_servicios_nube') }}">
                          {{ __('master.servicios_nube') }}</a> </li> --}}
                      <li class="dropdown"> <a class="dropdown-item dropdown-toggle px-2">{{ __('master.servicios_nube')
                          }} </a>
                        <ul class="dropdown-menu">
                          
                          <li> <a class="dropdown-item" href="{{ __('rutas.url_baas') }}"> {{ __('compra.ser_baas') }} </a> </li>
                          <li> <a class="dropdown-item" href="{{ __('rutas.url_servidores') }}"> {{
                              __('master.servidores') }} </a> </li>


                          <li><a class="dropdown-item" href="{{ __('rutas.url_seguridad') }}">{{ __('master.seguridad')
                              }} </a></li>
                        </ul>
                      </li>
                      <li class="dropdown"> <a class="dropdown-item px-2" href="{{ __('rutas.url_centro') }}">{{
                          __('master.centro_datos') }}</a> </li>
                      <li class="dropdown product"> <a href="#productos" class="dropdown-item dropdown-toggle px-2">{{
                          __('master.productos') }}</a>
                        <ul class="dropdown-menu">
                          <li> <a target="_blank" class="dropdown-item"
                              href="{{ __('https://www.facturaprofesional.com/') }}"> Factura Profesional </a> </li>
                          <li> <a target="_blank" class="dropdown-item"
                              href="{{ __('https://www.atencionprofesional.com/') }}"> Atención Profesional </a> </li>
                          <li> <a target="_blank" class="dropdown-item"
                              href="{{ __('https://www.comprobanteselectronicoscr.com/') }}"> Comprobantes
                              Electrónicos</a> </li>
                          <li> <a target="_blank" class="dropdown-item"
                              href="{{ __('http://www.smsempresarial.com/') }}"> SMS Empresarial </a> </li>
                       
                          <li> <a target="_blank" class="dropdown-item"
                              href="{{ __('https://www.factorajeelectronico.com/') }}"> Factoraje Electrónico </a> </li>

                        </ul>
                      </li>
                      <li class="dropdown market"> <a class="dropdown-item px-2 text-white"
                          href="{{ __('rutas.url_mercado') }}">Marketplace</a> </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>