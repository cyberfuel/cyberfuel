<div role="main" class="main shop">
  <div class="p-1">
    <div class="row">
      <div class="col px-0">
        <div class="alert alert-primary mt-4">
          <strong>{{ __('compra.elige_plan_hospedaje') }}</strong>
        </div>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-12">
        <div class="tabs">
          <ul class="nav nav-tabs nav-justified flex-column flex-md-row" id="myTab">
            <li class="nav-item"> <a class="nav-link active  text-3 h-100 px-md-0" href="#tabsCorreo" aria-controls="tabsCorreo"
                data-bs-toggle="tab" aria-selected="true">{{ __('compra.plan_correo') }}</a> </li>
            <li class="nav-item"> <a class="nav-link  text-3 h-100 px-md-0" href="#tabsPersonal" aria-controls="tabsPersonal" data-bs-toggle="tab" aria-selected="false">{{
                __('compra.plan_personal') }}</a> </li>
            <li class="nav-item"> <a class="nav-link text-3 h-100 px-md-0" href="#tabsPymes" aria-controls="tabsPymes" data-bs-toggle="tab" aria-selected="false">{{
                __('compra.plan_pymes') }}</a> </li>
            <li class="nav-item"> <a class="nav-link text-3 h-100 px-md-0" href="#tabsEmpresarial" aria-controls="tabsEmpresarial"
                data-bs-toggle="tab" aria-selected="false">{{ __('compra.plan_empresarial') }}</a> </li>
            <li class="nav-item"> <a class="nav-link  text-3 h-100 px-md-0" href="#tabsComercio" aria-controls="tabsComercio" data-bs-toggle="tab" aria-selected="false">{{
                __('compra.comercio') }}</a> </li>
          </ul>
            


          <div class="tab-content">
            <div class="tab-pane  active" id="tabsCorreo">
              <div class="row">
                <div class="col">
                    <div class="pricing-block border rounded">
                  <div class="row">
                  <div class="col-lg-9">
                      <div class="row">
                        <div class="col-12 pt-2 pt-md-0">
                          <h4 class="card-title  text-5 font-weight-bold">{{ __('compra.plan_correo') }}</h4>
                        </div>
                      </div>
                    
                           <div class="row">
                        <div class="col-lg-6">
                            @if (Session::get('language') == 'es')
                  <ul class="list list-primary mb-0">
                        <li> 1 Dominio internacional
                          (.com, .net, .org)</li>
                        <li> Una cuenta de reenvío</li>
                        <li> Una cuenta con buzón de 1 GB de espacio en disco</li>
                  
                      </ul>
                      @else
                    <ul class="list list-primary mb-0">
                        <li> 1 Internacional domain br (.com, .net, .org)</li>
                        <li> A forwarding account</li>
                        <li> An account with a mailbox with 1 GB of disk space</li>
                       
                      </ul>
                      @endif
                         </div>
                               
                                       <div class="col-lg-6">
                            @if (Session::get('language') == 'es')
                  <ul class="list list-primary mb-0">
                        <li> 1 Dominio internacional
                          (.com, .net, .org)</li>
                    
                        <li>Respaldo diario</li>
                        <li>Soporte Técnico 24/7</li>
                      </ul>
                      @else
                    <ul class="list list-primary mb-0">
                      
                        <li> Transfer 10 GB</li>
                        <li> Daily backup</li>
                        <li> Technical Support 24/7</li>
                      </ul>
                      @endif
                         </div>
                      </div>
                    </div>
             <div class="col-lg-3">
                            <h4 class="font-weight-bold text-uppercase"> {{__('comercio.precio') }} </h4>
                            <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                            </div>
                             <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                               <span class="price text-color-tertiary"><span class="price-unit">$</span> 30.00</span>
                              <label class="price-label text-uppercase text-color-tertiary">{{__('master.anual') }} *</label>
                            </div>
                             <!-- href=" __('rutas.url_compra') " -->
                            <a onclick=" mostrar_cd(); next();"
                              class="mb-1 mt-1 me-1 btn btn-outline btn-primary float-end"> {{
                              __('compra.agregar') }}<span><i class="fas fa-shopping-cart"></i></span> </a>

                 </div> 
         
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tabsPersonal">
              <div class="row">
                <div class="col">
                    <div class="pricing-block border rounded">
                  <div class="row">
                   <div class="col-lg-9">
                      <div class="row">
                        <div class="col-12 pt-2 pt-md-0">
                          <h4 class="card-title  text-5 font-weight-bold">{{ __('compra.plan_personal') }}</h4>
                        </div>
                      </div>
                    
                      
                           <div class="row">
                        <div class="col-lg-6">
                          @if (Session::get('language') == 'es')
                      <ul class="list list-primary mb-0">
                        <li> 3 espacio para dominios</li>
                        <li>5 GB de espacio</li>
                        <li>10 buzones de Correo </li>
                        <li>1 base datos MySQL</li>
                   
                      </ul>
                      @else
                  <ul class="list list-primary mb-0">
                        <li>Hosting for 3 domains</li>
                        <li>5 GB of disk space</li>
                        <li>10 Mailbox </li>
                        <li>1 MySQL data base</li>
                      
                      </ul>
                      @endif
                         </div>
                               
                        <div class="col-lg-6">
                          @if (Session::get('language') == 'es')
                      <ul class="list list-primary mb-0">
           
                        <li>Soporte para programación PHP</li>
                        <li>Respaldo diario</li>
                        <li> Soporte Técnico 24/7</li>
                      </ul>
                      @else
                  <ul class="list list-primary mb-0">
                    
                        <li>PHP support</li>
                        <li>Daily backup</li>
                        <li>Tech Support 24x7</li>
                      </ul>
                      @endif
                         </div>
                      </div>
                    </div>
                  <div class="col-lg-3">
                            <h4 class="font-weight-bold text-uppercase"> {{__('comercio.precio') }} </h4>
                            <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                            </div>
                             <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                               <span class="price text-color-tertiary"><span class="price-unit">$</span> 64.99</span>
                              <label class="price-label text-uppercase text-color-tertiary">{{__('master.anual') }} *</label>
                            </div>
                             <!-- href=" __('rutas.url_compra') " -->
                            <a onclick="mostrar_cd(); next()"
                              class="mb-1 mt-1 me-1 btn btn-outline btn-primary float-end"> {{
                              __('compra.agregar') }}<span><i class="fas fa-shopping-cart"></i></span> </a>

                 </div>
                  
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tabsPymes">
              <div class="row">
                <div class="col">
                    <div class="pricing-block border rounded">
                  <div class="row">
                   <div class="col-lg-9">
                      <div class="row">
                        <div class="col-12 pt-2 pt-md-0">
                          <h4 class="card-title  text-5 font-weight-bold">{{ __('compra.plan_pymes') }}</h4>
                        </div>
                      </div>
                     
                         <div class="row">
                        <div class="col-lg-6">
                          @if(Session::get('language') == 'es')
                       <ul class="list list-primary mb-0">
                        <li>6 espacio para dominios</li>
                        <li>10 GB de espacio</li>
                        <li>60 buzones de Correo </li>
                        <li>10 base datos MySQL</li>
                    
                      </ul>
                      @else
                      <ul class="list list-primary mb-0">
                        <li> Hosting for 6 domains</li>
                        <li> 10 GB of disk space</li>
                        <li> 60 Mailbox</li>
                        <li> 10 MySQL data base</li>
                    
                      </ul>
                      @endif
                         </div>
                             
                                 <div class="col-lg-6">
                          @if(Session::get('language') == 'es')
                       <ul class="list list-primary mb-0">
               
                        <li>Soporte para programación PHP</li>
                        <li>Respaldo diario</li>
                        <li>Soporte Técnico 24/7</li>
                      </ul>
                      @else
                      <ul class="list list-primary mb-0">
              
                        <li> PHP support</li>
                        <li> Daily backup</li>
                        <li> Tech Support 24x7</li>
                      </ul>
                      @endif
                         </div>
                      </div>
                    </div>
             <div class="col-lg-3">
                            <h4 class="font-weight-bold text-uppercase"> {{__('comercio.precio') }} </h4>
                            <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                            </div>
                             <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                               <span class="price text-color-tertiary"><span class="price-unit">$</span> 129.99</span>
                              <label class="price-label text-uppercase text-color-tertiary">{{__('master.anual') }} *</label>
                            </div>
                                <!-- href=" __('rutas.url_compra') " -->
                            <a onclick=" mostrar_cd(); next();"
                              class="mb-1 mt-1 me-1 btn btn-outline btn-primary float-end"> {{
                              __('compra.agregar') }}<span><i class="fas fa-shopping-cart"></i></span> </a>

                 </div>
            
                  </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tabsEmpresarial">
              <div class="row">
                <div class="col">
                    <div class="pricing-block border rounded">
                  <div class="row">
                 <div class="col-lg-9">
                      <div class="row">
                        <div class="col-12 pt-2 pt-md-0">
                          <h4 class="card-title  text-5 font-weight-bold">{{ __('compra.plan_empresarial') }}</h4>
                           
                        </div>
                      </div>
                  
                      <div class="row">
                        <div class="col-lg-6">
                          
                             @if (Session::get('language') == 'es')
                       <ul class="list list-primary mb-0">
                        <li>10 espacio para dominios</li>
                        <li>20 GB de espacio</li>
                        <li>120 buzones de Correo </li>
                        <li>20 base datos MySQL</li>
                  
                      </ul>
                      @else
                      <ul class="list list-primary mb-0">
                        <li> Hosting for 10 domains</li>
                        <li> 20 GB of disk space</li>
                        <li> 120 Mailbox</li>
                        <li> 20 MySQL data base</li>
                     
                      </ul>
                      @endif
                           
                         </div>       
                          <div class="col-lg-6">
                       
                             @if (Session::get('language') == 'es')
                      <ul class="list list-primary mb-0">
                        <li>Soporte para programación PHP</li>
                        <li>Respaldo diario</li>
                        <li>Soporte Técnico 24/7</li>
                      </ul>
                      @else
                       <ul class="list list-primary mb-0">
                        <li> PHP support</li>
                        <li> Daily backup</li>
                        <li> Tech Support 24x7</li>
                      </ul>
                      @endif
                           
                         </div>
                      </div>
                    </div>

                    
                    <div class="col-lg-3">
                            <h4 class="font-weight-bold text-uppercase"> {{__('comercio.precio') }} </h4>
                            <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                              
                            </div>
                              
                              
                                   <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                               <span class="price text-color-tertiary"><span class="price-unit">$</span> 299.99</span>
                              <label class="price-label text-uppercase text-color-tertiary">{{__('master.anual') }} *</label>
                             
                        
                            </div>

                         <a onclick="mostrar_cd(); next();"
                              class="mb-1 mt-1 me-1 btn btn-outline btn-primary float-end"> {{
                              __('compra.agregar') }}<span><i class="fas fa-shopping-cart"></i></span> </a>

                          </div>
             
                  </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tabsComercio">
              <div class="row">
                <div class="col">
                  <div class="row">
                    <div class="col">
                      <div class="pricing-block border rounded">
                        <div class="row">
                          <div class="col-lg-9">
                            <h4 class="card-title  text-5 font-weight-bold"> {{ __('comercio.adicional') }} </h4>
                           
                            <div class="row">
                              <div class="col-lg-6">
                                <ul class="list  list-primary mb-0">
                                  <li>{{ __('comercio.dominio_pro') }} (.com o .net) </li>
                                  <li> {{ __('comercio.hospedaje_web') }}</li>
                                  <li class="mb-lg-0"><a class="text-primary" href="" data-bs-toggle="modal"
                                      data-bs-target="#solucion">WooCommerce</a></li>
                                  <li class="mb-lg-0"><a class="text-primary" href="" data-bs-toggle="modal"
                                      data-bs-target="#componente">{{ __('comercio.ins_woo') }}</a></li>
                                  <li> (a) {{ __('comercio.integra_factura') }} </li>
                                  <li>(b) {{ __('comercio.perso') }} </li>
                                </ul>
                              </div>
                              <div class="col-lg-6">
                                <ul class="list list-primary mb-0">

                                  <li class="mb-lg-0">(c) {{ __('comercio.integra_bancos') }}</li>
                                  <li>{{ __('comercio.capa_inicial') }}</i></li>
                                  <li> {{ __('comercio.acceso') }}</li>
                                  <li>{{ __('comercio.asistencia') }}</li>
                                  <li class="mb-0"> {{ __('comercio.actuali') }}</li>
                                </ul>
                              </div>

                            </div>
                          </div>
                          <div class="col-lg-3">
                            <h4 class="font-weight-bold text-uppercase"> {{ __('comercio.precio') }} </h4>
                            <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                              {{-- @if((isset($planes_hospedaje["comercio_electronico"]["WH-LPEC"])) &&
                              ($planes_hospedaje["comercio_electronico"]["WH-LPEC"] != null)) <span class="price text-color-tertiary"><span
                                  class="price-unit">&dollar;</span>{{
                                number_format($planes_hospedaje["comercio_electronico"]["WH-LPEC"]["precio"], 0)
                                }}</span>
                              <label class="price-label text-uppercase text-color-tertiary">{{ __('comercio.por_mes') }} *</label>
                              <input type="hidden" id="codigo_comercio_electronico" name="codigo_comercio_electronico"
                                value="{{ $planes_hospedaje[" comercio_electronico"]["WH-LPEC"]["codigo"] }}">
                              <input type="hidden" id="familia_comercio_electronico" name="familia_comercio_electronico"
                                value="{{ $planes_hospedaje[" comercio_electronico"]["WH-LPEC"]["familia"] }}">
                              <input type="hidden" id="titulo_comercio_electronico" name="titulo_comercio_electronico"
                                value="{{ $planes_hospedaje[" comercio_electronico"]["WH-LPEC"]["titulo_web"] }}">
                              <input type="hidden" id="precio_comercio_electronico" name="precio_comercio_electronico"
                                value="{{ $planes_hospedaje[" comercio_electronico"]["WH-LPEC"]["precio"] }}">
                              @endif --}}
                            </div>
                              
                              
                                   <div class="plan-price mb-4">
                              <!-- PRECIO COMERCIO ELECTRONICO -->
                               <span class="price text-color-tertiary"><span
                                  class="price-unit">&dollar;</span> 64.99</span>
                              <label class="price-label text-uppercase text-color-tertiary">{{ __('comercio.por_mes') }} *</label>
                             {{--   <input type="hidden" id="codigo_comercio_electronico" name="codigo_comercio_electronico"
                                value="">
                              <input type="hidden" id="familia_comercio_electronico" name="familia_comercio_electronico"
                                value="">
                              <input type="hidden" id="titulo_comercio_electronico" name="titulo_comercio_electronico"
                                value="">
                              <input type="hidden" id="precio_comercio_electronico" name="precio_comercio_electronico"
                                value="">--}}
                        
                            </div>

                            <!-- href=" __('rutas.url_compra') " -->
                            <a onclick="mostrar_cd(); next();"
                              class="mb-1 mt-1 me-1 btn btn-outline btn-primary float-end"> {{
                              __('compra.agregar') }}<span><i class="fas fa-shopping-cart"></i></span> </a>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>  
              </div>

     
            </div>
          </div>
        </div>
      </div>
    

<div class="col-12"><div class="row g-0">
    <div class="home-intro light  border border-0 mb-0 py-4">
					<div class="container">

						<div class="row align-items-center">
							<div class="col-lg-8">
								<p class="font-weight-bold text-color-dark">
								
                                    
                                    
                             @if (Session::get('language') == 'es')
                   	Tabla comparativa de  <span class="highlighted-word highlighted-word-animation-1 text-color-primary font-weight-semibold text-5">hospedajes web</span>
									<span>Si tienes dudas aqui puedes ver las caracteristicas de cada uno.</span>
                      @else
                      Comparative table of <span class="highlighted-word highlighted-word-animation-1 text-color-primary font-weight-semibold text-5">Web Hosting</span>
									<span>If you have doubts here you can see the characteristics of each one.</span>
                      @endif
								</p>
							</div>
							<div class="col-lg-4">
								<div class="get-started text-start text-lg-end">
									<button data-bs-toggle="modal"
                        data-bs-target="#hospedaje" class="btn btn-primary btn-lg text-3 font-weight-semibold btn-py-2 px-4">  {{ __('compra.ver_mas_planes') }}</button>
								
								</div>
							</div>
						</div>

					</div>
</div>
    </div></div>
    </div>
  </div>

</div>

<div class="modal fade" id="solucion" tabindex="-1" role="dialog" aria-labelledby="solucionWoo" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="solucionWoo">{{ __('comercio.solucion') }}:</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        @if (Session::get('language') == 'es')
        <div class="row">
          <div class="col-12">
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i> Artículos
                <ul>
                  <li><i class="fas fa-caret-right"></i>Simples o con Atributos</li>
                  <li><i class="fas fa-caret-right"></i>Virtuales/Servicios</li>
                  <li><i class="fas fa-caret-right"></i>Descargables</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Categorías de artículos</li>
              <li><i class="fas fa-check"></i> Inventario</li>
              <li><i class="fas fa-check"></i> Mostrar tus productos con un catálogo online, <br>
                donde pueden hacer pedidos y solicitar presupuestos</li>
              <li><i class="fas fa-check"></i> Venta en línea
                <ul>
                  <li><i class="fas fa-caret-right"></i>Limitando por países</li>
                  <li><i class="fas fa-caret-right"></i>productos de afiliados de otras tiendas</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Carrito de Compras
              <li><i class="fas fa-check"></i> Órdenes y sus estados
              <li><i class="fas fa-check"></i> Informes
                <ul>
                  <li><i class="fas fa-caret-right"></i> Pedidos</li>
                  <li><i class="fas fa-caret-right"></i> Ventas</li>
                  <li><i class="fas fa-caret-right"></i> Clientes</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Configuración de métodos de transporte</li>
              <li><i class="fas fa-check"></i> Posicionamiento en buscadores “SEO”</li>
              <li><i class="fas fa-check"></i> Y muchas otras cosas más</li>
            </ul>
          </div>
        </div>
        @else
        <div class="row">
          <div class="col-12">
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i> Articles
                <ul>
                  <li><i class="fas fa-caret-right"></i> Simple or with Attributes</li>
                  <li><i class="fas fa-caret-right"></i> Virtual / Services</li>
                  <li><i class="fas fa-caret-right"></i>Downloadable</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Item categories</li>
              <li><i class="fas fa-check"></i> Inventory</li>
              <li><i class="fas fa-check"></i> Show your products with an online catalog,
                where they can order and request quotes</li>
              <li><i class="fas fa-check"></i> Online sale
                <ul>
                  <li><i class="fas fa-caret-right"></i>Limiting by countries</li>
                  <li><i class="fas fa-caret-right"></i>Affiliate products from other store</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Shopping Cart
              <li><i class="fas fa-check"></i> Orders and their statuses
              <li><i class="fas fa-check"></i> Reports
                <ul>
                  <li><i class="fas fa-caret-right"></i> Orders</li>
                  <li><i class="fas fa-caret-right"></i> Sales</li>
                  <li><i class="fas fa-caret-right"></i> Customers</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Transport method settings</li>
              <li><i class="fas fa-check"></i> Search engine optimization "SEO"</li>
              <li><i class="fas fa-check"></i>And many other things</li>
            </ul>
          </div>
        </div>
        @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">
          {{ __('comercio.cerrar') }}
        </button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="componente" tabindex="-1" role="dialog" aria-labelledby="componenteWoo" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="componenteWoo">{{ __('comercio.link_dos') }}:</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12">
            @if (Session::get('language') == 'es')
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i> Lista de deseos</li>
              <li><i class="fas fa-check"></i> Ofertas y descuentos</li>
              <li><i class="fas fa-check"></i> Personalización de los productos, según las preferencias del cliente como
                por ejemplo el caso de tazas, llaveros, almohadones, ropa, entre otros productos</li>
              <li><i class="fas fa-check"></i> Gestionas los gastos de envío</li>
              <li><i class="fas fa-check"></i> Te permite sincronizar con otras plataformas como Mercado Libre o Amazon
              </li>
              <li><i class="fas fa-check"></i> Cuentas con soporte multi-idioma</li>
              <li><i class="fas fa-check"></i> Atención al cliente online</li>
              <li><i class="fas fa-check"></i> Crea paquetes de productos</li>
              <li><i class="fas fa-check"></i> Recibe el pago en múltiples monedas</li>
              <li><i class="fas fa-check"></i> Crea paquetes de productos</li>
              <li><i class="fas fa-check"></i> Integración con métodos de pago bancarios</li>
              <li><i class="fas fa-check"></i> Crear eventos y vender entradas en la web para poder asistir (con el
                plugin: The Events Calendar)</li>
              <li><i class="fas fa-check"></i> Crear un sitio de membresía y ocultar contenido de la web a usuarios que
                no hayan comprado un producto (con el plugin WooCommerce Memberships)</li>
              <li><i class="fas fa-check"></i> Entre otros</li>
            </ul>
            @else
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i>Wish list</li>
              <li><i class="fas fa-check"></i>Offers and discounts</li>
              <li><i class="fas fa-check"></i>Customization of products, according to customer preferences, such as
                mugs, keychains, pillows, clothing, among other products</li>
              <li><i class="fas fa-check"></i>You manage shipping costs</li>
              <li><i class="fas fa-check"></i>It allows you to synchronize with other platforms such as Mercado Libre or
                Amazon</li>
              <li><i class="fas fa-check"></i>Accounts with multi-language support</li>
              <li><i class="fas fa-check"></i>Online customer service</li>
              <li><i class="fas fa-check"></i>Create product packages</li>
              <li><i class="fas fa-check"></i>Get paid in multiple currencies</li>
              <li><i class="fas fa-check"></i>Create product packages</li>
              <li><i class="fas fa-check"></i>Integration with bank payment methods</li>
              <li><i class="fas fa-check"></i>Create events and sell tickets on the web to attend (with the plugin: The
                Events Calendar)</li>
              <li><i class="fas fa-check"></i>Create a membership site and hide web content from users who have not
                purchased a product (with the WooCommerce Memberships plugin)</li>
              <li><i class="fas fa-check"></i>Among others</li>
            </ul>
            @endif
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">
          {{ __('comercio.cerrar') }}
        </button>
      </div>
    </div>
  </div>
</div>