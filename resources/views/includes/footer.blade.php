@yield('fix-footer')
<footer id="footer">
    <div class="container'fluid">
        <div class="row py-2 py-md-4 mt-4">
            <div class="col-10 offset-1 mb-4 mb-lg-0">
                <div class="row ">
                    <div class="col-12 col-lg-9 mb-4 mb-lg-0">
                        <div class="row d-md-none mb-4">
                            <div class="col-12">


                                <div class="accordion accordion-flush" id="acordionFooter">

                                    <div class="accordion-item">
                                        <h2 class="accordion-header">
                                            <button class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseOne"
                                                aria-expanded="false" aria-controls="flush-collapseOne">
                                                <p class="text-4 mb-3  text-light font-bold">{{
                                                    __('master.acerca_nosotros') }}</p>
                                            </button>
                                        </h2>
                                        <div id="flush-collapseOne"
                                            class="accordion-collapse collapse bg-color-quaternary"
                                            data-bs-parent="#acordionFooter">
                                            <div class="accordion-body">
                                                <p class="mb-0">
                                                <ul class="list list-icons list-icons-sm mb-0">
                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_acerca-de-cyberfuel') }}">{{
                                                            __('master.acerca_cyber')
                                                            }}</a></li>
                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_terminos-condiciones') }}">{{
                                                            __('master.ter_cond')
                                                            }}</a></li>
                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_politica-de-privacidad') }}"> {{
                                                            __('master.politica_priva') }}</a></li>
                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_politica-anti-spam') }}"> {{
                                                            __('master.politica_spam')
                                                            }}</a></li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-item">
                                        <h2 class="accordion-header">
                                            <button class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                                aria-expanded="false" aria-controls="flush-collapseTwo">
                                                <p class="text-4 mb-3  text-light font-bold">{{
                                                    __('master.centro_asistencia') }}</p>
                                            </button>
                                        </h2>
                                        <div id="collapseTwo" class="accordion-collapse collapse bg-color-quaternary"
                                            data-bs-parent="#acordionFooter">
                                            <div class="accordion-body">
                                                <p class="mb-0">
                                                <ul class="list list-icons list-icons-sm mb-0">
                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_contactenos') }}">{{
                                                            __('master.contactenos')
                                                            }}</a></li>
                                                    <li><i class="fas fa-angle-right top-8"></i> <a target="_blank"
                                                            class="link-hover-style-1 text-color-light"
                                                            href="http://ticket.cyberfuel.com/open.php">{{
                                                            __('master.crear_tiq')
                                                            }}</a>
                                                    </li>
                                                    <li><i class="fas fa-angle-right top-8"></i> <a target="_blank"
                                                            class="link-hover-style-1 text-color-light"
                                                            href="http://ticket.cyberfuel.com/view.php"> {{
                                                            __('master.historial_tique')
                                                            }}</a></li>
                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_herramientas') }}">{{
                                                            __('master.herramientas')
                                                            }}</a>
                                                    </li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header">
                                            <button class="accordion-button collapsed " type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                                aria-expanded="false" aria-controls="flush-collapseThree">
                                                <p class="text-4 mb-3 text-light font-bold">{{ __('master.recursos') }}
                                                </p>
                                            </button>
                                        </h2>
                                        <div id="collapseThree" class="accordion-collapse collapse bg-color-quaternary"
                                            data-bs-parent="#acordionFooter">
                                            <div class="accordion-body">
                                                <p class="mb-0">
                                                <ul class="list list-icons list-icons-sm mb-0">
                                                    <li><i class="fas fa-angle-right top-8"></i> <a type="button"
                                                            class="link-hover-style-1 text-color-light"
                                                            data-bs-toggle="modal" data-bs-target="#usuario"
                                                            data-bs-toggle="tooltip" data-bs-placement="bottom">{{
                                                            __('master.mi_cuenta') }}</a></li>
                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_carrito-de-compra') }}">{{
                                                            __('master.carrito_compras')
                                                            }}</a></li>

                                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                                            class="link-hover-style-1 text-color-light"
                                                            href="{{ __('rutas.url_mapa') }}">{{
                                                            __('master.mapa_sitio') }}</a></li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-4 col-lg-4 d-none d-md-block">
                                <p class="text-4 mb-3  text-light font-bold">{{
                                    __('master.acerca_nosotros') }}</p>
                                <ul class="list list-icons list-icons-sm mb-0">
                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_acerca-de-cyberfuel') }}">{{
                                            __('master.acerca_cyber')
                                            }}</a></li>
                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_terminos-condiciones') }}">{{
                                            __('master.ter_cond')
                                            }}</a></li>
                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_politica-de-privacidad') }}"> {{
                                            __('master.politica_priva') }}</a></li>
                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_politica-anti-spam') }}"> {{
                                            __('master.politica_spam')
                                            }}</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4 d-none d-md-block">
                                <p class="text-4 mb-3  text-light font-bold">{{
                                    __('master.centro_asistencia') }}</p>
                                <ul class="list list-icons list-icons-sm mb-0">
                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_contactenos') }}">{{ __('master.contactenos')
                                            }}</a></li>
                                    <li><i class="fas fa-angle-right top-8"></i> <a target="_blank"
                                            class="link-hover-style-1 text-color-light"
                                            href="http://ticket.cyberfuel.com/open.php">{{ __('master.crear_tiq')
                                            }}</a>
                                    </li>
                                    <li><i class="fas fa-angle-right top-8"></i> <a target="_blank"
                                            class="link-hover-style-1 text-color-light"
                                            href="http://ticket.cyberfuel.com/view.php"> {{
                                            __('master.historial_tique')
                                            }}</a></li>
                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_herramientas') }}">{{ __('master.herramientas')
                                            }}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4 d-none d-md-block">
                                <p class="text-4 mb-3 text-light font-bold">{{ __('master.recursos') }}
                                </p>
                                <ul class="list list-icons list-icons-sm mb-0">
                                    <li><i class="fas fa-angle-right top-8"></i> <a type="button"
                                            class="link-hover-style-1 text-color-light" data-bs-toggle="modal"
                                            data-bs-target="#usuario" data-bs-toggle="tooltip"
                                            data-bs-placement="bottom">{{
                                            __('master.mi_cuenta') }}</a></li>
                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_carrito-de-compra') }}">{{
                                            __('master.carrito_compras')
                                            }}</a></li>

                                    <li><i class="fas fa-angle-right top-8"></i> <a
                                            class="link-hover-style-1 text-color-light"
                                            href="{{ __('rutas.url_mapa') }}">{{
                                            __('master.mapa_sitio') }}</a></li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <div class="row py-2">
                                    <div class="col-12 col-sm-4">
                                        <div class="row align-items-center">
                                            <div class="col-2 col-md-auto p-0">
                                                <img width="50" height="50"
                                                    src="{{URL::asset('assets/imagenes/logos/ready.webp')}}" alt="">

                                            </div>
                                            <div class="col-10 col-md-auto p-0">

                                                <small class="text-white text-2">&nbsp;IP version 6 ready</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-8 mt-2 mt-sm-0">
                                        <div class="row align-items-center">
                                            <div class="col-2 col-md-auto p-0">
                                                <img width="50" height="50"
                                                    src="{{URL::asset('assets/imagenes/logos/rate3.webp')}}" alt="">

                                            </div>
                                            <div class="col-10 col-md-auto p-0">
                                                <small class="text-2 text-white">
                                                    @if (Session::get('language') == 'es')
                                                    Sus datos se encuentran seguros, almacenados un Centro de Datos <br
                                                        class="d-none d-lg-block">
                                                    con certificación internacional
                                                    @else
                                                    Your data is safe, stored in a Data Center with international
                                                    certification
                                                    @endif

                                                    <a class="text-white" target="_blank"
                                                        href="https://tiaonline.org/products-and-services/tia942certification/tia-942-certifications-ratings/datacenter/all/CRI/1/1/#tabs-1">

                                                        <strong>Rate 3 / TIA-942</strong></a>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <p class="text-2 text-white">
                                            @if (Session::get('language') == 'es')
                                            *Para Costa Rica, a nuestros precios se le debe agregar el IVA (Impuesto de
                                            Valor Agregado)
                                            @else
                                            *For Costa Rica, must be added to our prices to IVA Tax.
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 mb-4 mb-lg-0">
                        <p class="text-4 mb-3 pb-1 text-light font-bold">{{ __('master.contactenos') }}</p>
                        <ul class="list list-icons list-icons-lg" style="margin-left: -25px;">
                            <li class="text-4 text-color-light font-weight-bold"><a href="tel:50622049494"
                                    class="text-decoration-none text-color-light"><img
                                        src="{{URL::asset('assets/imagenes/blank.gif')}}" class="flag flag-es" alt=""
                                        title="Cyberfuel, su sitio web en manos de expertos" loading="lazy"> (+506)
                                    2204-9494</a></li>
                            <li class="text-4 text-color-light font-weight-bold mt-2"><a href="tel:13059097248"
                                    class="text-decoration-none text-color-light"><img
                                        src="{{URL::asset('assets/imagenes/blank.gif')}}" class="flag flag-en" alt=""
                                        title="Cyberfuel, su sitio web en manos de expertos" loading="lazy"> (+1)
                                    305-909-7248</a></li>
                        </ul>
                        <ul class="list list-icons list-icons-lg">
                            <li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i>
                                <p class="m-0 text-color-light">{{ __('master.edificio') }} E, Forum 1. Santa Ana <br>
                                    San José
                                    Costa Rica
                                </p>
                            </li>
                            <li class="mb-1"><i class="far fa-envelope text-color-primary"></i>
                                <p class="m-0 "><a class="text-color-light"
                                        href="mailto:Info@cyberfuel.com">Info@cyberfuel.com</a></p>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright footer-copyright-style-2">
        <div class="container py-2">
            <div class="row align-items-center">
                <div
                    class="col-lg-3 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                    <a href="index.html" class="logo pe-0 pe-lg-3">
                        <img width="216" height="51" alt="Cyberfuel"
                            src="{{URL::asset('assets/imagenes/logos/cyber.webp')}}" class="opacity-5">
                    </a>

                </div>
                <div class="col mb-4 mb-lg-0">
                    <p class="text-white text-center text-lg-left">© Copyright
                        <?php echo date("Y");?>. Cyberfuel All Rights Reserved.
                    </p>

                </div>
                <div
                    class="col-lg-3 d-flex align-items-center justify-content-center justify-content-lg-end mb-4 mb-lg-0">


                    <ul class="footer-social-icons social-icons   ms-0">
                        <li class="social-icons-facebook"><a href="https://www.facebook.com/cyberfuel/" target="_blank"
                                title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="social-icons-instagram"><a href="https://www.instagram.com/cyberfuel_sa/"
                                target="_blank" title="Twitter"><i class="fab fa-instagram"></i></a></li>
                        <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank"
                                title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>


                    <ul class="nav nav-pills">
                        <li
                            class="nav-item dropup nav-item-left-border  nav-item-left-border-remove nav-item-left-border-md-show">
                            <a class="nav-link" href="#" role="button" id="dropdownLanguage" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"> <img
                                    src="{{URL::asset('assets/imagenes/blank.gif')}}"
                                    class="flag flag-{{App::getLocale()}}" alt="{{App::getLocale()}}"> <span
                                    class="text-uppercase">{{App::getLocale()}}</span> <i class="fas fa-angle-up"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownLanguage" style=""> <a
                                    aria-level="Ingles" class="dropdown-item" href="{{route('language','en')}}"><img
                                        width="20" height="20" src="{{URL::asset('assets/imagenes/blank.gif')}}"
                                        class="flag flag-en" alt="Idioma ingles"> EN</a> <a aria-level="español"
                                    class="dropdown-item" href="{{route('language','es')}}"><img width="20" height="20"
                                        src="{{URL::asset('assets/imagenes/blank.gif')}}" class="flag flag-es"
                                        alt="Idioma español"> ES</a> </div>
                        </li>

                    </ul>
                </div>
            </div>


        </div>
    </div>
</footer>