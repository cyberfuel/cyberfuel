<div role="main" class="main shop">
    <div class="container">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-12 pt-0">
                    <div class="alert alert-primary">
                        <strong>
                            {{ __('compra.elige_los_complementos') }}
                        </strong>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-12 col-md-6"> </div>
                    </div>
                    <div role="main" class="main shop">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="adaptable">
                                    <thead>
                                        <tr>
                                            <th width="55%" scope="col">{{ __('compra.complementos') }} </th>
                                            <th width="15%" scope="col">{{ __('compra.precio') }}</th>
                                            <th width="30%" scope="col">{{ __('compra.agregar') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if((isset($complementos)) && ($complementos != null))
                                            @foreach($complementos as $info_complemento)
                                                <tr>
                                                    <td data-label="{{ __('compra.complementos') }}">
                                                        <h4 class="text-primary font-weight-bold m-0 ">
                                                            <div class="col-12 ">
                                                                <a class="p-2" type="button" data-bs-toggle="modal" data-bs-target="#info_complemento{{ $info_complemento["id_servicio"] }}">
                                                                    <i class="fas fa-info-circle text-success"></i>
                                                                </a>
                                                                {{ $info_complemento["nombre"] }}
                                                            </div>
                                                        </h4>

                                                        <input type="hidden" id="codigo_complemento{{ $info_complemento["id_servicio"] }}" name="codigo_complemento{{ $info_complemento["id_servicio"] }}" value="{{ $info_complemento["codigo"] }}">
                                                        <input type="hidden" id="familia_complemento{{ $info_complemento["id_servicio"] }}" name="familia_complemento{{ $info_complemento["id_servicio"] }}" value="{{ $info_complemento["familia"] }}">
                                                        <input type="hidden" id="titulo_complemento{{ $info_complemento["id_servicio"] }}" name="titulo_complemento{{ $info_complemento["id_servicio"] }}" value="{{ $info_complemento["titulo_web"] }}">
                                                        <input type="hidden" id="precio_complemento{{ $info_complemento["id_servicio"] }}" name="precio_complemento{{ $info_complemento["id_servicio"] }}" value="{{ $info_complemento["precio"] }}">
                                                    </td>
                                                    <td data-label="{{ __('compra.precio') }}">
                                                        <span class="amount font-weight-bold text-success h4">
                                                            &dollar;{{ number_format($info_complemento["precio"], 2) }}
                                                        </span>
                                                    </td>
                                                    <td data-label="">
                                                        <button id="btn_complemento{{ $info_complemento["id_servicio"] }}" type="button" onclick="agregar_complemento('{{ $info_complemento["id_servicio"] }}');" class="btn btn-outline btn-rounded btn-primary mb-2" href="#" title="Agregar Producto">
                                                            <span>
                                                                <i class="fas fa-cart-plus fa-2x"></i>
                                                            </span>
                                                        </button>
                                                        <!--<spam>&nbsp;</spam>
                                                        <span class="rounded-pill bg-danger">
                                                            <a href="#" class="p-1" title="Quitar Producto">
                                                                <i class="fas fa-times"></i>
                                                            </a>
                                                        </span>-->
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  modals complementos-->
@if((isset($complementos)) && ($complementos != null))
    @foreach($complementos as $info_complemento)
        <div class="modal fade" id="info_complemento{{ $info_complemento["id_servicio"] }}" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">
                            {{ $info_complemento["nombre"] }}
                        </h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p>{{ $info_complemento["descripcion"] }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">
                            {{ __('comercio.cerrar') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
