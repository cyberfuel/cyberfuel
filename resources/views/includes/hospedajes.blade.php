<div class="tabs tabs-bottom tabs-center tabs-simple">
    
  <div class="tab-content">
    <div class="tab-pane active" id="tabsHospedajes">
      <div class="card card-body p-0 p-md-2">
        <div class="row">
          <div class="col-12 pt-4">
            <div class="accordion accordion-flush" id="tabsHospedajes">
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-linuxuno">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseUno" aria-expanded="false" aria-controls="flush-collapseUno"> <span
                      class="featured-boxes featured-boxes-style-6 p-0 m-0"> <span
                        class="featured-box featured-box-quaternary-verde featured-box-effect-3 p-0 m-0"> </span>
                    </span> {{
                    __('hospedaje.cara_prin') }} <span class="featured-boxes featured-boxes-style-6 p-0 m-0"> <span
                        class="featured-box featured-box-quaternary-verde featured-box-effect-3 p-0 m-0"></span>
                    </span></button>
                </h2>
                <div id="flush-collapseUno" class="accordion-collapse collapse show" aria-labelledby="flush-linuxuno"
                  data-bs-parent="#tabsHospedajes">
                  <div class="accordion-body">
                    <div class="row">
                      <div class="col-12">
                        <table class="table table-striped hospedaje">
                          <thead class="">
                            <tr>
                              <th></th>
                              <th class="text-center">{{ __('inicio.correo') }}</th>
                              <th class="text-center">Personal</th>
                              <th class="text-center">{{ __('hospedaje.pymes') }}</th>
                              <th class="text-center">{{ __('hospedaje.empresarial') }}</th>
                            </tr>
                            <thead>
                            <tbody>
                              <tr>
                                <td>{{ __('hospedaje.can_domi') }}</td>
                                <td class="text-center">1</td>
                                <td class="text-center">3</td>
                                <td class="text-center">6</td>
                                <td class="text-center">10</td>
                              </tr>
                              <tr>

                                <td>{{ __('hospedaje.espa_dis') }}</td>
                                <td class="text-center">1 GB</td>
                                <td class="text-center">5 GB</td>
                                <td class="text-center">10 GB</td>
                                <td class="text-center">20 GB</td>
                              </tr>
                              <tr>

                                <td>{{ __('hospedaje.buzo_co') }}</td>
                                <td class="text-center">1</td>
                                <td class="text-center">10</td>
                                <td class="text-center">60</td>
                                <td class="text-center">120</td>
                              </tr>
                              <tr>

                                <td>{{ __('hospedaje.tra_men') }}</td>
                                <td class="text-center">10 GB</td>
                                <td class="text-center">10 GB</td>
                                <td class="text-center">60 GB</td>
                                <td class="text-center">120 GB</td>
                              </tr>
                              <tr>

                                <td>{{ __('hospedaje.bas_dat_micr') }}</td>
                                <td class="text-center">0</td>
                                <td class="text-center">1</td>
                                <td class="text-center">10</td>
                                <td class="text-center">20</td>
                              </tr>
                              <tr>
                                <td> {{ __('hospedaje.sop_prog') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.res_diar') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.sopor_tec') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td class="bg-gree-responsive" data-title="Precio">{{ __('hospedaje.pag_an') }}</td>
                                <td class="text-center">US $ 30.00 *</td>
                                <td class="text-center"> US $ 64.99 *</td>
                                <td class="text-center"> US $ 129.99 *</td>
                                <td class="text-center"> US $ 299.99 *</td>
                              </tr>
            
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-linuxdos">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseDos" aria-expanded="false" aria-controls="flush-collapseDos"> {{
                    __('hospedaje.adm') }} </button>
                </h2>
                <div id="flush-collapseDos" class="accordion-collapse collapse" aria-labelledby="flush-linuxdos"
                  data-bs-parent="#tabsHospedajes">
                  <div class="accordion-body">
                    <div class="row">
                      <div class="col-12">
                        <table class="table table-striped">
                          <thead class="">
                            <tr>
                              <th></th>
                              <th class="text-center">{{ __('inicio.correo') }}</th>
                              <th class="text-center">Personal</th>
                              <th class="text-center">{{ __('hospedaje.pymes') }}</th>
                              <th class="text-center">{{ __('hospedaje.empresarial') }}</th>
                            </tr>
                            <thead>
                            <tbody>
                              <tr>
                                <td>{{ __('hospedaje.pane_cont') }} </td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.acc_ftp') }} </td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.ed_arc') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.pro_dire') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.adm_tare') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.adm_zon') }} </td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.estad') }} </td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.adm_we') }} </td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td class="bg-gree-responsive" data-title="Precio">{{ __('hospedaje.per_pag') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-linuxtres">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseTres" aria-expanded="false" aria-controls="flush-collapseTres"> {{
                    __('hospedaje.dominios') }} </button>
                </h2>
                <div id="flush-collapseTres" class="accordion-collapse collapse" aria-labelledby="flush-linuxtres"
                  data-bs-parent="#tabsHospedajes">
                  <div class="accordion-body">
                    <div class="row">
                      <div class="col-12">
                        <table class="table table-striped">
                          <thead class="">
                            <tr>
                              <th></th>
                              <th class="text-center">{{ __('inicio.correo') }}</th>
                              <th class="text-center">Personal</th>
                              <th class="text-center">{{ __('hospedaje.pymes') }}</th>
                              <th class="text-center">{{ __('hospedaje.empresarial') }}</th>
                            </tr>
                            <thead>
                            <tbody>
                              <tr>
                                <td>{{ __('hospedaje.can_domi') }} </td>
                                <td class="text-center">1</td>
                                <td class="text-center">3</td>
                                <td class="text-center">6</td>
                                <td class="text-center">10</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.acce_sin') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.ali_domi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.subdo') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.ayu_grat') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-linuxcuatro">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseCuatro" aria-expanded="false" aria-controls="flush-collapseCuatro">
                    {{ __('hospedaje.ser_we') }} </button>
                </h2>
                <div id="flush-collapseCuatro" class="accordion-collapse collapse" aria-labelledby="flush-linuxcuatro"
                  data-bs-parent="#tabsHospedajes">
                  <div class="accordion-body">
                    <div class="row">
                      <div class="col-12">
                        <table class="table table-striped">
                          <thead class="">
                            <tr>
                              <th></th>
                              <th class="text-center">{{ __('inicio.correo') }}</th>
                              <th class="text-center">Personal</th>
                              <th class="text-center">{{ __('hospedaje.pymes') }}</th>
                              <th class="text-center">{{ __('hospedaje.empresarial') }}</th>
                            </tr>
                            <thead>
                            <tbody>
                              <tr>
                                <td>Apache 2 webserver</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>PHP (7.4, 8.0, 8.1, 8.2, 8.3)</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>Python</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>Perl 5</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>C/C++</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>CGI/Perl</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>SSI (Server Site Includes)</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.bo_app') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-linuxcinco">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseCinco" aria-expanded="false" aria-controls="flush-collapseCinco"> {{
                    __('hospedaje.email') }} </button>
                </h2>
                <div id="flush-collapseCinco" class="accordion-collapse collapse" aria-labelledby="flush-linuxcinco"
                  data-bs-parent="#tabsHospedajes">
                  <div class="accordion-body">
                    <div class="row">
                      <div class="col-12">
                        <table class="table table-striped">
                          <thead class="">
                            <tr>
                              <th></th>
                              <th class="text-center">{{ __('inicio.correo') }}</th>
                              <th class="text-center">Personal</th>
                              <th class="text-center">{{ __('hospedaje.pymes') }}</th>
                              <th class="text-center">{{ __('hospedaje.empresarial') }}</th>
                            </tr>
                            <thead>
                            <tbody>
                              <tr>
                                <td>{{ __('hospedaje.buzo_co') }}</td>
                                <td class="text-center">1</td>
                                <td class="text-center">10</td>
                                <td class="text-center">60</td>
                                <td class="text-center">120</td>
                              </tr>
                              <tr>
                                <td>WebMail</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.ali_co') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                                <td class="text-center">{{ __('hospedaje.ilimi') }}</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.ren_cor') }}</td>
                                <td class="text-center">1</td>
                                <td class="text-center">10</td>
                                <td class="text-center">60</td>
                                <td class="text-center">120</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.au_resp') }}</td>
                                <td class="text-center">1</td>
                                <td class="text-center">10</td>
                                <td class="text-center">60</td>
                                <td class="text-center">120</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.grup_cor') }}</td>
                                <td class="text-center">0</td>
                                <td class="text-center">5</td>
                                <td class="text-center">20</td>
                                <td class="text-center">40</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.lis_cor') }}</td>
                                <td class="text-center">0</td>
                                <td class="text-center">5</td>
                                <td class="text-center">20</td>
                                <td class="text-center">40</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.cuen_cat') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.det_vir') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.det_spa') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.det_spa_spf') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.fil_spa') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i> + $</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i> + $</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i> + $</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i> + $</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.pro_ima') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.pro_pop') }}</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-linuxseis">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseSeis" aria-expanded="false" aria-controls="flush-collapseSeis"> {{
                    __('hospedaje.bas_da') }} </button>
                </h2>
                <div id="flush-collapseSeis" class="accordion-collapse collapse" aria-labelledby="flush-linuxseis"
                  data-bs-parent="#tabsHospedajes">
                  <div class="accordion-body">
                    <div class="row">
                      <div class="col-12">
                        <table class="table table-striped">
                          <thead class="">
                            <tr>
                              <th></th>
                              <th class="text-center">{{ __('inicio.correo') }}</th>
                              <th class="text-center">Personal</th>
                              <th class="text-center">{{ __('hospedaje.pymes') }}</th>
                              <th class="text-center">{{ __('hospedaje.empresarial') }}</th>
                            </tr>
                            <thead>
                            <tbody>
                              <tr>
                                <td>{{ __('hospedaje.bas_da') }} MySQL</td>
                                <td class="text-center">0</td>
                                <td class="text-center">1</td>
                                <td class="text-center">10</td>
                                <td class="text-center">20</td>
                              </tr>
                              <tr>
                                <td>{{ __('hospedaje.op_resp') }} </td>
                                <td class="text-center">x</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                              <tr>
                                <td class="bg-gree-responsive" data-title="Precio">{{ __('hospedaje.adm_base') }}</td>
                                <td class="text-center">x</td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                                <td class="text-center"><i class="fas fa-check text-4 text-primary "></i></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>