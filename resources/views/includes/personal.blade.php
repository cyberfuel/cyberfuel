<div class=" pt-1">
    <div class="row">
        <div class="form-group col-12 col-md-4 input-group-lg">
            <label class="form-label">{{ __('master.nom') }} <span class="text-color-danger">(*)</span></label>
            <input type="text" id="nombre" name="nombre" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["nombre"])){ echo $datos_cliente_temp["nombre"]; } ?>"/>
        </div>
        <div class="form-group col-12 col-md-4 input-group-lg">
            <label class="form-label">{{ __('master.iden') }}<span class="text-color-danger" id="obligatorio">(*)</span></label>
            <input type="text" id="cedula" name="cedula" class="form-control border-color-primary h-auto py-2 requerido" onkeypress="return permitir_solo_numeros(event);" value="<?php if(isset($datos_cliente_temp["cedula"])){ echo $datos_cliente_temp["cedula"]; } ?>">
        </div>
        <div class="form-group col-md-4">
            <label class="form-label">{{ __('master.cor_ele') }} <span class="text-color-danger">(*)</span></label>
            <input type="email" id="correo" name="correo" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["correo"])){ echo $datos_cliente_temp["correo"]; } ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-12 col-md-4">
            <label class="form-label">{{ __('master.cel') }}<span class="text-color-danger">(*)</span></label>
            <div class="input-group mb-3">
                <span id="phone1"class="input-group-text phone">(+506)</span>
                <input type="text" id="celular" name="celular" class="form-control border-color-primary requerido" placeholder="" aria-label="Celular" aria-describedby="basic-addon2" value="<?php if(isset($datos_cliente_temp["celular"])){ echo $datos_cliente_temp["celular"]; } ?>">
            </div>
        </div>
        <div class="form-group col-12 col-md-4">
            <label class="form-label">{{ __('master.tel') }} <span class="text-color-danger">(*)</span></label>
            <div class="input-group mb-3">
                <span id="phone2" class="input-group-text phone">(+506)</span>
                <input type="text" id="telefono" name="telefono" class="form-control border-color-primary requerido" placeholder="" aria-label="Tel&eacute;fono" aria-describedby="basic-addon2" value="<?php if(isset($datos_cliente_temp["telefono"])){ echo $datos_cliente_temp["telefono"]; } ?>">
            </div>
        </div>

        <div class="form-group col-12 col-md-4" id="opcional">
            <label class="form-label">{{ __('master.telefono_opcional') }}</label>
            <form class="form-inline">
                <div class="form-group">
                    <div class="input-group">
                        <span id="phone3" class="input-group-text phone">(+506)</span>
                        <input type="text" id="telefono2" name="telefono2" class="form-control border-color-primary requerido" placeholder="" aria-label="Celular" aria-describedby="basic-addon2" value="<?php if(isset($datos_cliente_temp["telefono2"])){ echo $datos_cliente_temp["telefono2"]; } ?>">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="form-group col">
            <label class="form-label">{{ __('master.dire') }} <span class="text-color-danger">(*)</span></label>
            <input type="text" id="direccion" name="direccion" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["direccion"])){ echo $datos_cliente_temp["direccion"]; } ?>"/>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-auto ">
        <h4>{{ __('master.con_ad') }} </h4>
    </div>
</div>
<div class="row">
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.nom') }} <span class="text-color-danger">*</span></label>
        <input type="text" id="nombre_admin" name="nombre_admin" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["nombre_admin"])){ echo $datos_cliente_temp["nombre_admin"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.ape') }} <span class="text-color-danger">*</span></label>
        <input type="text" id="apellido_admin" name="apellido_admin" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["apellido_admin"])){ echo $datos_cliente_temp["apellido_admin"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.cor_ele') }} <span class="text-color-danger">*</span></label>
        <input type="email" id="correo_admin" name="correo_admin" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["correo_admin"])){ echo $datos_cliente_temp["correo_admin"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.tel') }} <span class="text-color-danger">(*)</span></label>
        <div class="input-group mb-3">
            <span id="phone4" class="input-group-text phone">CR (+506)</span>
            <input type="text" id="tel_admin" name="tel_admin" class="form-control border-color-primary requerido" placeholder="" aria-label="Celular" aria-describedby="basic-addon2" value="<?php if(isset($datos_cliente_temp["tel_admin"])){ echo $datos_cliente_temp["tel_admin"]; } ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-6 ">
        <h4>{{ __('master.con_tec') }} </h4>
        <p>
        <div class="form-check">
            <div class="row align-items-center justify-content-start">
                <div class="col-2">
                    <input type="checkbox" id="chk_tecnico" name="chk_tecnico" class="gdpe-input custom-checkbox-switch" onchange="datos_admin_igual_tecnico();" <?php if((isset($datos_cliente_temp["chk_tecnico"])) && ($datos_cliente_temp["chk_tecnico"] == "on")){ echo "checked"; } ?>>
                </div>
                <div class="col-10">
                    <label class="form-check-label" for="gridCheck1"> {{ __('master.us_mis') }}  </label>
                </div>
            </div>
        </div>
        </p>
    </div>
</div>
<div class="row">
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.nom') }} <span class="text-color-danger">*</span></label>
        <input type="text" id="nombre_tecnico" name="nombre_tecnico" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["nombre_tecnico"])){ echo $datos_cliente_temp["nombre_tecnico"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.ape') }} <span class="text-color-danger">*</span></label>
        <input type="text" id="apellido_tecnico" name="apellido_tecnico" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["apellido_tecnico"])){ echo $datos_cliente_temp["apellido_tecnico"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.cor_ele') }} <span class="text-color-danger">*</span></label>
        <input type="email" id="correo_tecnico" name="correo_tecnico" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["correo_tecnico"])){ echo $datos_cliente_temp["correo_tecnico"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.tel') }} <span class="text-color-danger">(*)</span></label>
        <div class="input-group mb-3">
            <span id="phone5" class="input-group-text phone">CR (+506)</span>
            <input type="text" id="tel_tecnico" name="tel_tecnico" class="form-control border-color-primary requerido" placeholder="" aria-label="Celular" aria-describedby="basic-addon2" value="<?php if(isset($datos_cliente_temp["tel_tecnico"])){ echo $datos_cliente_temp["tel_tecnico"]; } ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-6 ">
        <h4>{{ __('master.con_co') }} </h4>
        <p>
            <div class="form-check">
                <div class="row align-items-center justify-content-start">
                    <div class="col-2">
                        <input type="checkbox" id="chk_cobro" name="chk_cobro" class="gdpe-input custom-checkbox-switch" onchange="datos_admin_igual_cobros();" <?php if((isset($datos_cliente_temp["chk_cobro"])) && ($datos_cliente_temp["chk_cobro"] == "on")){ echo "checked"; } ?>>
                    </div>
                    <div class="col-10">
                        <label class="form-check-label" for="gridCheck1"> {{ __('master.us_mis') }}  </label>
                    </div>
                </div>
            </div>
        </p>
    </div>
</div>
<div class="row">
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.nom') }} <span class="text-color-danger">*</span></label>
        <input type="text" id="nombre_cobro" name="nombre_cobro" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["nombre_cobro"])){ echo $datos_cliente_temp["nombre_cobro"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.ape') }} <span class="text-color-danger">*</span></label>
        <input type="text" id="apellido_cobro" name="apellido_cobro" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["apellido_cobro"])){ echo $datos_cliente_temp["apellido_cobro"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.cor_ele') }} <span class="text-color-danger">*</span></label>
        <input type="email" id="correo_cobro" name="correo_cobro" class="form-control border-color-primary h-auto py-2 requerido" value="<?php if(isset($datos_cliente_temp["correo_cobro"])){ echo $datos_cliente_temp["correo_cobro"]; } ?>"/>
    </div>
    <div class="form-group col-6 col-lg-3">
        <label class="form-label">{{ __('master.tel') }} <span class="text-color-danger">(*)</span></label>
        <div class="input-group mb-3">
            <span id="phone6" class="input-group-text phone">CR (+506)</span>
            <input type="text" id="tel_cobro" name="tel_cobro" class="form-control border-color-primary requerido" placeholder="" aria-label="Celular" aria-describedby="basic-addon2" value="<?php if(isset($datos_cliente_temp["tel_cobro"])){ echo $datos_cliente_temp["tel_cobro"]; } ?>">
        </div>
    </div>
</div>

<div class="row justify-content-end">
    <div class="col-auto">
        <a  onclick="history.back()" type="button" class="btn btn-success btn-modern  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-3 py-3">
            <i class="fas fa-arrow-left ms-2"></i>
            {{ __('compra.anterior') }}
        </a>
    </div>
    <div class="col-auto">
     <!--   <a type="button" onclick="redireccionar_pago('{{ __('rutas.url_detalle_pago') }}');" class="btn btn-success btn-modern  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-3 py-3 ">
            {{ __('master.fina_pa') }}
            <i class="fas fa-arrow-right ms-2"></i>
        </a>
        -->
          <a type="button" href="{{ __('rutas.url_detalle_pago') }}" class="btn btn-dark btn-modern  text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-3 py-3">
            {{ __('master.com_orden') }}
            <i class="fas fa-arrow-right ms-2"></i>
        </a>
    </div>
</div>
