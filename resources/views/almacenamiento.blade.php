@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel</title>
<meta name="keywords" content="Seguridad, solución, escaneo, certificado, certificado de seguridad, ssl, waf, pci, pci-dss, servidor"/>
<meta name="description" content="Por medio de diferentes soluciones de seguridad en su servidor podrá tener protección, utilizando tecnologías como: certificado de seguridad SSL, WAF y/o certificando PCI-DSS ">
@endsection
@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel">
<meta property="og:title" content="Cyberfuel | Seguridad en su sitio">
@endsection
@section('content') 
<!--aca empieza el contenido del HTML-->



<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-6  align-self-center p-static ">
							
								<div class="overflow-hidden ">
									<h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>{{ __('master.almacenamiento')
                          }}</strong></h1>
								</div>
							</div>
						</div>
					</div>
				</section>
<div class="container">
	
	  <div class="row my-5">
    <div class="col-12 ">
    
        
		  @if (Session::get('language') == 'es')

   <p class="text-justify">La seguridad y la protección en su sitio web y su correo electrónico son muy importantes, por eso ponemos a su disposición diferentes soluciones como: Certificados de seguridad SSL, Firewall WEB conocido como WAF, escanear su servidor para obtener la certificación PCI-DSS y Filtrado de correo electrónico no deseado utilizando Spam Filter. </p>
        
        @else

	<p class="text-justify">Security and protection on your website and your email is very important, that's why we offer you different solutions such as: SSL security certificates, WEB Firewall known as WAF, Scanning your server to obtain PCI-DSS certification and Mail filtering unwanted via Spam Filter.</p>
        @endif
		  
		
    </div>
  </div>
	

	
	      <div class="row pb-3">
    <div class="col-12">
          <div class="d-md-flex"><div class="row">
        <div class="col-md-2"> <img class="img-fluid mx-auto d-block appear-animation" data-appear-animation="backInLeft" data-appear-animation-delay="0" data-appear-animation-duration="1s" src="{{URL::asset('assets/imagenes/ssl.webp')}}" alt=""> </div>
        <div class="col-md-10">
               @if (Session::get('language') == 'es')
              <h4>Certificado SSL</h4>
              <p class="text-justify">Siendo muy utilizados en los sitios de Internet que permiten comprar por medio de tarjeta de crédito o bien desean reforzar la seguridad entre el usuario y el sitio WEB. Nuestros certificados de seguridad SSL son 128/256-Bits de encriptación, con una llave de 2048-Bits de encriptación y compatible con el 99.9% de los Navegadores.</p>
              @else
              <h4>SSL Certificate</h4>
              <p class="text-justify"> It is used to encrypt sensitive information between the user and the website. It is used in Internet sites that allow you to buy using a credit card or if you want to reinforce the security.  Our SSL security certificates are 128/256-Bit encryption, with a 2048-Bit encryption key and compatible with 99.9% of Browsers.</p>
@endif <span class="float-end">
            <a href="{{ __('rutas.url_mercado') }}#seguridad" type="button" id=""  class="btn btn-outline btn-rounded btn-primary mb-2" title="Agregar Producto"> <span> <i class="fas fa-cart-plus fa-2x "></i> </span> </a>
            </span>
			  </div>
      </div>
		</div>
        </div>
  </div>
<!--fin el contenido del HTML--> 
@endsection 


@section('scripts') 
	      
@endsection 		  