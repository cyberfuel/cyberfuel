@if((isset($dominios_sugeridos)) && ($dominios_sugeridos != null))
<div class="alert alert-primary mt-2"> <strong>{{ __('compra.otras_opciones') }}</strong> </a> </div>
<div class="row align-items-center">
    <div class="col-12 col-md-6"> </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="adaptable">
            <thead>
                <tr>
                    <th scope="col">{{ __('compra.dominio') }}</th>
                    <th scope="col">{{ __('compra.precio') }}</th>
                    <th scope="col">Años</th>
                    <th scope="col">{{ __('compra.agregar') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dominios_sugeridos as $info_dominio_sugerido)
                <tr>
                    <td class="position-relative" data-label="{{ __('compra.dominio') }}">
                        <!--<span class="position-absolute top-50 end-0 translate-middle-y badge rounded-pill bg-danger m-1 p-2">
                                    <i class="fas fa-heart"></i>
                                </span>-->
                        <div class="d-none d-md-block">
                            <span class="d-block">{{ $info_dominio_sugerido["nombre"] }}</span>
                            <span>&nbsp;&nbsp;&nbsp;</span>
                            <span class="text-primary font-weight-bold m-0 text-5 font-weight-semibold line-height-1 ml-5">
                                {{ $info_dominio_sugerido["extension"] }}
                            </span>
                        </div>
                        <h4 class="text-primary font-weight-bold m-0 d-md-none">
                            {{ $info_dominio_sugerido["nombre"] }}
                            <span class="font-weight-semibold  line-height-1 ">
                                <a data-bs-toggle="tooltip" data-bs-animation="false" data-bs-placement="top" title="" data-bs-original-title="Páginas web relacionadas con el comercio." class="font-weight-semi-bold text-color-dark text-color-hover-primary text-decoration-none position-relative">
                                    {{ $info_dominio_sugerido["extension"] }}
                                </a>
                            </span>
                        </h4>
                    </td>
                    <td data-label="{{ __('compra.precio') }}">
                        <span class="d-flex flex-column align-items-md-start font-weight-bold">
                            <span>
                                <s class="d-inline-block text-3">$19.99</s>&nbsp;
                                <span class="d-inline-block text-5 ml-2 mr-2 text-success">
                                    @if((isset($registro_dominio["estado"])) && ($registro_dominio["estado"] == "ok"))
                                    @if((isset($registro_dominio["info"][0])) && ($registro_dominio["info"][0] != null))
                                    &dollar;{{ number_format($registro_dominio["info"][0]["precio"], 2) }}
                                    @endif
                                    @endif
                                </span>
                                <span class="d-block small">
                                    {{ __('compra.durante_primer_anio') }}
                                </span>
                            </span>
                        </span>
                    </td>
                    <td data-label="{{ __('compra.anios') }}">
                        <select id="anios{{ $info_dominio_sugerido["id_campo"] }}" name="anios{{ $info_dominio_sugerido["id_campo"] }}" class="form-select form-select-sm  w-75 d-inline-block">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                        <input type="hidden" id="dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" name="dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" value="{{ $info_dominio_sugerido["dominio"] }}">
                        <input type="hidden" id="codigo_dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" name="codigo_dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" value="{{ $registro_dominio["info"][0]["codigo"] }}">
                        <input type="hidden" id="familia_dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" name="familia_dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" value="{{ $registro_dominio["info"][0]["familia"] }}">
                        <input type="hidden" id="precio_dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" name="precio_dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" value="{{ $registro_dominio["info"][0]["precio"] }}">
                    </td>
                    <td data-label="">
                        <button type="button" id="btn_dominio_sugerido{{ $info_dominio_sugerido["id_campo"] }}" onclick="agregar_registro_dominio_sugerido('{{ $info_dominio_sugerido["id_campo"] }}');" class="btn btn-outline btn-rounded btn-primary mb-2 <?php if ($info_dominio_sugerido["agregado"] == "si") {
                                                                                                                                                                                                                                                                    echo "btn-success";
                                                                                                                                                                                                                                                                } ?>" title="Agregar Producto">
                            <span>
                                <i class="fas fa-cart-plus fa-2x <?php if ($info_dominio_sugerido["agregado"] == "si") {
                                                                        echo "fa-check";
                                                                    } ?>"></i>
                            </span>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif