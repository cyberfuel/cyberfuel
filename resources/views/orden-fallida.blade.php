@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Registrese con nosotros</title>
<meta name="keywords"
    content="Orden, costa rica, cr, solución, maquinas, servidores, archivos, sitio seguro, seguridad" />
<meta name="description"
    content="Con nuestra solución puede tener un respaldo de su computador o de sus servidores virtuales o físicos (completo o parcial) fuera de su lugar de trabajo o habitación, garantizando la información segura ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Registrese con nosotros">
<meta property="og:title" content="Cyberfuel | Ingreso de Nuevo Cliente">
@endsection
@section('content')
<!--aca empieza el contenido del HTML-->
<hr class=" py-2">
<div role="main" class="main shop pb-4">

    <div class="container-xl">


        <div class="row justify-content-center">
            <div class="col">
                <div class="card border-width-3 border-radius-0 border-color-danger">
                    <div class="card-body text-center">
                        <p class="text-color-dark font-weight-bold text-4-5 mb-0"><i
                                class="fas fa-exclamation text-color-danger me-1"></i> Disculpas su orden no pudo ser
                            procesada correctamente</p>
                    </div>
                </div>

            </div>



        </div>


        <div class="row">

            <div class="col">
                <div class="accordion accordion-flush" id="tabsOrden">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="orden-uno">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#orden-collapse-uno" aria-expanded="false"
                                aria-controls="orden-collapse-uno"> <span
                                    class="featured-boxes featured-boxes-style-6 p-0 m-0"> <span
                                        class="featured-box featured-box-quaternary-verde featured-box-effect-3 p-0 m-0">
                                    </span>
                                </span> El comercio no acepta el tipo de tarjeta que está utilizando <span
                                    class="featured-boxes featured-boxes-style-6 p-0 m-0"> <span
                                        class="featured-box featured-box-quaternary-verde featured-box-effect-3 p-0 m-0"></span>
                                </span></button>
                        </h2>
                        <div id="orden-collapse-uno" class="accordion-collapse collapse" aria-labelledby="orden-uno"
                            data-bs-parent="#tabsOrden" style="">
                            <div class="accordion-body">
                                <div class="row">
                                    <div class="col-12">
                                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Optio veritatis
                                        officia voluptatum! Rerum nisi aut blanditiis atque iste alias quaerat
                                        temporibus ipsum dignissimos pariatur consequuntur minus, non sapiente obcaecati
                                        nostrum.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="orden-dos">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#orden-collapse-dos" aria-expanded="false"
                                aria-controls="orden-collapse-dos"> La tarjeta tiene algún bloqueo por motivos de
                                seguridad </button>
                        </h2>
                        <div id="orden-collapse-dos" class="accordion-collapse collapse" aria-labelledby="orden-dos"
                            data-bs-parent="#tabsOrden" style="">
                            <div class="accordion-body">
                                <div class="row">
                                    <div class="col-12">
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Excepturi sint cum
                                        praesentium doloribus exercitationem voluptas hic blanditiis doloremque, impedit
                                        ab totam eveniet fuga et obcaecati amet laboriosam modi? Corrupti, est!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="orden-tres">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#orden-collapse-tres" aria-expanded="false"
                                aria-controls="orden-collapse-tres">Su tarjeta está vencida</button>
                        </h2>
                        <div id="orden-collapse-tres" class="accordion-collapse collapse" aria-labelledby="orden-tres"
                            data-bs-parent="#tabsOrden" style="">
                            <div class="accordion-body">
                                <div class="row">
                                    <div class="col-12">
                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod doloribus
                                        reprehenderit vel mollitia delectus impedit, temporibus molestiae voluptatibus
                                        inventore illum iusto quam incidunt autem, quidem est deleniti veritatis
                                        provident laborum.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
@endsection

@section('scripts')



@endsection