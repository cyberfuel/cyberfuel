@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Contáctenos</title>
<meta name="keywords"
    content="cyberfuel, hospedaje web, alojamiento web, web hosting, webhosting, linux, windows, costa rica, correo electronico, sitio, servicios, nube, seguridad, informatica" />
<meta name="description" content="{{ __('hospedaje.descripcion') }}">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Contáctenos con nuestros agentes de ventas">
<meta property="og:title" content="Cyberfuel | Contáctenos">
@endsection

@section('content')
<!--aca empieza el contenido del <HTML-->

<section
    class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8"
    style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-6  align-self-center p-static ">

                <div class="overflow-hidden ">
                    <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter"
                        data-appear-animation-delay="300"><strong>{{ __('master.contactenos') }}</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">

    @if(isset($errors) && $errors->any())
    <div class="container">
        <div class="row">
            <div class="col-xs-12 mt-4">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $e)
                        <li> {{ $e }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endif


    <div class="row align-items-center">
        <div class="col-lg-6 ">
            <form class="contact-form" id="contacto" name="contacto" action="{{ route('contactenos.send') }}"
                method="POST" ovalidate="novalidate">
                @csrf

                <div class="row">
                    <div class="form-group col-lg-6">
                        <label class="form-label mb-1 text-2">{{ __('master.nom_comp') }}</label>
                        <input type="text" value="{{ old('nombre') }}" maxlength="100"
                            class="form-control text-3 h-auto py-2 border-color-primary" name="nombre"
                            required="required">
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="form-label mb-1 text-2">{{ __('master.cor_ele') }}</label>
                        <input type="email" value="{{ old('email') }}" maxlength="100"
                            class="form-control text-3 h-auto py-2 border-color-primary" name="email"
                            required="required">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label class="form-label mb-1 text-2">{{ __('compra.departamento') }}</label>
                        <select class="form-select form-control border-color-primary text-3 h-auto py-2"
                            name="departamento" id="departamento">
                            <option value="ventas" {{ old('departamento')=='ventas' ? 'selected' : '' }} selected>{{
                                __('inicio.ventas') }}</option>
                            <option value="soporte" {{ old('departamento')=='soporte' ? 'selected' : '' }}>{{
                                __('inicio.soporte') }}</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="form-label mb-1 text-2">{{ __('inicio.servicio') }}</label>
                        <select class="form-select form-control border-color-primary text-3 h-auto py-2" name="servicio"
                            id="servicio">
                            <option value="dominio" {{ old('servicio')=='dominio' ? 'selected' : '' }} selected>{{
                                __('master.nombre_dominio_internet') }}</option>

                            <option value="hospedaje" {{ old('servicio')=='hospedaje' ? 'selected' : '' }}>{{
                                __('inicio.hospedaje_web') }}</option>
                            <option value="ssl" {{ old('servicio')=='ssl' ? 'selected' : '' }}>{{ __('master.ssl_cer')
                                }}</option>
                            <!--  <option value="WAR" {{ old('servicio') == 'WAR'? 'selected' : '' }}>WAR (Web Application Firewall)</option>-->
                            <option value="PCI" {{ old('servicio')=='PCI' ? 'selected' : '' }}>PCI-DDS</option>
                            <option value="Spam" {{ old('servicio')=='Spam' ? 'selected' : '' }}>SpamFilter</option>
                            <option value="BIT" {{ old('servicio')=='BIT' ? 'selected' : '' }}>BIT-Ninja</option>
                            <option value="Baas" {{ old('servicio')=='Baas' ? 'selected' : '' }}>{{
                                __('compra.ser_baas') }}</option>
                            <option value="datos" {{ old('servicio')=='datos' ? 'selected' : '' }}>{{
                                __('master.centro_datos') }}</option>
                            <option value="unidad" {{ old('servicio')=='unidad' ? 'selected' : '' }}>{{
                                __('master.colocacion_unidad') }}</option>
                            <option value="gabinete" {{ old('servicio')=='gabinete' ? 'selected' : '' }}>{{
                                __('master.colocacion_gabinete') }}</option>
                            <!-- <option value="">{{ __('master.respaldo_servicio') }}</option>
							   <option value="">{{ __('master.recuperacion_desastres') }}</option>-->
                        </select>



                        <?php /*?> <select class="form-select form-control border-color-primary text-3 h-auto py-2"
                            name="servicio" id="servicio">
                            <option value="">Seleccione un servicio</option>
                            @foreach (config("servicio") as $val => $cfg_servicio)
                            <option value="{{ $val }}" {{ (old('servicio')===$val || ($servicio ?? '' )===$val)
                                ? 'selected' : '' }}>
                                {{ $cfg_servicio['str'] }}
                            </option>
                            @endforeach
                        </select>
                        <?php */?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label class="form-label mb-1 text-2">{{ __('master.mensaje') }}</label>
                        <textarea maxlength="5000" rows="5" class="form-control text-3 h-auto py-2 border-color-primary"
                            name="mensaje" id="mensaje" required="required">{{ old('mensaje') }}</textarea>
                    </div>
                </div>


                <div class="row">
                    <div class="form-group col">
                        <button id="btnEnviar" type="submit" value="Enviar" class="btn btn-primary">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-6">
            <div class="ml-5  appear-animation animated fadeIn appear-animation-visible" data-appear-animation="fadeIn"
                data-appear-animation-delay="800" data-plugin-options="{'accY': -200}" style="animation-delay: 800ms;">
                <h4 class="pt-5">{{ __('master.nuest_offi') }}</h4>
                <ul class="cont list list-icons list-icons-style-3 mt-2">
                    <li>
                        <i class="fas fa-map-marker-alt top-6"></i>
                        <strong>{{ __('master.dire') }}:</strong> Edificio E, Forum 1. Santa Ana San José Costa Rica
                    </li>
                    <li>
                        <i class="fas fa-phone top-6"></i>
                        <strong>{{ __('master.tel') }}:</strong> <a href="tel:50622049494">(+506) 2204-9494</a>
                    </li>
                    <li>
                        <i class="fas fa-envelope top-6"></i>
                        <strong>{{ __('master.cor_ele') }}:</strong>
                        <a href="mailto:mail@example.com">Info@cyberfuel.com</a>
                    </li>
                </ul>
            </div>
            <div class="ml-5 appear-animation animated fadeIn appear-animation-visible" data-appear-animation="fadeIn"
                data-appear-animation-delay="1000" data-plugin-options="{'accY': -200}"
                style="animation-delay: 1000ms;">
                <h4 class="pt-5">{{ __('master.nuest_ho') }}

                </h4>
                <ul class="list list-icons list-dark mt-2">
                    <li>
                        <i class="far fa-clock top-6"></i> {{ __('master.asisten') }}
                    </li>
                    <li class="row">
                        <div class="col-8  col-md-6"><img src="{{URL::asset('assets/imagenes/logos/24-2.webp')}}"
                                alt="Cyberfuel" class="img-fluid"></div>
                    </li>
                </ul>
            </div>

        </div>
    </div>


</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 p-0">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125737.05883262958!2d-84.38355797867258!3d9.993458594628672!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1740f4a7c60b88d1!2sCyberfuel%20SA!5e0!3m2!1ses!2scr!4v1574983799803!5m2!1ses!2scr"
                width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</div>
<!--fin el contenido del HTML-->
@endsection

@section('scripts')
<script src="https://www.google.com/recaptcha/api.js?render={{$recaptcha_site}}"></script>
<script>
    $('#contacto').on('submit', function(e) {
        e.preventDefault();
        grecaptcha.ready(function() {
            grecaptcha.execute("{{$recaptcha_site}}", { action: 'contacto' }).then(function(token) {
                $('#btnEnviar').prepend('<input type="hidden" id="recaptcha_response" name="recaptcha_response" value="' + token + '">');
                $('#contacto').unbind('submit').submit();
            });
        });
    });

    
</script>



@endsection