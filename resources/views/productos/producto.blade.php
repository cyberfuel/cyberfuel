
@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel</title>
<meta name="keywords" content="Seguridad, solución, escaneo, certificado, certificado de seguridad, ssl, waf, pci, pci-dss, servidor"/>
<meta name="description" content="Por medio de diferentes soluciones de seguridad en su servidor podrá tener protección, utilizando tecnologías como: certificado de seguridad SSL, WAF y/o certificando PCI-DSS ">
@endsection
@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/seguridad.webp')}}">
<meta property="og:description" content="Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel">
<meta property="og:title" content="Cyberfuel | Seguridad en su sitio">
@endsection
@section('content') 
<!--aca empieza el contenido del HTML-->


 
<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-6  align-self-center p-static ">
							
								<div class="overflow-hidden ">
									<h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>Producto</strong></h1>
								</div>
							</div>
						</div>
					</div>
				</section>
<div class="container">
	<div class="row">
	<div class="col-12">
		<ul class="breadcrumb font-weight-semibold text-4 negative-ls-1">
									<li><a href="{{ url('/') }}">Inicio</a></li>
									<li class="text-color-primary"><a href="{{ __('rutas.url_mercado') }}">Todos los Productos</a></li>
									<li class="active text-color-primary">Producto</li>
								</ul>
		</div>
	</div>
	
	<div class="row pt-4">
		<div class="col-md-7">
		
		<div class="feature-box">
										
											<div class=""> <img class="lazyload " src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{URL::asset('assets/imagenes/prueba.png')}}" alt="Productos Cyberfuel"> </div>
										
										<div class="feature-box-info">
											<h4 class="mb-2">Customer Support</h4>
											<p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
										</div>
									</div>
		</div>
		<div class="col-md-5">
		<p class="price text-7 mb-0"> <span class="sale  font-weight-semi-bold px-2 text-primary">$69,00</span> <span class="amount">$59,00</span> <span class="boton-ver">
                           <a  class="btn btn-outline btn-rounded btn-primary  btn-with-arrow mb-2 mt-lg-2">
                                                                Agregar<span><i class="fas fa-shopping-cart"></i></span>
                                                            </a>
                        </span> </p>
		</div>

	</div>
	  <div class="row my-5">
		  <div class="col-12"><h2 class="font-weight-extra-bold">Descripción</h2></div>
		  <div class="col-12"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut aperiam maiores corporis placeat eveniet assumenda non voluptates unde repudiandae ab saepe facere, beatae asperiores iste fugit, eligendi nostrum enim blanditiis.</p></div>
		  
		 <div class="col-12 mt-2"><h2 class="font-weight-extra-bold">Detalle</h2></div>
<div class="col-12">

								<table class="table table-hover">
										<thead>
											<tr>
												<th>
													#
												</th>
												<th>
													First Name
												</th>
												<th>
													Last Name
												</th>
												<th>
													Username
												</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													1
												</td>
												<td>
													Mark
												</td>
												<td>
													Otto
												</td>
												<td>
													@mdo
												</td>
											</tr>
											<tr>
												<td>
													2
												</td>
												<td>
													Jacob
												</td>
												<td>
													Thornton
												</td>
												<td>
													@fat
												</td>
											</tr>
											<tr>
												<td>
													3
												</td>
												<td>
													Larry
												</td>
												<td>
													the Bird
												</td>
												<td>
													@twitter
												</td>
											</tr>
										</tbody>
									</table>

								</div>
  </div>
	



</div>

<div class="container container-xl-custom">
						<div class="row align-items-center">
							<div class="col-lg-6 " style="min-height: 33vw;">
								<div class="fluid-col fluid-col-left">
									<img src="{{URL::asset('assets/imagenes/bg-h.webp')}}" class="img-fluid" alt="">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="pe-5">
									<span class="d-block font-weight-light negative-ls-1 text-5 mb-1"><em class="text-2 opacity-8">This is a</em></span>
									<h2 class="font-weight-extra-bold text-color-dark negative-ls-3 line-height-1 text-7 mb-3"><em>LEFT BIG IMAGE SECTION</em></h2>
									<span class="d-block alternative-font text-color-primay text-5 mb-4 pb-2">Enjoy the power</span>
									<p class="lead mb-4 pb-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec finibus orci, id luctus est. Phasellus tristique libero id rhoncus tincidunt. Aliquam luctus.</p>
									<div class="row">
										<div class="col-md-6">
											<ul class="list list-icons list-style-none text-4 ps-none mb-2 pb-2 pe-3">
												<li class="text-1 text-color-primary mb-3"><i class="fa fa-check text-primary text-4 me-1"></i> HTML5+JS+CSS+SCSS</li>
												<li class="text-1 text-color-primary mb-3"><i class="fa fa-check text-primary text-4 me-1"></i> Wide &amp; Boxed</li>
												<li class="text-1 text-color-primary mb-3"><i class="fa fa-check text-primary text-4 me-1"></i> Dark &amp; Light</li>
												<li class="text-1 text-color-primary mb-0 mb-md-3"><i class="fa fa-check text-primary text-4 me-1"></i> RTL Support</li>
											</ul>
										</div>
										<div class="col-md-6">
											<ul class="list list-icons list-style-none text-4 ps-none mb-2 pb-2 pe-3">
												<li class="text-1 text-color-primary mb-3"><i class="fa fa-check text-primary text-4 me-1"></i> Bootstrap</li>
												<li class="text-1 text-color-primary mb-3"><i class="fa fa-check text-primary text-4 me-1"></i> SEO Optimized</li>
												<li class="text-1 text-color-primary mb-3"><i class="fa fa-check text-primary text-4 me-1"></i> Blog Pages</li>
												<li class="text-1 text-color-primary mb-3"><i class="fa fa-check text-primary text-4 me-1"></i> Shop Pages</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
	
	

					</div>
 <span class="py-4">&nbsp;</span>
<!--fin el contenido del HTML--> 
@endsection 


