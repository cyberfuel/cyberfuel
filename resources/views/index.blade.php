@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com</title>
<meta name="keywords"
  content="cyberfuel, nombres de dominio, hospedaje web, alojamiento web, web hosting, servidores virtuales, centro de datos, factura electronica, costa rica, cr" />
<meta name="description"
  content="Cyberfuel ofrece Registro de Dominios, Alojamiento Web, correo electrónico, Comercio Electrónico, Factura Electrónica – todo en nuestro centro de datos en Costa Rica (CR) ">
@endsection
@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="¡Su sitio web en manos de verdaderos expertos!">
<meta property="og:title" content="Cyberfuel">
@endsection
@section('css')
<link rel="stylesheet" href="{{URL::asset('assets/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/owl.theme.default.min.css')}}">
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12 px-0 "> @include('includes.slider') </div>

  </div>
</div>
<section class="call-to-action call-to-action-primary mb-5 p-4">
  <div class="row justify-content-center w-100">
    <div class="col-md-6 col-lg-6">
      <div class="call-to-action-content align-items-center">
        <h2
          class="font-weight-bold text-6 mb-0 appear-animation animated fadeInUpShorter appear-animation-visible text-white pl-4 text-left"
          data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750"
          style="animation-delay: 200ms;">{{__('master.desde') }} ‪$15.99*‬/ {{__('master.ano') }}</h2>

      </div>
    </div>
    <div class="col-md-6 col-lg-6">
      <div class="row">
        <!--  <div class="col-12">
          <h2 class="card-title mb-1 text-4 text-white font-weight-bold text-center" data-plugin-animated-letters data-plugin-options="{'startDelay': 500, 'minWindowWidth': 0, 'animationName': 'typeWriter', 'animationSpeed': 50}">{{ __('compra.ingrese_dominio') }}</h2>
        </div>-->
        <div class="col-12">
          <div class="row justify-content-start">
            <div class="col-12 p-0">
              <div class="input-group ">
                <input type="text" id="nombre_dominio_pc" name="nombre_dominio_pc"
                  class="form-control  text-center border-color-success w-50" placeholder="{{ __('compra.encuentra') }}"
                  required=""> <br>
                <!--            <button onclick="consultar_dominio('{{ __('rutas.url_compra') }}', 'pc');" type="button"
              class="btn btn-success mostrardominio text-3 py-1 d-block"><i class="fas fa-search ms-1 "></i> </button> -->
                <button onclick="location.href = '{{ __('rutas.url_dominio') }}'" type="button"
                  class="btn btn-success mostrardominio text-3 py-1 d-block"><i class="fas fa-search ms-1 "></i>
                </button>

              </div>
            </div>
            <div class="col-12 p-0 pt-2 text-left">

              <div class="btn-group bg-white" role="group" aria-label="">
                <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
                <label class="btn btn-outline-success rounded-0" for="btnradio1">{{__('master.regis') }}</label>

                <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
                <label class="btn btn-outline-success rounded-0" for="btnradio2">{{__('master.transferir') }}</label>

                <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
                <label class="btn btn-outline-success rounded-0" for="btnradio3">{{__('master.utilizar') }}</label>
              </div>

            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
</section>


@include('includes.servicios')

<section
  class="section section-height-5 bg-primary border-0 pt-4 m-0 appear-animation animated fadeIn appear-animation-visible"
  data-appear-animation="fadeIn" style="animation-delay: 100ms;">
  <div id="productos" class="container-fluid mt-0">
    <div class="row pt-0 ">
      <div class="col">
        <h4 class="text-center text-6 text-white">{{ __('inicio.nuestros') }} {{ __('inicio.productos')
          }}</h4>
      </div>
    </div>
    <div class="divider divider-primary">
      <i class="fas fa-chevron-down"></i>
    </div>
    <div class="row mt-4 mt-lg-5">
      <div class="col col-md-10 offset-md-1">
        @include('includes.productos')
      </div>
    </div>

  </div>
</section>

<div class="pt-4">
  @include('includes.logos')
</div>
@endsection
@section('scripts')

@endsection