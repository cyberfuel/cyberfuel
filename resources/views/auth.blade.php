@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Registrese con nosotros</title>
<meta name="keywords"
  content="autentificacion, costa rica, cr, solución, maquinas, servidores, archivos, sitio seguro, seguridad" />
<meta name="description"
  content="Con nuestra solución puede tener un respaldo de su computador o de sus servidores virtuales o físicos (completo o parcial) fuera de su lugar de trabajo o habitación, garantizando la información segura ">
@endsection


@section('content')
<!--aca empieza el contenido del HTML-->
<hr class=" py-2">
<div role="main" class="main shop pb-4">
  <div class="container">
    <div class="card py-4">
      <div class="row g-0">
        <div class="col-lg-4"> <img src="{{URL::asset('assets/imagenes/auth.svg')}}"
            class="img-fluid rounded-start d-none d-lg-block" alt="autentificacion"> <img
            src="{{URL::asset('assets/imagenes/auth2.svg')}}" class="img-fluid rounded-start d-lg-none"
            alt="autentificacion"></div>
        <div class="col-lg-8">
          <div class="card-body">
            <h4 class="card-title mb-1 text-4 font-weight-bold text-primary">Doble Factor de Autentificación</h4>
            <div id="codigoAuth" class="d-block">
              <div class="row">
                <div class="col-2 col-md-1">
                  <input type="checkbox" id="customSwitch" name="customSwitch"
                    class="gdpe-input custom-checkbox-switch">
                </div>
                <div class="col-10 col-md-11 px-4">
                  <p class="card-text mb-2">&nbsp;Activar el doble factor de autentificación</p>
                </div>
              </div>
              <div id="eleccion">
                <p class="text-2">¿Prefieres hacerlo más tarde? <br>
                  <a href="https://app.planillaprofesional.com"
                    class="read-more text-color-primary font-weight-semibold">Lo haré luego <i
                      class="fas fa-angle-right position-relative top-1 ms-1"></i></a> <br>
                  <span class="read-more text-color-success font-weight-semibold">Solo</span><span
                    id="span_total_carrito_compras"
                    class="cart-qty badge badge-success badge-md rounded-pill text-uppercase px-2 py-1 mx-2">3</span><span
                    class="read-more text-color-success font-weight-semibold">oportunidades te quedan para hacerlo
                    luego</span>
                </p>
              </div>
              <div id="miDiv" class="d-none mt-4">
                <div class="row align-items-center">
                  <div class=" col-12 col-md-8">
                    <div class="input-group mb-3">
                      <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                        aria-expanded="false"><img src="{{URL::asset('assets/imagenes/blank.gif')}}"
                          class="flag flag-cr" alt="Cyberfuel" title="Cyberfuel" loading="lazy">&nbsp;+506</button>
                      <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#"><img src="{{URL::asset('assets/imagenes/blank.gif')}}"
                              class="flag flag-cr" alt="Cyberfuel" title="Cyberfuel" loading="lazy">&nbsp;+506</a> </li>
                        <li><a class="dropdown-item" href="#"><img src="{{URL::asset('assets/imagenes/blank.gif')}}"
                              class="flag flag-en" alt="Cyberfuel" title="Cyberfuel" loading="lazy">&nbsp;+1</a> </li>
                        <li><a class="dropdown-item" href="#"><img src="{{URL::asset('assets/imagenes/blank.gif')}}"
                              class="flag flag-ca" alt="Cyberfuel" title="Cyberfuel" loading="lazy">&nbsp;+1</a> </li>
                      </ul>
                      <input type="text" class="form-control" aria-label="Text input with dropdown button"
                        placeholder="8665 5087">
                      <button id="mostrarAuth"
                        class="btn btn-dark btn-modern w-100 text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-3 py-3 mt-4"
                        aria-label="Enviar código y activar 2F">Enviar código y activar 2F<i
                          class="fas fa-arrow-right ms-2"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div> </div>

            <!--/*----------------------------pantallas poner codigo------------------------*/-->
            <section id="colocarCodigo"
              class="call-to-action justify-content-center featured featured-primary mb-5 d-none p-4">
              <div class="col-12">
                <div class="alert alert-success py-4 w-100">
                  <p class="text-4 text-center py-0 mb-0">Se ha enviado un código de verificación al número de teléfono
                    terminado en &nbsp; <span class="text-primary">xxxxxx80</span> </p>
                </div>
              </div>
              <div class="col-sm-12">
                <form method="POST" id="frm-verificar" action="">
                  <input type="hidden" name="" value="">
                  <input type="hidden" name="_method" value="POST">
                  <div class="mb-3 text-center">
                    <label for="codigo_verificacion" class="mb-0 text-blue-m1 text-4">Código de Verificación</label>
                  </div>
                  <div class="input-group mb-3" id="inputs-container">
                    <input type="hidden" name="" id="" value="">
                    <input type="text" class="form-control auth text-center telefono" id="1" placeholder="X"
                      maxlength="1" required="required" autocomplete="off">
                    &nbsp;&nbsp;
                    <input type="text" class="form-control auth text-center telefono" id="2" placeholder="X"
                      maxlength="1" required="required" autocomplete="off">
                    &nbsp;&nbsp;
                    <input type="text" class="form-control auth text-center telefono" id="3" placeholder="X"
                      maxlength="1" required="required" autocomplete="off">
                    &nbsp;&nbsp;
                    <input type="text" class="form-control auth text-center telefono" id="4" placeholder="X"
                      maxlength="1" required="required" autocomplete="off">
                  </div>
                  <div class="mb-3 text-center"> <a href="https://app.planillaprofesional.com"
                      class="btn btn-outline btn-rounded btn-primary btn-with-arrow mb-2">Verificar<span><i
                          class="fas fa-chevron-right"></i></span></a> </div>
                </form>
              </div>
              <div class="col-sm-12">
                <p class="text-2 text-center m-auto"> ¿No has recibido el código? <a href="/"
                    class="read-more text-color-primary font-weight-semibold"> Reenviar código </a> </p>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
  // Cuando cambia el estado del switch
  $("#customSwitch").change(function(){
    // Si el switch está marcado
    if($(this).prop('checked')) {
      // Ocultar el div
      $("#eleccion").addClass('d-none');
      $("#miDiv").removeClass('d-none');
    } else {
      $("#eleccion").removeClass('d-none');
      $("#miDiv").addClass('d-none');
    }
  });

    
    $('#mostrarAuth').on("click", function() {
    $('#codigoAuth').addClass('d-none');
    $('#colocarCodigo').removeClass('d-none');
});
});
</script>
@endsection