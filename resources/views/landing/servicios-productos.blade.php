@extends('layout.landing', [
'header'=>'assets/imagenes/landing/servicios/header.webp',
'sector'=>'Servicios y Productos'
])


@section('meta')
<meta name="Description" content="Factura electrónica para el sector Asadas de Costa Rica">
<meta name="Keywords"
    content="factura electronica, factura electrónica, sector, asadas, agua, asociación, asociacion, medición, medidor, profesional">
@endsection
@section('face')
<meta property="og:url" content="https://www.facturaprofesional.com">
<meta property="og:image" content="{{ URL::asset('assets/imagenes/landing/servicios/servicios.webp') }}">
<meta property="og:description" content="Factura electrónica para el sector Asadas de Costa Rica">
<meta property="og:title" content="Factura Profesional | Sector Asadas">
@endsection
@section('content')
<div class="container-xxl">

    <div class="row mt-4">
        <div class="col-12 col-md-8">

            <!--   <h3 class="h4 text-info">Beneficios:</h3>
            <ul style=" list-style:none; " class="px-0">

                <li>
                    <i class="fas fa-check"></i> Control de Contratos y medidores
                </li>
                <li>
                    <i class="fas fa-check"></i> Administración de Tarifas Comerciales y Residenciales
                </li>
                <li>
                    <i class="fas fa-check"></i> Cálculo automatizado del IVA por consumo
                </li>
                <li>
                    <i class="fas fa-check"></i> PDF con historial de consumo
                </li>
                <li>
                    <i class="fas fa-check"></i> Llevar el control de Facturas generadas pendientes de pago o pagadas
                </li>
                <li>
                    <i class="fas fa-check"></i> Envío de facturas (documentos) por diferentes medios (WhatsApp,
                    Telegram o SMS)
                </li>
                <li>
                    <i class="fas fa-check"></i> Incluye reportes de ventas, gastos, borradores tributarios (D-104 IVA,
                    D-101 Renta,
                    D-125 Alquiler) y auditoría
                </li>
                <li>
                    <i class="fas fa-check"></i> Creación del usuario Contador, para que su Contador pueda ayudarle con
                    su
                    contabilidad
                </li>
                <li>
                    <i class="fas fa-check"></i> Recepción de facturas electrónicas
                </li>
                <li>
                    <i class="fas fa-check"></i> Soporte técnico las 24 horas
                </li>

            </ul>
            <h3 class="h4 text-info">Aplica para:</h3>
            <ul style=" list-style:none; " class="px-0">
                <li>
                    <i class="fas fa-check"></i> ASADAS
                </li>
                <li>
                    <i class="fas fa-check"></i> entre otros del sector...
                </li>
            </ul>-->
        </div>
    </div>
</div>

<section class="section section-height-5  border-0 pt-4 m-0 appear-animation animated fadeIn appear-animation-visible"
    data-appear-animation="fadeIn" style="animation-delay: 100ms;">
    <div id="productos" class="container-fluid mt-0">
        <div class="row pt-0 ">
            <div class="col">
                <h4 class="text-center text-6 text-primary">{{ __('inicio.nuestros') }} {{ __('inicio.productos')
                    }}</h4>
            </div>
        </div>
        <div class="divider divider-primary">
            <i class="fas fa-chevron-down"></i>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8"> @include('includes.productos')</div>

            <div class="col-12 col-lg-4">
                <div class="row">
                    <div class="col col-md-10 offset-md-1">

                        <a target="blank_" href="https://www.comprobanteselectronicoscr.com/"
                            class="btn btn-3d btn-primary rounded-0 mb-2 mt-4"> <img class="img-fluid"
                                src="{{ URL::asset('assets/imagenes/landing/servicios/servicios.webp') }}"
                                alt="Cyberfuel Servicios y Productos"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row align-items-center justify-content-center mt-5">
            <div class="col-6 pt-4">
                <form class="contact-form" id="contacto" name="contacto" action="{{ route('contactenos.send') }}"
                    method="POST" ovalidate="novalidate">
                    @csrf

                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label class="form-label mb-1 text-2" for="nombre">Nombre completo</label>
                            <input type="text" value="{{ old('nombre') }}" class="form-control text-3 h-auto py-2"
                                name="nombre" id="nombre">
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="form-label mb-1 text-2" for="empresa">Empresa</label>
                            <input type="text" value="{{ old('empresa') }}" class="form-control text-3 h-auto py-2"
                                name="empresa" id="empresa">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label class="form-label mb-1 text-2" for="telefono">Teléfono</label>
                            <input type="text" value="{{ old('telefono') }}" class="form-control text-3 h-auto py-2"
                                name="telefono" id="telefono">
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="form-label mb-1 text-2" for="email_contacto">Correo electrónico</label>
                            <input type="email" value="{{ old('email') }}" class="form-control text-3 h-auto py-2"
                                name="email" id="email_contacto">
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group col">
                            <button id="btnEnviar" type="submit" value="Enviar" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</section>


<?php /*?>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2">
            <h3 class="h4 text-info">Regístrese para más información</h3>
            @include('partial.form_contactenos', ['departamento'=>'ventas', 'codigo'=>'Formulario: Asadas',
            'render_errors'=>true, 'redirect'=>'landings/gracias_servicios'])
        </div>
    </div>
</div>
<?php */?>
@endsection