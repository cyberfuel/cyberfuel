@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com</title>
<meta name="keywords" content="cyberfuel, nombres de dominio, hospedaje web, alojamiento web, web hosting, servidores virtuales, centro de datos, factura electronica, costa rica, cr"/>
<meta name="description" content="Cyberfuel ofrece Registro de Dominios, Alojamiento Web, correo electrónico, Comercio Electrónico, Factura Electrónica – todo en nuestro centro de datos en Costa Rica (CR) ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.jpg')}}">
<meta property="og:description" content="¡Su sitio web en manos de verdaderos expertos!">
<meta property="og:title" content="Cyberfuel">
@endsection


@section('content') 

		<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-6  align-self-center p-static ">
							
								<div class="overflow-hidden ">
									<h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>{{ __('master.herramientas') }}</strong></h1>
								</div>
							</div>
						</div>
					</div>
				</section>

@if (Session::get('language') == 'es')

<div class="container">
  <div class="row pb-5">
    <div class="col-12">
      <div class="row">
        <div class="col-lg-4">
          <div class="tabs tabs-vertical tabs-left tabs-navigation">
            <ul class="nav nav-tabs col-sm-3">
              <li class="nav-item active"> <a class="nav-link active" href="#tabsNavigation1" data-bs-toggle="tab"><i class="fas fa-globe"></i> Acceso a Servicios</a> </li>
              <li class="nav-item"> <a class="nav-link" href="#tabsNavigation2" data-bs-toggle="tab"><i class="fas fa-tools"></i> Herramientas de Red</a> </li>
              <li class="nav-item"> <a class="nav-link" href="#tabsNavigation3" data-bs-toggle="tab"><i class="fas fa-download"></i> Descargas</a> </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8">
          <div class="tab-pane tab-pane-navigation active" id="tabsNavigation1">
            <section class="">
              <h4>Ingresa a tu Webmail</h4>
              <div class="row align-items-center ">
                <div class="col-10 col-sm-10 col-md-8 col-lg-8 px-1">
                  <input id="" type="text" name="" class="form-control  border-color-primary text-4 h-auto py-2" placeholder="midominioperfecto.com" required="">
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 px-1"> <a href="compra" type="submit" class="btn btn-primary mb-2 text-2 text-white   text-center mostrardominio"><i class="fas fa-arrow-square-right"></i> Ingresar</a> </div>
              </div>
            </section>
            <section class="pt-4">
              <h4>Ingresa a tu Panel de Control</h4>
              <div class="row align-items-center ">
                <div class="col-10 col-sm-10 col-md-8 col-lg-8 px-1">
                  <input id="" type="text" name="" class="form-control  border-color-primary text-4 h-auto py-2" placeholder="midominioperfecto.com" required="">
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 px-1"> <a href="compra" type="submit" class="btn btn-primary mb-2 text-2 text-white   text-center mostrardominio"><i class="fas fa-arrow-square-right"></i> Ingresar</a> </div>
				  
				  
              </div>
            </section>
          </div>
          <div class="tab-pane tab-pane-navigation" id="tabsNavigation2">
            <div class="col-12">
              <div class="table-responsive table-striped ">
               <h4 class="text-primary"><i class="fas fa-map-marker "></i> Su IP de salida a Internet es:</h4>
               <strong class="h2"> <?php  
                   $ip = $_SERVER['REMOTE_ADDR'];
                   echo $ip;?></strong>
                <button type="button" class="btn btn-primary  mb-2 ml-4">Copiar</button>
                <hr class="py-1">
                <h4 class="text-primary"><i class="fas fa-lock "></i> Verifique si su IP está bloqueada por SPAM:</h4>
                <a href="http://www.spamhaus.org/query/bl?ip=186.15.131.35" target="_blank" type="button" class="btn btn-primary text-white mb-2 ml-2">Verificar</a>
                <hr class="py-1">
                <p> </p>
                <h4 class="text-primary">Verificación puerto 25 (SMTP):</h4>
                <ul>
                  <li> Windows: De clic en el botón de Windows que está en la esquina inferior izquierda, digite <strong>cmd</strong> y enter, aparecerá una ventana "MS-DOS" o "Command Prompt"</li>
                  <li> MAC: Generalmente está su terminal en Mac HD-&gt;"Aplicaciones"-&gt;"Utilidades".  Abra el programa que se llama Terminal</li>
                  <li> Linux: Abra su terminal favorita</li>
                </ul>
                <p>Comandos a utilizar:</p>
                <p><strong>Telnet sudominio.com 25</strong></p>
                Si en la respuesta se indica "200 sudominio.com ESMTP", es que si hay conexión con el puerto 25 (de salida de correo electrónico).
                <p> </p>
                <h4 class="text-primary">Verificación puerto 110 (POP):</h4>
                <ul>
                  <li> Windows: De clic en el botón de Windows que está en la esquina inferior izquierda, digite <strong>cmd</strong> y enter, aparecerá una ventana "MS-DOS" o "Command Prompt"</li>
                  <li> MAC: Generalmente está su terminal en Mac HD-&gt;"Aplicaciones"-&gt;"Utilidades".  Abra el programa que se llama Terminal</li>
                  <li> Linux: Abra su terminal favorita</li>
                </ul>
                <p>Comandos a utilizar:</p>
                <p><strong>Telnet sudominio.com 110</strong></p>
                Si en la respuesta se indica "+OK Hello there.", es que si hay conexión con el puerto 110 (de recepción de correo electrónico).
                <p> </p>
              </div>
            </div>
          </div>
          <div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
            <div class="table-responsive table-striped "> <a name="soporte-remoto">&nbsp;</a>
              <h4 class="text-primary">Clientes de Accesso Remoto:</h4>
              <ul>
                <li> <a href="https://anydesk.com/en/downloads/thank-you?dv=win_exe" target="_blank" class="black">AnyDesk (Windows)</a></li>
                <li> <a href="https://anydesk.com/en/downloads/thank-you?dv=mac_dmg" target="_blank" class="black">AnyDesk (Mac)</a></li>
               
              </ul>
              <h4 class="text-primary">Navegadores:</h4>
              <ul>
                <li> <a href="https://www.mozilla.org/es-ES/firefox/new/" target="_blank" class="black">Mozilla FireFox</a></li>
                <li> <a href="https://www.google.com/chrome" target="_blank" class="black">Google Chrome</a></li>
                <li> <a href="http://www.opera.com/" target="_blank" class="black">Opera Browser</a></li>
              </ul>
              <p> </p>
              <h4 class="text-primary">Clientes de Correo:</h4>
              <ul>
                <li> <a href="https://www.mozilla.org/en-US/thunderbird/" target="_blank" class="black">Mozilla Thunderbird</a></li>
                <li> <a href="http://www.opera.com/computer/mail" target="_blank" class="black">Opera Mail</a></li>
              </ul>
              <p> </p>
              <h4 class="text-primary">Clientes de FTP:</h4>
              <ul>
                <li> <a href="https://filezilla-project.org/" target="_blank" class="black">FileZilla FTP</a></li>
                <li> <a href="http://www.wsftple.com/" target="_blank" class="black">WS FTP</a></li>
                <li> <a href="https://www.smartftp.com/" target="_blank" class="black">SmartFTP</a></li>
              </ul>
              <p> </p>
              <h4 class="text-primary">Clientes de SSH:</h4>
              <ul>
                <li> <a href="http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html" target="_blank" class="black">Putty SSH</a></li>
              </ul>
              <p> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        @else

<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="row">
        <div class="col-lg-4">
          <div class="tabs tabs-vertical tabs-left tabs-navigation">
            <ul class="nav nav-tabs col-sm-3">
              <li class="nav-item active"> <a class="nav-link active" href="#tabsNavigation1" data-bs-toggle="tab"><i class="fas fa-globe"></i> {{ __('master.acce_ser') }}</a> </li>
              <li class="nav-item"> <a class="nav-link" href="#tabsNavigation2" data-bs-toggle="tab"><i class="fas fa-tools"></i> {{ __('master.herra_re') }}</a> </li>
              <li class="nav-item"> <a class="nav-link" href="#tabsNavigation3" data-bs-toggle="tab"><i class="fas fa-download"></i> {{ __('master.descar') }}</a> </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8">
          <div class="tab-pane tab-pane-navigation active mb-4" id="tabsNavigation1">
            <section class="">
              <h4>{{ __('master.ingre_we') }}</h4>
              <div class="row align-items-center ">
                <div class="col-10 col-sm-10 col-md-8 col-lg-8 px-1">
                  <input id="" type="text" name="" class="form-control  border-color-primary text-4 h-auto py-2" placeholder="midominioperfecto.com" required="">
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 px-1"> <a href="compra" type="submit" class="btn btn-primary rounded-0 btn-3d  btn-l text-4   text-center mostrardominio"><i class="fas fa-arrow-square-right"></i> {{ __('master.ingre') }}</a> </div>
              </div>
            </section>
            <section class="pt-4">
              <h4>{{ __('master.ingre_pane') }}</h4>
              <div class="row align-items-center ">
                <div class="col-10 col-sm-10 col-md-8 col-lg-8 px-1">
                  <input id="" type="text" name="" class="form-control  border-color-primary text-4 h-auto py-2" placeholder="midominioperfecto.com" required="">
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 px-1"> <a href="compra" type="submit" class="btn btn-primary rounded-0 btn-3d  btn-l text-4   text-center mostrardominio"><i class="fas fa-arrow-square-right"></i> {{ __('master.ingre') }}</a> </div>
              </div>
            </section>
          </div>
          <div class="tab-pane tab-pane-navigation" id="tabsNavigation2">
            <div class="col-12">
              <div class="table-responsive table-striped ">
                <h4 class="text-primary"><i class="fas fa-map-marker "></i> {{ __('master.ip_sali') }}:</h4>
                <strong class="h2">186.15.131.35 </strong>
                <button type="button" class="btn btn-primary rounded-0 mb-2 ml-2">{{ __('master.copiar') }}</button>
                <hr class="py-1">
                <h4 class="text-primary"><i class="fas fa-lock "></i> {{ __('master.veri_ip') }}:</h4>
                <a href="http://www.spamhaus.org/query/bl?ip=186.15.131.35" target="_blank" type="button" class="btn btn-primary rounded-0 mb-2 ml-2">{{ __('master.verificar') }}</a>
                <hr class="py-1">
                <p> </p>
                <h4 class="text-primary">{{ __('master.veri_puer') }}:</h4>
                <ul>
					@if (Session::get('language') == 'es')
					  <li> Windows: De clic en el botón de Windows que está en la esquina inferior izquierda, digite <strong>cmd</strong> y enter, aparecerá una ventana "MS-DOS" o "Command Prompt"</li>
					  <li> MAC: Generalmente está su terminal en Mac HD-&gt;"Aplicaciones"-&gt;"Utilidades".  Abra el programa que se llama Terminal</li>
					  <li> Linux: Abra su terminal favorita</li>
					@else
					<li> Windows: Click on the Windows button in the lower left corner, type <strong> cmd </strong> and enter, an "MS-DOS" or "Command Prompt" window will appear </li>
					<li> MAC: Usually your terminal is on Mac HD-> "Applications" -> "Utilities". Open the program called Terminal </li>
					<li> Linux: Open your favorite terminal </li>
					@endif
				</ul>
				  
				  	@if (Session::get('language') == 'es')
				<p>Comandos a utilizar:</p>
                <p><strong>Telnet sudominio.com 25</strong></p>
               
                <p> Si en la respuesta se indica "200 sudominio.com ESMTP", es que si hay conexión con el puerto 25 (de salida de correo electrónico). </p>
                <h4 class="text-primary">Verificación puerto 110 (POP):</h4>
                <ul>
                  <li> Windows: De clic en el botón de Windows que está en la esquina inferior izquierda, digite <strong>cmd</strong> y enter, aparecerá una ventana "MS-DOS" o "Command Prompt"</li>
                  <li> MAC: Generalmente está su terminal en Mac HD-&gt;"Aplicaciones"-&gt;"Utilidades".  Abra el programa que se llama Terminal</li>
                  <li> Linux: Abra su terminal favorita</li>
                </ul>
                <p>Comandos a utilizar:</p>
                <p><strong>Telnet sudominio.com 110</strong></p>
               
                <p>  Si en la respuesta se indica "+OK Hello there.", es que si hay conexión con el puerto 110 (de recepción de correo electrónico).</p>
					@else
					
                <p> Commands to use: </p>
                <p> <strong> Telnet yourdomain.com 25 </strong> </p>
               
                <p> If the response indicates "200 yourdomain.com ESMTP", it means that there is a connection to port 25 (outgoing e-mail). </p>
                <h4 class = "text-primary"> Verification port 110 (POP): </h4>
                <ul>
                  <li> Windows: Click on the Windows button in the lower left corner, type <strong> cmd </strong> and enter, an "MS-DOS" or "Command Prompt" window will appear </li>
                  <li> MAC: Usually your terminal is on Mac HD-> "Applications" -> "Utilities". Open the program called Terminal </li>
                  <li> Linux: Open your favorite terminal </li>
                </ul>
                <p> Commands to use: </p>
                <p> <strong> Telnet yourdomain.com 110 </strong> </p>
               
                <p> If the response indicates "+ OK Hello there.", it means that there is a connection to port 110 (for receiving e-mail). </p>  
					@endif
            
              </div>
            </div>
          </div>
          <div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
            <div class="table-responsive table-striped "> <a name="soporte-remoto">&nbsp;</a>
              <h4 class="text-primary">{{ __('master.clien_acce') }}:</h4>
              <ul>
                <li> <a href="https://anydesk.com/en/downloads/thank-you?dv=win_exe" target="_blank" class="black">AnyDesk (Windows)</a></li>
                <li> <a href="https://anydesk.com/en/downloads/thank-you?dv=mac_dmg" target="_blank" class="black">AnyDesk (Mac)</a></li>
              </ul>
              <h4 class="text-primary">{{ __('master.naveg') }}:</h4>
              <ul>
                <li> <a href="https://www.mozilla.org/es-ES/firefox/new/" target="_blank" class="black">Mozilla FireFox</a></li>
                <li> <a href="https://www.google.com/chrome" target="_blank" class="black">Google Chrome</a></li>
                <li> <a href="https://support.apple.com/downloads/safari" target="_blank" class="black">Apple Safari</a></li>
                <li> <a href="http://www.opera.com/" target="_blank" class="black">Opera Browser</a></li>
              </ul>
              <p> </p>
              <h4 class="text-primary">{{ __('master.clien_co') }}:</h4>
              <ul>
                <li> <a href="https://www.mozilla.org/en-US/thunderbird/" target="_blank" class="black">Mozilla Thunderbird</a></li>
                <li> <a href="http://www.opera.com/computer/mail" target="_blank" class="black">Opera Mail</a></li>
              </ul>
              <p> </p>
              <h4 class="text-primary">{{ __('master.clien_ftp') }}:</h4>
              <ul>
                <li> <a href="https://filezilla-project.org/" target="_blank" class="black">FileZilla FTP</a></li>
                <li> <a href="http://www.wsftple.com/" target="_blank" class="black">WS FTP</a></li>
                <li> <a href="http://www.cuteftp.com/cuteftp/" target="_blank" class="black">CuteFTP</a></li>
                <li> <a href="https://www.smartftp.com/" target="_blank" class="black">SmartFTP</a></li>
              </ul>
              <p> </p>
              <h4 class="text-primary">{{ __('master.clien_ssh') }}:</h4>
              <ul>
                <li> <a href="http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html" target="_blank" class="black">Putty SSH</a></li>
                <li> <a href="https://winscp.net/eng/docs/free_ssh_client_for_windows" target="_blank" class="black">WinSCP</a></li>
              </ul>
              <p> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>        
       
        @endif

@endsection