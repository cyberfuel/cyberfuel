@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com</title>
<meta name="keywords"
	content="cyberfuel, nombres de dominio, hospedaje web, alojamiento web, web hosting, servidores virtuales, centro de datos, factura electronica, costa rica, cr" />
<meta name="description"
	content="Cyberfuel ofrece Registro de Dominios, Alojamiento Web, correo electrónico, Comercio Electrónico, Factura Electrónica – todo en nuestro centro de datos en Costa Rica (CR) ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.jpg')}}">
<meta property="og:description" content="¡Su sitio web en manos de verdaderos expertos!">
<meta property="og:title" content="Cyberfuel">
@endsection


@section('content')

<section
	class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8"
	style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
	<div class="container">
		<div class="row">
			<div class="col-12  align-self-center p-static ">

				<div class="overflow-hidden ">
					<h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter"
						data-appear-animation-delay="300"><strong>{{ __('inicio.acer_cyber') }}</strong></h1>
				</div>
			</div>
		</div>
	</div>
</section>
@if (Session::get('language') == 'es')
<div class="container">
	<div class="row my-4 text-justify">
		<div class="col"><span class="sub-title text-3 mt-4">Cyberfuel inició operaciones en 1997 ofreciendo los
				servicios de desarrollo e integración de Software y ofreciendo soluciones de Hospedaje o Alojamiento WEB
				para su sitio WEB y correo electrónico en la nube utilizando la Red Mundial de Internet. En el año 2001
				incorporamos nuestro Centro de Datos / Data Center a nuestros servicios ofreciendo un lugar seguro y de
				alta disponibilidad para colocar los equipos de nuestros clientes. Estamos ubicados en el Parque
				Empresarial FORUM 1 Santa Ana, donde tienen sus sedes compañías de renombre como GBM (An IBM Alliance
				Company), Procter & Gamble, Bolsa Nacional de Valores, ASSA compañía de seguros y Chiquita Brands, entre
				otros.
			</span></div>
	</div>

	<div class="divider divider-primary">
		<i class="fas fa-chevron-down"></i>
	</div>
	<div class="row">
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon bg-success">
					<i class="fas fa-eye"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">VISIÓN</h4>
					<p class="mb-4">Ser la empresa líder en brindar soluciones de servicios integrados de nueva
						generación en el mercado nacional e internacional, mediante soluciones y modelos de negocios
						innovadores, servicios de excelencia y bajo una alta eficiencia operativa de nuestro recurso
						humano capacitado. </p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon bg-success">
					<i class="fas fa-flag"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">MISIÓN</h4>
					<p class="mb-4">Proveer servicios de calidad en el campo de las telecomunicaciones y la tecnología
						de la información en pro de la plena satisfacción de nuestros clientes, con una integración
						eficiente del recurso humano, tecnología, organización y los aliados estratégicos.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box dark">
				<div class="feature-box-icon bg-success">
					<i class="fas fa-user"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">VALORES</h4>
					<p class="">

					<ul class="list list-icons list-icons-style-3 ">
						<li class="pt-0"><i class="fas fa-check"></i> Conducta ética: Integridad, honestidad, compromiso
							y responsabilidad</li>
						<li class="pt-0"><i class="fas fa-check"></i> Satisfacción de nuestros clientes</li>
						<li class="pt-0"><i class="fas fa-check"></i> Orientación al logro, la calidad y la seguridad
						</li>
						<li class="pt-0"><i class="fas fa-check"></i> Trabajo en equipo</li>
						<li class="pt-0"><i class="fas fa-check"></i> Responsabilidad Social: Con nuestros
							colaboradores, la comunidad y el medio ambiente </li>
					</ul>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon bg-success">
					<i class="fas fa-briefcase"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">¿QUIÉNES SON NUESTROS CLIENTES?</h4>
					<p class="mb-4">Aquellas personas y empresas que requieran servicios en Internet, tales como:
						Hospedaje WEB, Comercio Electrónico, Soluciones WEB, DataCenter, Desarrollo de Aplicaciones y
						demás servicios que brindamos.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon bg-success">
					<i class="fas fa-bullseye"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">OBJETIVOS LOGRADOS</h4>
					<p class="">
					<ul class="list list-icons list-icons-style-3 ">
						<li class="pt-0"><i class="fas fa-check"></i> Personal capacitado</li>
						<li class="pt-0"><i class="fas fa-check"></i>Centro de Datos / Data Center de Clase Mundial,
							certificación ANSI/TIA-942 Rated 3</li>
						<li class="pt-0"><i class="fas fa-check"></i>Implementar cambios para reducir nuestro impacto
							con el medio ambiente</li>
						<li class="pt-0"><i class="fas fa-check"></i>Brindar soluciones WEB</li>
						<li class="pt-0"><i class="fas fa-check"></i>Ser Socios de negocios de empresas internacionales,
							tales como: Network Solutions, Plesk, Cpanel, VMware, Microsoft, Sectigo, entre otras.</li>
						<li class="pt-0"><i class="fas fa-check"></i>Lograr la Bandera Azul</li>



					</ul>
					</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box dark">
				<div class="feature-box-icon bg-success">
					<i class="far fa-dot-circle"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">NUEVOS OBJETIVOS</h4>
					<p class="mb-4">
					<ul class="list list-icons list-icons-style-3 ">
						<li class="pt-0"><i class="fas fa-check"></i> Mantener y mejorar la tecnología de punta a
							nuestros servicios</li>
						<li class="pt-0"><i class="fas fa-check"></i>Brindar a nuestros clientes nuevas soluciones WEB
						</li>
						<li class="pt-0"><i class="fas fa-check"></i>Realizar alianzas a nivel Nacional e Internacional
							para ampliar nuestros servicios</li>
						<li class="pt-0"><i class="fas fa-check"></i>Continuar con la implementación de cambios para
							reducir nuestro impacto con el medio ambiente </li>

					</ul>

					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@else



<div class="container">

	<div class="row my-4 text-justify">
		<div class="col">
			<p><span class="sub-title text-3 mt-4">Cyberfuel began operations in 1997 offering Software development and
					integration services and offering Hosting or WEB Hosting solutions for your WEB site and email in
					the cloud using the World Wide Web. In 2001 we incorporated our Data Center / Data Center to our
					services, offering a safe and highly available place to place our clients' equipment. We are located
					in FORUM 1 Santa Ana Business Park, where renowned companies such as GBM (An IBM Alliance
					Company), Procter & Gamble, National Stock Exchange, ASSA insurance company and Chiquita Brands,
					among others, have their headquarters.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon">
					<i class="fas fa-eye"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">OUR VISION</h4>
					<p class="mb-4">To be the lead company offering solutions of integrated services of new generation
						in the national and international market, giving solutions and models of innovating businesses,
						services of excellence and under a high operative efficiency of our enabled human resource.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon">
					<i class="fas fa-flag"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">OUR MISSION</h4>
					<p class="mb-4">To provide services in the field of the telecommunications and the technology of the
						information with high quality levels and stability for the total satisfaction of our clients,
						supported in the efficient integration of our human resource, the strategic technology,
						organization and allies. </p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box dark">
				<div class="feature-box-icon">
					<i class="fas fa-user"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">VALUES</h4>
					<p class="">

					<ul class="list list-icons list-icons-style-3 ">


						<li class="pt-0"><i class="fas fa-check"></i> Ethical conduct: Integrity, honesty, commitment
							and responsibility</li>
						<li class="pt-0"><i class="fas fa-check"></i> Satisfaction of our customers</li>
						<li class="pt-0"><i class="fas fa-check"></i> Orientation towards achievement, quality and
							safety</li>
						<li class="pt-0"><i class="fas fa-check"></i> Teamwork</li>
						<li class="pt-0"><i class="fas fa-check"></i> Social Responsibility: With our collaborators, the
							community and the environment </li>
					</ul>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon">
					<i class="fas fa-briefcase"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">¿HOW ARE OURS CUSTOMERS?</h4>
					<p class="mb-4">People, Small companies, Companies and Corporations that need a quality and reliable
						services on Internet, like: Web Hosting, E-commerce, WEB Solutions, Data Center, Web Development
						& Integration and others services that we offer.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box">
				<div class="feature-box-icon">
					<i class="fas fa-check"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">ACHIEVED OBJECTIVES</h4>
					<p class="">
					<ul class="list list-icons list-icons-style-3 ">
						<li class="pt-0"><i class="fas fa-check"></i>Trained Personal</li>
						<li class="pt-0"><i class="fas fa-check"></i>World class Data Center</li>
						<li class="pt-0"><i class="fas fa-check"></i>Reduce our impact with the environment</li>
						<li class="pt-0"><i class="fas fa-check"></i>Offer WEB Solutions</li>
						<li class="pt-0"><i class="fas fa-check"></i>Became partners of some International Companies,
							like: Network Solutions, Plesk, CPanel, VMware, Microsoft, Sectigo and others.</li>
						<li class="pt-0"><i class="fas fa-check"></i>Costa Rica Blue Flag Environment program</li>
					</ul>
					</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 appear-animation animated fadeInLeftShorter appear-animation-visible"
			data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
			<div class="feature-box dark">
				<div class="feature-box-icon">
					<i class="far fa-dot-circle"></i>
				</div>
				<div class="feature-box-info">
					<h4 class="mb-2">NEW GOALS</h4>
					<p class="mb-4">
					<ul class="list list-icons list-icons-style-3 ">

						<li class="pt-0"><i class="fas fa-check"></i>Maintain and improve the high technology of our
							services</li>
						<li class="pt-0"><i class="fas fa-check"></i>Continue with the development of new WEB Solutions
						</li>
						<li class="pt-0"><i class="fas fa-check"></i>Make new national and international alliances, to
							expand our services</li>
						<li class="pt-0"><i class="fas fa-check"></i>Continuing with the implementation of changes to
							reduce our impact with the environment</li>
					</ul>

					</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endif

@endsection