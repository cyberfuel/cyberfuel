@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com</title>
<meta name="keywords"
	content="cyberfuel, nombres de dominio, hospedaje web, alojamiento web, web hosting, servidores virtuales, centro de datos, factura electronica, costa rica, cr" />
<meta name="description"
	content="Cyberfuel ofrece Registro de Dominios, Alojamiento Web, correo electrónico, Comercio Electrónico, Factura Electrónica – todo en nuestro centro de datos en Costa Rica (CR) ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.jpg')}}">
<meta property="og:description" content="¡Su sitio web en manos de verdaderos expertos!">
<meta property="og:title" content="Cyberfuel">
@endsection
@section('css')
<style>
	td:empty {
		width: 100%;
	}
</style>
@endsection

@section('content')

<div class="card-body">
	<div role="main" class="main shop">
		<div class="container-xxl">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-12">
							<div class="alert alert-primary">
								<h4 class="font-weight-bold text-uppercase text-4  text-white mb-0">
									{{ __('compra.cantidad_articulos') }}
									<span class="pl-4">
										<a href="" class="header-nav-features-toggle">
											<i class="fas fa-shopping-basket text-white"></i>
											<span
												class="cart-qty badge badge-dark badge-md rounded-pill text-uppercase px-2 py-1">
												3
											</span>
										</a>
									</span>
								</h4>
							</div>
							<form method="post" action="">

								<div id="carrito_responsivo">
									<table>

										<thead>
											<tr>
												<th class="font-weight-bold text-primary text-1-2" scope="col">{{
													__('compra.producto') }}</th>
												<th class="font-weight-bold text-primary text-1-2" scope="col">Cantidad
												</th>
												<th class="font-weight-bold text-primary text-1-2" scope="col">{{
													__('compra.precio') }}</th>

												<th class="font-weight-bold text-primary text-1-2" scope="col">Total
												</th>
												<th scope="col"></th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td data-label="{{ __('compra.producto') }}">Certificado Ninja
												</td>
												<td data-label="Cantidad">1</td>
												<td class="font-weight-bold text-success h6"
													data-label="{{ __('compra.precio') }}">
													$1.99</td>

												<td class="font-weight-bold text-success h6" data-label="Total">
													$4.10</td>
												<td data-label="">
													<span>&nbsp;</span>
													<span class=" quitar_producto">
														<a class="p-1" title="Quitar Producto" style="cursor: pointer;">
															<i class="fas fa-trash-alt text-5 text-dark"></i>
														</a>
													</span>
												</td>

											</tr>
											<tr>
												<td scope="row" data-label="{{ __('compra.producto') }}">midominio.com
												</td>
												<td data-label="Cantidad">1</td>
												<td class="font-weight-bold text-success h6"
													data-label="{{ __('compra.precio') }}">
													$12.99</td>

												<td class="font-weight-bold text-success h6" data-label="Total">
													$20.10</td>
												<td data-label="">
													<span>&nbsp;</span>
													<span class=" quitar_producto">
														<a class="p-1" title="Quitar Producto" style="cursor: pointer;">
															<i class="fas fa-trash-alt text-5 text-dark"></i>
														</a>
													</span>
												</td>
											</tr>
											<tr>
												<td scope="row" data-label="{{ __('compra.producto') }}">Hospedaje web
												</td>
												<td data-label="Cantidad">1</td>
												<td class="font-weight-bold text-success h6"
													data-label="{{ __('compra.precio') }}">
													$40.99</td>

												<td class="font-weight-bold text-success h6" data-label="Total">
													$49.10</td>
												<td data-label="">
													<span>&nbsp;</span>
													<span class=" quitar_producto">
														<a class="p-1" title="Quitar Producto" style="cursor: pointer;">
															<i class="fas fa-trash-alt text-5 text-dark"></i>
														</a>
													</span>
												</td>
											</tr>



											<tr class="esconder">
												<td colspan="1" scope="row">
													<span>&nbsp;</span>
												</td>

												<td>
													<span>&nbsp;</span>
												</td>
												<td>
													<span>&nbsp;</span>
												</td>
												<td class="font-weight-bold text-4">
													Total en dólares: $73.30*
												</td>


												<td>
													<span></span>
												</td>
											</tr>

											<tr class="d-md-none">
												<td class="text-4">Total en dólares: $73.30*</td>
											</tr>

										</tbody>
									</table>


								</div>
							</form>
						</div>
					</div>


				</div>
			</div>
			<div class="row justify-content-end mt-lg-0">
				<div class="col">
					<p class="text-2 text-dark text-end py-0 mt-2">
						@if (Session::get('language') == 'es')
						*Para Costa Rica, a nuestros precios se le debe agregar el IVA (Impuesto de
						Valor Agregado)
						@else
						*For Costa Rica, must be added to our prices to IVA Tax.
						@endif
					</p>
				</div>
			</div>

			<div class="row justify-content-end mt-4 mt-lg-0">
				<div class=" col-md-6 col-lg-3">
					<a href="{{ __('rutas.url_nuevo_cliente') }}"
						class="btn btn-dark btn-modern w-100 text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-3 py-3">{{
						__('master.continuar') }} <i class="fas fa-arrow-right ms-2"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>




@endsection

@section('scripts')

<script>
	$(document).ready(function(){
    // Agrega un evento de clic a todos los botones con la clase "eliminarFila"
    $(".quitar_producto").click(function(){
      // Encuentra el padre <tr> del botón actual y lo elimina
      $(this).closest('tr').remove();
    });
  });
</script>

@endsection