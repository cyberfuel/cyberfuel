@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com</title>
<meta name="keywords" content="cyberfuel, nombres de dominio, hospedaje web, alojamiento web, web hosting, servidores virtuales, centro de datos, factura electronica, costa rica, cr"/>
<meta name="description" content="Cyberfuel ofrece Registro de Dominios, Alojamiento Web, correo electrónico, Comercio Electrónico, Factura Electrónica – todo en nuestro centro de datos en Costa Rica (CR) ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.jpg')}}">
<meta property="og:description" content="¡Su sitio web en manos de verdaderos expertos!">
<meta property="og:title" content="Cyberfuel">
@endsection


@section('content') 
  <hr class="py-1">
@if (Session::get('language') == 'es')
        
     <div class="container"> 
      
			<div class="row">
		     <div class="col-md-12 block-content-general">
          <h2 class="text-success"><i class="fas fa-book "></i> Política Anti-Spam</h2>
         </div>
		 </div>
	
       <div class="row">

          
           <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
             <h4>Política ANTI-SPAM de Cyberfuel</h4>
             <p class="text-justify">Cyberfuel no tolera la transmisión de correo no deseado. Hacemos un seguimiento de todo el tráfico hacia y desde nuestros servidores web, de las indicaciones de spam y mantener un centro de abuso de spam compatible para registrar las denuncias de abuso de spam. Los clientes sospechosos de estar usando productos de Cyberfuel y servicios con el fin de enviar spam se investiguen exhaustivamente. Una vez que Cyberfuel determina que hay un problema con el spam, Cyberfuel tomará las medidas apropiadas para resolver la situación. Nuestro centro de abuso de spam compatible se puede llegar por correo electrónico a <a class="btn-link" href="mailto:abuse@cyberfuel.com">abuse@cyberfuel.com</a>.</p>
             
             <h5> ¿Cómo Definir Spam?</h5>
             <p class="text-justify">Se define el spam como el envío de mensajes comerciales no solicitados (con sus siglas en inglés "UCE" Unsolicited Commercial Email), correo electrónico no solicitado a granel (con sus siglas en inglés "UBE" Unsolicited Buld Email) o no solicitados facsímiles (fax), que es el correo electrónico o fax enviado a los destinatarios como un anuncio o no, sin antes obtener el consentimiento previo confirmado para recibir estos comunicaciones del remitente. Esto puede incluir, pero no limitado a, los siguientes:</p>
             <ol>
             	<li>Mensajes de correo electrónico</li>
                <li>Grupos de noticias</li>
                <li>Mensajes del sistema de Windows</li>
                <li>Mensajes pop-up (también conocido como "adware" o mensajes de "spyware")</li>
                <li>Los mensajes instantáneos (con AOL, MSN, Yahoo u otros programas de mensajería instantánea)</li>
                <li>Sala de chat en línea anuncios</li>
                <li>Libro de visitas o mediante su publicación Foros</li>
                <li>Facsímil Solicitaciones</li>
             </ol>

             <h5>Lo que aprobamos y no permiten</h5>
             <p class="text-justify">No permitiremos que nuestros servidores y servicios que se utilizarán para los fines descritos anteriormente. Para poder utilizar nuestros productos y servicios, no sólo debe cumplir con todas las leyes y reglamentos aplicables, que incluyen la Ley Can-Spam del 2003 y la Telephone Consumer Protection Act, pero también debe cumplir con las políticas de spam de Cyberfuel. Correos electrónicos de publicidad comercial y/o a granel o faxes sólo se pueden enviar a destinatarios que ya han "optado por" recibir mensajes del remitente en particular. Deben incluir una dirección de retorno legítimo y la dirección de respuesta, la dirección física del remitente, y optar por el método de eliminación en el pie de página del correo electrónico o fax. A petición de Cyberfuel, una prueba concluyente de opt-in pueden ser necesarios para una dirección de correo electrónico o número de fax.</p>
             
             
             <h5>¿Qué hacemos?</h5>
             <p class="text-justify">Si Cyberfuel determina los servicios en cuestión están siendo utilizadas en asociación con el spam, Cyberfuel volverá a dirigir, suspender o cancelar cualquier sitio de alojamiento web, registro de dominios, buzones de correo electrónico u otros servicios aplicables por un período no menor de dos ( 2) días. El solicitante de registro o el cliente tendrá que responder por correo electrónico a Cyberfuel  indicando que dejará de enviar spam y/o spam enviado en su nombre. Cyberfuel tendrá un cargo de reactivación no reembolsable a pagar antes de que el sitio, buzones de correo electrónico y/o servicios se reactivan. En el caso de Cyberfuel determina el abuso no se ha detenido después de los servicios han sido restauradas por primera vez, Cyberfuel puede terminar los servicios de hosting y correo electrónico asociada con el nombre de dominio en cuestión. Para reportar abuso de spam: Animamos a todos los clientes y destinatarios de correo electrónico generados por nuestros productos y servicios para reportar spam sospechoso. La sospecha de abuso puede ser reportado por correo electrónico a nuestro de departamento de servicio al cliente, mediante el correo electrónico: <a class="btn-link" href="mailto:abuse@cyberfuel.com">abuse@cyberfuel.com</a>.</p>
             
            
             
             <p class="text-justify">Cyberfuel se ha comprometido a hacer de Internet un lugar más seguro.</p>
             
             <p class="text-justify">En cumplimiento de este compromiso, hemos unido fuerzas con diferentes proveedores y ISP para la utilización del listas de bloqueo, detección, IP's de posibles correos no deseados y virus con el fin de ofrecerle a nuestros clientes mejores servicios de detección, ofreciendo un servicio adicional de filtrado de correo no deseado.</p>

             <small>
             	Revisado: 28/Enero/2014<br>
                Copyright © 1997-<?php echo date("Y");?> Cyberfuel.com. Todos los derechos reservados.<br><br><br>
             </small>
             
           </div>
      
      
       </div>
       <!-- /.row --> 
     </div>
        
        @else
<div class="container">
        	<div class="row">
		     <div class="col-md-12 block-content-general">
          <h2 class="text-success"><i class="fas fa-book "></i> Anti-Spam Policy</h2>
         </div>
		 </div>
         

<div class="row text-justify">

           <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
             <h4>Cyberfuel ANTI-SPAM Policy</h4>
             <p class="text-justify">Cyberfuel does not tolerate the transmission of spam. We  monitor all traffic to and from our web servers for indications of spamming and  maintain a spam abuse compliant center to register allegations of spam abuse.  Customers suspected to be using Cyberfuel products and services for the purpose  of sending spam are fully investigated. Once Cyberfuel determines there is a  problem with spam, Cyberfuel will take the appropriate action to resolve the  situation. Our spam abuse compliant center can be reached by email at <a href="mailto:abuse@cyberfuel.com">abuse@cyberfuel.com</a>.</p>
             
             <h5> How We Define Spam</h5>
             <p class="text-justify">We define spam as the sending of Unsolicited Commercial  Email (UCE), Unsolicited Bulk Email (UBE) or Unsolicited Facsimiles (Fax),  which is email or facsimile sent to recipients as an advertisement or  otherwise, without first obtaining prior confirmed consent to receive these  communications from the sender. This can include, but is not limited to, the  following:</p>
             <ol>
             	<li>Email  Messages</li>
                <li>Newsgroup  postings</li>
                <li>Windows  system messages</li>
                <li>Pop-up  messages (aka "adware" or "spyware" messages)</li>
                <li>Instant  messages (using AOL, MSN, Yahoo or other instant messenger programs)</li>
                <li>Online chat room advertisements</li>
                <li>Guestbook  or Website Forum postings</li>
                <li>Facsimile  Solicitations</li>
             </ol>

             <h5>What We Allow and  Don't Allow</h5>
             <p class="text-justify">We will not allow our servers and services to be used for  the purposes described above. In order to use our products and services, you  must not only abide by all applicable laws and regulations, which include the  Can-Spam Act of 2003 and the Telephone Consumer Protection Act, but you must  also abide by Cyberfuel's no spam policies. Commercial advertising and/or bulk  emails or faxes may only be sent to recipients who have already  "opted-in" to receive messages from the sender specifically. They  must include a legitimate return address and reply-to address, the sender's  physical address, and an opt-out method in the footer of the email or fax. Upon  request by Cyberfuel, conclusive proof of opt-in may be required for an email  address or fax number.</p>
             
             
             <h5>What We Do</h5>
             <p class="text-justify">If Cyberfuel determines the services in question are being  used in association with spam, Cyberfuel will re-direct, suspend, or cancel any  web site hosting, domain registration, email boxes or other applicable services  for a period of no less than two (2) days. The registrant or customer will be  required to respond by email to Cyberfuel stating that they will cease to send  spam and/or have spam sent on their behalf. Cyberfuel will require a  non-refundable reactivation fee to be paid before the site, email boxes and/or  services are reactivated. In the event Cyberfuel determines the abuse has not  stopped after services have been restored the first time, Cyberfuel may  terminate the hosting and email boxes associated with domain name in question.  To Report Spam Abuse: We encourage all customers and recipients of email  generated from our products and services to report suspected spam. Suspected  abuse can be reported by email: <a href="mailto:abuse@cyberfuel.com">abuse@cyberfuel.com</a>.</p>
                                     
             <p class="text-justify">Cyberfuel is committed to making the Internet a safer place</p>
             
             <p class="text-justify">In furtherance of this commitment, we have joined forces  with the different providers and ISP to implement Block list, detection, IP's  of spam and virus &nbsp;servers; to offer our  customers better detection services and offering an additional spam filtering  service. </p>

             <small>
             	Revised: 28/January/2014<br>
               <p> Copyright © 1997-<?php echo date("Y");?> Cyberfuel. Derechos reservados.</p>
             </small>
             
           </div>
        
         </div>
</div>
        @endif


     
                                       
                                       

@endsection