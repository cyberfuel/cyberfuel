@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com</title>
<meta name="keywords" content="cyberfuel, nombres de dominio, hospedaje web, alojamiento web, web hosting, servidores virtuales, centro de datos, factura electronica, costa rica, cr"/>
<meta name="description" content="Cyberfuel ofrece Registro de Dominios, Alojamiento Web, correo electrónico, Comercio Electrónico, Factura Electrónica – todo en nuestro centro de datos en Costa Rica (CR) ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.jpg')}}">
<meta property="og:description" content="¡Su sitio web en manos de verdaderos expertos!">
<meta property="og:title" content="Cyberfuel">
@endsection

@section('content')

	<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-6  align-self-center p-static ">
							
								<div class="overflow-hidden ">
									<h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>{{ __('master.mapa_sitio') }}</strong></h1>
								</div>
							</div>
						</div>
					</div>
				</section>
<hr class="py-1">
<div class="container py-4">
    
    <div class="row">
     <div class="col-md-4">
        <h3 class="font-weight-bold text-4 mb-2">{{ __('master.inicio') }} , {{ __('master.nombre_dominio') }} &  {{ __('master.hospedaje_web') }}</h3>
            <ul class="list list-icons list-icons-sm mb-4">
        <li><a href="{{ url('/') }}"><i class="far fa-file"></i>{{ __('master.inicio') }}</a></li>
        <li><a href=""><i class="far fa-file"></i>{{ __('master.nombre_dominio') }}</a></li>
         <li><a href="{{ __('rutas.url_hospedaje') }}"><i class="far fa-file"></i>{{ __('master.planes_hospedaje') }} </a></li>
                </ul>
     </div>
        
      <div class="col-md-4">
             <h3 class="font-weight-bold text-4 mb-2">
          {{ __('master.servicios_nube') }}
          </h3>
            <ul class="list list-icons list-icons-sm mb-4">
			 <li> <a  href="{{ __('rutas.url_servicios_en_nube') }}"><i class="far fa-file"></i> {{ __('master.servicios_nube') }}</a> </li>
             <li><a href="{{ __('rutas.url_seguridad') }}"><i class="far fa-file"></i> {{ __('master.seguridad') }} </a></li>
            <li><a href="{{ __('rutas.url_servidores') }}"><i class="far fa-file"></i> {{ __('master.servidores') }} </a></li>
            <li><a href="{{ __('rutas.url_almacenamiento') }}"><i class="far fa-file"></i> {{ __('master.almacenamiento') }} </a></li>
            <li><a href="{{ url('baas') }}"><i class="far fa-file"></i> {{
                              __('master.respaldo_servicio') }} (BaaS) </a></li>
			</ul>
     </div>
        
      <div class="col-md-4">
          <h3 class="font-weight-bold text-4 mb-2">
          {{ __('master.centro_datos') }} & Marketplace
          </h3>
          
           <ul class="list list-icons list-icons-sm mb-4">
			   <li><a href="{{ __('rutas.url_centro') }}"><i class="far fa-file"></i> {{ __('master.centro_datos') }}</a></li>
               <li><a href="{{ __('rutas.url_mercado') }}"><i class="far fa-file"></i> Marketplace</a></li>
			</ul>
     </div>
    </div>
  <div class="row">

    <div class="col-md-4">
      <h3 class="font-weight-bold text-4 mb-2">{{ __('master.acerca_nosotros') }}</h3>
      <ul class="list list-icons list-icons-sm mb-4">
        <li>
          <ul class="list list-icons list-icons-sm mb-3 text-2">
            <li><a href="{{ __('rutas.url_acerca-de-cyberfuel') }}"><i class="far fa-file"></i>{{ __('master.acerca_cyber') }}</a></li>
            <li><a href="{{ __('rutas.url_teminos-condiciones') }}"><i class="far fa-file"></i>{{ __('master.ter_cond') }}</a></li>
            <li><a href="{{ __('rutas.url_politica-de-privacidad') }}"><i class="far fa-file"></i>{{ __('master.politica_priva') }}</a></li>
            <li><a href="{{ __('rutas.url_politica-anti-spam') }}"><i class="far fa-file"></i>{{ __('master.politica_spam') }}</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="col-md-4">
      <h3 class="font-weight-bold text-4 mb-2">Centro de Asistencia</h3>
      <ul class="list list-icons list-icons-sm mb-4">
        <li>
          <ul class="list list-icons list-icons-sm mb-3 text-2">
            <li><a href="{{ __('rutas.url_contactenos') }}"><i class="far fa-file"></i>{{ __('master.contactenos') }}</a></li>
            <li><a target="_blank" href="http://ticket.cyberfuel.com/open.php"><i class="far fa-file"></i>{{ __('master.crear_tiq') }}</a></li>
            <li><a target="_blank" href="http://ticket.cyberfuel.com/view.php"><i class="far fa-file"></i>{{ __('master.historial_tique') }}</a></li>
            <li><a href="{{ __('rutas.url_herramientas') }}"><i class="far fa-file"></i>{{ __('master.herramientas') }}</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="col-md-4">
      <h3 class="font-weight-bold text-4 mb-2">Recursos</h3>
      <ul class="list list-icons list-icons-sm mb-4">
        <li>
          <ul class="list list-icons list-icons-sm mb-3 text-2">
            <li><a href=""><i class="far fa-file"></i>{{ __('master.mi_cuenta') }}</a></li>
            <li><a href="{{ __('rutas.url_carrito-de-compra') }}"><i class="far fa-file"></i>{{ __('master.carrito_compras') }}</a></li>
            <li><a href="{{ __('rutas.url_mapa-del-sitio') }}"><i class="far fa-file"></i>{{ __('master.mapa_sitio') }}</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>
@endsection