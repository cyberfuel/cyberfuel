@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com</title>
<meta name="keywords" content="cyberfuel, nombres de dominio, hospedaje web, alojamiento web, web hosting, servidores virtuales, centro de datos, factura electronica, costa rica, cr"/>
<meta name="description" content="Cyberfuel ofrece Registro de Dominios, Alojamiento Web, correo electrónico, Comercio Electrónico, Factura Electrónica – todo en nuestro centro de datos en Costa Rica (CR) ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.jpg')}}">
<meta property="og:description" content="¡Su sitio web en manos de verdaderos expertos!">
<meta property="og:title" content="Cyberfuel">
@endsection


@section('content') 
  <hr class="py-1">
@if (Session::get('language') == 'es')
        
     <div class="container"> 
       	<div class="row">
		     <div class="col-md-12 block-content-general">
          <h2 class="text-success"><i class="fas fa-book "></i> Política de Privacidad</h2>
         </div>
		 </div>
         
		
	
       <div class="row">
   
           <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-justify">
             <h4>Política de Cyberfuel de Privacidad, Seguridad, de su información</h4>
             <h5>Cyberfuel  respeta su privacidad</h5>
             <p>Nuestro sitio web como nuestro personal mantienen contratos de privacidad de los datos, según las diferentes normativas, recopilación y uso del mismo.  Le invitamos a leer nuestra política de seguridad y privacidad con el fin de que usted esté bien informado de la misma. </p>
             <h5>Recopilación y uso de nuestro sitio web</h5>
             <p>Nuestro sitio utiliza formularios en que usted nos proporciona información de contacto (incluyendo su nombre, dirección, número de teléfono y dirección de correo electrónico) para que pueda realizar pedidos, solicitar información y apoyo, y hacer sugerencias de productos. Para algunos servicios, también puede solicitar una tarjeta de crédito número, número de identificación, u otra información financiera. A continuación, creará un número de cuenta para usted.</p>
             <p>Nosotros recibimos y almacenamos cualquier información que usted ingrese en nuestro sitio o nos da de otra manera, incluso a través de correo electrónico, teléfono u otras comunicaciones con nuestro departamento de servicio al cliente. Si usted nos contacto para el soporte, vamos a mantener un registro interno de lo que se apoyó.</p>
             <p>Utilizamos su información para contactarlo con respecto a los cambios de funcionalidad de nuestros productos, nuestro sitio, nuevos servicios de Cyberfuel, y ofertas especiales que creemos le será valiosa. Si usted no desea recibir esta información, por favor consulte la sección "Cómo actualizar su información" a continuación sobre cómo cambiar sus preferencias.</p>
             <p>También podemos utilizar su información para presentar una oferta de marca compartida con nuestros socios o afiliados. Si recopilamos información de usted en relación con una oferta de marca conjunta, quedará claro en el punto de recogida que se está recogiendo la información y cuya declaración de privacidad se aplica.</p>
             <p>Utilizamos la información recopilada sobre usted de nuestras estadísticas de la web (por ejemplo, su dirección IP) para ayudar a diagnosticar problemas con nuestro servidor y para administrar nuestro sitio. También recopilamos información demográfica de estos datos para ayudarnos a mejorar nuestro sitio y hacer que su navegación y experiencia de compra más agradable. Esto no está vinculado a ninguna información de identificación personal, excepto cuando sea necesario para prevenir el fraude o abuso en nuestro sistema. </p>
             <p>Nuestro sitio utiliza cookies y/o sesiones para realizar un seguimiento de su carrito de la compra y recibos. Nosotros utilizamos cookies y/o sesiones para identificar lo que no es necesario iniciar sesión cada vez que visita nuestro sitio. Las cookies son vinculados a su número de cliente, que se asocia con la información de su cuenta.</p>
             <p>Este sitio también contiene enlaces a otros sitios web. Por desgracia, Cyberfuel no se hace responsable de las prácticas de privacidad o el contenido de tales sitios.</p>
             <h5>Seguridad</h5>
             <p>Este sitio tiene medidas de seguridad para proteger contra la pérdida, mal uso o alteración de la información bajo nuestro control. Por favor, consulte nuestra página segura de pedido para más detalles. </p>
             <h5>Actualización de la información</h5>
             <p>Usted puede alterar la información de su cuenta o dejar de recibir comunicaciones de nosotros y nuestros socios en cualquier momento. Tenemos las siguientes opciones para cambiar y modificar la información de su cuenta o preferencias de contacto.</p>
             <ol>
               <li>Usted puede enviar un correo electrónico a soporte@cyberfuel.com</li>
               <li>Usted puede visitar y actualizar su cuenta en línea</li>
               <li>Usted puede llamarnos al: +506 2204-9494 opción #1 del menú</li>
               <li>Usted puede enviarnos un fax al: +506 2204-9495</li>
               <li>Usted puede enviar un correo a Cyberfuel en la siguiente dirección postal:</li>
             </ol>
             <small> Cyberfuel S.A.<br>
             P.O.Box 77 -06155 FORUM<br>
             Santa Ana, Costa Rica </small>
             <h5>El envío de correos electrónicos</h5>
             <p>Usamos mensajes de correo electrónico para comunicarnos con usted, para confirmar los pedidos realizados, y para enviar información que usted haya solicitado. También proporcionamos enlaces de correo electrónico, como en nuestro "acerca de Cyberfuel" de la página, para que pueda contactar directamente con nosotros. Nos esforzamos para responder rápidamente a sus mensajes.</p>
             <h5>Cumplimiento de las leyes y hacer cumplir la ley</h5>
             <p>Colaboramos con agentes de las fuerzas del gobierno y de derecho y los particulares para hacer cumplir y cumplir con la ley. Vamos a revelar cualquier información sobre usted a funcionarios de gobierno o la ley o los particulares, pues nosotros, en nuestra única discreción, creemos necesario o apropiado para responder a las demandas y procedimientos legales (incluyendo, sin limitación de citaciones), para proteger nuestra propiedad y los derechos o la bienes y derechos de un tercero, para proteger la seguridad del público o cualquier persona, o para prevenir o detener la actividad que consideramos ilegal o poco ético. También compartiremos su información en la medida necesaria para cumplir con las normas de ICANN, reglamentos y políticas. </p>
             <h5>Terceros proveedores de servicios</h5>
             <p>A veces podemos proporcionar información sobre usted a terceros para prestar diversos servicios en nuestro nombre, como el procesamiento de pagos con tarjeta de crédito, servicio de anuncios, la realización de concursos o encuestas, realizar análisis de nuestros productos o demografía de los clientes, el transporte marítimo de mercancías o servicios, y gestión de las relaciones. Sólo compartiremos información sobre usted que es necesario para que el tercero para prestar el servicio solicitado. Estas empresas tienen prohibido retener, compartir, almacenar o utilizar su información personalmente identificable para ningún propósito secundario.</p>
             <p>En el caso de que usamos compañías de publicidad de tercera parte para servir anuncios en nuestro nombre, estas empresas pueden emplear cookies y etiquetas de acción (también conocido como regalo de un píxel o web beacons) para medir la efectividad de la publicidad. Cualquier información que estos terceros recopilen a través de cookies y etiquetas de acción es totalmente anónima.</p>
             <h5>Complementación de la información</h5>
             <p>Con el fin de prestar determinados servicios a usted, en ocasiones, pueden complementar la información personal que usted envíe a nosotros con la información de fuentes de terceros (por ejemplo, la información de nuestros socios estratégicos, proveedores de servicios, o el código postal. Esto lo hacemos para mejorar nuestra capacidad de servir, para adaptar nuestros productos y servicios a usted, y ofrecerle la oportunidad de comprar productos o servicios que creemos que pueden ser de interés para usted</p>
             <h5>Concursos / Encuestas</h5>
             <p>De vez en cuando, podemos ofrecerle la oportunidad de participar en concursos o encuestas. Si decide participar, puede solicitar cierta información personal identificable de usted. La participación en estos concursos o encuestas es totalmente voluntaria y por lo tanto tiene una opción si o no revelar la información solicitada. La información solicitada típicamente incluye información de contacto (como nombre y dirección) e información demográfica (como código postal y el nivel de edad - en cuenta que debe tener 18 o más para entrar). Usamos esta información para notificar a los ganadores del concurso y premios, para supervisar el tráfico del sitio, y para personalizar nuestro sitio.</p>
             <p>Podemos utilizar un tercero proveedor de servicios para llevar a cabo estas encuestas o concursos. Cuando lo hacemos, esa empresa será prohibido el uso de la información de nuestros usuarios personalmente identificable para ningún otro propósito. No compartiremos la información personal identificable que usted proporciona a través de un concurso o encuesta con otros terceros a menos que darle un aviso previo y la elección.</p>
             <h5>Dile a un Amigo</h5>
             <p>Si un usuario decide utilizar nuestro servicio de referencia para informar a un amigo sobre nuestro sitio, le pedimos al usuario el nombre del amigo y la dirección de correo electrónico. Cyberfuel enviará automáticamente al amigo un único correo electrónico invitándole a que visite nuestro sitio.  El amigo puede contactar con Cyberfuel en soporte@cyberfuel.com para solicitar la eliminación de esta información de nuestra base de datos.</p>
             <h5>¿Qué pasa con mi información personal al terminar mi cuenta con Cyberfuel?</h5>
             <p>Cuando usted cancela (ya sea voluntaria o involuntariamente) toda su información de identificación personal se coloca en el estado "inactivo" o "desactivado" en nuestras bases de datos.  Sin embargo, la desactivación de su cuenta no quiere decir que su información personal ha sido borrado de nuestra base de datos por completo. Vamos a conservar y utilizar su información de identificación personal si es necesario a fin de cumplir con nuestras obligaciones legales, resolver conflictos, o de hacer cumplir nuestros acuerdos.</p>
             <h5>Transferencia de datos en el Extranjero</h5>
             <p>Si usted está visitando este sitio desde un país distinto del país en el que nuestros servidores se encuentran, sus comunicaciones con nosotros puede resultar en la transferencia de información a través de fronteras internacionales. Al visitar este sitio y comunicarse electrónicamente con nosotros, usted autoriza la utilización de dichas transferencias.</p>
             <h5>Cambios en nuestras prácticas</h5>
             <p>Nos reservamos el derecho de modificar esta Política de Privacidad en cualquier momento. Si decidimos cambiar nuestra política de privacidad, publicaremos esos cambios en esta política de privacidad y otros lugares que consideremos apropiados, por lo que usted está enterado de qué información recogemos, cómo la usamos y bajo qué circunstancias, si las hubiere, revelamos.</p>
             <p>Si hacemos cambios materiales a esta política, le notificaremos aquí, por correo electrónico, o por medio de un anuncio en nuestra página principal, por lo menos treinta (30) días antes de la implementación de los cambios.</p>
             <h5>Ponerse en contacto con nuestro sitio</h5>
             <p>Si usted tiene alguna pregunta acerca de estos Términos de Uso, por favor ponte en contacto con nosotros por correo electrónico o correo ordinario a la siguiente dirección:</p>
             <small>
                Cyberfuel S.A.<br>
                Parque Empresarial FORUM<br>
                Edificio E, segundo piso<br>
                legal@cyberfuel.com<br>&nbsp;<br>
                Revisado: 28/Enero/2014<br>
                Copyright © 1997-<?php echo date("Y");?> Cyberfuel. All Rights Reserved.  Todos los derechos reservados.<br><br><br>             
             </small>   
         	</div>
       
       </div>
       <!-- /.row --> 
     </div>
        
        @else
<div class="container">
        	<div class="row">
		     <div class="col-md-12 block-content-general">
          <h2 class="text-success"><i class="fas fa-book "></i> Privacy Policy</h2>
         </div>
		 </div>
         

<div class="row text-justify">
           <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
             <h4>Cyberfuel Privacy, Security and Information Policy</h4>
             <h5>Cyberfuel&nbsp;respects your privacy</h5>
             <p>Our website and our staff have contracts of data privacy, according to different standards, data collection and usage. We invite you to read our security and privacy so that you are well informed of it.</p>
             <h5>Information Collection and Use</h5>
             <p>Our Site uses forms in which you give us contact information (including your name, address, telephone number, and email address) so you can place orders, request information and support, and make product suggestions. For certain services, we may also request a credit card number, government identification number, or other financial information. We will then create an account number for you.</p>
             <p>We receive and store any information you enter on our Site or give us in any other way, including through email, telephone, or other communications with our customer service department. If you contact us for support, we will keep an internal record of what support was given.</p>
             <p>We use your information to contact you regarding functionality changes to our products, our Site, new Cyberfuel services, and special offers we think you'll find valuable. If you would rather not receive this information, please see the "Updating Your Information" section below on how to change your preferences.</p>
             <p>We may also use your information to present a co-branded offer with our partners or affiliates. If we collect information from you in connection with a co-branded offer, it will be clear at the point of collection who is collecting the information and whose privacy statement applies.</p>
             <p>We use information gathered about you from our Site statistics (for example, your IP address) to help diagnose problems with our server, and to administer our Site. We also gather broad demographic information from this data to help us improve our Site and make your browsing and purchasing experience more enjoyable. This is not linked to any personally identifiable information, except as necessary to prevent fraud or abuse on our system.</p>
             <p>Our Site uses cookies to keep track of your shopping cart and receipts. We use cookies to identify you so you don't need to log in each time you visit our Site. The cookies are linked to your customer number, which is associated with the information in your account.</p>
             <p>This Site also contains links to other websites. Unfortunately, Cyberfuel is not responsible for the privacy practices or the content of such sites.</p>
             <h5>Security</h5>
             <p>This Site has security measures in place to protect against the loss, misuse or alteration of the information under our control. Please view our Secure Ordering page for further details.</p>
             <h5>Updating Your Information</h5>
             <p>You may alter your account information or opt out of receiving communications from us and our partners at any time. We have the following options for changing and modifying your account information or contact preferences.</p>
             <ol>
               <li>You may send an email to support@cyberfuel.com</li>
               <li>You may visit your online account information </li>
               <li>You may call us at: +506 2204-9494 option #1 of the menu</li>
               <li>You may fax us at: +506 2204-9495</li>
               <li>You may send mail to Cyberfuel at the following postal address:</li>
             </ol>
             <small> Cyberfuel S.A.<br>
             P.O.Box 77 -06155 FORUM<br>
             Santa Ana, Costa Rica </small>
             <h5>Sending Emails</h5>
             <p>We use emails to communicate with you, to confirm your placed orders, and to send information that you have requested. We also provide email links, as on our "About Cyberfuel" page, to allow you to contact us directly. We strive to promptly reply to your messages.</p>
             <p>The information you send to us may be stored and used to improve this Site and our products, or it may be reviewed and discarded.</p>
             <h5>Compliance with Laws and Law Enforcement</h5>
             <p>We cooperate with government and law enforcement officials and private parties to enforce and comply with the law. We will disclose any information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate to respond to claims and legal process (including without limitation subpoenas), to protect our property and rights or the property and rights of a third party, to protect the safety of the public or any person, or to prevent or stop activity we consider to be illegal or unethical. We will also share your information to the extent necessary to comply with ICANN's rules, regulations and policies.</p>
             <h5>Third Party Service Providers</h5>
             <p>We may at times provide information about you to third parties to provide various services on our behalf, such as processing credit card payments, serving advertisements, conducting contests or surveys, performing analyses of our products or customer demographics, shipping of goods or services, and customer relationship management. We will only share information about you that is necessary for the third party to provide the requested service. These companies are prohibited from retaining, sharing, storing or using your personally identifiable information for any secondary purposes.</p>
             <p>In the event that we use third party advertising companies to serve ads on our behalf, these companies may employ cookies and action tags (also known as single pixel gift or web beacons) to measure advertising effectiveness. Any information that these third parties collect via cookies and action tags is completely anonymous. </p>
             <h5>Supplementation of Information</h5>
             <p>In order to provide certain services to you, we may on occasion supplement the personal information you submit to us with information from third party sources (e.g., information from our strategic partners, service providers, or Postal Service).&nbsp;We do this to enhance our ability to serve you, to tailor our products and services to you, and to offer you opportunities to purchase products or services that we believe may be of interest to you.</p>
             <h5>Contests/Surveys</h5>
             <p>From time-to-time, we may provide you with the opportunity to participate in contests or surveys. If you choose to participate, we may request certain personally identifiable information from you. Participation in these contests or surveys is completely voluntary and you therefore have a choice whether or not to disclose the requested information. The requested information typically includes contact information (such as name and address), and demographic information (such as zip code and age level - note that you must be 18 or above to enter). We use this information to notify contest winners and award prizes, to monitor site traffic, and to personalize our Site.</p>
             <p>We may use a third party service provider to conduct these surveys or contests. When we do, that company will be prohibited from using our users' personally identifiable information for any other purpose. We will not share the personally identifiable information you provide through a contest or survey with other third parties unless we give you prior notice and choice.</p>
             <h5>Tell-A-Friend</h5>
             <p>If a user elects to use our referral service to inform a friend about our Site, we ask the user for the friend's name and email address. Cyberfuel will automatically send the friend a one-time email inviting them to visit our Site. Cyberfuel stores this information for the sole purpose of sending this one-time email. The friend may contact Cyberfuel at support@cyberfuel.com to request the removal of this information from our database.</p>
             <h5>What Happens to my Personal Information if I Terminate my Cyberfuel Account?</h5>
             <p>When your Cyberfuel account is cancelled (either voluntarily or involuntarily) all of your personally identifiable information is placed in "deactivated" status on our relevant Cyberfuel databases. However, deactivation of your account does not mean your personally identifiable information has been deleted from our database entirely. We will retain and use your personally identifiable information if necessary in order to comply with our legal obligations, resolve disputes, or enforce our agreements.</p>
             <h5>Transfer of Data Abroad</h5>
             <p>If you are visiting this Site from a country other than the country in which our servers are located, your communications with us may result in the transfer of information across international boundaries.&nbsp;By visiting this Site and communicating electronically with us, you consent to such transfer</p>
             <h5>Changes in Our Practices</h5>
             <p>We reserve the right to modify this Privacy Policy at any time. If we decide to change our Privacy Policy, we will post those changes to this Privacy Policy and any other places we deem appropriate, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it.</p>
             <p>If we make material changes to this Policy, we will notify you here, by email, or by means of a notice on our home page, at least thirty (30) days prior to the implementation of the changes.</p>
             <h5>Contacting Our Site</h5>
             <p>If you have any questions about our Privacy Policy, the practices of this Site, or your dealings with this Site, please contact us at:</p>
             <small>
                Cyberfuel S.A.<br>
                FORUM 1 Santa Ana Business Park<br>
                Building E, second floor<br>
                legal@cyberfuel.com<br>&nbsp;<br>
                Revised: 28/January/2014<br>
                Copyright © 1997-<?php echo date("Y");?> Cyberfuel. All Rights Reserved.<br><br><br>             
             </small>              
            </div>
         </div>
	
	</div>
        @endif


  

@endsection