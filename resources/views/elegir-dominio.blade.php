@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | {{ __('hospedaje.titulo') }}</title>
<meta name="keywords" content="cyberfuel, hospedaje web, alojamiento web, web hosting, webhosting, linux, windows, costa rica, correo electronico, sitio"/>
<meta name="description" content="{{ __('hospedaje.descripcion') }}">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/hospedaje.jpg')}}">
<meta property="og:description" content="{{ __('hospedaje.titulo') }}">
<meta property="og:title" content="Cyberfuel | {{ __('hospedaje.hospedaje_web') }}">
@endsection


@section('content') 
<!--aca empieza el contenido del HTML-->

		<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-sm-5  align-self-center p-static pt-4">
							
								<div class="overflow-hidden ">
									<h1 class="text-10 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>{{ __('hospedaje.planes') }}</strong></h1>
								</div>
								<div class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
									<span class="sub-title text-4 mt-4">{{ __('hospedaje.titulo') }}</span>
								</div>
								<div class="appear-animation d-inline-block" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
									<a href="#" class="btn btn-modern btn-dark mt-4">{{ __('compra.comprar_ahora') }} <i class="fas fa-arrow-right ms-1"></i></a>
								</div>
								<div class="appear-animation d-inline-block" data-appear-animation="rotateInUpRight" data-appear-animation-delay="500">
									<span class="arrow hlt" style="top: 40px;"></span>
								</div>
							</div>
							<div class="col-sm-7  align-items-end justify-content-end d-flex pt-md-5">
								<div style="min-height: 350px;" class="overflow-hidden">
									<img  alt="" src="{{URL::asset('assets/imagenes/hospedaje.png')}}" class="img-fluid appear-animation" data-appear-animation="slideInUp" data-appear-animation-delay="600" data-appear-animation-duration="1s">
								</div>
							</div>
						</div>
					</div>
				</section>



<div class="container">
  <div class="row mt-5">
    <div class="col-12 ">
     @if (Session::get('language') == 'es')
        
      <p class="text-justify">Aquí es donde usted puede alojar su sitio de Internet "página WEB" y utilizar cuentas de correo electrónico con el mismo dominio (ejemplo: usuario@sudominio.com).  Este servicio cuenta con un panel de control para que usted pueda administrarlo y con servicio al cliente las 24 horas. </p>
        @else
        
         <p class="text-justify">Web Hosting is the platform that allows you to have a space to host your site "WEB PAGE" and use email accounts mailboxes with the same domain (example: user@yourdomain.com). This service has a control panel to manage and with a 24-hour customer service.  </p>
        @endif
     
    </div>
  </div>
	 </div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
        <div class="card card-background-image-hover border-0" style="background-image: url({{URL::asset('assets/imagenes/bg.jpg')}});">
          <div class="card-body  p-3">
            <h4 class="card-title mt-2 mb-2 text-5 font-weight-bold">{{ __('compra.plan_personal') }}</h4>
            <p class="card-text">
         
			  @if (Session::get('language') == 'es')
        
         <ul class="text-left">
              <li> 3 espacio para dominios</li>
              <li>5 GB de espacio</li>
              <li>10 buzones de Correo </li>
              <li>1 base datos MySQL</li>
              <li>Soporte para programación PHP</li>
              <li>Respaldo diario</li>
              <li> Soporte Técnico 24/7</li>
            </ul>
        
        @else
        
         <ul class="text-left">
            <li> Hosting for 3 domains</li>
			 <li>5 GB of disk space</li>
			 <li>10 Mailbox </li>
			 <li>1 MySQL data base</li>
			 <li>PHP support</li>
			 <li>Daily backup</li>
			 <li>Tech Support 24x7</li>

            </ul>
        @endif
            </p>
            <select class="form-select form-control border-color-primary mb-3">
              <option selected=""value="1" > $5.91/mo ($5.91 por 1 mo)</option >
              <option > $5.91/mo ($70,92 por 12 mos)</option >
              <option > $5.91/mo ($141,84 por 24 mos)</option >
              <option > $5.91/mo ($212,76 por 36 mos)</option >
            </select>
            <div class="h5 text-primary"><span class="h6">{{ __('compra.precio_mensual') }}: US</span> $ 5.91</div>
            <div class="h5 text-tertiary"><span class="h6">{{ __('compra.tarifa_anual') }}: US</span> $ 64.99</div>
            <p class="small">{{ __('hospedaje.conocer_carac') }}</p>
            <a href="#" role="button" class="btn btn-dark btn-modern m-auto d-block">{{ __('hospedaje.agregar_carrito') }}</a> </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
        <div class="card card-background-image-hover border-0" style="background-image: url({{URL::asset('assets/imagenes/bg.jpg')}});">
          <div class="card-body  p-3">
            <h4 class="card-title mt-2 mb-2 text-5 font-weight-bold">{{ __('compra.plan_pymes') }}</h4>
            <p class="card-text">
      
			  @if (Session::get('language') == 'es')
        
            <ul class="text-left">
              <li>6 espacio para dominios</li>
              <li>10 GB de espacio</li>
              <li>60 buzones de Correo </li>
              <li>10 base datos MySQL</li>
              <li>Soporte para programación PHP</li>
              <li>Respaldo diario</li>
              <li>Soporte Técnico 24/7</li>
            </ul>
        
        @else
              <ul class="text-left">
            <li> Hosting for 6 domains</li>
			 <li>10 GB of disk space</li>
			 <li>60 Mailbox</li>
			 <li>10 MySQL data base</li>
			 <li>PHP support</li>
			 <li>Daily backup</li>
			 <li>Tech Support 24x7</li>

            </ul>
        @endif
            </p>
            <select class="form-select form-control border-color-primary mb-3">
              <option selected=""value="1" > $5.91/mo ($5.91 por 1 mo)</option >
              <option > $5.91/mo ($70,92 por 12 mos)</option >
              <option > $5.91/mo ($141,84 por 24 mos)</option >
              <option > $5.91/mo ($212,76 por 36 mos)</option >
            </select>
            <div class="h5 text-primary"><span class="h6">{{ __('compra.precio_mensual') }}: US</span> $ 11.82</div>
            <div class="h5 text-tertiary"><span class="h6">{{ __('compra.tarifa_anual') }}: US</span> $ 129.99</div>
            <p class="small">{{ __('hospedaje.conocer_carac') }}</p>
            <a href="#" role="button" class="btn btn-dark btn-modern m-auto d-block">{{ __('hospedaje.agregar_carrito') }}</a> </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-5 mb-lg-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
        <div class="card card-background-image-hover border-0" style="background-image: url({{URL::asset('assets/imagenes/bg.jpg')}});">
          <div class="card-body  p-3">
            <h4 class="card-title mt-2 mb-2 text-5 font-weight-bold">{{ __('compra.plan_empresarial') }}</h4>
            <p class="card-text">
            <ul class="text-left">
       
				
				@if (Session::get('language') == 'es')
        
            <li>10 espacio para dominios</li>
              <li>20 GB de espacio</li>
              <li>120 buzones de Correo </li>
              <li>20 base datos MySQL</li>
              <li>Soporte para programación PHP</li>
              <li>Respaldo diario</li>
              <li>Soporte Técnico 24/7</li>
        
        @else
        
            <li> Hosting for 10 domains</li>
			 <li>20 GB of disk space</li>
			 <li>120 Mailbox</li>
			 <li>20 MySQL data base</li>
			 <li>PHP support</li>
			 <li>Daily backup</li>
			 <li>Tech Support 24x7</li>

        @endif
            </ul>
            </p>
            <select class="form-select form-control border-color-primary mb-3">
              <option selected=""value="1" > $5.91/mo ($5.91 por 1 mo)</option >
              <option > $5.91/mo ($70,92 por 12 mos)</option >
              <option > $5.91/mo ($141,84 por 24 mos)</option >
              <option > $5.91/mo ($212,76 por 36 mos)</option >
            </select>
            <div class="h5 text-primary"><span class="h6">{{ __('compra.precio_mensual') }}: US</span> $ 27.28</div>
            <div class="h5 text-tertiary"><span class="h6">{{ __('compra.tarifa_anual') }}: US</span> $ 299.99</div>
            <p class="small">{{ __('hospedaje.conocer_carac') }}</p>
            <a href="#" role="button" class="btn btn-dark btn-modern m-auto d-block">{{ __('hospedaje.agregar_carrito') }}</a> </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center mt-md-4">
      <div class="col-12 col-sm-8 col-md-4"><a data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="btn btn-primary rounded-0 m-auto d-block">{{ __('hospedaje.ver_mas_planes') }}</a></div>
    </div>
  </div>
<div class="container">
  <div class="collapse" id="collapseExample">

	  @include('includes.hospedajes')
  </div>
  <div class="row pt-2">
    <div class="col-12">
      
		
		@if (Session::get('language') == 'es')
        
    <p>* En caso de excederse la transferencia mensual se cobrará US $ 1.00 por cada GB de transferencia adicional.
        ** En caso de excederse espacio en disco duro se cobrará US $ 2.00 por cada GB de adicional por mes. <a href="">*** Términos y Condiciones del servicio</a></p>
        
        @else
        
      <p>* In case of exceeding the monthly transfer, US $ 1.00 will be charged for each additional GB of transfer.
        ** In case of exceeding hard disk space, US $ 2.00 will be charged for each additional GB per month. <a href="">*** Terms and conditions of the service</a></p>
        @endif
    </div>
  </div>
</div>
<!--fin el contenido del HTML--> 
@endsection 