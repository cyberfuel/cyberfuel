@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Tenga un respaldo fuera de sus equipos</title>
<meta name="keywords"
  content="Respaldo, Baas, costa rica, cr, solución, maquinas, servidores, archivos, sitio seguro, seguridad" />
<meta name="description"
  content="Con nuestra solución puede tener un respaldo de su computador o de sus servidores virtuales o físicos (completo o parcial) fuera de su lugar de trabajo o habitación, garantizando la información segura ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Tenga un respaldo fuera de sus equipos">
<meta property="og:title" content="Cyberfuel | BaaS">
@endsection
@section('content')
<section
  class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8"
  style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
  <div class="container">
    <div class="row">
      <div class="col-12  align-self-center p-static ">

        <div class="overflow-hidden ">
          <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter"
            data-appear-animation-delay="300"><strong>{{
              __('master.respaldo_servicio') }} (BaaS)</strong></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row ">
    <div class="col-12 ">
      @if (Session::get('language') == 'es')

      <p class="text-justify">En el servicio BaaS (Backup as a Service) o en español “Respaldo como Servicio”, permite a
        los usuarios realizar un respaldo de un computador, servidor, una carpeta específica, realizando una respaldo
        conciso y apenas perceptible por el Internet con el fin de eliminar la pérdida de información y minimizando la
        posibilidad de Ransomware (que en los últimos años ha costado grandes costos a las empresas y personas al perder
        su información), teniendo la información en un lugar fuera de sitio.</p>

      @else

      <p class="text-justify">In the BaaS service (Backup as a Service) or in Spanish "Backup as a Service", it allows
        users to make a backup of a computer, server, a specific folder, making a concise and barely perceptible backup
        over the Internet in order to eliminating the loss of information and minimizing the possibility of Ransomware
        (which in recent years has cost companies and individuals great costs by losing their information), having the
        information in an off-site place.</p>
      @endif
    </div>
  </div>



  <div class="row">
    <div class="col">
      <div class="tabs tabs-bottom tabs-center tabs-simple">
        <ul class="nav nav-tabs">
          <li class="nav-item active"> <a class="nav-link active" href="#tabsNavigationSimple1" data-bs-toggle="tab">{{
              __('master.ventajas') }}</a> </li>
          <li class="nav-item"> <a class="nav-link" href="#tabsNavigationSimple2" data-bs-toggle="tab">{{
              __('master.beneficios') }}</a> </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tabsNavigationSimple1">
            <div class="row align-items-center">
              <div class="col-12"><img class="img-fluid m-auto d-block"
                  src="{{URL::asset('assets/imagenes/ventajas.webp')}}" alt="Cyberfuel"></div>
              <div class="col-12">
                <div class="px-2">

                  @if (Session::get('language') == 'es')
                  <ul class="text-justify list list-icons list-primary">
                    <li><i class="fas fa-check"></i> <strong>Copia automática y programada:</strong> Las copias se hacen
                      automáticamente del ordenador a un servicio remoto de backup online con la planificación que
                      necesites.</li>
                    <li><i class="fas fa-check"></i> <strong>Copia automatizada:</strong> Únicamente los datos que
                      sufren variación son los que se copian </li>
                    <li><i class="fas fa-check"></i> <strong>Accesible:</strong> Puedes acceder en cuestión de minutos y
                      recuperar los datos desde otro equipo.</li>
                  </ul>
                  @else
                  <ul class="text-justify list list-icons list-primary">
                    <li><i class="fas fa-check"></i> <strong>Automatic and scheduled copy</strong>: The copies are made
                      automatically from the computer to a remote online backup service with the planning you need.</li>
                    <li><i class="fas fa-check"></i> <strong>Automated copy</strong>: Only the data that undergoes
                      variation are those that are copied.</li>
                    <li><i class="fas fa-check"></i> <strong>Accessible</strong>: You can access in a matter of minutes
                      and recover the data from another computer.</li>
                  </ul>
                  @endif

                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tabsNavigationSimple2">
            <div class="row align-items-center">
              <div class="col-12"><img class="img-fluid m-auto d-block"
                  src="{{URL::asset('assets/imagenes/beneficios.webp')}}" alt="Cyberfuel"></div>
              <div class="col-12">
                <div class="px-2">


                  @if (Session::get('language') == 'es')
                  <ul class="text-justify list list-icons list-primary">
                    <li><i class="fas fa-check"></i>Gestión sencilla</li>
                    <li><i class="fas fa-check"></i> Optimización de costes</li>
                    <li><i class="fas fa-check"></i> Seguridad</li>
                    <li><i class="fas fa-check"></i> Recuperación fácil y rápida de la información</li>
                    <li><i class="fas fa-check"></i> Optimización de los recursos IT</li>
                    <li><i class="fas fa-check"></i> Escalabilidad</li>
                  </ul>
                  @else
                  <ul class="text-justify list list-icons list-primary">
                    <li><i class="fas fa-check"></i> Simple management</li>
                    <li><i class="fas fa-check"></i> Costs optimization</li>
                    <li><i class="fas fa-check"></i> Security</li>
                    <li><i class="fas fa-check"></i> Quick and easy retrieval of information</li>
                    <li><i class="fas fa-check"></i> Optimization of IT resources</li>
                    <li><i class="fas fa-check"></i> Scalability</li>
                  </ul>
                  @endif



                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <section class="call-to-action mb-5">
      <div class="col-sm-9 col-lg-9">
        <div class="call-to-action-content">
          <h3>Proteja <strong class="font-weight-extra-bold">sus datos</strong> y asegúrese de que estén disponibles
            para usted ante un desastre.</h3>



        </div>
      </div>
      <div class="col-sm-3 col-lg-3">
        <div class="call-to-action-btn">
          <button type="button" id="redireccionarBtn" class="btn btn-modern btn-primary mt-3" title="Agregar Producto">
            <span> <i class="fas fa-plus"></i></span> {{ __('inicio.consulta_ser')
            }} </button>
        </div>


      </div>
    </section>
  </div>


</div>
<!--fin el contenido del HTML-->
@endsection

@section('scripts')




<script>
  $(document).ready(function() {
      $('#redireccionarBtn').click(function() {
          // Redirigir a la página de contacto después de hacer clic en el botón
          window.location.href = '/contactenos';
      });
  
      $(window).on('load', function() {
          // Obtener el elemento select después de que se haya cargado completamente la página de contacto
          var selectElement = $('#servicio');
          
          // Obtener el valor de la opción deseada ('Baas' en este caso)
          var opcionDeseada = 'Baas';
          
          // Recorrer todas las opciones del select
          selectElement.find('option').each(function() {
              // Verificar si el valor de la opción es el deseado
              if ($(this).val() === opcionDeseada) {
                  // Seleccionar la opción deseada
                  $(this).prop('selected', true);
                  // Salir del bucle una vez encontrada la opción deseada
                  return false;
              }
          });
      });
  });
</script>
@endsection