@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | {{ __('compra.titulo') }}</title>
<meta name="keywords"
  content="cyberfuel, nombre de dominio, dominio, dominios, registro, compra, web, costa rica, .CR, .com" />
<meta name="description" content="{{ __('compra.descripcion') }}">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="{{ __('compra.titulo') }}">
<meta property="og:title" content="Cyberfuel | {{ __('compra.nombre_de') }} {{ __('compra.dominio') }}s">
@endsection

@section('content')
<!--aca empieza el contenido del HTML-->

<!-- <section
  class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8 mb-0"
  style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
  <div class="container">
    <div class="row">
      <div class="col-md-6  align-self-center p-static">

        <div class="overflow-hidden">
          <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter"
            data-appear-animation-delay="300"><strong>{{ __('compra.nombre_de') }} {{ __('compra.dominio') }}</strong>
          </h1>
        </div>
      </div>
    </div>
  </div>
</section> -->

<div class="container-fluid  mb-4">
  <div class="row">
      <div class="col-12">
        <div class="accordion" id="carrodecompras">
          <div class="card border-0">
            @include('includes.elegir-dominio')
          </div>
        </div>
      </div>
      <!--<div class="col-xxl-2  d-none d-xxl-block ">
        @include('includes.side-compra')
      </div>-->
    </div>
  </div>
  <!--fin el contenido del HTML-->
  @endsection
  @section('fix-footer')
  <section id="footer-fix" class="section bg-primary border-0 m-0 fixed-bottom py-0">
    <div class="container-fluid">
      <div class="row justify-content-around justify-content-md-start align-items-center d-flex  py-3">
        <div class="col-md-6">
          <div class="px-4 mb-2 mb-md-0">
            <h3 class="text-3-5 font-weight-medium font-alternative text-transform-none line-height-3 mb-0"><span
                class="position-relative">
                @if((Session::has("cyberfuel_carrito_compras")) && (Session::get("cyberfuel_carrito_compras") !=
                null)) <span id="span_total_carrito_compras"
                  class="position-absolute top-50 start-0 translate-middle badge rounded-pill bg-danger"> @php
                  $carrito_compras = Session::get("cyberfuel_carrito_compras");
                  if((isset($carrito_compras)) && ($carrito_compras != null) && is_array($carrito_compras)){
                  $total_productos_carrito_compras = count($carrito_compras);
                  if($total_productos_carrito_compras > 0){
                  echo $total_productos_carrito_compras;
                  }
                  }
                  @endphp </span> @endif &nbsp; &nbsp; <span id="span_total_carrito_compras"
                  class="cart-qty "></span></span>
            </h3>
            <p class="price text-5 m-0">
            
              <span class="subtotal text-light">
                <h4 class="font-weight-bold text-uppercase text-3 mb-3">
                            Cantidad de Artículos
                            <span class="pl-4">
                                <a href="" class="header-nav-features-toggle" aria-label="">
                                   &nbsp; <i class="fas fa-shopping-basket text-white"></i>&nbsp;
                                    <span class="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1">5 </span>
                                </a>
                            </span>
                        </h4>
                </span>
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="float-md-end lista-carrito">
            <a href="{{ __('rutas.url_carrito-de-compra') }}" class="btn btn-modern btn-light box-shadow-2  rounded-0 sidecar">
              <i class="fas fa-shopping-basket  text-5"></i> &nbsp; Ver mi compra <span id="span_total_carrito_compras"
                class="cart-qty"></span> <span>&nbsp;</span> </a>
            </a>
            <a data-bs-toggle="modal" data-bs-target="#usuario" data-bs-toggle="tooltip" data-bs-placement="bottom"
              type="button" class="btn btn-md btn-modern btn-quaternary box-shadow-2 rounded-0 "> <i
                class="fas fa-arrow-right text-2 me-2"></i>&nbsp;Continuar con la compra</a>
          </div>

        </div>
      </div>
    </div>
  </section>

  <!--  modal hopedajes-->
  <div class="modal fade" id="hospedaje" tabindex="-1" role="dialog" aria-labelledby="modalHospedajes"
    aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header alert-primary">
          <h4 class="modal-title text-white" id="modalHospedajes">{{ __('hospedaje.planes') }}</h4>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body bg-color-grey-scale-1">
          @include('includes.hospedajes')
        </div>
        <div class="modal-footer alert-primary">
          <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('comercio.cerrar') }}</button>
        </div>
      </div>
    </div>
  </div>
  @endsection


  <!--//<script>
//const stepButtons = document.querySelectorAll('.step-button');
//const progress = document.querySelector('#progress');
//
//Array.from(stepButtons).forEach((button,index) => {
//    button.addEventListener('click', () => {
//        progress.setAttribute('value', index * 100 /(stepButtons.length - 1) );//there are 3 buttons. 2 spaces.
//
//        stepButtons.forEach((item, secindex)=>{
//            if(index > secindex){
//                item.classList.add('done');
//            }
//            if(index < secindex){
//                item.classList.remove('done');
//            }
//        })
//    })
//})
//	
//
//</script> -->



@section('scripts')



@endsection
