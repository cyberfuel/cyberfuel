@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Marketplace de Cyberfuel</title>
<meta name="keywords"
  content="Solución, ante un desastre, servicio, DRaaS, Costa Rica, CR, sitio alterno, plan B, seguro, sitio principal" />
<meta name="description" content="Marketplace de Cyberfuel">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Marketplace de Cyberfuel">
<meta property="og:title" content="Cyberfuel | Markedplace">
@endsection

@section('css')

@endsection
@section('content')
<!--aca empieza el contenido del HTML-->
<section class="section section-default bg-color-light-scale-2 py-4 mt-5 mt-xl-0">
 <div role="main" class="main shop">
          <div class="container">
           @include('includes.menu-market') 
          </div>
      </div>
</section>

<!--fin el contenido del HTML-->
@endsection

@section('scripts')


<script>
  $(document).ready(function () {
        $('#select2-dropdown').select2({
    containerCssClass: "custom-container",
    dropdownCssClass: "custom-dropdown",
		//	placeholder: "Seleccione Instancia",
//			allowClear: true
});
		

    });
</script>
@endsection