@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Registrese con nosotros</title>
<meta name="keywords"
	content="Orden, costa rica, cr, solución, maquinas, servidores, archivos, sitio seguro, seguridad" />
<meta name="description"
	content="Con nuestra solución puede tener un respaldo de su computador o de sus servidores virtuales o físicos (completo o parcial) fuera de su lugar de trabajo o habitación, garantizando la información segura ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Registrese con nosotros">
<meta property="og:title" content="Cyberfuel | Ingreso de Nuevo Cliente">
@endsection
@section('content')
<!--aca empieza el contenido del HTML-->
<hr class=" py-2">
<div role="main" class="main shop pb-4">

	<div class="container-xl">

		<div class="row justify-content-center">
			<div class="col-lg-10">

			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-12 gy-4">
				<div class="card border-width-3 border-radius-0 border-color-success">
					<div class="card-body text-center">
						<p class="text-color-dark font-weight-bold text-4-5 mb-0"><i
								class="fas fa-check text-color-success me-1"></i> Gracias su orden ha sido recibida</p>
					</div>
				</div>

			</div>

			<div class="col-12 col-md-8 gy-4">

				<div class="card border-width-3 border-radius-0 border-color-hover-dark" style="width: 100%;">
					<div class="card-body p-4">

						<table class="shop_table cart-totals mb-4">
							<tbody>
								<tr class="cart-subtotal">
									<td class="border-top-0">
										<strong class="text-color-dark">Día</strong>
									</td>
									<td class="border-top-0 text-end">
										<strong><span class=" font-weight-medium">Enero 17, 2024</span></strong>
									</td>
								</tr>

								<tr class="">
									<td class="border-top-0">
										<strong class="text-color-dark">Número de Factura</strong>
									</td>
									<td class="border-top-0 text-end">
										<strong><span class=" font-weight-medium ">31891</span></strong>
									</td>
								</tr>

								<tr class="">
									<td class="border-top-0">
										<strong class="text-color-dark">Email</strong>
									</td>
									<td class="border-top-0 text-end">
										<strong><span class=" font-weight-medium">gsoto@cyberfuel.com</span></strong>
									</td>
								</tr>

								<tr class="">
									<td class="border-top-0">
										<strong class="text-color-dark">Método de pago</strong>
									</td>
									<td class="border-top-0 text-end">
										<strong><span class=" font-weight-medium">Tarjeta de Crédito</span></strong>
									</td>
								</tr>

								<tr class="">
									<td class="border-top-0">
										<strong class="text-color-dark">Número de autorización</strong>
									</td>
									<td class="border-top-0 text-end">
										<strong><span class=" font-weight-medium">1256</span></strong>
									</td>
								</tr>


								<tr class="total">
									<td>
										<strong class="text-color-dark text-3-5">Pago total</strong>
									</td>
									<td class="text-end">
										<strong class="text-color-dark"><span
												class="amount text-color-dark text-5">$431.00</span></strong>
									</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>

			</div>

		</div>

	</div>

</div>
@endsection

@section('scripts')


@endsection