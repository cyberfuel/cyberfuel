<!DOCTYPE html>
<html lang="es">
    <head>
        



        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @yield('meta')
		@yield('face') 
        <title>Cyberfuel - {{ $sector ?? '' }}</title>
       <link rel="icon" type="image/png" href="{{ asset('assets/imagenes/favicon.webp') }}" sizes="16x16" />
       <link rel="shortcut icon" href="{{URL::asset('assets/images/favicon.png')}}" type="image/x-icon" />
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/landing.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/toastr/toastr.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('assets/css/demo.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all">
        <!-- Theme CSS -->
            <link rel="stylesheet" href="{{URL::asset('assets/css/theme.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/css/theme-elements.min.css')}}">
        <!-- Skin CSS -->
        <link id="skinCSS" rel="stylesheet" href="{{URL::asset('assets/css/master.css')}}">
        <link id="skinCSS" rel="stylesheet" href="{{URL::asset('assets/css/simple-line-icons.min.css')}}">
        
        
    </head>
    <body>



        <div class="p-4 p-md-5 text-white rounded bg-dark" style="background-color: #343a40 !important; background-image: url('{{ asset($header) }}') !important; background-repeat: no-repeat !important; background-size: cover;">
            <div class="row">
            <div class="col-md-4 col-lg-3">
                <img class="img-fluid" src="{{ URL::asset('assets/imagenes/landing/cyber.webp') }}" alt="Cyberfuel">
                </div>
            </div>
          
        </div>
        @yield('content')
        <div class="container mt-4">
            <div class="row">
                <div class="col-12 py-2" align="center">
                    <h3 class="h4 text-info">Respaldo y Garantía:</h3>
                </div>
                <div class="col-6 col-md-3">
                    <img class="img-fluid w-75" src="{{ URL::asset('assets/imagenes/landing/gs1.png') }}" alt="Cyberfuel">
                    <p class="small"> Con Sello de Garantía GS1</p>
                </div>
                <div class="col-6 col-md-3">
                    <img class="img-fluid  w-75" src="{{ URL::asset('assets/imagenes/landing/logo-24.png') }}" alt="Cyberfuel">
                    <p class="small">Le atenderemos las 24 horas los 365 días para su completa satisfacción</p>
                </div>
                <div class="col-6 col-md-3">
                    <img class="img-fluid  w-75" src="{{ URL::asset('assets/imagenes/landing/epi.png') }}" alt="Cyberfuel"><br>
                    <p class="small">Centro de Datos de Cyberfuel Certificado en Recurso Humano en Operación y Mantenimiento</p>
                </div>
                <div class="col-6 col-md-3">
                    <img class="img-fluid  w-75" src="{{ URL::asset('assets/imagenes/logos/rate3.webp') }}" alt="Cyberfuel"><br>
                    <p class="small">Centro de Datos de Cyberfuel Certificado en Infraestructura en Facilidades Construidas</p>
                </div>
            </div>
        </div>
        <footer class="container-fluid">
            <div class="row">
                <div class="col-12 mt-2 mt-sm-5">
                    <ul class="list-unstyled list-inline social text-center">
                        <li class="list-inline-item">
                            <a target="_blank" href="https://www.facebook.com/cyberfuel/"><i class="fab fa-facebook"></i></a>
                        </li>
                      <!--  <li class="list-inline-item">
                            <a target="_blank" href="https://www.youtube.com/channel/UCcX3ywuSnbxpLeEKEdugWvw"><i class="fab fa-youtube"></i></a>
                        </li>-->
                        <li class="list-inline-item">
                            <a target="_blank" href="https://www.instagram.com/cyberfuel_sa/"><i class="fab fa-instagram"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="mailto:ventas@cyberfuel.com" target="_blank"><i class="fa fa-envelope"></i></a>
                        </li>
                    </ul>
                </div>
                <hr>
            </div>
            <p class="text-center">
           <?php /*?>     <a href="{{ route('index') }}">&copy;{{ now()->year }}, Cyberfuel.</a><?php */?>
            </p>
        </footer>
        <script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/popper.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/jquery.appear.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.easing.min.js') }}"></script>
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
                crossorigin="anonymous"></script>
        <script src="{{ URL::asset('assets/js/landing.min.js') }}"></script>
        <!-- Theme Base, Components and Settings -->
        <script src="{{ URL::asset('assets/js/theme.min.js') }}"></script>
        <!-- Current Page Vendor and Views -->
        <!-- Theme Custom -->
        <script src="{{ URL::asset('assets/js/custom.js') }}"></script>
        <!-- Theme Initialization Files -->
        <script src="{{ URL::asset('assets/js/theme.init.js') }}"></script>
        <script src="{{ URL::asset('assets/js/lightboxes.js') }}"></script>
        <script src="{{ URL::asset('assets/js/toastr/toastr.min.js') }}"></script>
        @yield('scripts')
        <a href="#" id="scroll" style="display: none;"><span></span></a>
    <?php /*?>    {!! Toastr::message() !!}
        {!! NoCaptcha::renderjs() !!}<?php */?>
		
		<script>
		
		$(".scroll-to-top").addClass("d-none")
		</script>
    </body>
</html>
