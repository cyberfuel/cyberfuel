<!DOCTYPE html>
<html lang="es">

  <head>

    <!-- Google Tag Manager -->

    <!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSHCJ9W');</script>-->

    <!-- End Google Tag Manager -->
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="cyberfuel.com">
    <meta name="theme-color" content="#2aa3e0" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/png" href="{{ asset('assets/imagenes/favicon.webp') }}" sizes="16x16" />
    @yield('metas')
    @yield('face')
    <link rel="shortcut icon" href="{{URL::asset('assets/images/favicon.png')}}" type="image/x-icon" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::asset('assets/css/theme.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/theme-elements.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/all.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/animate.compat.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/cargando.css')}}">
    <link id="skinCSS" rel="stylesheet" href="{{URL::asset('assets/css/master.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/custom.css')}}">
    <link rel="preload" href="{{URL::asset('assets/css/theme-shop.css')}}" as="style"
      onload="this.onload=null;this.rel='stylesheet'">
    <link rel="stylesheet" href="{{URL::asset('assets/css/default.css')}}">
    @yield('css')

    <!-- Theme CSS -->

    <!-- Skin CSS -->

    <link rel="stylesheet" href="{{URL::asset('assets/css/countrySelect.css')}}">

    <!--
    <link rel="preload" href="styles.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="styles.css"></noscript>-->
    <script src="{{ URL::asset('assets/js/modernizr.min.js') }}"></script>
  </head>


  <body data-plugin-options="{'hideDelay': 0, 'effect': 'pulse'}" data-loading-overlay="" data-plugin-page-transition=""
    class="" cz-shortcut-listen="true">
    <div class="loading-overlay">
      <div class="bounce-loader">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <!-- Menu -->
    @if (session()->has('error'))
    <div class="alert alert-danger"> {{session()->get('error')}} </div>
    @endif

    @include('includes.header')
    <div role="main" class="main p-main">
      <!--  aca se carga el contenido de las secciones    -->
      @yield('content')
    </div>
    <div id="div_cargando_sitio" class="loading_full_site" style="display:none;"> <i
        class="fas fa-spinner fa-pulse fa-3x"></i> </div>
    </div>
    @include('includes.footer') <a href="https://api.whatsapp.com/send?phone=50622049494" class="whatsapp"
      target="_blank" aria-label="Cyberfuel"> <span class="anim-hover-scale-1-2 transition-3ms d-inline-block"> <img
          src="{{URL::asset('assets/imagenes/logos/whatsapp.svg')}}" height="45" title="Cyberfuel atención al cliente"
          loading="lazy" width="45"> </span> </a>
    <!--     modal de loguin-->
    <div class="modal fade" id="usuario" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel"
      aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="largeModalLabel">{{ __('master.iniciar')
              }}</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row" id="login">
              <div class="col-12 mb-5 mb-lg-0">
                <form action="/" id="frmSignIn" method="post" class="needs-validation">
                  <div class="row">
                    <div class="col-12">
                      <h2 class="font-weight-bold text-5 mb-0">{{ __('master.ingrese_datos')
                        }}</h2>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col">
                      <label class="form-label text-color-dark text-3">{{
                        __('master.usuario')
                        }} <span class="text-color-danger">*</span></label>
                      <input type="text" value="" class="form-control form-control-lg text-4" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col">
                      <label class="form-label text-color-dark text-3">{{ __('master.contra')
                        }} <span class="text-color-danger">*</span></label>
                      <input type="password" value="" class="form-control form-control-lg text-4" required>
                    </div>
                  </div>
                  <div class="row justify-content-between">
                    <div class="form-group col-md-auto">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="rememberme">
                        <label class="form-label custom-control-label cur-pointer text-2" for="rememberme">{{
                          __('master.recordarme') }}</label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col"> <a id="conRegistros" type="button"
                        class="btn btn-primary btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 pt-3 mb-0">{{
                        __('master.iniciar') }}</a> </div>
                  </div>
                  <div class="row justify-content-end">
                    <div class="form-group col-md-auto"> <a data-bs-toggle="collapse" data-bs-target=".login-recuperar"
                        class="text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2 recupera"
                        href="#">{{ __('master.recuperar') }}?</a> </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="form-group col"> <a href="{{ __('rutas.url_nuevo_cliente') }}" type="submit"
                        class="btn btn-success btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3"
                        data-loading-text="Loading...">{{ __('master.si_aun') }}</a> </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="row collapse login-recuperar">
              <div class="col-12 mb-5 mb-lg-0">
                <h2 class="font-weight-bold text-5 mb-0">{{ __('master.recuperar') }}</h2>
                <form action="/" id="frmSignUp" method="post">
                  <div class="row">
                    <div class="form-group col">
                      <label class="p font-weight-bold "> {{ __('master.ingrese_correo') }} </label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col">
                      <label class="form-label text-color-dark text-3">{{ __('master.cor_ele')
                        }} <span class="text-color-danger">*</span></label>
                      <input type="text" value="" class="form-control form-control-lg text-4" required="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col">
                      <button type="submit"
                        class="btn btn-dark btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3"
                        data-loading-text="Loading...">{{ __('master.continuar') }}</button>
                    </div>
                  </div>
                  <div class="form-group col-md-auto"> <a data-bs-toggle="collapse" data-bs-target=".login-recuperar"
                      class="text-center text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2 regresa"
                      href="#">{{ __('master.regresar_login') }}</a> </div>
                </form>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('master.cerrar')
              }}</button>
          </div>
        </div>
      </div>
    </div>
    @include('includes.compra')

    <!-- Vendor -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
      integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script src="{{ URL::asset('assets/js/jquery.appear.min.js') }}"></script>
    <!-- <script src="{{ URL::asset('assets/js/jquery.easing.min.js') }}"></script>-->

    <script src="{{ URL::asset('assets/js/jquery.cookie.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <!-- Current Page Vendor and Views -->
    <script src="{{ URL::asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/lazysizes.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.isotope.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.magnific-popup.min.js') }}">
    </script>
    {{-- para videos backgrounds --}}
    {{-- <script src="{{ URL::asset('assets/js/jquery.vide.min.js') }}">
    </script> --}}

    {{-- para animaciones svg --}}
    {{-- <script src="{{ URL::asset('assets/js/vivus.min.js') }}">
    </script> --}}

    <!-- Theme Base, Components and Settings -->
    <script src="{{ URL::asset('assets/js/theme.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/custom.js') }}"></script>
    <script src="{{ URL::asset('assets/js/theme.init.js') }}"></script>

    <!-- Sweet Alert 2 -->
    <script src="{{ URL::asset('assets/js/sweetalert2.all.min.js') }}"></script>

    <!-- Funciones programacion -->
    <script>
      var base_path = "{{ URL::to('/') }}";
                var idioma_global = "{{ Session::get('language') }}";
    </script>
    <script src="{{ URL::asset('assets/js/funciones_generales.js') }}"></script>

    <!-- Theme Initialization Files -->

    <script src="{{ URL::asset('assets/js/countryselect.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/examples.gallery.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{ URL::asset('assets/js/examples.portfolio.js') }}"></script>
    <script src="{{ URL::asset('assets/js/card.js') }}"></script>
    @yield('scripts')
<!--    <script src="https://app.atencionprofesional.com/webchat/webchat.js" type="text/javascript"></script>
    <script>
      init_chat({
        "channel" : "d1022f11-96b5-4a5c-8501-e7576d114969",
        "label_name": "Atención Profesional",
        "welcome_message": "Hola, como podemos ayudarle?" ,
        "placeholder": "Enviar un mensaje",
        "colors": ['#00abd6','#2baab1'],
});


    </script>-->

    <!-- END chatstack.com Live Chat HTML Code -->

  </body>

</html>