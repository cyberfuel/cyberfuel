<ul class="row justify-content-end pt-2">
    <div class="col-auto text-light  m-0 px-4 ">
        <h4 class="font-weight-bold text-uppercase text-2 mb-3 text-white">
            <a onclick="toggleCart()" class="header-nav-features-toggle" style="cursor:pointer;">
                Cerrar &nbsp;
                <span class="pl-4">
                    <i class="far text-success  float-right fa-arrow-alt-circle-right mt-1"></i>
                </span>
            </a>
        </h4>
    </div>
    </div>

    <div class="row">
        <div class="col-10 offset-1 position-relative">
            <h4 class="font-weight-bold text-uppercase text-4 mb-3 text-white">
                Tu compra &nbsp;
                <span class="pl-4">
                    <a href="" class="header-nav-features-toggle">
                        <i class="fas fa-shopping-basket"></i>
                        <span class="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1">16</span>
                    </a>
                </span>
            </h4>

            <table class="w-100 mb-3 " align="left">
                <tbody>


                    <tr class="cart-subtotal">
                        <td class="border-top-0">
                            <strong class="text-white">Subtotal</strong>
                        </td>
                        <td class="border-top-0 text-end">
                            <strong><span class="amount font-weight-medium">$431</span></strong>
                        </td>
                    </tr>
                    <tr class="total">
                        <td>
                            <strong class="text-white text-3-5">Total</strong>
                        </td>
                        <td class="text-end">
                            <strong class="text-white">
                                <span class="amount text-white text-5">$431</span>
                            </strong>
                        </td>
                    </tr>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="p-2">
        <button type="button" class="btn btn-success w-100">Finalizar Compra</button>
    </div>

