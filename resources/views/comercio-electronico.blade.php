@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | {{ __('comercio.titulo') }}</title>
<meta name="keywords" content="hospedaje web, comercio electrónico, ecommerce, e-commerce, woocommerce, tienda, en línea, costa rica, cr, virtual"/>
<meta name="description" content="Crea tu propia tienda en línea en segundos con la solución de Woocommerce y el hospedaje web de e-commerce que Cyberfuel para comercio electrónico en Costa Rica">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/online.webp')}}">
<meta property="og:description" content="{{ __('comercio.titulo') }}">
<meta property="og:title" content="Cyberfuel | Comercio Eelectrónico">
@endsection
@section('content') 
<!--aca empieza el contenido del HTML-->

<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
  <div class="container">
    <div class="row">
      <div class="col-sm-5  align-self-center p-static pt-4">
        <div class="overflow-hidden ">
          <h1 class="text-10 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"> <br>
            <strong>{{ __('comercio.comercio') }} {{ __('comercio.electronico') }}</strong></h1>
        </div>
        <div class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400"> <span class="sub-title text-4 mt-4">{{ __('comercio.descripcion') }}</span> </div>
        <div class="appear-animation d-inline-block" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400"> <a href="#" class="btn btn-modern btn-dark mt-4">{{ __('comercio.consulta') }} <i class="fas fa-arrow-right ms-1"></i></a> </div>
        <div class="appear-animation d-inline-block" data-appear-animation="rotateInUpRight" data-appear-animation-delay="500"> <span class="arrow hlt" style="top: 40px;"></span> </div>
      </div>
      <div class="col-sm-7  align-items-end justify-content-end d-flex pt-md-5">
        <div style="min-height: 350px;" class="overflow-hidden"> <img  alt="" src="{{URL::asset('assets/imagenes/comerce.webp')}}" class="img-fluid appear-animation" data-appear-animation="slideInUp" data-appear-animation-delay="600" data-appear-animation-duration="1s"> </div>
      </div>
    </div>
  </div>
</section>
<div class="container mt-5">
  <div class="row align-items-center">
    <div class="col-12  col-md-10 offset-md-1 mb-4 mb-lg-0 ">
      <div class="card card-background-image-hover border-0" style="background-image: url({{URL::asset('assets/imagenes/bg.webp')}});">
        <div class="card-body  p-5">
          <h4 class="card-title mt-2 mb-2 text-5 font-weight-bold">{{ __('comercio.componentes') }}:</h4>
          <p class=" ">{{ __('comercio.link_uno') }}: <a type="button" class="btn btn-primary btn-with-arrow mb-2" data-bs-toggle="modal" data-bs-target="#solucion">{{ __('comercio.ver_mas') }}<span><i class="fas fa-chevron-right"></i></span></a></p>
          <p class="">{{ __('comercio.link_dos') }}: <a type="button" class="btn btn-primary btn-with-arrow mb-2" data-bs-toggle="modal" data-bs-target="#componente">{{ __('comercio.ver_mas') }}<span><i class="fas fa-chevron-right"></i></span></a></p>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-4">
    <div class="col">
      <div class="pricing-block border rounded">
        <div class="row">
          <div class="col-lg-9">
            <h2 class="font-weight-semibold text-6 line-height-1 mb-3">{{ __('comercio.adicional') }}</h2>
            <hr>
            <div class="row">
              <div class="col-lg-4">
                <ul class="list  list-primary mb-0">
                  <li>{{ __('comercio.dominio_pro') }} (.com o .net) </li>
                  <li> {{ __('comercio.hospedaje_web') }}</li>
                  <li> {{ __('comercio.ins_word') }}</li>
                  <li class="mb-lg-0">{{ __('comercio.ins_woo') }}</li>
                </ul>
              </div>
              <div class="col-lg-4">
                <ul class="list list-primary mb-0">
                  <li> (a) {{ __('comercio.integra_factura') }} </li>
                  <li>(b) {{ __('comercio.perso') }} </li>
                  <li class="mb-lg-0">(c) {{ __('comercio.integra_bancos') }}</li>
                </ul>
              </div>
              <div class="col-lg-4">
                <ul class="list  list-primary mb-0">
                  <li >{{ __('comercio.capa_inicial') }}</i></li>
                  <li></i> {{ __('comercio.acceso') }}</li>
                  <li>{{ __('comercio.asistencia') }}</li>
                  <li class="mb-0"> {{ __('comercio.actuali') }}</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <h4 class="font-weight-bold text-uppercase">{{ __('comercio.precio') }}</h4>
            <div class="plan-price mb-4"> <span class="price"><span class="price-unit">$</span>69</span>
              <label class="price-label text-uppercase">{{ __('comercio.por_mes') }} + IVA</label>
            </div>
            <a href="#" class="btn btn-primary btn-modern btn-xl">{{ __('comercio.consultar') }}</a> </div>
        </div>
      </div>
    </div>
  </div>
  @if (Session::get('language') == 'es')
  <div class="row pt-4">
    <div class="col-12">
      <h4>Notas Importantes:</h4>
      <p class="text-justify"> a. Para la integración con FacturaProfesional.com debe contar con un plan y el API de conexión para la integración. <br>
        b. Personalización: el producto se entrega con una visualización estándar, si el cliente desea que se personalice colores, diseño y otros elementos, se cotizará por parte de Cyberfuel.<br>
        c. Se realiza la integración con BAC Credomatic (VISA, MASTERCARD, AMERICAN EXPRESS, entre otras tarjetas), Paypal, PROMERICA (VISA y MASTERCARD) o BNCR (VISA y MASTERCARD). Para cada uno debe el cliente contactar directamente al procesador y brindarnos la información para realizar la interconexión. Cada banco tiene sus propias condiciones y costos, por lo que se cotizará por parte de Cyberfuel.<br>
        d. En caso de excederse en el plan de hospedaje aplican las mismas tarifas de excedentes de un plan de hospedaje WEB. </p>
    </div>
  </div>
  @else
  <div class="row pt-4"> @if (Session::get('language') == 'es')
    <div class="col-12">
      <h4>Notas Importantes:</h4>
      <p class="text-justify"> a. Para la integración con FacturaProfesional.com debe contar con un plan y el API de conexión para la integración. <br>
        b. Personalización: el producto se entrega con una visualización estándar, si el cliente desea que se personalice colores, diseño y otros elementos, se cotizará por parte de Cyberfuel.<br>
        c. Se realiza la integración con BAC Credomatic (VISA, MASTERCARD, AMERICAN EXPRESS, entre otras tarjetas), Paypal, PROMERICA (VISA y MASTERCARD) o BNCR (VISA y MASTERCARD). Para cada uno debe el cliente contactar directamente al procesador y brindarnos la información para realizar la interconexión. Cada banco tiene sus propias condiciones y costos, por lo que se cotizará por parte de Cyberfuel.<br>
        d. En caso de excederse en el plan de hospedaje aplican las mismas tarifas de excedentes de un plan de hospedaje WEB. </p>
    </div>
    @else
    <div class="col-12">
      <h4>Important notes:</h4>
      <p class="text-justify"> a. For integration with FacturaProfesional.com you must have a plan and the connection API for the integration<br>
        b. Personalization: the product is delivered with a standard display, if the customer wants colors, design and other elements to be customized, it will be quoted by Cyberfuel.<br>
        c. Integration is carried out with BAC Credomatic (VISA, MASTERCARD, AMERICAN EXPRESS, among other cards), Paypal, PROMERICA (VISA and MASTERCARD) or BNCR (VISA and MASTERCARD). For each one, the client must contact the processor directly and provide us with the information to carry out the interconnection. Each bank has its own conditions and costs, so it will be quoted by Cyberfuel.<br>
        d. In case of exceeding the hosting plan, the same surplus rates of a WEB hosting plan apply. </p>
    </div>
    @endif </div>
  @endif </div>
<!--fin el contenido del HTML-->

<div class="modal fade" id="solucion" tabindex="-1" role="dialog" aria-labelledby="solucionWoo" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="solucionWoo">{{ __('comercio.solucion') }}:</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body"> @if (Session::get('language') == 'es')
        <div class="row">
          <div class="col-12">
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i> Artículos
                <ul>
                  <li><i class="fas fa-caret-right"></i>Simples o con Atributos</li>
                  <li><i class="fas fa-caret-right"></i>Virtuales/Servicios</li>
                  <li><i class="fas fa-caret-right"></i>Descargables</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Categorías de artículos</li>
              <li><i class="fas fa-check"></i> Inventario</li>
              <li><i class="fas fa-check"></i> Mostrar tus productos con un catálogo online, <br>
                donde pueden hacer pedidos y solicitar presupuestos</li>
              <li><i class="fas fa-check"></i> Venta en línea
                <ul>
                  <li><i class="fas fa-caret-right"></i>Limitando por países</li>
                  <li><i class="fas fa-caret-right"></i>productos de afiliados de otras tiendas</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Carrito de Compras
              <li><i class="fas fa-check"></i> Órdenes y sus estados
              <li><i class="fas fa-check"></i> Informes
                <ul>
                  <li><i class="fas fa-caret-right"></i> Pedidos</li>
                  <li><i class="fas fa-caret-right"></i> Ventas</li>
                  <li><i class="fas fa-caret-right"></i> Clientes</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Configuración de métodos de transporte</li>
              <li><i class="fas fa-check"></i> Posicionamiento en buscadores “SEO”</li>
              <li><i class="fas fa-check"></i> Y muchas otras cosas más</li>
            </ul>
          </div>
        </div>
        @else
        <div class="row">
          <div class="col-12">
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i> Articles
                <ul>
                  <li><i class="fas fa-caret-right"></i> Simple or with Attributes</li>
                  <li><i class="fas fa-caret-right"></i> Virtual / Services</li>
                  <li><i class="fas fa-caret-right"></i>Downloadable</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Item categories</li>
              <li><i class="fas fa-check"></i> Inventory</li>
              <li><i class="fas fa-check"></i> Show your products with an online catalog,
                where they can order and request quotes</li>
              <li><i class="fas fa-check"></i> Online sale
                <ul>
                  <li><i class="fas fa-caret-right"></i>Limiting by countries</li>
                  <li><i class="fas fa-caret-right"></i>Affiliate products from other store</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Shopping Cart
              <li><i class="fas fa-check"></i> Orders and their statuses
              <li><i class="fas fa-check"></i> Reports
                <ul>
                  <li><i class="fas fa-caret-right"></i> Orders</li>
                  <li><i class="fas fa-caret-right"></i> Sales</li>
                  <li><i class="fas fa-caret-right"></i> Customers</li>
                </ul>
              </li>
              <li><i class="fas fa-check"></i> Transport method settings</li>
              <li><i class="fas fa-check"></i> Search engine optimization "SEO"</li>
              <li><i class="fas fa-check"></i>And many other things</li>
            </ul>
          </div>
        </div>
        @endif </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('comercio.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="componente" tabindex="-1" role="dialog" aria-labelledby="componenteWoo" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="componenteWoo">{{ __('comercio.link_dos') }}:</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12"> @if (Session::get('language') == 'es')
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i> Lista de deseos</li>
              <li><i class="fas fa-check"></i> Ofertas y descuentos</li>
              <li><i class="fas fa-check"></i> Personalización de los productos, según las preferencias del cliente como por ejemplo el caso de tazas, llaveros, almohadones, ropa, entre otros productos</li>
              <li><i class="fas fa-check"></i> Gestionas los gastos de envío</li>
              <li><i class="fas fa-check"></i> Te permite sincronizar con otras plataformas como Mercado Libre o Amazon</li>
              <li><i class="fas fa-check"></i> Cuentas con soporte multi-idioma</li>
              <li><i class="fas fa-check"></i> Atención al cliente online</li>
              <li><i class="fas fa-check"></i> Crea paquetes de productos</li>
              <li><i class="fas fa-check"></i> Recibe el pago en múltiples monedas</li>
              <li><i class="fas fa-check"></i> Crea paquetes de productos</li>
              <li><i class="fas fa-check"></i> Integración con métodos de pago bancarios</li>
              <li><i class="fas fa-check"></i> Crear eventos y vender entradas en la web para poder asistir (con el plugin: The Events Calendar)</li>
              <li><i class="fas fa-check"></i> Crear un sitio de membresía y ocultar contenido de la web a usuarios que no hayan comprado un producto (con el plugin WooCommerce Memberships)</li>
              <li><i class="fas fa-check"></i> Entre otros</li>
            </ul>
            @else
            <ul class="list list-icons list-primary list-borders">
              <li><i class="fas fa-check"></i>Wish list</li>
              <li><i class="fas fa-check"></i>Offers and discounts</li>
              <li><i class="fas fa-check"></i>Customization of products, according to customer preferences, such as mugs, keychains, pillows, clothing, among other products</li>
              <li><i class="fas fa-check"></i>You manage shipping costs</li>
              <li><i class="fas fa-check"></i>It allows you to synchronize with other platforms such as Mercado Libre or Amazon</li>
              <li><i class="fas fa-check"></i>Accounts with multi-language support</li>
              <li><i class="fas fa-check"></i>Online customer service</li>
              <li><i class="fas fa-check"></i>Create product packages</li>
              <li><i class="fas fa-check"></i>Get paid in multiple currencies</li>
              <li><i class="fas fa-check"></i>Create product packages</li>
              <li><i class="fas fa-check"></i>Integration with bank payment methods</li>
              <li><i class="fas fa-check"></i>Create events and sell tickets on the web to attend (with the plugin: The Events Calendar)</li>
              <li><i class="fas fa-check"></i>Create a membership site and hide web content from users who have not purchased a product (with the WooCommerce Memberships plugin)</li>
              <li><i class="fas fa-check"></i>Among others</li>
            </ul>
            @endif </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('comercio.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>
@endsection 