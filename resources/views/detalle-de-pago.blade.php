@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Registrese con nosotros</title>
<meta name="keywords"
    content="Detalle de pago, costa rica, cr, solución, maquinas, servidores, archivos, sitio seguro, seguridad" />
<meta name="description"
    content="Con nuestra solución puede tener un respaldo de su computador o de sus servidores virtuales o físicos (completo o parcial) fuera de su lugar de trabajo o habitación, garantizando la información segura ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/portada.jpg')}}">
<meta property="og:description" content="Registrese con nosotros">
<meta property="og:title" content="Cyberfuel | Detalle de pago">
@endsection

@section('css')
<style>
</style>
@endsection
@section('content')
<!--aca empieza el contenido del HTML-->
<div role="main" class="main shop pb-4">
    <div class="container-lg info">
        <div class="row mt-4">

            <div class="col-12 mb-2 mb-lg-0">
                <h2 class="text-color-dark font-weight-bold text-5-5 mb-3">
                    {{ __('master.deta_pa') }}
                </h2>
            </div>
        </div>



        <div class="row mt-5">
            <div class="col-lg-6">


                <div class="row justify-content-center">
                    <div class="col-12">

                        <div class="card border-width-3" data-plugin-sticky=""
                            data-plugin-options="{'minWidth': 991, 'containerSelector': '.row', 'padding': {'top': 85}}"
                            style="width: 100%;">
                            <div class="card-body p-4">
                                <h4 class="font-weight-bold text-uppercase text-4 mb-3">
                                    {{ __('compra.cantidad_articulos') }}
                                    <span class="pl-4">
                                        <a href="" class="header-nav-features-toggle">
                                            <i class="fas fa-shopping-basket text-success"></i>
                                            <span
                                                class="badge badge-dark badge-sm rounded-pill text-uppercase px-2 py-1">
                                                1
                                            </span>
                                        </a>
                                    </span>
                                </h4>
                                <table class="shop_table cart-totals mb-4">
                                    <tbody>

                                        <tr>
                                            <td>
                                                <strong
                                                    class="d-block text-color-dark line-height-1 font-weight-semibold">Dominio
                                                    <span class="product-qty">x1</span></strong>
                                                <span class="text-1">8965</span>
                                            </td>
                                            <td class="text-end align-top">
                                                <span class="amount font-weight-medium text-color-grey">$15.00</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border-top-0 pt-0">
                                                <strong
                                                    class="d-block text-color-dark line-height-1 font-weight-semibold">Hospedaje
                                                    Personal <span class="product-qty">x1</span></strong>
                                                <span class="text-1">235</span>
                                            </td>
                                            <td class="border-top-0 text-end align-top pt-0">
                                                <span class="amount font-weight-medium text-color-grey">$28.00</span>
                                            </td>
                                        </tr>


                                        <tr class="cart-descuento">
                                            <td class="border-top-0">
                                                <div class="col-12">
                                                    @if($cupon_descuento_aplicado == null)
                                                    <div class="row">
                                                        <div class="col">
                                                            <p class="text-color-dark">
                                                                {{ __('master.ti_cup') }} <br>
                                                                <a class="text-color-hover-primary text-uppercase text-decoration-none font-weight-bold"
                                                                    data-bs-toggle="collapse"
                                                                    data-bs-target=".coupon-form-wrapper"
                                                                    style="cursor:pointer;">
                                                                    <i class="fas fa-ticket"></i> {{
                                                                    __('master.digi_cup') }}
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    @else
                                                    <div class="row">&nbsp;</div>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="border-top-0 text-end">
                                                <strong><span class="font-weight-medium">0</span>
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr class="cart-cupon">

                                            <div class="row coupon-form-wrapper collapse mb-5">
                                                <div class="col-12 p-0">
                                                    <div class="row justify-content-end">
                                                        <div class="col-auto">
                                                            <a data-bs-toggle="collapse"
                                                                data-bs-target=".coupon-form-wrapper">
                                                                <i class="fas fa-times fa-2x text-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="card border-width-3 border-radius-0">
                                                        <div class="card-body">
                                                            <div class="d-flex align-items-center">
                                                                <input type="text" id="cupon_descuento"
                                                                    name="cupon_descuento"
                                                                    class="form-control border-color-primary h-auto border-radius-0 line-height-1 py-3"
                                                                    placeholder="{{ __('master.cod_cup') }}" />
                                                                <a onClick="aplicar_cupon_descuento();"
                                                                    data-bs-toggle="collapse"
                                                                    data-bs-target=".login-cupon"
                                                                    class="btn btn-success btn-modern text-white bg-color-success text-color-hover-light bg-color-hover-success text-uppercase text-2 font-weight-bold border-0 border-radius-0 ws-nowrap btn-px-2 py-3 ms-1">
                                                                    {{ __('master.apli_cup') }}
                                                                </a>
                                                            </div>

                                                            <div id="div_error_cupon_descuento" class="text-danger"
                                                                style="display:none;">
                                                                {{ __('master.error_cupon_descuento') }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>

                                        <tr class="cart-subtotal">
                                            <td class="border-top-0">
                                                <strong class="text-color-dark">Subtotal Neto</strong>
                                            </td>
                                            <td class="border-top-0 text-end">
                                                <strong><span class="amount font-weight-medium">$43.00</span></strong>
                                            </td>
                                        </tr>

                                        <tr class="cart-subtotal">
                                            <td class="border-top-0">
                                                <strong class="text-color-dark">Impuesto</strong>
                                            </td>
                                            <td class="border-top-0 text-end">
                                                <strong><span class="amount font-weight-medium">$3.00</span></strong>
                                            </td>
                                        </tr>



                                        <tr class="total">
                                            <td>
                                                <strong class="text-color-dark text-3-5">Total</strong>
                                            </td>
                                            <td class="text-end">
                                                <strong class="text-color-dark"><span
                                                        class="amount text-color-dark text-5">$47.00</span></strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">

                <div class="demo-container w-100">
                    <div class="col">

                        <div class="tabs tabs-bottom tabs-center tabs-simple">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item active" role="presentation">
                                    <a class="nav-link active" href="#tabsNavigationSimpleIcons1" data-bs-toggle="tab"
                                        aria-selected="true" role="tab">

                                        <p class="mb-0 pb-0">Tarjeta</p>
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#tabsNavigationSimpleIcons2" data-bs-toggle="tab"
                                        aria-selected="false" role="tab" tabindex="-1">

                                        <p class="mb-0 pb-0">PayPal</p>
                                    </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active show" id="tabsNavigationSimpleIcons1" role="tabpanel">
                                    <div class="text-center">
                                        <div class="row align-items-center" id="cash-on-delivery">
                                            <div class="col-2 p-0">
                                                <div class="row">
                                                    <div class="col-12 mt-2"> <img width="45"
                                                            src="{{URL::asset('assets/imagenes/logos/pago/visa.png')}}"
                                                            alt="Cyberfuel | Pago"></div>
                                                    <div class="col-12 mt-2"><img width="45"
                                                            src="{{URL::asset('assets/imagenes/logos/pago/mastercard.png')}}"
                                                            alt="Cyberfuel | Pago"></div>
                                                    <div class="col-12 mt-2"> <img width="45"
                                                            src="{{URL::asset('assets/imagenes/logos/pago/american.png')}}"
                                                            alt="Cyberfuel | Pago"></div>
                                                </div>
                                            </div>


                                            <div class="col-10 p-0">
                                                <div class="row">
                                                    <div class="card-wrapper py-4"></div>
                                                </div>
                                            </div>

                                            <div class="col">

                                                <div class="form-container active">

                                                    <form id="miPago" action="" class="needs-validation" novalidate>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label for="" class="form-label">{{ __('master.nu_tar')
                                                                    }} <span class="text-color-danger">*</span></label>
                                                                <input id="card_number"
                                                                    class="form-control border-color-primary h-auto py-2"
                                                                    placeholder="Card number" type="tel" name="number"
                                                                    required>


                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="" class="form-label">{{
                                                                    __('master.nom_titular')
                                                                    }} <span class="text-color-danger">*</span></label>
                                                                <input id="card_name"
                                                                    class="form-control border-color-primary h-auto py-2"
                                                                    placeholder="Full name" type="text" name="name"
                                                                    required>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label class="form-label">
                                                                    {{ __('master.a_expi') }}
                                                                    <span class="text-color-danger">*</span>
                                                                </label>
                                                                <input id="card_expiry"
                                                                    class="form-control border-color-primary h-auto py-2"
                                                                    placeholder="MM/YY" type="tel" name="expiry"
                                                                    required>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="validationTooltip06" class="form-label">
                                                                    {{ __('master.cod_seg') }}
                                                                    <span class="text-color-danger">*</span>
                                                                </label>
                                                                <input id="card_cvc"
                                                                    class="form-control border-color-primary h-auto py-2"
                                                                    placeholder="CVC" type="text" name="cvc" required>
                                                            </div>
                                                        </div>

                                                        <div class="row justify-content-end align-items-end">
                                                            <div class="col-auto">

                                                                <a href="javascript: history.back()" type="button"
                                                                    id="btn_registro_compra"
                                                                    class="btn btn-success btn-modern  text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-3 py-3 ">
                                                                    <i class="fas fa-arrow-left ms-2"></i>
                                                                    Regresar

                                                                </a>
                                                                <button disabled type="button" id="submitButton"
                                                                    class="btn btn-dark btn-modern  text-uppercase bg-color-hover-primary border-color-hover-primary border-radius-0 text-3 py-3 ">
                                                                    {{ __('master.pago') }}
                                                                    <i class="fas fa-arrow-right ms-2"></i>
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="tabsNavigationSimpleIcons2" role="tabpanel">
                                    <div class="text-center">
                                        <div class="row py-3" id="paypal">
                                            <div class="col">
                                                @if (Session::get('language') == 'es')
                                                <h4>Usted ha seleccionado el método de pago PayPal.</h4>
                                                @else
                                                <h4>You have selected to pay via PayPal.</h4>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>



                </div>

            </div>


        </div>


    </div>
</div>

<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="card border-width-3 border-radius-0 border-color-success">
                            <div class="card-body text-center">
                                <p class="text-color-dark font-weight-bold text-4-5 mb-0"><i
                                        class="fas fa-check text-color-success me-1"></i> {{ __('master.gra_reci') }}
                                </p>
                            </div>
                        </div>
                        <div class="d-flex flex-column flex-md-row justify-content-between py-3 px-4 my-4">
                            <div class="text-center"> <span> {{ __('master.nu_or') }} <br>
                                    <strong class="text-color-dark">31891</strong> </span>
                            </div>
                            <div class="text-center mt-4 mt-md-0"> <span> {{ __('master.fech') }} <br>
                                    <strong class="text-color-dark">Octubre 17, 2020</strong> </span>
                            </div>
                            <div class="text-center mt-4 mt-md-0"> <span> {{ __('master.cor_ele') }} <br>
                                    <strong class="text-color-dark">gsoto@cyberfuel.com</strong> </span>
                            </div>
                            <div class="text-center mt-4 mt-md-0"> <span> Total <br>
                                    <strong class="text-color-dark">$30</strong> </span>
                            </div>
                            <div class="text-center mt-4 mt-md-0">
                                <span> {{ __('master.me_pa') }} <br>
                                    <strong class="text-color-dark">{{ __('master.tar') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="card border-width-3 border-radius-0 border-color-hover-dark mb-4">
                            <div class="card-body">
                                <h4 class="font-weight-bold text-uppercase text-4 mb-3">
                                    {{ __('master.t_or') }}
                                </h4>

                                <table class="w-100 mb-3">
                                    <tbody>
                                        <tr>
                                            <td><strong
                                                    class="d-block text-color-dark line-height-1 font-weight-semibold">gbeart.com</strong>
                                                <span class="text-1">Dominio</span>
                                            </td>
                                            <td class="text-end align-top"><span
                                                    class="amount font-weight-medium text-color-grey">$15</span></td>
                                        </tr>
                                        <tr>
                                            <td class="border-top-0 pt-0"><strong
                                                    class="d-block text-color-dark line-height-1 font-weight-semibold">SSL
                                                    <span class="d-inline-block mt-1">{{ __('compra.gratis')
                                                        }}</span></strong></td>
                                            <td class="border-top-0 text-end align-top pt-0"><span
                                                    class="amount font-weight-medium text-color-grey">$0</span></td>
                                        </tr>
                                        <tr>
                                            <td class="border-top-0 pt-0"><strong
                                                    class="d-block text-color-dark line-height-1 font-weight-semibold">CUPON
                                                    <span class="d-inline-block mt-1"></span></strong></td>
                                            <td class="border-top-0 text-end align-top pt-0"><span
                                                    class="amount font-weight-medium text-color-grey">SAVE
                                                    <del>$15</del></span></td>
                                        </tr>
                                        <tr class="cart-subtotal">
                                            <td class="border-top-0"><strong class="text-color-dark">Subtotal</strong>
                                            </td>
                                            <td class="border-top-0 text-end"><strong><span
                                                        class="amount font-weight-medium">$431</span></strong></td>
                                        </tr>
                                        <tr class="total">
                                            <td><strong class="text-color-dark text-3-5">Total</strong></td>
                                            <td class="text-end"><strong class="text-color-dark"><span
                                                        class="amount text-color-dark text-5">$431</span></strong></td>
                                        </tr>
                                        <tr></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    {{ __('master.cerrar') }}
                </button>
                <button type="button" class="btn btn-primary">
                    {{ __('master.nu_com') }}
                </button>
            </div>
        </div>
    </div>
</div>
<!--fin el contenido del HTML-->
@endsection

@section('scripts')
<script>
    /*function vercodigo(){
            $(".cart-promo").removeClass("d-none");
        }*/

    $(document).ready(function(){

        function verpago(){
            $('.info').addClass('d-none');
            $('.recibido').removeClass('d-none');
        }



 


    // Función para verificar si todos los campos tienen contenido
    function verificarContenidoCampos() {
        var inputNumero = $('#card_number').val();
        var inputNombre = $('#card_nombre').val();
        var inputExpiry = $('#card_expiry').val();
        var inputCvc = $('#card_cvc').val();

        // Verificar si todos los campos tienen contenido
        if (inputNumero !== '' && inputNombre !== '' && inputExpiry !== '' && inputCvc !== ''){
            $('#submitButton').prop('disabled', false); // Habilitar el botón

 
        } else {
            $('#submitButton').prop('disabled', true); // Deshabilitar el botón
   
        }
    }

    $('#card_number').on('blur', function() {
        var inputNumero = $(this).val();
        if (inputNumero.length < 16) {
            Swal.fire({
  icon: "error",
  title: "Oops...",
  text: "Debe completar los numeros de la tarjeta!",
 
});
        }
    });



    // Adjuntar eventos de cambio a los campos de entrada
    $('#card_number, #card_nombre, #card_expiry, #card_cvc').on('input', function() {
        verificarContenidoCampos();
    });

    // Adjuntar evento de clic al botón
    $('#submitButton').on('click', function() {
        // Redirigir a la página destino si todos los campos tienen contenido
        if (!$(this).prop('disabled')) {

            Swal.fire({
                position: "top-center",
                icon: "success",
                title: "Se completo exitosamente",
                showConfirmButton: false,
                timer: 1500
            }).then((result) => {
                window.location.href = "{{ __('rutas.url_orden_completa') }}"
			});
          
            
        }
    });

    // Verificar inicialmente si todos los campos tienen contenido
    verificarContenidoCampos();

    var card = new Card({

form: document.querySelector('form'),

container: '.card-wrapper', // *required*

width: 350, // optional — default 350px
formatting: true, // optional - default true

});
});
</script>
@endsection