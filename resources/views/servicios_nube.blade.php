@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel</title>
<meta name="keywords" content="Seguridad, solución, escaneo, certificado, certificado de seguridad, ssl, waf, pci, pci-dss, servidor"/>
<meta name="description" content="Por medio de diferentes soluciones de seguridad en su servidor podrá tener protección, utilizando tecnologías como: certificado de seguridad SSL, WAF y/o certificando PCI-DSS ">
@endsection
@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel">
<meta property="og:title" content="Cyberfuel | Seguridad en su sitio">
@endsection
@section('content') 
<!--aca empieza el contenido del HTML-->

<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
  <div class="container">
    <div class="row">
      <div class="col-md-6  align-self-center p-static ">
        <div class="overflow-hidden ">
          <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>{{ __('master.servidores_flex') }}</strong></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row my-5">
    <div class="col-12 "> @if (Session::get('language') == 'es')
      <h2 class="text-color-dark font-weight-normal text-6 mb-2">¿Qué es?</h2>
      <p class="text-justify">Los servidores VPS Flex en la nube es un sistema escalable en el cual se puede aumentar las características específicas del servidor según sus necesidades.  Están diseñados para contribuir al crecimiento dinámico de su negocio, acorde a la flexibilidad y necesidades de su operación. </p>
      @else
        <h2 class="text-color-dark font-weight-normal text-6 mb-2">¿What is?</h2>
      <p class="text-justify">VPS Flex servers in the cloud is a scalable system in which the specific characteristics of the server can be increased according to your needs. They are designed to contribute to the dynamic growth of your business, according to the flexibility and needs of your operation.</p>
      @endif </div>
  </div>
  <span class="py-4">&nbsp;</span> </div>
<section class="section section-height-3 bg-color-grey-scale-1 m-0 border-0">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-lg-6 pb-sm-4 pb-lg-0 pe-lg-5 mb-sm-5 mb-lg-0">
           @if (Session::get('language') == 'es')
        <h2 class="text-color-dark font-weight-normal text-6 mb-2">¿Cómo funciona?</h2>
        <p class="pe-5 me-5 text-justify">Los Servidores Virtuales funcionan como un servidor tradicional, cuentan con su propio sistema operativo y recursos de cómputo como procesador, memoria RAM y almacenamiento; en un espacio privado y seguro, con la ventaja de que tener el servidor en minutos listo para ser utilizado y pudiendo aumenta las características de estos en segundos con un menor costo tanto de adquisición de Hardware, Software, facilidades físicas de colocación y mantenimiento.</p>
        @else
          
            <h2 class="text-color-dark font-weight-normal text-6 mb-2">¿How does it work?</h2>
        <p class="pe-5 me-5 text-justify">Virtual Servers work like a traditional server, they have their own operating system and computing resources such as processor, RAM and storage; in a private and secure space, with the advantage of having the server ready to be used in minutes and being able to increase its characteristics in seconds with a lower cost of both hardware acquisition, software, physical placement and maintenance facilities.</p>
          
          @endif  
      </div>
      <div class="col-sm-8 col-md-6 col-lg-4 offset-sm-4 offset-md-4 offset-lg-2 position-relative mt-sm-5" style="top: 1.7rem;"> <img src="{{URL::asset('assets/imagenes/servidor/servidor_318x288.webp')}}" class="img-fluid position-absolute d-none d-sm-block appear-animation animated expandIn appear-animation-visible" data-appear-animation="expandIn" data-appear-animation-delay="300" style="top: 10%; left: -50%; animation-delay: 300ms;" alt=""> <img src="{{URL::asset('assets/imagenes/servidor/servidor_164x148.webp')}}" class="img-fluid position-absolute d-none d-sm-block appear-animation animated expandIn appear-animation-visible" data-appear-animation="expandIn" style="top: -33%; left: -29%; animation-delay: 100ms;" alt=""> <img src="{{URL::asset('assets/imagenes/servidor/servidor_212x186.webp')}}" class="img-fluid position-relative appear-animation mb-2 animated expandIn appear-animation-visible" data-appear-animation="expandIn" data-appear-animation-delay="600" alt="" style="animation-delay: 600ms;"> </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row justify-content-center pt-5">
    <div class="col-12 pb-3">
      <div class="row">
        <div class=" col-md-2"> <img width="160" height="auto" class="img-fluid mx-auto d-block appear-animation" data-appear-animation="backInLeft" data-appear-animation-delay="0" data-appear-animation-duration="1s" src="{{URL::asset('assets/imagenes/caracteristicas.webp')}}" alt="Cyberfuel" title="Cyberfuel" loading="lazy"> </div>
        <div class="col-md-10">
            @if (Session::get('language') == 'es')
          <h4>Características:</h4>
          <ul class="list list-icons list-icons-lg">
            <li><i class="fas fa-check"></i>Panel de Control: Administre desde una única consola web los recursos y funcionalidades de tus servidores, al mismo tiempo que accede remotamente al servidor virtual.</li>
            <li><i class="fas fa-check"></i>Monitoreo del uso del espacio ocupado en la nube</li>
            <li><i class="fas fa-check"></i>Versatilidad: se pueden instalar todo tipo de programas y aplicaciones.</li>
            <li><i class="fas fa-check"></i>Posibilidad de configurar nuevos servicios para los clientes: a través del panel de control Plesk que permite crear paquetes de hosting y que tiene una interfaz muy fácil de usar.</li>
            <li><i class="fas fa-check"></i>Seguros: Estas máquinas virtuales están redundadas, de forma que no están vinculadas a un único servidor físico. Esto permite que, si un componente utilizado por el cliente se detiene por alguna razón, hay otro recorrido válido, por lo tanto, el servicio sigue siempre activo y no hay ningún fallo.</li>
              
                @else
              
              <h4>Characteristic:</h4>
          <ul class="list list-icons list-icons-lg">
              
              
 <li><i class="fas fa-check"></i> Control Panel: Manage the resources and functionalities of your servers from a single web console, while remotely accessing the virtual server.</li>
 <li><i class="fas fa-check"></i>Monitoring of the use of space occupied in the cloud</li>
 <li><i class="fas fa-check"></i> Versatility: all kinds of programs and applications can be installed.</li>
 <li><i class="fas fa-check"></i> Possibility of configuring new services for clients: through the Plesk control panel that allows you to create hosting packages and has a very easy-to-use interface.</li>
 <li><i class="fas fa-check"></i>Secure: These virtual machines are redundant, so they are not tied to a single physical server. This allows that, if a component used by the client stops for some reason, there is another valid route, therefore, the service is always active and there is no failure.</li>
          
          @endif 
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-center pt-2">
    <div class="col-12 pb-3">
      <div class="row">
        <div class="col-md-2"> <img width="160" height="auto" class="img-fluid mx-auto d-block appear-animation" data-appear-animation="backInLeft" data-appear-animation-delay="0" data-appear-animation-duration="1s" src="{{URL::asset('assets/imagenes/ventajas2.webp')}}" alt="Cyberfuel" title="Cyberfuel" loading="lazy"> </div>
        <div class="col-md-10">
            @if (Session::get('language') == 'es')
          <h4>Ventajas:</h4>
          <ul class="list list-icons list-icons-lg">
            <li><i class="fas fa-check"></i>Fácil de usar</li>
            <li><i class="fas fa-check"></i>No es necesario ser un experto en TI para utilizar nuestro software </li>
            <li><i class="fas fa-check"></i>Después de una línea, no hay necesita cualquier tipo de actualización o mantenimiento, por lo que puede ahorrar tiempo y dinero</li>
            <li><i class="fas fa-check"></i>Alojado en un Centro de Datos Construido con certificación de facilidades construidas nivel R3 de ANSI/TIA-942 y personal certificado en Operación y Mantenimiento nivel DCOS 3</li>
            <li><i class="fas fa-check"></i>Solución basada en VMware líder en virtualización</li>
            <li><i class="fas fa-check"></i>En caso de tener varios equipos se les puede entregar un Centro de Datos Virtual (Virtual Data Centre) donde podrá tener el control de configuración y reinicio de los equipos</li>
              
                @else
              
                <h4>Advantage:</h4>
          <ul class="list list-icons list-icons-lg">
          
              
            <li><i class="fas fa-check"></i>	Easy to use</li>
            <li><i class="fas fa-check"></i>You don't have to be an IT expert to use our software</li>
            <li><i class="fas fa-check"></i> After a line, there is no need for any type of update or maintenance, so you can save time and money</li>
            <li><i class="fas fa-check"></i> Hosted in a Data Center Built with ANSI/TIA-942 level R3 certification of built facilities and personnel certified in Operation and Maintenance level DCOS 3</li>
            <li><i class="fas fa-check"></i> Solution based on VMware leader in virtualization</li>
            <li><i class="fas fa-check"></i>If you have multiple computers, you can provide them with a Virtual Data Center where you can control the configuration and restart of the computers.</li>
          
          @endif 
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-center pt-2">
    <div class="col-12 pb-5">
      <div class="row">
        <div class="col-md-2"> <img width="160" height="auto" class="img-fluid mx-auto d-block appear-animation" data-appear-animation="backInLeft" data-appear-animation-delay="0" data-appear-animation-duration="1s" src="{{URL::asset('assets/imagenes/beneficios2.webp')}}" alt="Cyberfuel" title="Cyberfuel" loading="lazy"> </div>
        <div class="col-md-10">
            @if (Session::get('language') == 'es')
          <h4>Beneficios:</h4>
          <ul class="list list-icons list-icons-lg">
            <li><i class="fas fa-check"></i>Flexibilidad a nivel de recursos del equipo</li>
            <li><i class="fas fa-check"></i>Escalabilidad a nivel de almacenamiento </li>
              <li><i class="fas fa-check"></i>Rentabilidad de pago por uso de recursos requeridos</li>
      
          </ul>
          <span class="float-end"> <a href="mercado#servidores" class="btn btn-primary  font-weight-semibold text-white btn-px-5 btn-py-2 border-width-1 text-4 mt-3" aria-label="Cyberfuel">Contáctenos</a> </span> </div>
            
              @else
          
          
           <h4>Benefits:</h4>
          <ul class="list list-icons list-icons-lg">
            <li><i class="fas fa-check"></i>Flexibility at the level of team resources</li>
            <li><i class="fas fa-check"></i>Storage-level scalability </li>
              <li><i class="fas fa-check"></i>Profitability of payment for use of required resources</li>
      
          </ul>
          <span class="float-end"> <a href="mercado#servidores" class="btn btn-primary  font-weight-semibold text-white btn-px-5 btn-py-2 border-width-1 text-4 mt-3" aria-label="Cyberfuel">Contact Us</a> </span> </div>
          
          @endif 
      </div>
    </div>
  </div>
</div>

<!--fin el contenido del HTML--> 
@endsection 