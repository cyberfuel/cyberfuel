@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | {{ __('master.tenga_plan') }}</title>
<meta name="keywords" content="Solución, ante un desastre, servicio, DRaaS, Costa Rica, CR, sitio alterno, plan B, seguro, sitio principal"/>
<meta name="description" content="La solución de DRaaS (solución ante un desastre como servicio), permite que en minutos pueda tener su operación arriba si tuviera problemas en el sitio principal">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/draas.webp')}}">
<meta property="og:description" content="{{ __('master.tenga_plan') }}">
<meta property="og:title" content="Cyberfuel | DraaS">
@endsection 
@section('content') 
<!--aca empieza el contenido del HTML-->

<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
  <div class="container">
    <div class="row">
      <div class="col-12 align-self-center p-static ">
        <div class="overflow-hidden ">
          <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>Recuperación de desastres como servicio (DRaaS)</strong></h1>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row mt-5">
    <div class="col-12 "> @if (Session::get('language') == 'es')
      <p class="text-justify">En los últimos años hemos visto eventos naturales que han dejado fuera de operación a empresas por horas, semanas o meses generando una pérdida grande para la empresa y es por eso que cada vez las empresas tienen un Plan de Recuperación de Desastres (Disaster Recovery Plan – DRP) que es utilizado por empresas que necesitan prevenir cualquier tipo de incidentes.</p>
      @else
      <p class="text-justify">In recent years we have seen natural events that have left companies out of operation for hours, weeks or months, generating a large loss for the company and that is why every time companies have a Disaster Recovery Plan (Disaster Recovery Plan – DRP) that is used by companies that need to prevent any type of incidents.</p>
      @endif </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="tabs tabs-bottom tabs-center tabs-simple">
        <ul class="nav nav-tabs">
          <li class="nav-item active"> <a class="nav-link active" href="#tabsNavigationSimple1" data-bs-toggle="tab">{{ __('master.ventajas') }}</a> </li>
          <li class="nav-item"> <a class="nav-link" href="#tabsNavigationSimple2" data-bs-toggle="tab">{{ __('master.beneficios') }}</a> </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tabsNavigationSimple1">
            <div class="row align-items-center">
              <div class="col-12"><img class="img-fluid m-auto d-block" src="{{URL::asset('assets/imagenes/ventajas.webp')}}" alt="Cyberfuel"></div>
              <div class="col-12">
               @if (Session::get('language') == 'es')
                   <ul class="text-justify list list-icons list-primary">
    <li><i class="fas fa-check"></i>  <strong>COPIA AUTOMÁTICA Y PROGRAMADA:</strong> Las copias se hacen automáticamente del ordenador a un servicio remoto de backup online con la planificación que necesites.</li>
     <li><i class="fas fa-check"></i>  <strong>COPIA OPTIMIZADA:</strong> Se hace copia únicamente de todo lo que se ha hecho nuevo.</li>
     <li><i class="fas fa-check"></i>  <strong>ACCESIBLE:</strong> Puedes acceder en cuestión de minutos y recuperar los datos desde otro equipo.</li>
     <li><i class="fas fa-check"></i>  <strong>SEGURA:</strong> Tus archivos se guardan de forma segura, con cifrado de datos, ya que se hace a través de un servicio que especialmente pensado para empresas. La transmisión se realiza mediante canales seguros de datos.</li>
     <li><i class="fas fa-check"></i> <strong>GENERA TRANQUILIDAD:</strong> Se pueden prevenir problemas repentinos ocasionados por la pérdida de datos derivados de robos, incendios, y otras incidencias que pueden afectar a nuestro sistema de tratamiento de datos.</li>
     
    </ul>
                  @else
            <ul class="text-justify list list-icons list-primary">
    <li><i class="fas fa-check"></i> <strong>AUTOMATIC AND SCHEDULED COPY:</strong> The copies are made automatically from the computer to a remote online backup service with the schedule you need.</li>
     <li><i class="fas fa-check"></i>  <strong>OPTIMIZED COPY:</strong> A copy is made only of everything that has been made new.</li>
     <li><i class="fas fa-check"></i>  <strong>ACCESSIBLE:</strong> You can access in a matter of minutes and recover the data from another computer.</li>
     <li><i class="fas fa-check"></i>   <strong>SECURE:</strong> Your files are stored securely, with data encryption, as it is done through a service specially designed for companies. The transmission is carried out through secure data channels.</li>
     <li><i class="fas fa-check"></i>  <strong>GENERATES PEACE OF MIND:</strong> Sudden problems caused by the loss of data resulting from theft, fire, and other incidents that may affect our data processing system can be prevented.</li>
     
    </ul>
                  @endif 
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tabsNavigationSimple2">
            <div class="row align-items-center">
              <div class="col-12"><img class="img-fluid m-auto d-block" src="{{URL::asset('assets/imagenes/beneficios.webp')}}" alt="Cyberfuel"></div>
              <div class="col-12">
                <div class="px-2">
                  <ul class="text-justify list list-icons list-primary">
                    @if (Session::get('language') == 'es')
                    <li><i class="fas fa-check"></i> <strong>Rentable:</strong> El costo de DRaaS se define principalmente por la cantidad de servidores virtuales que ejecutará en la nube y el volumen de recursos de almacenamiento que consumirán sus datos. Es probable que sea mucho menor que el costo de construir y mantener un duplicado físico de su entorno de TI existente para fines de recuperación de desastres, y hace que DRaaS sea una oferta particularmente atractiva para organizaciones más pequeñas con presupuestos de TI relativamente modestos.</li>
                    <li><i class="fas fa-check"></i> <strong>Elimina los gastos generales de mantenimiento:</strong> Ya no tiene que preocuparse por mantener la infraestructura física de recuperación ante desastres, incluidos los servidores y el almacenamiento, o asegurarse de que siempre haya un experto en recuperación ante desastres dentro de su organización para organizar una recuperación.</li>
                    <li><i class="fas fa-check"></i> <strong>Tiempo de inactividad casi nulo:</strong> Una solución de respaldo existente puede ofrecer una opción de recuperación ante desastres con un objetivo de tiempo de recuperación (RTO) medido en horas, pero DRaaS ofrece una forma viable de garantizar que sus operaciones y, por lo tanto, sus clientes y socios, experimenten casi ninguna interrupción en El evento de un desastre.</li>
                    <li><i class="fas fa-check"></i> <strong>Agrega capacidad de DR:</strong> Si ya tiene una solución de DR para sus aplicaciones de misión crítica, DRaaS puede ser una forma útil de garantizar que las aplicaciones menos importantes (o nuevas) también tengan cobertura de recuperación ante desastres.</li>
                    <li><i class="fas fa-check"></i> <strong>Proporciona un entorno de prueba:</strong> Se puede utilizar un entorno DRaaS para llevar a cabo pruebas de seguridad u otras que podrían ser perjudiciales sin afectar su infraestructura ni operaciones principales.</li>
                    <li><i class="fas fa-check"></i> <strong>Cyberfuel cuenta con un Data Center certificado:</strong> Rated-3 según la norma ANSI/TIA 942-B:2017, para responder a los demandantes requerimientos del negocio.</li>
                    <li><i class="fas fa-check"></i> <strong>Relaciones de largo plazo con los clientes:</strong> Enfocándose en una venta consultiva que permite flexibilidad para satisfacer las necesidades del negocio.</li>
                    <li><i class="fas fa-check"></i> <strong>Data Center interconectado:</strong> Permite acelerar la implementación de la replicación, en caso de que el ambiente productivo y el de replicación sean gestionados por Cyberfuel.</li>
                    <li><i class="fas fa-check"></i> <strong>Pioneros en servicios:</strong> En la nube (1997) y de servicios de Data Center (2001)</li>
                    @else
                    <li><i class="fas fa-check"></i> <strong>Profitable:</strong> The cost of DRaaS is primarily defined by the number of virtual servers you will run in the cloud and the volume of storage resources your data will consume. This is likely to be much less than the cost of building and maintaining a physical duplicate of your existing IT environment for disaster recovery purposes, and makes DRaaS a particularly attractive offering for smaller organizations with relatively modest IT budgets.</li>
                    <li><i class="fas fa-check"></i> <strong>Eliminate maintenance costs in general:</strong> You no longer have to worry about maintaining physical disaster recovery infrastructure, including servers and storage, or ensuring that a disaster recovery expert is always within your organization to orchestrate a recovery.</li>
                    <li><i class="fas fa-check"></i> <strong>Nearly cero downtime: </strong>An existing backup solution may offer a disaster recovery option with a recovery time objective (RTO) measured in hours, but DRaaS offers a viable way to ensure that your operations, and therefore your your customers and partners experience almost no disruption in the event of a disaster.</li>
                    <li><i class="fas fa-check"></i> <strong>Add DR capability:</strong> If you already have a DR solution for your mission-critical applications, DRaaS can be a useful way to ensure that less critical (or new) applications also have DR coverage.</li>
                    <li><i class="fas fa-check"></i> <strong>Provides a test environment:</strong> A DRaaS environment can be used to perform security or other potentially disruptive tests without impacting your core infrastructure or operations.</li>
                    <li><i class="fas fa-check"></i> <strong>Cyberfuel has a certified Data Center:</strong> Rated-3 according to the ANSI/TIA 942-B:2017 standard, to respond to the demanding business requirements.</li>
                    <li><i class="fas fa-check"></i> <strong>Long-term relationships with clients:</strong>Focusing on a consultative sale that allows flexibility to meet the needs of the business.</li>
                    <li><i class="fas fa-check"></i> <strong>Interconnected Data Center:</strong> It allows to accelerate the implementation of the replication, in case the productive environment and the replication environment are managed by Cyberfuel.</li>
                    <li><i class="fas fa-check"></i><strong> Pioneers in cloud services:</strong> In the cloud (1997) and Data Center services (2001)</li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <section class="call-to-action mb-5">
      <div class="col-sm-9 col-lg-9">
        <div class="call-to-action-content">
          <h3>Disfrute de <strong class="font-weight-bold">protección multinivel</strong> en su entorno con mayor facilidad de uso y a un precio menor.</h3>
          <p class="mb-0">Obtenga copias de seguridad optimizadas, automáticas y programables</p>
        </div>    
      </div>
      <div class="col-sm-3 col-lg-3">
        <div class="call-to-action-btn">
          <button type="button" id=""  class="btn btn-outline btn-rounded btn-primary mb-2 " title="Agregar Producto"> <span> <i class="fas fa-cart-plus fa-2x "></i> </span> </button>
        </div>
      </div>
    </section>
  </div>
    
    
</div>
<!--fin el contenido del HTML--> 
@endsection 