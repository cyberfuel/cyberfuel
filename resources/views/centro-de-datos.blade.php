@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | {{ __('master.datos_edge') }}</title>
<meta name="keywords"
  content="Centro de datos, edge, certificado, datacenter, data center, data centre, Costa Rica, CR, alta dispoinilidad, Rated 3" />
<meta name="description"
  content="Nuestro centro de datos, cuenta con certificaciones de infraestructura y operación que nos permite mantener una disponibilidad superior al 99.98% de un nivel Rated 3 o Tier III">
@endsection


@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/datos.webp')}}">
<meta property="og:description" content="{{ __('master.datos_edge') }}">
<meta property="og:title" content="Cyberfuel | Centro de Datos">
@endsection

@section('css')
<link rel="stylesheet" href="{{URL::asset('assets/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/owl.theme.default.min.css')}}">
@endsection
@section('content')
<!--aca empieza el contenido del HTML-->



<section
  class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8"
  style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
  <div class="container">
    <div class="row">
      <div class="col-12  align-self-center p-static ">

        <div class="overflow-hidden ">
          <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter"
            data-appear-animation-delay="300"><strong>{{ __('master.datos_edge') }}</strong></h1>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="container">
  <div class="row justify-content-center">

    <div class="col-sm-10  col-lg-6 mb-4 mb-lg-0">
      <div class="featured-boxes y my-4">
        <div class="featured-box">
          <div class="box-content px-lg-3 px-xl-4 py-lg-4"> <img width="160" height="auto"
              class="img-fluid appear-animation" data-appear-animation="fadeInUpBig" data-appear-animation-delay="0"
              data-appear-animation-duration="1s" src="{{URL::asset('assets/imagenes/colocation-per-unit.webp')}}"
              alt="Cyberfuel | Centro de Datos">
            <h2 class="font-weight-normal text-4">{{ __('master.coloca_unidad') }}</strong></h2>

            @if (Session::get('language') == 'es')
            <p class="mb-0">Colocación de equipo de comunicación o servidor según el alto del mismo. Los tamaños van por
              unidades de rack de 1U hasta 6U.</p>
            @else
            <p class="mb-0">Placement “Colo” of communication equipment or server according to its height. Sizes range
              from 1U to 6U rack units.</p>
            @endif

            <a href="{{ __('rutas.url_contactenos') }}"
              class="btn btn-primary  font-weight-semibold text-white btn-px-5 btn-py-2 border-width-1 text-4 mt-3">{{
              __('master.contactenos') }}</a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-10  col-lg-6 mb-4 mb-lg-0">
      <div class="featured-boxes  my-4">
        <div class="featured-box">
          <div class="box-content px-lg-3 px-xl-4 py-lg-4"> <img width="160" height="auto"
              class="img-fluid appear-animation" data-appear-animation="fadeInUpBig" data-appear-animation-delay="0"
              data-appear-animation-duration="2s"
              src="{{URL::asset('assets/imagenes/colocation-per-rack-or-cage.webp')}}"
              alt="Cyberfuel | Centro de Datos">
            <h2 class="font-weight-normal text-4"> {{ __('master.coloca_completo') }}</h2>

            @if (Session::get('language') == 'es')
            <p class="mb-0">Para colocación "Colo" de equipos de comunicación o servidores en medio gabinete o un
              gabinete o varios gabinetes en un recinto cerrado o jaula. </p>
            @else
            <p class="mb-0">For placement "Colo" of communication equipment or servers in a half cabinet or a cabinet or
              several cabinets in a closed enclosure or cage.</p>
            @endif

            <a href="{{ __('rutas.url_contactenos') }}"
              class="btn btn-primary  font-weight-semibold text-white btn-px-5 btn-py-2 border-width-1 text-4 mt-3">{{
              __('master.contactenos') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">

      <h2 class="font-weight-semibold text-6 mb-4 text-center" data-plugin-animated-letters
        data-plugin-options="{'startDelay': 500, 'minWindowWidth': 0, 'animationName': 'typeWriter', 'animationSpeed': 50}">
        {{ __('master.algunos_beneficios') }}</h2>
    </div>
    <div class="col-12 col-md-6">
      <ul class="list list-icons list-icons-lg">


        @if (Session::get('language') == 'es')

        <li><i class="fas fa-check"></i> Data Center certificado Rated 3 por ANSI/TIA-942 versión 2017, superior a una
          certificación Tier III de Uptime</li>
        <li><i class="fas fa-check"></i>Centro neutro con interconexiones a diferentes proveedores de Internet (ISP)
        </li>
        <li><i class="fas fa-check"></i>Conexión única y directa al NAP de las Américas en Miami, FL</li>
        <li><i class="fas fa-check"></i>Conexión a diferenes IXP's (Internet Exchange Point) de Costa Rica (CRIX y IXP
          de INFOCOM)</li>

        @else


        <li> <i class="fas fa-check"> </i> Data Center certified Rated 3 by ANSI / TIA-942 version 2017, higher than an
          Uptime Tier III certification </li>
        <li> <i class="fas fa-check"> </i> Neutral center with interconnections to different Internet providers (ISP)
        </li>
        <li> <i class="fas fa-check"> </i> Single and direct connection to the NAP of the Americas in Miami, FL </li>
        <li> <i class="fas fa-check"> </i> Connection to different IXP's (Internet Exchange Point) of Costa Rica (CRIX
          and IXP of INFOCOM) </li>
        @endif

      </ul>
    </div>

    <div class="col-12 col-md-6">
      <ul class="list list-icons list-icons-lg">


        @if (Session::get('language') == 'es')
        <li><i class="fas fa-check"></i>Sitio confiable y seguro, en la mejor zona tecnológica de Costa Rica</li>
        <li><i class="fas fa-check"></i>Expertos brindándole el servicio desde el año 2001</li>
        <li><i class="fas fa-check"></i>NOC y Soporte en sitio 24x7x365</li>
        @else

        <li> <i class="fas fa-check"> </i> Reliable and secure site, in the best technological zone of Costa Rica </li>
        <li> <i class="fas fa-check"> </i> Experts providing the service since 2001 </li>
        <li> <i class="fas fa-check"> </i> NOC and 24x7x365 on-site support </li>
        @endif
      </ul>
    </div>
  </div>
</div>

<section class="section section-default bg-color-light-scale-2">
  <div class="container-fluid  pt-2 pb-2">
    <div class="featured-boxes featured-boxes-style-3 row">
      <div class="container">
        <div class="row">
          @if (Session::get('language') == 'es')

          <h2 class="font-weight-semibold text-6 mb-4 text-center" data-plugin-animated-letters
            data-plugin-options="{'startDelay': 500, 'minWindowWidth': 0, 'animationName': 'typeWriter', 'animationSpeed': 50}">
            Razones por las cuales contamos con una disponibilidad superior al 99.98%</h2>
          @else

          <h2 class="font-weight-semibold text-6 mb-4 text-center" data-plugin-animated-letters
            data-plugin-options="{'startDelay': 500, 'minWindowWidth': 0, 'animationName': 'typeWriter', 'animationSpeed': 50}">
            Reasons why we have an availability greater than 99.98% of uptime</h2>
          @endif
        </div>
        <div class="row">
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-share-alt top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>

                <!--<h4 class="font-weight-normal text-5 mt-3">Our <strong class="font-weight-extra-bold">Customers</strong></h4>-->
                <p class="mb-0 text-4">{{ __('master.redu_internet') }}</p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content "> <i class="icon-featured fas fa-sync-alt top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <p class="mb-0 text-4">{{ __('master.disponi_ixp') }} </p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-list-ol top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <p class="mb-0 text-4">{{ __('master.direcciona_ip') }}v6 </p>
              </div>
            </div>
          </div>

          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-cloud top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <!--<h4 class="font-weight-normal text-5 mt-3">Our <strong class="font-weight-extra-bold">Customers</strong></h4>-->
                <p class="mb-0 text-4">{{ __('master.control_inte') }}</p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-bolt top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <p class="mb-0 text-4">{{ __('master.redu_electrica') }} </p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-temperature-low top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <p class="mb-0 text-4">{{ __('master.redu_enfria') }} </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-server top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <!--<h4 class="font-weight-normal text-5 mt-3">Our <strong class="font-weight-extra-bold">Customers</strong></h4>-->
                <p class="mb-0 text-4">{{ __('master.lugar_seguro') }}</p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-tachometer-alt top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <p class="mb-0 text-4">{{ __('master.monitoreo') }}</p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-4">
            <div class="featured-box featured-box-tertiary ">
              <div class="box-content"> <i class="icon-featured fas fa-headset top-0 appear-animation"
                  data-appear-animation="zoomInUp" data-appear-animation-delay="0"
                  data-appear-animation-duration="1s"></i>
                <p class="mb-0 text-4">{{ __('master.soporte') }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="container">

  <div class="row my-5">

    <div class="col-12 my-4">

      @if (Session::get('language') == 'es')
      <p class=" text-4 text-justify">Nuestro Centro de Datos ubicado en Edificio E, Forum 1. Santa Ana, San José Costa
        Rica, le garantizamos una disponibilidad superior al 99.98%, y es un sitio ideal si está pensando en un Centro
        de Datos Borde (Edge) porque contamos con el IXP de INFOCOM.</p>
      @else
      <p class=" text-4 text-justify">Our Data Center is located in Building E, Forum 1. Santa Ana, San José, Costa
        Rica, we guarantee an availability greater than 99.98% of uptime, and it is an ideal place if you are thinking
        of an Edge Data Center because we have the INFOCOM IXP.</p>

      @endif
    </div>
    <div class="col-12 ">

      @if (Session::get('language') == 'es')
      <p class="text-justify"> Nuestra infraestructura es en facilidades construidas (no en diseño) y contamos con la
        certificación ANSI (American National Standards Institute) miembro de ISO (International Organization for
        Standardization), la cual es ANSI/TIA-942 (versión 2017) de un nivel RATED 3 en los pilares de Arquitectura
        (ubicación, sistema de incendios, construcción, sitio seguro, entre otros parámetros que se validan), Mecánica
        (sistemas de enfriamiento), Eléctrico (donde contamos con una característica de un nivel superior igual RATED 4,
        que es la conexión a dos subestaciones diferentes, manteniendo siempre dos acometidas eléctricas completamente
        redundantes) y en Telecomunicaciones (contando también con un nivel RATED 4, porque contamos con una cantidad de
        diferentes proveedores de comunicaciones).
        Nuestro personal tiene una certificación en Operación y Mantenimiento de DCOS nivel 3, que demuestra la madurez
        de documentación y entrenamiento del personal ante cualquier eventualidad. </p>

      @else
      <p class="text-justify">Our infrastructure is in built facilities (not in design) and we are ANSI (American
        National Standards Institute); member of ISO (International Organization for Standardization) certified, which
        is ANSI / TIA-942 (2017 version) of a RATED 3 level. In the pillars of Architecture (location, fire system,
        construction, safe site, among other parameters that are validated), Mechanical (cooling systems), Electrical
        (where we have a characteristic of a higher level equal to RATED 4, which is the connection to two different
        substations, always maintaining two completely redundant electrical connections) and in Telecommunications (also
        having a RATED 4 level, because we have a number of different communications providers). Our staff has an
        Operation and Maintenance of DCOS level 3 certification, which demonstrates the maturity of documentation and
        training of the staff in any eventuality.</p>
      @endif
    </div>
  </div>

</div>

<div class="container">
  <div class="row align-items-center justify-content-center">
    <div class="col-6 col-sm-4 col-md-3"><img class="img-fluid appear-animation" data-appear-animation="backInLeft"
        data-appear-animation-delay="0" data-appear-animation-duration="1s"
        src="{{URL::asset('assets/imagenes/logos/dcos.webp')}}" alt="Certificados de Cyberfuel"></div>
    <div class="col-6 col-sm-4 col-md-3"><img class="img-fluid appear-animation" data-appear-animation="backInRight"
        data-appear-animation-delay="0" data-appear-animation-duration="1s"
        src="{{URL::asset('assets/imagenes/logos/rate3.webp')}}" alt="Certificados de Cyberfuel"></div>
  </div>
</div>




<div class="container">
  <div class="row">
    <div class="col">

      <div class="owl-carousel owl-theme carousel-center-active-item"
        data-plugin-options="{'responsive': {'0': {'items': 3}, '480': {'items': 3}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/cloud_very.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/comodo-secure-icon.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/dcos.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/ipv6-ready.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/logo-certificado.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/Microsoft-Partner.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/Odin_Partner-level_bronze2.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/positivessl-icon.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/Rated3.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
        <div> <img class="img-fluid" src="{{URL::asset('assets/imagenes/logos/Vmware-Partner.webp')}}"
            alt="Certificados de Cyberfuel"> </div>
      </div>
    </div>
  </div>
</div>
<!--fin el contenido del HTML-->
@endsection
@section('scripts')
<script src="{{ URL::asset('assets/js/owl.carousel.min.js') }}"></script>
@endsection