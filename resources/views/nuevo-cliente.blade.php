@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Registrese con nosotros</title>
<meta name="keywords"
    content="Respaldo, Baas, costa rica, cr, solución, maquinas, servidores, archivos, sitio seguro, seguridad" />
<meta name="description"
    content="Con nuestra solución puede tener un respaldo de su computador o de sus servidores virtuales o físicos (completo o parcial) fuera de su lugar de trabajo o habitación, garantizando la información segura ">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Registrese con nosotros">
<meta property="og:title" content="Cyberfuel | Ingreso de Nuevo Cliente">
@endsection
@section('content')
<!--aca empieza el contenido del HTML-->
<hr class=" py-2">
<div class="container ">
    <div class="row justify-content-end"> </div>
    <div class="row login-form-wrapper collapse mb-5">
        <div class="col">
            <div class="card border-width-3 border-radius-0 border-color-hover-dark">
                <div class="card-body">
                    <div class="row justify-content-end">
                        <div class="col-auto">
                            <a href="#" data-bs-toggle="collapse" data-bs-target=".login-form-wrapper">
                                <i class="fas fa-times fa-2x text-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="row" id="login">
                        <div class="col-12 mb-5 mb-lg-0">
                            <h2 class="font-weight-bold text-5 mb-0">
                                {{ __('master.ingrese_datos') }}
                            </h2>

                            <form action="/" id="frmSignIn" method="post" class="needs-validation">
                                <div class="row">
                                    <div class="form-group col col-md-6">
                                        <label class="form-label text-color-dark text-3">{{ __('master.usuario') }}
                                            <span class="text-color-danger">*</span></label>
                                        <input type="text" value=""
                                            class="form-control border-color-primary form-control border-color-primary-lg text-4"
                                            required>
                                    </div>
                                    <div class="form-group col col-md-6">
                                        <label class="form-label text-color-dark text-3">{{ __('master.contra') }} <span
                                                class="text-color-danger">*</span></label>
                                        <input type="password" value=""
                                            class="form-control border-color-primary form-control border-color-primary-lg text-4"
                                            required>
                                    </div>
                                </div>

                                <div class="row justify-content-between">
                                    <div class="form-group col-md-auto">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="rememberme">
                                            <label class="form-label custom-control-label cur-pointer text-2"
                                                for="rememberme">{{ __('master.recordarme') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-auto"> <a data-bs-toggle="collapse"
                                            data-bs-target=".login-recuperar"
                                            class="text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2 recupera"
                                            href="#">{{ __('master.recuperar') }}</a> </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="form-group col-auto">
                                        <button type="submit"
                                            class="btn btn-dark btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 pt-3 mb-0"
                                            data-loading-text="Loading...">{{ __('master.iniciar') }}</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row collapse login-recuperar">
                        <div class="col-12 mb-5 mb-lg-0">
                            <h2 class="font-weight-bold text-5 mb-0">{{ __('master.recuperar') }}</h2>
                            <form action="/" id="frmSignUp" method="post">
                                <div class="row">
                                    <div class="form-group col">
                                        <label class="p font-weight-bold "> {{ __('master.ingrese_correo') }} </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col">
                                        <label class="form-label text-success text-3">{{ __('compra.aprobar_servicios')
                                            }} <span class="text-color-danger">*</span></label>
                                        <input type="text" value=""
                                            class="form-control border-color-success form-control border-color-success-lg text-4"
                                            required="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col">
                                        <button type="submit"
                                            class="btn btn-dark btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3"
                                            data-loading-text="Loading...">{{ __('master.continuar') }}</button>
                                    </div>
                                </div>
                                <div class="form-group col-md-auto"> <a data-bs-toggle="collapse"
                                        data-bs-target=".login-recuperar"
                                        class="text-center text-decoration-none text-success text-color-hover-success font-weight-semibold text-2 regresa"
                                        href="#">{{ __('master.regresar_log') }}</a> </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>

<div role="main" class="main seclien pb-4">
    <div class="container">
        <div class="row align-items-center justify-content-start">
            <div class="col-sm-7 col-md-5 col-lg-4 col-xl-4">
                <h2 class="text-success font-weight-bold text-5-5 mb-1">{{ __('master.deta_fac') }}</h2>
            </div>
            <div class="col-sm-5 col-md-5 col-lg-3 col-xl-4">
                <p class="mb-1">
                    {{ __('master.soy_cliente') }}
                    <a href="#"
                        class="text-success text-color-hover-success text-uppercase text-decoration-none font-weight-bold"
                        data-bs-toggle="collapse" data-bs-target=".login-form-wrapper">
                        {{ __('master.ingre') }}
                    </a>
                </p>
            </div>
        </div>

        <form id="form_registro" name="form_registro" method="POST">
            <div class="row align-items-center">
                <div class=" col-12 col-md-6 ">
                    <label class="form-label w-100">{{ __('master.selec_pai') }}<span
                            class="text-color-danger">*</span></label>
                    <input type="text" id="paises" name="paises" class="form-control border-color-primary h-auto py-2"
                        value="<?php if(isset($datos_cliente_temp[" paises"])){ echo $datos_cliente_temp["paises"]; }
                        ?>">
                </div>
                <div class="col-12 col-md-6  info-personal">
                    <label class="form-label w-100">
                        {{ __('master.tip_clien') }}<span class="text-color-danger">*</span>
                    </label>
                    <select id="tipo_cedula" name="tipo_cedula"
                        class="form-select form-control border-color-primary h-auto py-2 requerido"
                        data-msg-required="Elija tipo de cliente">
                        <option value="" <?php if(!isset($datos_cliente_temp["tipo_cedula"])){ echo "selected" ; } ?>>{{
                            __('master.tip_clien') }}</option>
                        <option value="fisico" <?php if((isset($datos_cliente_temp["tipo_cedula"])) &&
                            ($datos_cliente_temp["tipo_cedula"]=="fisico" )){ echo "selected" ; } ?>>{{
                            __('master.per_fi') }}</option>
                        <option value="juridico" <?php if((isset($datos_cliente_temp["tipo_cedula"])) &&
                            ($datos_cliente_temp["tipo_cedula"]=="juridico" )){ echo "selected" ; } ?>>{{
                            __('master.per_ju') }}</option>
                        <option value="dimex" <?php if((isset($datos_cliente_temp["tipo_cedula"])) &&
                            ($datos_cliente_temp["tipo_cedula"]=="dimex" )){ echo "selected" ; } ?>>DIMEX</option>
                        <option value="nite" <?php if((isset($datos_cliente_temp["tipo_cedula"])) &&
                            ($datos_cliente_temp["tipo_cedula"]=="nite" )){ echo "selected" ; } ?>>NITE</option>
                        <option value="extranjero" <?php if((isset($datos_cliente_temp["tipo_cedula"])) &&
                            ($datos_cliente_temp["tipo_cedula"]=="extranjero" )){ echo "selected" ; } ?>>{{
                            __('master.extran') }}</option>
                    </select>
                </div>
                <div class="col-12 ">
                    <div>
                        <h4 class="pt-5">{{ __('master.inf_clien') }}</h4>
                      <!--  aqui van los inputs de la informacion-->
                        @include('includes.personal')
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
            $("#paises").countrySelect({
                defaultCountry: "cr",
                onlyCountries: ['cr', 'us', 'ca']
            });

            $("#paises").on('change', function(){
                var titulo = $(".selected-flag").attr("title");
                if(titulo !== "Costa Rica") {
                    $("#obligatorio").addClass("d-none")
                    $("#opcional").removeClass("d-none")
                }else{
                    $("#obligatorio").removeClass("d-none")
                    $("#opcional").addClass("d-none")
                }

                switch (titulo) {
                    case "Costa Rica":
                        $(".info-personal").removeClass("d-none");
                        document.getElementById("phone1").innerHTML = "(+506)";
                        document.getElementById("phone2").innerHTML = "(+506)";
                        document.getElementById("phone3").innerHTML = "(+506)";
                        document.getElementById("phone4").innerHTML = "(+506)";
                        document.getElementById("phone5").innerHTML = "(+506)";
                        document.getElementById("phone6").innerHTML = "(+506)";
                        break;

                    case "Canada":
                        $(".info-personal").addClass("d-none");
                        document.getElementById("phone1").innerHTML = "(+1)";
                        document.getElementById("phone2").innerHTML = "(+1)";
                        document.getElementById("phone3").innerHTML = "(+1)";
                        document.getElementById("phone4").innerHTML = "(+1)";
                        document.getElementById("phone5").innerHTML = "(+1)";
                        document.getElementById("phone6").innerHTML = "(+1)";
                        break;

                    case "United States":
                        $(".info-personal").addClass("d-none");
                        document.getElementById("phone1").innerHTML = "(+1)";
                        document.getElementById("phone2").innerHTML = "(+1)";
                        document.getElementById("phone3").innerHTML = "(+1)";
                        document.getElementById("phone4").innerHTML = "(+1)";
                        document.getElementById("phone5").innerHTML = "(+1)";
                        document.getElementById("phone6").innerHTML = "(+1)";
                        break;

                    default:
                        break;
                }
            });
        });
</script>
@endsection