@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel</title>
<meta name="keywords"
      content="Seguridad, solución, escaneo, certificado, certificado de seguridad, ssl, waf, pci, pci-dss, servidor" />
<meta name="description"
      content="Por medio de diferentes soluciones de seguridad en su servidor podrá tener protección, utilizando tecnologías como: certificado de seguridad SSL, WAF y/o certificando PCI-DSS ">
@endsection
@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Le ayudamos a que tenga seguridad en su sitio web - Cyberfuel">
<meta property="og:title" content="Cyberfuel | Seguridad en su sitio">
@endsection
@section('content')
<!--aca empieza el contenido del HTML-->



<section
      class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8"
      style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
      <div class="container">
            <div class="row">
                  <div class="col-md-6  align-self-center p-static ">

                        <div class="overflow-hidden ">
                              <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter"
                                    data-appear-animation-delay="300"><strong>{{ __('master.seguridad') }}</strong></h1>
                        </div>
                  </div>
            </div>
      </div>
</section>
<div class="container">

      <div class="row my-5">
            <div class="col-12 ">


                  @if (Session::get('language') == 'es')

                  <p class="text-justify">La seguridad y la protección en su sitio web y su correo electrónico son muy
                        importantes, por eso ponemos a su disposición diferentes soluciones como: Certificados de
                        seguridad SSL, Firewall WEB conocido como WAF, escanear su servidor para obtener la
                        certificación PCI-DSS y Filtrado de correo electrónico no deseado utilizando Spam Filter. </p>

                  @else

                  <p class="text-justify">Security and protection on your website and your email is very important,
                        that's why we offer you different solutions such as: SSL security certificates, WEB Firewall
                        known as WAF, Scanning your server to obtain PCI-DSS certification and Mail filtering unwanted
                        via Spam Filter.</p>
                  @endif


            </div>
      </div>



      <div class="row pb-3">
            <div class="col-12">
                  <div class="d-md-flex">
                        <div class="row">
                              <div class="col-md-2"> <img class="img-fluid mx-auto d-block appear-animation"
                                          data-appear-animation="backInLeft" data-appear-animation-delay="0"
                                          data-appear-animation-duration="1s"
                                          src="{{URL::asset('assets/imagenes/ssl.webp')}}" alt=""> </div>
                              <div class="col-md-10">
                                    @if (Session::get('language') == 'es')
                                    <h4>Certificado SSL</h4>
                                    <p class="text-justify">Siendo muy utilizados en los sitios de Internet que permiten
                                          comprar por medio de tarjeta de crédito o bien desean reforzar la seguridad
                                          entre el usuario y el sitio WEB. Nuestros certificados de seguridad SSL son
                                          128/256-Bits de encriptación, con una llave de 2048-Bits de encriptación y
                                          compatible con el 99.9% de los Navegadores.</p>
                                    @else
                                    <h4>SSL Certificate</h4>
                                    <p class="text-justify"> It is used to encrypt sensitive information between the
                                          user and the website. It is used in Internet sites that allow you to buy using
                                          a credit card or if you want to reinforce the security. Our SSL security
                                          certificates are 128/256-Bit encryption, with a 2048-Bit encryption key and
                                          compatible with 99.9% of Browsers.</p>
                                    @endif <span class="float-end">
                                          <a href="{{ __('rutas.url_contactenos') }}" type="button" id=""
                                                class="btn btn-modern btn-primary mt-3" title="Agregar Producto">
                                                <span> <i class="fas fa-plus"></i></span> {{ __('inicio.consulta_ser')
                                                }} </a>
                                    </span>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
      <!--      <div class="row pb-3">
    <div class="col-12">
          <div class="d-md-flex"><div class="row">
        <div class="col-md-2"> <img class="img-fluid mx-auto d-block appear-animation" data-appear-animation="backInLeft" data-appear-animation-delay="0" data-appear-animation-duration="1s" src="{{URL::asset('assets/imagenes/firewall.webp')}}" alt=""> </div>
        <div class="col-md-10">
              <h4>WAF (Web Application Firewall)</h4>
              @if (Session::get('language') == 'es')
              <p>La solución permite estar monitoreando los accesos al servidor web y previniendo que el servidor no sea víctima de ataques o abusos de usuarios.  Es muy utilizado especialmente cuando se cuenta con sistemas CMS (Content Management System) como WordPress, Joomla, Drupal, Magento entre otros.</p>
              @else
              <p>The solution allows monitoring access to the web server and preventing the server from being the victim of attacks or user abuse. It is widely used especially when you have CMS (Content Management System) systems such as WordPress, Joomla, Drupal, Magento among others.</p>
              @endif<span class="float-end">
            <a  href="{{ __('rutas.url_contactenos') }}" type="button" href="#" class="btn btn-outline btn-rounded btn-primary mb-2 " title="Agregar Producto"> <span> <i class="fas fa-cart-plus fa-2x "></i> </span> </a>
            </span> </div>
      </div>
		</div>
        </div>
  </div>-->

      <div class="row pb-3">
            <div class="col-12">
                  <div class="d-md-flex">
                        <div class="row">
                              <div class="col-md-2"> <img class="img-fluid mx-auto d-block appear-animation"
                                          data-appear-animation="backInLeft" data-appear-animation-delay="0"
                                          data-appear-animation-duration="1s"
                                          src="{{URL::asset('assets/imagenes/pci.webp')}}" alt=""> </div>
                              <div class="col-md-10">
                                    <h4>PCI-DSS</h4>
                                    @if (Session::get('language') == 'es')
                                    <p>El servicio consiste en realizar escaneos al sitio web y/o servidor solicitado
                                          mostrando la lista de vulnerabilidades para que puedan ser corregidas. El
                                          escaneo tiene una validez de 3 meses.</p>
                                    @else
                                    <p>The service consists of carrying out scans to the requested website and / or
                                          server showing the list of vulnerabilities so that they can be corrected. The
                                          scan is valid for 3 months.</p>
                                    @endif<span class="float-end">
                                          <a href="{{ __('rutas.url_contactenos') }}" type="button" id=""
                                                class="btn btn-modern btn-primary mt-3" title="Agregar Producto">
                                                <span> <i class="fas fa-plus"></i></span> {{ __('inicio.consulta_ser')
                                                }} </a>
                                    </span>
                              </div>
                        </div>
                  </div>
            </div>
      </div>


      <div class="row pb-3">
            <div class="col-12">
                  <div class="d-md-flex">
                        <div class="row">
                              <div class="col-md-2"> <img class="img-fluid mx-auto d-block appear-animation"
                                          data-appear-animation="backInLeft" data-appear-animation-delay="0"
                                          data-appear-animation-duration="1s"
                                          src="{{URL::asset('assets/imagenes/spam.webp')}}" alt=""> </div>
                              <div class="col-md-10">
                                    <h4>Spam Filter</h4>

                                    @if (Session::get('language') == 'es')

                                    <p class="text-justify">El correo electrónico es una vía de comunicación muy valiosa
                                          para las empresas y en los últimos años hemos visto como llegan toda clase de
                                          correos que nos genera esfuerzo y tiempo en poder eliminarlos. Por medio del
                                          servicio de Filtro de Correo no deseado (filtro anti-spam), el sistema emite
                                          un puntaje de cada correo según información recibida y lo deja ingresar o bien
                                          lo deja en cuarentena. Agregando que no solo protege del SPAM, si no también
                                          que cuenta con un Anti-Virus que detecta y bloquea correos y enlaces
                                          maliciosos de PHISHING o descarga de Virus que pueden bloquear tu equipo por
                                          medio de un Ransomeware.</p>
                                    @else

                                    <p class="text-justify">Email is one of the most popular communication tools for
                                          companies, but it receives a lot of offers, phishing, virus, malware, and
                                          others that take effort and time to eliminate. With the Spam Filter service
                                          (anti-spam filter), the system scores each email and allow it to block,
                                          quarantine or deliver it. Adding that it not only protects against SPAM, it
                                          also has an Anti-Virus that detects and blocks emails and emails links of
                                          PHISHING or download links of Virus that can block your equipment using
                                          Ransomeware.</p>
                                    @endif<span class="float-end">
                                          <a href="{{ __('rutas.url_contactenos') }}" type="button" id=""
                                                class="btn btn-modern btn-primary mt-3" title="Agregar Producto">
                                                <span> <i class="fas fa-plus"></i></span> {{ __('inicio.consulta_ser')
                                                }} </a>
                                    </span>
                              </div>
                        </div>
                  </div>
            </div>
      </div>


      <div class="row pb-3">
            <div class="col-12">
                  <div class="d-md-flex">
                        <div class="row">
                              <div class="col-md-2"> <img class="img-fluid mx-auto d-block appear-animation"
                                          data-appear-animation="backInLeft" data-appear-animation-delay="0"
                                          data-appear-animation-duration="1s"
                                          src="{{URL::asset('assets/imagenes/ninja.webp')}}" alt=""> </div>
                              <div class="col-md-10">
                                    <h4>BIT-NINJA</h4>

                                    @if (Session::get('language') == 'es')

                                    <p class="text-justify">Bit-Ninja es una herramienta de defensa para el servidor,
                                          este realiza una protección contra hackers, botnets, atacantes y actividades
                                          maliciosas, ofrece protección proactiva contra todo tipo de ciberamenzas
                                          mediante un sistema de defensa social y varios módulos defensivos.</p>
                                    <p class="text-2 text-danger">
                                          Este servicio es solo para servidores Linux
                                    </p>
                                    @else

                                    <p class="text-justify">Bit-Ninja is a tool for the server, it performs protection
                                          against hackers, botnets, attackers and malicious activities, it offers
                                          proactive defense against all types of cyber threats through a social defense
                                          system and various defensive modules.</p>
                                    <p class="text-2 text-danger">
                                          This service is only for Linux servers
                                    </p>
                                    @endif<span class="float-end">
                                          <a href="{{ __('rutas.url_contactenos') }}" type="button" id=""
                                                class="btn btn-modern btn-primary mt-3" title="Agregar Producto">
                                                <span> <i class="fas fa-plus"></i></span> {{ __('inicio.consulta_ser')
                                                }} </a>
                                    </span>
                              </div>
                        </div>
                  </div>
            </div>

      </div>

      <span class="py-4">&nbsp;</span>

</div>

<!--fin el contenido del HTML-->
@endsection


@section('scripts')



@endsection