@php
$rutaActual = $_SERVER['REQUEST_URI'];
@endphp
@if($rutaActual == "/")
<div class="col-sm-6 col-lg-4 mt-4">
    <a class="productos" target="blank_" href="{{$link}}" aria-label="">
        <span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded">
            <span class="thumb-info-wrapper">
                <img lazyload width="100%" height="" class="img-fluid m-auto  d-block" src="{{$imagen}}"
                    data-src="{{$imagen}}" alt="Productos Cyberfuel">
                <span class="thumb-info-title">
                    <span class="thumb-info-inner">{{$titulo}}</span>
                    <span class="thumb-info-type">{{$producto}}</span>
                </span>
                <span class="thumb-info-action">
                    <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                </span>
            </span>
        </span>
    </a>
</div>
@elseif($rutaActual == "/landings/servicios-productos")
<div class="col-sm-6 col-lg-6 col-xxl-4">
    <a class="productos" target="blank_" href="{{$link}}" aria-label="">
        <span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded">
            <span class="thumb-info-wrapper">
                <img lazyload width="100%" height="" class="img-fluid m-auto  d-block" src="{{$imagen}}"
                    data-src="{{$imagen}}" alt="Productos Cyberfuel">
                <span class="thumb-info-title">
                    <span class="thumb-info-inner">{{$titulo}}</span>
                    <span class="thumb-info-type">{{$producto}}</span>
                </span>
                <span class="thumb-info-action">
                    <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                </span>
            </span>
        </span>
    </a>
</div>
@endif