
    <div>
    <div class="d-none d-md-block"><img src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{$imagen}}" class="lazyload w-100" alt="Cyberfuel"></div>
    <div class="d-block d-md-none"><img src="{{URL::asset('assets/imagenes/lazy.png')}}" data-src="{{$imagenmobile}}" class="lazyload w-100" alt="Cyberfuel"></div> 
    <div class="carousel-caption">
    <h4 class="text-white">{{$texto}}</h4>
    
    </div>
        <a href="{{$link}}" type="button" class="btn btn-modern btn-sm btn- btn-success rounded-0 mb-1 m-auto">{{ __('inicio.ver_mas') }}</a>
</div>