@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | {{ __('compra.titulo') }}</title>
<meta name="keywords" content="cyberfuel, nombre de dominio, dominio, dominios, registro, compra, web, costa rica, .CR, .com"/>
<meta name="description" content="{{ __('compra.descripcion') }}">
@endsection

@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/mobile.jpg')}}">
<meta property="og:description" content="{{ __('compra.titulo') }}">
<meta property="og:title" content="Cyberfuel | {{ __('compra.nombre_de') }} {{ __('compra.dominio') }}s">
@endsection

@section('content') 
<!--aca empieza el contenido del HTML-->

	<section class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8 mb-0" style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-6  align-self-center p-static">
							
								<div class="overflow-hidden">
									<h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"><strong>{{ __('compra.nombre_de') }} {{ __('compra.dominio') }}</strong></h1>
								</div>
							</div>
						</div>
					</div>
				</section>


<div class="container-fluid  mb-4">
    <div class="mx-2">
    <div class="row">
        <div class="col col-lg-10 offset-lg-1 px-0">
<div class="row">
<div class="col col-xl-8 px-0">
  <div class="accordion" id="carrodecompras">
    <div class="card border-0">
      <div  id="headingDominio"> </div>
      <!--	primer collapse para escoger dominio-->
      <div id="collapseDominio" class="collapse show" aria-labelledby="headingDominio" data-bs-parent="#carrodecompras"> @include('includes.elegir-dominio')

		
		</div>
    </div>
    <div class="card border-0">
      <div  id="headingHospedaje"> </div>
      <!--	segundo colapse para escoger hosting-->
      <div id="collapseHospedaje" class="collapse" aria-labelledby="headingHospedaje" data-bs-parent="#carrodecompras"> @include('includes.elegir-hosting')

      </div>
    </div> 
   
  </div>
</div>
      <div class="col-xl-4 position-relative mt-4">
					  @include('includes.side-compra')
						</div>
</div>
    </div>
    </div>
    </div>
</div>

 


<!--fin el contenido del HTML--> 
@endsection
