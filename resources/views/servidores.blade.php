@extends('layout.master_cyber')
@section('metas')
<title>Cyberfuel.com | Ahora puede tener su servidor o infraestructura virtual en cr</title>
<meta name="keywords"
  content="Servidor, infraestructura, virtual, servidores, vmware, alta disponibilidad, solución, costa rica, cr, precio" />
<meta name="description"
  content="Tenga sus equipos en un ambiente virtual en un centro de datos en costa rica, con más de un 99.98% de alta disponibilidad con tecnología vmware y a un precio excelente ">
@endsection
@section('face')
<meta property="og:url" content="https://www.cyberfuel.com">
<meta property="og:image" content="{{URL::asset('assets/imagenes/cyberfuel.webp')}}">
<meta property="og:description" content="Ahora puede tener su servidor o infraestructura virtual en cr">
<meta property="og:title" content="Cyberfuel | Servidores">
@endsection
@section('content')
<!--aca empieza el contenido del HTML-->



<section
  class="page-header page-header-modern page-header-background page-header-background-md py-0  overlay-show overlay-op-8 mb-0"
  style="background-image: url({{URL::asset('assets/imagenes/header_paginas.webp')}});">
  <div class="container">
    <div class="row">
      <div class="col-md-6  align-self-center p-static">

        <div class="overflow-hidden">
          <h1 class="text-10 appear-animation py-4" data-appear-animation="fadeInUpShorter"
            data-appear-animation-delay="300"><strong>{{ __('master.servidores') }}</strong></h1>
        </div>

      </div>
    </div>
  </div>
</section>


<div class="container">
  <div class="col-12 py-4">
    @if (Session::get('language') == 'es')
    Es un sistema escalable en el cual se puede aumentar las características específicas del servidor según sus
    necesidades. Están diseñados para contribuir al crecimiento dinámico de su negocio, acorde a la flexibilidad y
    necesidades de su operación
    @else

    It is a scalable system in which you can increase the specific features of the server according to your needs. They
    are designed to contribute to the dynamic growth of your business, according to the flexibility and needs of your
    operation
    @endif
  </div>
  <div class="col-12 mb-4 mb-lg-0">
    <h2 class="text-color-dark font-weight-bold text-5-5 mb-3">
      @if (Session::get('language') == 'es')
      Características
      @else
      Characteristics
      @endif
    </h2>
  </div>
  <form class="">
    <div class="row">
      <div class="col text-4"><strong>
          @if (Session::get('language') == 'es')
          Sistema operativo
          @else
          OS
          @endif
        </strong></div>
      <div class="form-group col-12 ">
        <label class="form-label w-100">
          @if (Session::get('language') == 'es')
          Elija el sistema operativo en el que desea ejecutar las instancias de Cyberfuel
          @else
          Choose the operating system you want to run Cyberfuel instances on
          @endif
          <span class="text-color-danger">*</span></label>
        <select id="informa" name="informa" class="form-select form-control border-color-primary h-auto py-2"
          data-msg-required="Plataforma" required="">
          <option selected="" value="Linux">Linux</option>
          <option value="Windows">Windows</option>
        </select>
      </div>
    </div>

    {{-- <div class="row mt-4">
      <div class="col ">
        <div class="tabs">
          <ul class="nav nav-tabs nav-justified flex-column flex-md-row">
            <li class="nav-item"> <a class="nav-link  active" href="#busqueda" data-bs-toggle="tab">
                @if (Session::get('language') == 'es')
                Busque las instancias por nombres:
                @else
                Search for instances by name:
                @endif
              </a> </li>
            <li class="nav-item"> <a class="nav-link" href="#requisitos" data-bs-toggle="tab">
                @if (Session::get('language') == 'es')
                Escriba los requisitos mínimos para cada instancia:
                @else
                Write the minimum requirements for each instance:
                @endif
              </a> </li>
          </ul>
          <div class="tab-content px-0 border-0">

            <div id="busqueda" class="tab-pane active">
              <div class="row">
                <div class="col-12" wire:ignore>
                  <label class="form-label w-100">
                    @if (Session::get('language') == 'es')
                    La busqueda se da por Servidor + Redundancia + vCPUs y Ram
                    @else
                    The search is for Server + Redundancy + vCPUs and Ram
                    @endif
                    <span class="text-color-danger"> (CS-Win-HDD-1x4)</span></label>
                  <select class="form-select form-control border-color-primary h-auto py-2" id="select2-dropdown">
                    <option value=""><i class="fas fa-search "></i> Elige instancia</option>
                    <option value="">CS-Win-HDD-1x4 </option>
                    <option value="">CS-Lin-SSHD-1x1 </option>
                    <option value="">CS-Win-SSHD-1x4 </option>
                    <option value="">CS-Lin-SSD-1x1</option>
                    <option value="">CS-Win-HDD-1x6</option>
                    <option value="">CS-Lin-SSHD-1x2 </option>
                    <option value="">CS-Win-SSHD-1x6 </option>
                    <option value="">CS-Lin-SSD-1x2</option>
                    <option value="">CS-Win-HDD-1x8 </option>
                    <option value="">CS-Lin-SSHD-1x3 </option>
                    <option value="">CS-Win-SSHD-1x8 </option>
                    <option value="">CS-Lin-SSD-1x3</option>
                    <option value="">CS-Win-HDD-2x4 </option>
                    <option value="">CS-Lin-SSHD-1x4 </option>
                    <option value="">CS-Win-SSHD-2x4 </option>
                    <option value="">CS-Lin-SSD-1x4</option>
                    <option value="">CS-Win-HDD-2x6</option>
                    <option value="">CS-Lin-SSHD-1x6 </option>
                    <option value="">CS-Win-SSHD-2x6 </option>
                    <option value="">CS-Lin-SSD-1x6</option>
                    <option value="">CS-Win-HDD-2x8 </option>
                    <option value="">CS-Lin-SSHD-1x8</option>
                    <option value="">CS-Win-SSHD-2x8 </option>
                    <option value="">CS-Lin-SSD-1x8</option>
                    <option value="">CS-Win-HDD-2x10 </option>
                    <option value="">CS-Lin-SSHD-2x1 </option>
                    <option value="">CS-Win-SSHD-2x10 </option>
                    <option value="">CS-Lin-SSD-2x1 </option>
                    <option value="">CS-Win-HDD-2x12 </option>
                    <option value="">CS-Lin-SSHD-2x2 </option>
                    <option value="">CS-Win-SSHD-2x12 </option>
                    <option value="">CS-Lin-SSD-2x2</option>
                    <option value="">CS-Win-HDD-2x14 </option>
                    <option value="">CS-Lin-SSHD-2x4 </option>
                    <option value="">CS-Win-SSHD-2x14 </option>
                    <option value="">CS-Lin-SSD-2x4</option>
                    <option value="">CS-Win-HDD-2x16 </option>
                    <option value="">CS-Lin-SSHD-2x6 </option>
                    <option value="">CS-Win-SSHD-2x16 </option>
                    <option value="">CS-Lin-SSD-2x6</option>
                    <option value="">CS-Win-HDD-4x4 </option>
                    <option value="">CS-Lin-SSHD-2x8 </option>
                    <option value="">CS-Win-SSHD-4x4 </option>
                    <option value="">CS-Lin-SSD-2x8</option>
                    <option value="">CS-Win-HDD-4x8 </option>
                    <option value="">CS-Lin-SSHD-2x10</option>
                    <option value="">CS-Win-SSHD-4x8 </option>
                    <option value="">CS-Lin-SSD-2x10</option>
                    <option value="">CS-Win-HDD-4x12 </option>
                    <option value="">CS-Lin-SSHD-2x12 </option>
                    <option value="">CS-Win-SSHD-4x12 </option>
                    <option value="">CS-Lin-SSD-2x12</option>
                    <option value="">CS-Win-HDD-4x16 </option>
                    <option value="">CS-Lin-SSHD-2x14 </option>
                    <option value="">CS-Win-SSHD-4x16 </option>
                    <option value="">CS-Lin-SSD-2x14</option>
                    <option value="">CS-Win-HDD-4x20 </option>
                    <option value="">CS-Lin-SSHD-2x16 </option>
                    <option value="">CS-Win-SSHD-4x20 </option>
                    <option value="">CS-Lin-SSD-2x16</option>
                    <option value="">CS-Win-HDD-4x24 </option>
                    <option value="">CS-Lin-SSHD-4x4 </option>
                    <option value="">CS-Win-SSHD-4x24 </option>
                    <option value="">CS-Lin-SSD-4x4</option>
                    <option value="">CS-Win-HDD-4x28 </option>
                    <option value="">CS-Lin-SSHD-4x8 </option>
                    <option value="">CS-Win-SSHD-4x28 </option>
                    <option value="">CS-Lin-SSD-4x8</option>
                    <option value="">CS-Win-HDD-4x32 </option>
                    <option value="">CS-Lin-SSHD-4x12 </option>
                    <option value="">CS-Win-SSHD-4x32 </option>
                    <option value="">CS-Lin-SSD-4x12</option>
                    <option value="">CS-Lin-SSHD-4x16 </option>
                    <option value="">CS-Win-SSHD-8x8 </option>
                    <option value="">CS-Lin-SSD-4x16</option>
                    <option value="">CS-Lin-SSHD-4x20 </option>
                    <option value="">CS-Win-SSHD-8x16 </option>
                    <option value="">CS-Lin-SSD-4x20</option>
                    <option value="">CS-Lin-SSHD-4x24 </option>
                    <option value="">CS-Win-SSHD-8x24 </option>
                    <option value="">CS-Lin-SSD-4x24</option>
                    <option value="">CS-Lin-SSHD-4x28 </option>
                    <option value="">CS-Win-SSHD-8x32 </option>
                    <option value="">CS-Lin-SSD-4x28</option>
                    <option value="">CS-Lin-SSHD-4x32 </option>
                    <option value="">CS-Lin-SSD-4x32</option>
                    <option value="">CS-Lin-SSD-8x8</option>
                    <option value="">CS-Lin-SSD-8x16</option>
                    <option value="">CS-Lin-SSD-8x24</option>
                    <option value="">CS-Lin-SSD-8x32</option>
                  </select>
                </div>
              </div>
            </div>
            <div id="requisitos" class="tab-pane ">
              <div class="row " id="">
                <div class="form-group col-12 col-sm-6">
                  <div class="input-group">
                    <label class="form-label w-100">vCPUs<span class="text-color-danger">*</span></label>
                    <select class="form-select form-control border-color-primary h-auto py-2"
                      aria-label="Default select example">
                      <option selected>{{__('master.selec_vcp') }}</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                    </select>
                  </div>
                </div>
                <div class="form-group col-12 col-sm-6">
                  <div class="input-group">
                    <label class="form-label w-100">{{__('master.memoria_ram') }}<span
                        class="text-color-danger">*</span></label>
                    <select class="form-select form-control border-color-primary h-auto py-2"
                      aria-label="Default select example">
                      <option selected>{{__('master.selec_ram') }}</option>
                      <option value="1">10</option>
                      <option value="2">20</option>
                      <option value="3">30</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div> --}}
    <div class="row mt-2">
      <div class="col text-4"><strong>{{__('master.almacenamiento') }} (SSD)</strong></div>
      <div class="form-group col-12">
        <div class="input-group">
          <label class="form-label w-100">{{__('master.canti_almacenamiento') }} (GB) <span
              class="text-color-danger">*</span></label>
          <input type="text" class="form-control border-color-primary h-auto py-2" name="" value="" required=""
            placeholder="20">
        </div>
        <p class="text-2 text-danger">
          @if (Session::get('language') == 'es')
          Linux debe ser minimo 20 GB en adelante hasta 1 TB, más de 1 TB debe debe contactar a un ejecutivo de ventas.
          @else
          Linux must be a minimum of 20 GB and up to 1 TB, more than 1 TB you must contact a sales executive.
          @endif
        </p>
      </div>
    </div>

    <div class="row">
      <div class="col-12 d-none" id="plesk">
        <label class="form-label w-100 text-4">{{__('master.elija_ser_dinamico') }} Plesk.</span> </label>
        <select id="" name="" class="form-select form-control border-color-primary h-auto py-2"
          data-msg-required="Plataforma" required="">

          <option value="" selected="">Control Panel Plesk WEB Host Edition (unlimited domains) & Power Pack</option>
          <option value="">Control Panel Plesk WEB Admin Edition(10 domains) & Power Pack</option>
          <option value=""> Control Panel Plesk WEB PRO Edition(30 domains) & Power Pack</option>

        </select>
      </div>

      <div class="col-12 d-none" id="cpanel">
        <label class="form-label w-100 text-4">{{__('master.elija_ser_dinamico') }} Cpanel.</span> </label>
        <select id="" name="" class="form-select form-control border-color-primary h-auto py-2"
          data-msg-required="Plataforma" required="">

          <option value="" selected="">Control Panel Plesk WEB Host Edition (unlimited domains) & Power Pack</option>
          <option value="">Control Panel Plesk WEB Admin Edition(10 domains) & Power Pack</option>
          <option value=""> Control Panel Plesk WEB PRO Edition(30 domains) & Power Pack</option>

        </select>
      </div>


    </div>


    <div class="row pt-4">
      <div class="col text-4"><strong>{{__('master.resumen') }}</strong></div>
    </div>
    <div class="my-4 sombra redondeada p-4 position-relative alert-info">
      <div class="row">
        <div class="col-md-8">
          <div class="row  ">
            <div class="col-12 col-md-6 mb-2">
              <p class="text-grey" for="">{{__('master.costo_ins') }}</p>
              <strong class="text-4">$100</strong>
            </div>
            <div class="col-12 col-md-6 mb-2">
              <p class="text-grey" for="">{{__('master.costo_alma') }}</p>
              <strong class="text-4">$30</strong>
            </div>
          </div>
          <div class="row py-3">
            <div class="col-12 col-md-6 mb-2">
              <p class="text-grey" for="">vCPU</p>
              <strong>4 cores</strong>
            </div>
            <div class="col-12 col-md-6 mb-2">
              <p class="text-grey" for="">{{__('master.memoria_ram') }} (GiB)</p>
              <strong>16 GIB</strong>
            </div>
          </div>
          <div class="row ">
            <div class="col-12 col-md-6 mb-2">
              <p class="text-grey" for="">{{__('master.sis_operativo') }}</p>
              <strong>
                <div id="informaPlace"></div>
              </strong>
            </div>
            <div class="col-12 col-md-6 mb-2">
              <p class="text-grey" for="">{{__('master.tipo_almacenamiento') }}</p>
              <strong><span id="almacenamientoPlace"></span> + <span id="redundanciaPlace"></span></strong>
            </div>
          </div>
        </div>
        <div class="col-md-4 pl-1">
          <div class="py-2">
            <div class="row mb-3">
              <div class="col-12 text-4"><strong>{{__('master.canti_compra') }}</strong></div>
              <div class="col-12">
                <table class="w-100 mb-3">
                  <tbody>
                    <tr class="cart-subtotal">
                      <td class="border-top-0"><strong class="text-color-dark">Subtotal</strong></td>
                      <td class="border-top-0 text-end"><strong> <span
                            class="text-color-dark amount font-weight-medium"> $130 </span> </strong></td>
                    </tr>
                    <tr class="total">
                      <td><strong class="text-color-dark"> <span class="amount text-color-dark text-5">Total </span>
                        </strong></td>
                      <td class="text-end"><strong class="text-color-dark"> <span class="amount text-color-dark text-5">
                            $150 </span> </strong></td>
                    </tr>
                    <tr> </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-12 mb-2">
                <button type="button" class="btn btn-outline btn-rounded btn-primary  btn-with-arrow mb-2 mt-lg-4">
                  {{__('compra.agregar') }} <span><i class="fas fa-shopping-cart"></i></span> </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="container">
  <div class="row">
    <div class="col-12 col-md-6"> </div>
    <div class="col-md-6 py-2">
      <div class="row justify-content-end">
        <div class="col-auto">
          <div class="col-auto"><a href="{{ __('rutas.url_nuevo_cliente') }}"
              class="btn btn-success btn-modern w-100 text-uppercase bg-color-hover-success border-color-hover-success border-radius-0 text-3 py-3">{{__('compra.aprobar_servicios')
              }} <i class="fas fa-arrow-right ms-2"></i></a> </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--fin el contenido del HTML-->
@endsection

@php
$icoShopping = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
  class="bi bi-cart-check-fill" viewBox="0 0 16 16">
  <path
    d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0m7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0m-1.646-7.646-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708.708z" />
</svg>';


@endphp


@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.full.min.js"></script>
<script>
  $(document).ready(function () {
        $('#select2-dropdown').select2({
    containerCssClass: "custom-container",
    dropdownCssClass: "custom-dropdown",
		//	placeholder: "Seleccione Instancia",
//			allowClear: true
});
		
		//capturar info select
		  $("#informa").on("change",function(){
            var valorInforma=$(this).val()
			
            document.getElementById('informaPlace').innerHTML= valorInforma;
         })
		
		  $("#almacenamiento").on("change",function(){
            var valorAlmacena=$(this).val()
			
            document.getElementById('almacenamientoPlace').innerHTML= valorAlmacena;
         })
		
			  $("#redundancia").on("change",function(){
            var valorRedun=$(this).val()
			
            document.getElementById('redundanciaPlace').innerHTML= valorRedun;
         })
		
		
    

    });




	
	/*validacion*/
function permite(elEvento, permitidos) {
  // Variables que definen los caracteres permitidos
  var numeros = "0123456789";
  var caracteres = " aábcdeéfghiíjklmnñoópqrstuvwxyzAÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUVWXYZ";
  var numeros_caracteres = numeros + caracteres;
  var teclas_especiales = [8, 37, 39, 46];
  // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha
 
 
  // Seleccionar los caracteres a partir del parámetro de la función
  switch(permitidos) {
    case 'num':
      permitidos = numeros;
      break;
    case 'car':
      permitidos = caracteres;
      break;
    case 'num_car':
      permitidos = numeros_caracteres;
      break;
  }
 
  // Obtener la tecla pulsada 
  var evento = elEvento || window.event;
  var codigoCaracter = evento.charCode || evento.keyCode;
  var caracter = String.fromCharCode(codigoCaracter);
 
  // Comprobar si la tecla pulsada es alguna de las teclas especiales
  // (teclas de borrado y flechas horizontales)
  var tecla_especial = false;
  for(var i in teclas_especiales) {
    if(codigoCaracter == teclas_especiales[i]) {
      tecla_especial = true;
      break;
    }
  }
 
  // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
  // o si es una tecla especial
  return permitidos.indexOf(caracter) != -1 || tecla_especial;
}
	


	
	
	//cambiar el campo select de los servidores
	var hash = window.location.hash;
	
	
	switch(hash) {
  case "#plesk":
    window.onload = function () {
  
		$("#plesk").removeClass("d-none");
    }
    break;
  case "#cpanel":

			 window.onload = function () {
  
		 $("#cpanel").removeClass("d-none");
    }
    break;
  default:
   
} 
</script>
@endsection