
<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="x-apple-disable-message-reformatting">
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--<![endif]-->
<title>Formulario de Contacto</title>

<style type="text/css"></style>
</head>
<body style="margin:0;padding:0;word-spacing:normal;background-color:#ffffff;">
<div role="article" aria-roledescription="email" lang="es" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#ffffff;">
  <table role="presentation" style="width:100%;border:0;border-spacing:0;">
    <tr>
      <td align="center"><!--[if mso]>
          <table role="presentation" align="center" style="width:660px;">
          <tr>
          <td style="padding:20px 0;">
          <![endif]-->
        
     <div class="outer" style="width:96%;max-width:660px;margin:20px auto;">
          <table role="presentation" style="width:100%;border:0;border-spacing:0;">
            <tr>
              <td align="center" style="padding:10px 10px 20px 10px;font-family:Arial,sans-serif;font-size:24px;line-height:28px;font-weight:bold;"><img src="{{URL::asset('assets/imagenes/cyberfuel.jpg')}}" width="600" alt="Formulario de Contacto" style="width:100%;height:auto;" data-auto-embed="attachment" /></td>
            </tr> 
			  
		
          </table>
          <table cellpadding="0" cellspacing="0" width="100%">
         
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td valign="top" style="background-color:#f8f8f8; padding:5px"><table cellpadding="0" cellspacing="0" width="100%" style="background-color:#f1f1f1; color:#555555; font-size:16px; font-family:Arial,sans-serif;">
                
                  <tr>
                    <td width="40%" style="color:#999999; font-size:16px; font-weight:bold; font-family:Arial,sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8;  padding:15px 10px">Nombre del cliente</td>
                    <td style="color:#555555; font-size:16px; font-family:Arial,sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">{{ $data->nombre }}</td>
                  </tr>
                    <tr bgcolor="#ffffff">
                    <td width="40%" style="color:#999999; font-size:16px; font-weight:bold; font-family:Arial,sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8;  padding:15px 10px">Email</td>
                    <td style="color:#555555; font-size:16px; font-family:Arial,sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">{{ $data->email }}</td>
                  </tr>
              
                  <tr>
                    <td width="40%" style="color:#999999; font-size:16px; font-weight:bold; font-family:Arial,sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8;  padding:15px 10px">Departamento</td>
                    <td style="color:#555555; font-size:16px; font-family:Arial,sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">{{ $data->departamento }}</td>
                  </tr>
                  
                    <tr bgcolor="#ffffff">
                    <td width="40%" style="color:#999999; font-size:16px; font-weight:bold; font-family:Arial,sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8;  padding:15px 10px">Servicio</td>
                    <td style="color:#555555; font-size:16px; font-family:Arial,sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">{{ $data->servicio }}</td>
                  </tr>
                      <tr>
                    <td width="40%" style="color:#999999; font-size:16px; font-weight:bold; font-family:Arial,sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8;  padding:15px 10px">Mensaje</td>
                    <td style="color:#555555; font-size:16px; font-family:Arial,sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">{{  $data->mensaje }}</td>
                  </tr>
                </table></td>
            </tr>
          </table>
        </div>
        
        <!--[if mso]>
          </td>
          </tr>
          </table>
          <![endif]--></td>
    </tr>
  </table>
</div>
</body>
</html>