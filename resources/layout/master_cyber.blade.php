<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="cyberfuel.com">
	  <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
	 @yield('metas')
	 @yield('face')
	 @yield('css') 
   
  
    <link rel="shortcut icon" href="{{URL::asset('assets/images/favicon.png')}}" type="image/x-icon"/>
    <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/all.min.css')}}">
	
	 <link rel="stylesheet" href="{{URL::asset('assets/css/animate.compat.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/theme-shop.css')}}">
    <!-- Theme CSS -->
	
    <link rel="stylesheet" href="{{URL::asset('assets/css/theme.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/css/theme-elements.min.css')}}">
    <!-- Skin CSS -->
    <link id="skinCSS" rel="stylesheet" href="{{URL::asset('assets/css/master.css')}}">
    <link id="skinCSS" rel="stylesheet" href="{{URL::asset('assets/css/simple-line-icons.min.css')}}">

	
	<style>
	
	</style>
</head>
<body>
<div class="body">
    <!-- Menu -->

	

		<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': true, 'stickyStartAt': 120, 'stickyHeaderContainerHeight': 70}">
				<div class="header-body border-color-primary header-body-bottom-border">
					<div class="header-top header-top-default border-bottom-0">
						<div class="container">
							<div class="header-row py-2">
								<div class="header-column justify-content-start">
									<div class="header-row">
										<nav class="header-nav-top">
						
										
											
												<ul class="header-social-icons social-icons d-none d-sm-block">
											<li class="social-icons-facebook"><a href="https://www.facebook.com/cyberfuel" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
											<li class="social-icons-instagram"><a href="https://www.instagram.com/cyberfuel_sa/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
											<li class="social-icons-linkedin"><a href="https://www.linkedin.com/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2F1017615%2Fadmin%2F" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
											
										</nav>
									</div>
								</div>
								<div class="header-column justify-content-end">
									<div class="header-row">
										<nav class="header-nav-top">
											<ul class="nav nav-pills">
                                              <li class="nav-item"> <span class="infoToggler"><img src="{{URL::asset('assets/imagenes/logos/estados-unidos.png')}}" width="16px" height="16px"/> <img src="{{URL::asset('assets/imagenes/logos/espana.png')}}" width="16px" height="16px" style="display:none"/> </span> </li>
												<li class="nav-item">
												   <a href="mailto:info@cyberfuel.com" class="text-quaternary text-color-hover-primary"><i class="far fa-envelope text-4  " style="top: 1px;"></i> <span class="d-none d-md-inline-block">Info@cyberfuel.com</span></a>
												</li>
												<li class="nav-item">
													 <a href="tel:+50622049494" class="text-quaternary text-color-hover-primary"><i class="fas fa-phone text-4  " style="top: 0;"></i> <span class="d-none d-md-inline-block">(+506) 2204-9494</span></a>
												</li>
												
												<li class="nav-item">
														<a target="_blank" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Comunicate vía Whatsapp" class="text-quaternary text-color-hover-success" href="https://api.whatsapp.com/send?phone=50622049494"><i class='fab fa-whatsapp text-5'></i></a>
												</li>
												
												<li class="nav-item">
													
													<a data-bs-toggle="modal" data-bs-target="#usuario" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Ingreso | Registro" href="#" class=" text-quaternary text-color-hover-primary ">
														<i class="fas fa-user text-4 "></i>
													</a>
												
												</li>
											</ul>
										</nav>
									</div>
								</div>
									<div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
										<div class="header-nav-feature header-nav-features-search d-inline-flex">
											<a href="#" class="header-nav-features-toggle text-decoration-none" data-focus="headerSearch"><i class="fas fa-search header-nav-top-icon"></i></a>
											<div class="header-nav-features-dropdown" id="headerTopSearchDropdown">
												<form role="search" action="page-search-results.html" method="get">
													<div class="simple-search input-group">
														<input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Search...">
														<button class="btn" type="submit">
															<i class="fas fa-search header-nav-top-icon"></i>
														</button>
													</div>
												</form>
											</div>
										</div>
										<div class="header-nav-feature header-nav-features-cart d-inline-flex ms-2">
											<a href="#" class="header-nav-features-toggle">
												<img src="{{URL::asset('assets/imagenes/logos/icon-cart.svg')}}" width="14" alt="" class="header-nav-top-icon-img"> 
												<span class="cart-info d-none">
													<span class="cart-qty">1</span>
												</span>
											</a>
											<div class="header-nav-features-dropdown" id="headerTopCartDropdown">
												<ol class="mini-products-list">
													<li class="item">
														<a href="#" title="Hospedaje Web" class="product-image"><img src="{{URL::asset('assets/imagenes/hospedaje.jpg')}}" alt="Hospedaje Web"></a>
														<div class="product-details">
															<p class="product-name">
																<a href="#">Hospedaje Web</a>
															</p>
															<p class="qty-price">
																 1X <span class="price">$890</span>
															</p>
															<a href="#" title="Remove This Item" class="btn-remove"><i class="fas fa-times"></i></a>
														</div>
													</li>
												</ol>
												<div class="totals">
													<span class="label">Total:</span>
													<span class="price-total"><span class="price">$890</span></span>
												</div>
												<div class="actions">
													<a class="btn btn-dark" href="#">Ver Carrito</a>
													<a class="btn btn-primary" href="#">Checkout</a>
												</div>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										   <a href="/">
                                    <img alt="Cyberfuel" width="270" height="66" data-sticky-width="200" data-sticky-height="49" src="{{URL::asset('assets/imagenes/cyber2.png')}}">
                                </a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<div class="header-nav header-nav-links order-2 order-lg-1">
										<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li class="dropdown">
													 <a class="active dropdown-item px-2" href="{{ url('/') }}"> Inicio </a>
													</li>
													
													    <li class="dropdown">
                                                <a class="dropdown-item px-2" href="{{ url('nombres-de-dominios') }}"> Nombres de dominios </a>
                                            </li>
													
												
													
													         <li class="dropdown">
                                                <a class="dropdown-item dropdown-toggle px-2">Hospedaje WEB </a>
												
												<ul class="dropdown-menu">
													
														<li>
																<a class="dropdown-item" href="{{ url('hospedaje-web') }}">
																	Planes de Hospedaje
																</a>
															</li>	
														<li>
																<a class="dropdown-item" href="{{ url('comercio-electronico') }}">
																	Comercio Electrónico
																</a>
															</li>
															<li>
																<a class="dropdown-item" href="{{ url('filtro-de-correo-no-deseado') }}">
																 Filtro de Correo no deseado
																</a>
															</li>
													<li>
																<a class="dropdown-item" href="{{ url('seguridad-ssl-waf-pci-dss') }}">
																Seguridad
																</a>
															</li>
											   </ul>
                                            </li>
															<li class="dropdown">
                                                <a class="dropdown-item dropdown-toggle px-2">Servicios en la Nube  </a>
													<ul class="dropdown-menu">
													
														<li>
																<a class="dropdown-item" href="{{ url('servidores') }}">
																	Servidores
																</a>
															</li>	
														<li>
																<a class="dropdown-item" href="{{ url('baas') }}">
																	Respaldo como servicio (BaaS)
																</a>
															</li>
															<li>
																<a class="dropdown-item" href="{{ url('draas') }}">
																	Recuperación de Desastres como Servicio (DRaaS)
																</a>
															</li>
														
											
											
											   </ul>
                                            </li>
													  <li class="dropdown">
                                                <a class="dropdown-item px-2" href="{{ url('centro-de-datos') }}">Centro de Datos</a>
                                            </li>
													<li class="dropdown">
														<a data-bs-toggle="modal" data-bs-target="#productos" class=" px-2" href="#">
															Productos
														</a>
														<!--<ul class="dropdown-menu">
															<li>
																<div class="dropdown-mega-content">
																	<div class="row  align-items-center">
																	
																		<div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/atencion.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	
																	 <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.daycarecontrol.com/es_CR/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/day-care.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	
																		
																		     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.factorajeelectronico.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/factoraje.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																		
																			     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.facturaprofesional.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/factura.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	</div>
																			<div class="row justify-content-center align-items-center">
																	
																				
																					     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.comprobanteselectronicoscr.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/comprobantes.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																
																				     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('http://www.smsempresarial.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/sms.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	
                                                                        <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/voto.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	</div>
															
																</div>
															</li>
														</ul>-->
													</li>
                                             
												
													
														<li class="d-md-none">
																<a class="dropdown-item" href="{{ url('contactenos') }}">
																	Contáctenos
																</a>
															</li>
													
													<li class="d-md-none">
					  <div class="col-md-6 col-lg-2">
                <h5 class="text-3 mb-3 text-color-black">SIGUENOS EN LAS REDES</h5>
           
				<ul class="social-icons  social-icons-dark-2 d-flex ">
						<li class="social-icons-facebook"><a href="https://www.facebook.com/cyberfuel/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
						<li class="social-icons-twitter ml-1"><a href="https://www.instagram.com/cyberfuel_sa/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
						<li class="social-icons-linkedin ml-1"><a href="https://www.linkedin.com/company/1017615/admin/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
            </div>
															</li>
											
												</ul>
											</nav>
										</div>
									
										<button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

    <div role="main" class="main">
        @if(isset($errors) && $errors->any())
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 mt-4">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $e)
                                    <li> {{ $e }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <!--  aca se carga el contenido de las secciones    -->
        @yield('content')
    </div>
</div>
<footer id="footer" class="bg-color-light-scale-1">
    <div class="container">
        <div class="footer-ribbon">
            <span>Contáctanos</span>
        </div>
        <div class="row py-5 my-4 align-items-start">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <h5 class="text-3 mb-3 text-color-black">Suscríbete a nuestro Blog</h5>
                <p class="pe-1">Mantente al día con nuestras características y tecnología de productos en constante evolución. Ingresa tu correo electrónico y suscríbete a nuestro canal.</p>
                <div class="alert alert-success d-none" id="newsletterSuccess">
                    <strong>Success!</strong> You've been added to our email list.
                </div>
                <div class="alert alert-danger d-none" id="newsletterError"></div>
                <!--							<form id="newsletterForm" action="" method="" class="me-4 mb-3 mb-md-0">-->
                <div class="input-group input-group-rounded">
                    <input class="form-control form-control-sm bg-light" placeholder="Dirección de correo electrónico" name="newsletterEmail" id="newsletterEmail" type="text">
                    <button class="btn btn-light text-color-dark" type="button">
                        <strong>¡ENVIAR!</strong>
                    </button>
                </div>
                <!--							</form>-->
            </div>
            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                <h5 class="text-3 mb-3"></h5>
                <div>
                    <img src="{{URL::asset('assets/images/logos/sello-garantia-FE-DGT-R-033-2019.png')}}" class="w-75 img-fluid d-block mx-auto" alt="">
                    <img src="{{URL::asset('assets/images/logos/certificado_l.png')}}" class="w-75 img-fluid d-block mx-auto" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                <h5 class="text-3 mb-3 text-color-black">CONTÁCTANOS</h5>
                <ul class="list list-icons list-icons-lg">
                    <li class="mb-1">
                        <i class="far fa-dot-circle text-color-primary"></i>
                        <p class="m-0">
                            <strong>Dirección:</strong>
                            <span>Costa Rica, San José, Santa Ana, FORUM I</span>
                        </p>
                    </li>
                    <li class="mb-1">
                        <i class="fab fa-whatsapp text-color-primary"></i>
                        <p class="m-0">
                            <a href="tel:50622049494"><strong>Teléfono:</strong>
                                <span>(+506) 2204-9494</span></a>
                        </p>
                    </li>
                    <li class="mb-1">
                        <i class="far fa-envelope text-color-primary"></i>
                        <p class="m-0">
                            <strong>Email:</strong>
                            <span><a href="mailto:Info@cyberfuel.com">Info@cyberfuel.com</a></span>
                        </p>
                    </li>
                    <li class="mb-1">
                        <i class="far fa-clock text-color-primary"></i>
                        <p class="m-0">
                            <strong>Horario:</strong>
                            <span>Atención al cliente 24/7</span>
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-2">
                <h5 class="text-3 mb-3 text-color-black">SIGUENOS EN LAS REDES</h5>
           
				<ul class="social-icons social-icons-big social-icons-dark-2">
						<li class="social-icons-facebook"><a href="https://www.facebook.com/cyberfuel/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
						<li class="social-icons-twitter"><a href="https://www.instagram.com/cyberfuel_sa/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
						<li class="social-icons-linkedin"><a href="https://www.linkedin.com/company/1017615/admin/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container py-2">
            <div class="row py-4">
                <div class="col-lg-2 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                    <a href="{{ url('/') }}" class="logo pe-0 pe-lg-3">
                        <img alt="Porto Website Template" src="{{URL::asset('assets/imagenes/logos/cyber.png')}}" class="opacity-10" width="125" height="30">
                    </a>
                </div>
                <div class="col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
                    <p class="text-white">© Copyright 2021. Derechos Reservados.</p>
                </div>
            </div> 
        </div>
    </div>
	
	
</footer>
	
	                                     <!--  modal de productos-->

									<div class="modal fade" id="productos" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title" id="largeModalLabel">Productos by Cyberfuel</h4>
													<button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
												</div>
												<div class="modal-body">
													<div class="row  align-items-center">
																	
																		<div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/atencion.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	
																	 <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.daycarecontrol.com/es_CR/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/day-care.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	
																		
																		     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.factorajeelectronico.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/factoraje.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																		
																			     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.facturaprofesional.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/factura.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	</div>
													<div class="row justify-content-center align-items-center">
																	
																				
																					     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('https://www.comprobanteselectronicoscr.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/comprobantes.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																
																				     <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('http://www.smsempresarial.com/') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/sms.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	
                                                                        <div class="col-6 col-md-6 col-lg-3 mt-2 mt-lg-0">
                                                                                <div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary m-0 mb-4 pb-3">
                                                                            <div class="featured-box featured-box-no-borders featured-box-box-shadow"> <a href="{{ url('') }}" class="text-decoration-none"> <span class="box-content px-1 py-4 text-center d-block"> <img class="card-img-top w-75" src="{{URL::asset('assets/imagenes/logos/voto.png')}}" alt="Cyberfuel"></a> </span> </a> </div>
                                                                          </div>
                                                                              </div>
																	</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
												</div>
											</div>
										</div>
									</div>
                        
										<!--     modal de loguin-->


									<div class="modal fade" id="usuario" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-sm">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title" id="largeModalLabel">Login</h4>
													<button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
												</div>
												<div class="modal-body">
												<div class="row">
													<div class="col-12 mb-5 mb-lg-0">
							<h2 class="font-weight-bold text-5 mb-0">Ingresa tus datos</h2>
							<form action="/" id="frmSignIn" method="post" class="needs-validation">
								<div class="row">
									<div class="form-group col">
										<label class="form-label text-color-dark text-3">Código de Cliente <span class="text-color-danger">*</span></label>
										<input type="text" value="" class="form-control form-control-lg text-4" required>
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										<label class="form-label text-color-dark text-3">Clave <span class="text-color-danger">*</span></label>
										<input type="password" value="" class="form-control form-control-lg text-4" required>
									</div>
								</div>
								<div class="row justify-content-between">
									<div class="form-group col-md-auto">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="rememberme">
											<label class="form-label custom-control-label cur-pointer text-2" for="rememberme">Recordarme</label>
										</div>
									</div>
									<div class="form-group col-md-auto">
										<a class="text-decoration-none text-color-dark text-color-hover-primary font-weight-semibold text-2" href="#">Recuperar contraseña?</a>
									</div>
								</div>
								<div class="row">
									<div class="form-group col">
										<button type="submit" class="btn btn-dark btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3" data-loading-text="Loading...">Iniciar</button>
									</div>
								</div>
							</form>
						</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
												</div>
											</div>
										</div>
									</div>



<!-- Vendor -->
<script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>

	
<script src="{{ URL::asset('assets/js/bootstrap.bundle.min.js') }}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{ URL::asset('assets/js/theme.js') }}"></script>
<!-- Current Page Vendor and Views -->
<!-- Theme Custom -->
<script src="{{ URL::asset('assets/js/custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ URL::asset('assets/js/theme.init.js') }}"></script>


@yield('scripts')
	
	<script> 
	$(".infoToggler").click(function() {
    $(this).find('img').toggle();
});

	</script>

</body>
</html>
