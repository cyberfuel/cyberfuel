<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

'titulo' => 'Your virtual or online store using the e-commerce solution',
'descripcion' => 'Create your own online store in seconds with the Woocommerce solution and e-commerce web hosting that Cyberfuel for electronic commerce in Costa Rica',
'comercio' => 'Woocomerce',
'electronico' => '',
'si_desea' => 'If you want to sell through the Internet, this is the solution that will allow you to do so.',
'consulta' => 'Query now',
'componentes' => 'Components or Extensions',
'link_uno' => 'Through the Woocommerce solution you can adapt different components or additional extensions that allow you to adapt to your needs. In the basic solution you will find: ',
'link_dos' => 'Other additional components or extensions, which may or may not have an additional cost',
'ver_mas' => 'See more',
'adicional' => 'Additional',
'dominio_pro' => 'Own domain',
'hospedaje_web' => 'WEB hosting',
'ins_woo' => 'Additional WooCommerce extensions',
'integra_factura' => 'Integration for Electronic Billing with FacturaProfesional.com',
'perso' => 'Personalization',
'integra_bancos' => 'Integration with Banks',
'capa_inicial' => 'Virtual initial training',
'acceso' => 'Access to Video Tutorials',
'asistencia' => 'Operational Assistance',
'actuali' => 'Free updates',
'precio' => 'Price',
'por_mes' => 'Per month',
'consultar' => 'Consult',
'solucion' => 'In the basic solution you will find',
'cerrar' => 'Close',
    
'servidor_dinamico_nube' => 'Dynamic Cloud Server',
'servidor_dinamico_plesk' => 'Dynamic Cloud Server with Plesk ',
'servidor_dinamico_plesk_wordpress' => 'Dynamic Cloud Server with Plesk + Wordpress ',
'servidor_dinamico_cpanel' => 'Dynamic Server in the cloud with Cpanel',
'planes_administracion' => 'Management Plans',
'respaldp_basas' => 'BaaS Backup Service',
'bitninja' => 'BitNinja Service',
'spamfilter' => 'Spamfilter Service',
'certificado' => 'SSL certificate ',
'' => '',

	
]; 