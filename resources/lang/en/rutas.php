<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
	'url_inicio' => 'home',
	'url_dominio' => 'domain-names',
	'url_hospedaje' => 'web-hosting',
	'url_comercio' => 'electronic-commerce',
	'url_spam' => 'spam-filter',
	'url_seguridad' => 'security-and-protection',
	'url_servidores' => 'servers',
	'url_baas' => 'baas',
	'url_draas' => 'draas',
	'url_centro' => 'data-center',
	'url_nuevo_cliente' => 'new-client',
	'url_compra' => 'buy',
	'url_detalle_pago' => 'payment-detail',
	'url_politica-de-privacidad' => 'privacy-policy',
	'url_politica-anti-spam' => 'anti-spam-policy',
	'url_acerca-de-cyberfuel' => 'about-cyberfuel',
	'url_terminos-condiciones' => 'terms-conditions',
	'url_carrito-de-compra' => 'shopping-cart',
	'url_contactenos' => 'contact-us',
	'url_herramientas' => 'tools',
	'url_mapa' => 'site-map',
	'url_mercado' => 'marketplace',
	'url_producto' => 'product',
	'url_servicios_nube' => 'cloud-services',
	'url_almacenamiento' => 'storage',
	'url_orden_completa' => 'order-complete',
	'url_orden_fallida' => 'order-fail',
	'url_auth' => 'auth',

];
