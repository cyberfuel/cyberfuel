<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


  'inicio' => 'Home',
  'nombre_dominio' => 'Domain names',
  'nombre_dominio_internet' => 'Internet Domain Names',
  'planes_hospedaje' => 'Hosting Plans',
  'hospedaje_web' => 'Web Hosting',
  'comercio' => 'Woocomerce',
  'filtro_correo' => 'Spam Filter',
  'seguridad' => 'Protection and security',
  'servicios_nube' => 'Cloud Services',
  'servidores' => 'Servers',
  'servidores_flex' => 'Flex Servers',
  'respaldo_servicio' => 'Backup as a service',
  'recuperacion_desastres' => 'Disaster Recovery as a Service',
  'centro_datos' => 'Data Center',
  'productos' => 'Products',
  'contactenos' => 'Contact us',
  'siguenos_redes' => 'Follow us on the Networks',
  'enviar' => 'Send',
  'direccion' => 'Address',
  'telefono' => 'Phone',
  'telefono_opcional' => 'Phone Optional',
  'horario' => 'Hours',
  'atencion' => 'Customer service',
  'productos_por' => 'Products by',
  'ingrese_datos' => 'Enter your data',
  'ingrese_correo' => 'Enter your email and we will send the instructions.',
  'codigo_cliente' => 'Customer Code',
  'clave' => 'Password',
  'recordarme' => 'Remember me',
  'recuperar' => 'Recover password',
  'cerrar' => 'Close',
  'iniciar' => 'Start',
  'buscar' => 'Search',
  'registrar' => 'Sign up',
  'registrar' => 'Sign up',
  'regis' => 'To register',
  'o' => 'or',
  'ssl_cer' => 'SSL Certificate',
  'almacenamiento' => 'Storage',
  'edificio' => 'Building',
  'desde' => 'From',
  'ano' => 'Year',
  'transferir' => 'Transfer',
  'utilizar' => 'Use',
  'si_aun' => 'Register If you don t have an account yet',
  'registrate' => 'Sign up',
  'regresar_login' => 'Return to login',

  /*seguridad*/
  'requerimientos' => 'Requirements',
  'ventajas' => 'Advantage',
  'beneficios' => 'Benefits',
  'ad_ser' => 'Acquire service',
  'p_me' => 'per month',
  'p_cun' => 'per account',
  'prec' => 'price',

  /*footer*/
  'acerca_cyber' => 'About Cyberfuel',
  'ter_cond' => 'Terms and Conditions',
  'politica_priva' => 'Privacy policy',
  'politica_spam' => 'Anti-SPAM Policy',
  'crear_tiq' => 'Create a Ticket',
  'historial_tique' => 'History of a ticket',
  'herramientas' => 'Tools',
  'mi_cuenta' => 'My Account',
  'reg_nuevo_cliente' => 'New client registration',
  'carrito_compras' => 'Shopping Cart',
  'mapa_sitio' => 'Site Map',
  'acerca_nosotros' => 'About Us',
  'centro_asistencia' => 'Assistance Center',
  'recursos' => 'Resources',

  /*Draas*/
  'tenga_plan' => 'Always have a plan B, in case of an eventuality',
  /*centro de datos*/
  'datos_edge' => 'High Availability Edge Data Center',

  'colocacion_unidad' => 'Colocation by Unit',
  'colocacion_gabinete' => 'Colocation Complete Cabinet or Cage',
  'redu_internet' => 'Internet redundancy using BGP4 to different service providers',
  'disponi_ixp' => 'Availability of different bandwidths to the Internet or to the IXP',
  'direcciona_ip' => 'IPv4 and IPv6 addressing',
  'control_inte' => 'Intelligent routing control (Like having Waze on the network)',
  'redu_electrica' => 'Electrical redundancy through two different sources',
  'redu_enfria' => 'Redundancy on cooling',
  'lugar_seguro' => 'Safe place',
  'monitoreo' => 'Permanent monitoring',
  'soporte' => '24x7 support',
  'coloca_unidad' => 'Colocation by Unit',
  'coloca_completo' => 'Colocation Complete Cabinet or Cage',
  'algunos_beneficios' => 'Some of the benefits',
  'cotizar' => 'Quote',

  /*herramientas*/
  'acce_ser' => 'Access to Services',
  'herra_re' => 'Network Tools',
  'descar' => 'Downloads',
  'ingre_we' => 'Enter your Webmail',
  'ingre' => 'Enter',
  'ingre_pane' => 'Enter your Control Panel',
  'ip_sali' => 'Your exit IP to the Internet is',
  'veri_ip' => 'Check if your IP is blocked by SPAM',
  'veri_puer' => 'Verification port 25 (SMTP)',
  'clien_acce' => 'Remote Access Clients',
  'naveg' => 'Browsers',
  'clien_co' => 'Mail Clients',
  'clien_ftp' => 'FTP clients',
  'clien_ssh' => 'SSH Clients',
  'verificar' => 'Check',
  'copiar' => 'Copy',

  /*contactenos*/


  'men_envi' => 'Your message was sent',
  'exito' => 'Success',
  'men_no_envi' => 'Your message was not sent',
  'nom_comp' => 'Full Name',
  'cor_ele' => 'Email',
  'tema' => 'Topic',
  'mensaje' => 'Message',
  'nuest_offi' => 'Our Office',
  'dire' => 'Address',
  'tel' => 'Telephone',
  'asisten' => 'Assistance 24 hours a day, every day',
  'esla' => 'is the',
  'solu' => 'SOLUTION',
  'Nego_empre' => 'for your business or company',
  'empi_aho' => 'Start Now',
  'nuest_ho' => 'Our schedule',

  /*nuevo cliente*/


  'deta_fac' => 'Billing Details',
  'soy_cliente' => 'I am a customer',
  'selec_pai' => 'Select your Country',
  'tip_clien' => 'Choose the type of client',
  'per_ju' => 'Legal Person',
  'per_fi' => 'Physical Person',
  'extran' => 'Foreigner',
  'inf_clien' => 'Customer Information',
  'nom' => 'Name',
  'ape' => 'Surname',
  'iden' => 'Identification',
  'cel' => 'Mobile phone',
  'contra' => 'Password',
  'ol_contra' => 'Forgot your Password',

  'fina_pa' => 'Finalize payment',

  'con_tec' => 'Technical Contact',
  'us_mis' => 'Use the same administrative contact details',
  'con_co' => 'Payment Contact',
  'con_ad' => 'Administrative Contact',

  /*detalle de pago*/
  'deta_pa' => 'Payment Details',
  'ti_cup' => 'Do you have a coupon?',
  'digi_cup' => 'Enter your coupon here',
  'apli_cup' => 'Apply coupon',
  'co_tar' => 'With Card',
  'nu_tar' => 'Card number',
  'cod_seg' => 'Security code',
  'm_expi' => 'Expiration month',
  'a_expi' => 'Expiration year',
  'fi_pag' => 'Complete payment',
  'gra_reci' => 'Thank you. Your order has been received. ',
  'nu_or' => 'Order number',
  'fech' => 'Date',
  'me_pa' => 'Payment method',
  'tar' => 'Card',
  't_or' => 'Your order',
  'cod_cup' => 'Promotion Code',
  'descuento_promo' => 'Discount Promotion',
  'error_cupon_descuento' => 'Invalid discount code',
  'impuesto' => 'Tax',
  'servicios' => 'Services',
  'continuar' => 'Continue',
  'regresar_log' => 'Return Login',
  'anual' => 'Yearly',
  'anos_regis' => 'Years of Registration',
  'regis_priva' => 'Private Registry',
  'filtro_anti' => 'AntiSpam Filter',
  'respaldo' => 'Backup',

  /*servidores*/

  'selec_vcp' => 'Select the vCPUs',
  'selec_ram' => 'Select memory',
  'memoria_ram' => 'Ram',
  'canti_compra' => 'Purchase quantity',
  'sis_operativo' => 'OS',

  'tipo_almacenamiento' => 'Storage type and local redundancy',
  'elija_ser_dinamico' => 'Choose Dynamic Cloud Server with.',
  'canti_almacenamiento' => 'Storage Quantity',
  'resumen' => 'Summary',
  'costo_ins' => 'Monthly instance cost',
  'costo_alma' => 'Storage cost',
  'conti_carrito' => 'Continue and go to cart',
  'usuario' => 'User',
  'com_orden' => 'Complete order',
  'nom_titular' => 'Name of owner',
  'pago' => 'Pay'
];
