<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
 
'cara_prin' => 'Main Features',
'can_domi' => 'Number of Domains',
'espa_dis' => 'Hard disk space',
'buzo_co' => 'Email Boxes',
'tra_men' => 'Monthly transfer (*)',
'bas_dat_micr' => 'Microsoft SQL-Sever Database',
'sop_prog' => 'Support for Programming in ASP, ASPX, PHP & CGI',
'res_diar' => 'Daily backup',
'sopor_tec' => 'Technical Support 24x7',
'pag_an' => 'Annual Payment',
'pane_cont' => 'WEB Control Panel',
'acc_ftp' => 'FTP access',
'ed_arc' => 'File editor',
'pro_dire' => 'Directory protection',
'adm_tare' => 'Task manager (Cron Jobs)',
'adm_zon' => 'DNS Zone Manager',
'estad' => 'Statistics',
'adm_we' => 'Web Users Administration',
'per_pag' => 'Customization of error pages',
'adm' => 'Administration',
'acce_sin' => 'Access without "www"',
'ali_domi' => 'Domain Aliases',
'subdo' => 'Subdomains',
'ayu_grat' => 'Free help to transfer your DNS',
'ali_co' => 'Email aliases',
'ren_cor' => 'Forward email',
'au_resp' => 'Auto-reply',
'grup_cor' => 'Mail groups',
'lis_cor' => 'Mailing lists',
'cuen_cat' => 'Catch-all account',
'det_vir' => 'Virus detection',
'det_spa' => 'SPAM detection (level 1 and 2)',
'det_spa_spf' => 'SPAM detection by SPF',
'fil_spa' => 'SPAM filter (level 1-8)',
'pro_ima' => 'IMAP protocol',
'pro_pop' => 'POP3 / SMTP protocol',
'bas_da' => 'Databases',
'op_resp' => 'Backup / Restore Option',
'adm_base' => 'Database Administrator via WEB',
'ser_we' => 'WEB server',
'bo_app' => 'Vault of Apps to install (CMS like Joomla, WordPress and many more ...)',
'ilimi' => 'Unlimited',
	'pymes' => 'SBEs',
	'empresarial' => 'Business',
	'dominios' => 'Domains',
	'planes' => 'Hosting Plans',
	'email' => 'Email',
	'plane' => 'Plans',
	'hospedaje' => 'Web Hosting',
		'titulo' => 'Acquire your web hosting plan for your site and email with Cyberfuel',
	'descripcion' => 'Web hosting, WEB Hosting or WEB Hosting and email for your site on both Linux and Windows',
	'hospedaje_web' => 'Web Hosting',
	'agregar_carrito' => 'Add to cart',
	'conocer_carac' => 'Know more characteristics of the lodging?',
	'ver_mas_planes' => 'See more about hosting plans',
]; 