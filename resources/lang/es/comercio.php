<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


	'titulo' => 'Tu tienda virtual o en línea usando la solución de comercio electrónico',
	'descripcion' => 'Crea tu propia tienda en línea en segundos con la solución de Woocommerce y el hospedaje web de e-commerce que Cyberfuel para comercio electrónico en Costa Rica',
	'comercio' => 'Comercio',
	'electronico' => 'Electrónico',
	'si_desea' => 'Si usted desea vender por medio de Internet, esta es la solución que le permitirá realizarlo.',
	'consulta' => 'Consulta ahora ',
	'componentes' => 'Componentes o Extensiones',
	'link_uno' => 'Por medio la solución de Woocommerce puede adaptar diferentes componentes o extensiones adicionales que le permitan adaptarse a sus necesidades. En la solución básica encontrará:',
	'link_dos' => 'Otros componente o extensiones adicionales, que podrían tener un costo adicional o no',
	'ver_mas' => 'Ver más',
	'adicional' => 'Adicional',
	'dominio_pro' => 'Dominio propio',
	'hospedaje_web' => 'Hospedaje WEB',
	'ins_woo' => 'Extensiones adicionales de WooCommerce',
	'integra_factura' => 'Integración para Facturación Electrónica con FacturaProfesional.com',
	'perso' => 'Personalización',
	'integra_bancos' => 'Integración con Bancos',
	'capa_inicial' => 'Capacitación inicial virtual',
	'acceso' => 'Acceso a Videotutoriales',
	'asistencia' => 'Asistencia Operativa',
	'actuali' => 'Actualizaciones gratuitas',
	'precio' => 'Precio',
	'por_mes' => 'Por mes',
	'consultar' => 'Consultar',
	'solucion' => 'En la solución básica encontrará',
	'cerrar' => 'Cerrar',
    
    'servidor_dinamico_nube' => 'Servidor Dinámico en la Nube',
    'servidor_dinamico_plesk' => 'Servidor Dinámico en la nube con Plesk',
    'servidor_dinamico_plesk_wordpress' => 'Servidor Dinámico en la Nube con Plesk + Wordpress',
    'servidor_dinamico_cpanel' => 'Servidor Dinámico en la nube con Cpanel',
    'planes_administracion' => 'Planes de Administración',
    'respaldp_basas' => 'Servicio de Respaldo BaaS',
    'bitninja' => 'Servicio de BitNinja',
    'spamfilter' => 'Servicio de Spamfilter',
    'certificado' => 'Certificado SSL',
    '' => '',
	
]; 