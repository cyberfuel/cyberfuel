<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
 

	'cara_prin' => 'Características principales Planes',
	'can_domi' => 'Cantidad de Dominios',
	'espa_dis' => 'Espacio en disco duro',
	'buzo_co' => 'Buzones de correo',
	'tra_men' => 'Transferencia mensual (*)',
	'bas_dat_micr' => 'Base de datos Microsoft SQL-Sever ',
	'sop_prog' => 'Soporte para Programación en ASP, ASPX, PHP & CGI ',
	'res_diar' => 'Respaldo diario ',
	'sopor_tec' => 'Soporte Técnico 24x7',
	'pag_an' => 'Pago Anual',
	'pane_cont' => 'Panel de Control WEB',
	'acc_ftp' => 'Acceso FTP',
	'ed_arc' => 'Editor de archivos',
	'pro_dire' => 'Protección de directorios',
	'adm_tare' => 'Administrador de tareas (Cron Jobs)',
	'adm_zon' => 'Administrador de Zona de DNS ',
	'estad' => 'Estadísticas',
	'adm_we' => 'Administración de Usuarios Web.',
	'per_pag' => 'Personalización de páginas de error',
	'adm' => 'Administración',
	'acce_sin' => 'Acceso sin "www"',
	'ali_domi' => 'Alias de Dominio',
	'subdo' => 'Subdominios ',
	'ayu_grat' => 'Ayuda gratuita para transferir su DNS',
	'ali_co' => 'Alias de correo electrónico ',
	'ren_cor' => 'Renvío de correo electrónico ',
	'au_resp' => 'Auto-respuesta ',
	'grup_cor' => 'Grupos de correo',
	'lis_cor' => 'Listas de correo',
	'cuen_cat' => 'Cuenta de Catch-all',
	'det_vir' => 'Detección de virus ',
	'det_spa' => 'Detección de SPAM (nivel 1 y 2)',
	'det_spa_spf' => 'Detección de SPAM por SPF',
	'fil_spa' => 'Filtro de SPAM (nivel 1-8)',
	'pro_ima' => 'Protocolo IMAP ',
	'pro_pop' => 'Protocolo POP3/SMTP',
	'bas_da' => 'Bases de datos',
	'op_resp' => 'Opción de Respaldo/Restauración ',
	'adm_base' => 'Administrador de Base de datos vía WEB',
	'ser_we' => 'Servidor WEB',
	'bo_app' => 'Bóveda de Apps para instalar (CMS como Joomla, WordPress y muchos más...)',
	'ilimi' => 'Ilimitado',
	'pymes' => 'Pymes',
	'empresarial' => 'Empresarial',
	'dominios' => 'Dominios',
	'planes' => 'Planes de Hospedaje',
	'email' => 'Correo electrónico',
	'plan' => 'Planes de',
	'hospedaje' => 'Hospedaje Web',
	'titulo' => 'Adquiere tu plan de hospedaje web para tu sitio y correo electrónico con Cyberfuel',
	'descripcion' => 'Web hosting, Alojamiento WEB o Hospedaje WEB y correo electrónico para tu sitio tanto en Linux como Windows',
	'hospedaje_web' => 'Hospedaje Web',
	'agregar_carrito' => 'Agregar al carrito',
	'conocer_carac' => 'Conocer más características del hospedaje?',
	'ver_mas_planes' => 'Ver más sobre los planes de hospedaje',
	
]; 