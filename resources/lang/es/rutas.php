<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
	'url_inicio' => '/',
	'url_dominio' => 'nombres-de-dominios',
	'url_hospedaje' => 'hospedaje-web',
	'url_comercio' => 'comercio-electronico',
	'url_spam' => 'filtro-de-correo-no-deseado',
	'url_seguridad' => 'seguridad-y-proteccion',
	'url_servidores' => 'servidores',
	'url_baas' => 'baas',
	'url_draas' => 'draas',
	'url_centro' => 'centro-de-datos',
	'url_nuevo_cliente' => 'nuevo-cliente',
	'url_compra' => 'compra',
	'url_detalle_pago' => 'detalle-de-pago',
	'url_politica-de-privacidad' => 'politica-de-privacidad',
	'url_politica-anti-spam' => 'politica-anti-spam',
	'url_acerca-de-cyberfuel' => 'acerca-de-cyberfuel',
	'url_terminos-condiciones' => 'terminos-condiciones',
	'url_carrito-de-compra' => 'carrito-de-compra',
	'url_contactenos' => 'contactenos',
	'url_herramientas' => 'herramientas',
	'url_mapa' => 'mapa-del-sitio',
	'url_mercado' => 'mercado',
	'url_producto' => 'producto',
	'url_servicios_nube' => 'servicios-nube',
	'url_almacenamiento' => 'almacenamiento',
	'url_orden_completa' => 'orden-completa',
	'url_orden_fallida' => 'orden-fallida',
	'url_auth' => 'auth',

];
