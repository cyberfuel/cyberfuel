$(function(){
    var form=$('form#frm_nuevo_cliente');
    var periodo=form.find('#periodo');
    var plan=form.find('#plan');

    plan.on('change', function(){
        var t=$(this);
        fn_change_plan(t.val());
        periodo.val('').change();
    });
    set_value_if_exist(plan, global_const._plan);
    set_value_if_exist(periodo, global_const._periodo);
    var provincia=form.find('#provincia');
    provincia.cargar_provincia();
    var metrica=form.find('#metrica');
    metrica.cargar_metrica();
    form.find(':not(select):input').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
});
(function(){
    fn_change_plan=function(plan){
        $('[tipo_plan_remove*="'+plan+'"]').remove();
        $('[tipo_plan_hide]').filter('[tipo_plan_hide*="'+plan+'"]').hide();
        $('[tipo_plan_hide]').not('[tipo_plan_hide*="'+plan+'"]').show();
        $('[tipo_plan_show]').filter('[tipo_plan_show*="'+plan+'"]').show();
        $('[tipo_plan_show]').not('[tipo_plan_show*="'+plan+'"]').hide();
        $('[tipo_plan_disable]').filter('[tipo_plan_disable*="'+plan+'"]').attr('disabled', 'disabled');
        $('[tipo_plan_disable]').not('[tipo_plan_disable*="'+plan+'"]').attr('disabled', null);
        $('[tipo_plan_enable]').filter('[tipo_plan_enable*="'+plan+'"]').attr('disabled', null);
        $('[tipo_plan_enable]').not('[tipo_plan_enable*="'+plan+'"]').attr('disabled', 'disabled');
    };
})();
$.fn.change_periodo=function(){
    var t=$(this);
    var form=t.closest('form');
    var selected_plan=form.find('#plan option:selected')
    var costo=t.closest('form').find('#costo');
    costo.data('costo',0);
    if(t.val()===global_const._codigo_concepto_mensual){
        costo.data('costo',selected_plan.data("costo_mensual"));
    }else if(t.val()===global_const._codigo_concepto_anual){
        costo.data('costo',selected_plan.data("costo_anual"));
    }
    costo.val('$ '+costo.data('costo'));
    form.aplicar_descuento();
};
$.fn.apiHaciendaCliente=function(){
    var t=$(this);
    var val=$.trim(t.val());
    if(val===""){
        return;
    }
    t.val(val);
    var toastr_waiting=null;
    $.ajax({
        url: "https://api.hacienda.go.cr/fe/ae",
        method: "GET",
        data: {
            identificacion: val
        },
        beforeSend: function(){
            toastr_waiting=toastr_show('Ministerio de Hacienda','Verificando número de identificación.','info',{});
        },
        success: function(resp, status, xhr){
            if(typeof xhr.responseJSON==='object' && xhr.responseJSON!==null){
                var json=xhr.responseJSON;
                var form=t.closest('form');
                form.find('#nombre').val(json.nombre);
                form.find('#tipo_identificacion').val(parseInt(json.tipoIdentificacion));
            }
        },
        error: function(xhr, status, error){
            var ajaxMessage='Ha ocurrido un error al realizar la petición';
            if(xhr.status==0){
                ajaxMessage+=' (Sin conexión)';
            }else if(xhr.status==404){
                ajaxMessage+=' (No encontrado)';
            }
            toastr_show('Ministerio de Hacienda', ajaxMessage, 'error',{});
        },
        complete: function(){
            toastr_waiting.hide();
        }
    });
};
$.fn.cargar_provincia=function(){
    var provincia=$(this);
    $.ajax({
        url: global_const._routes.uri_ajax+global_const._routes.provincia,
        method: "GET",
        data: {
            "_token": global_const._token
        },
        success: function(resp, status, xhr){
            if(typeof xhr.responseJSON==='object' && xhr.responseJSON!==null){
                var json=xhr.responseJSON;
                if(typeof json.data=='object' && json.data!=null){
                    provincia.html($('<option>', {
                        text: 'Seleccione una provincia',
                        selected: true,
                        disabled: true
                    }));
                    $.each(json.data, function(index, v){
                        $('<option>', {
                            text: v.nombre,
                            value: v.cod_hacienda
                        }).appendTo(provincia);
                    });
                    global_const._ubicacion.provincia=set_value_if_exist(provincia, global_const._ubicacion.provincia);
                }
            }
        },
        error: function(xhr, status, error){
            var ajaxMessage='Ha ocurrido un error al cargar las provincias.';
            if(xhr.status==0){
                ajaxMessage+=' (Sin conexión)';
            }else if(xhr.status==404){
                ajaxMessage+=' (No encontrado)';
            }
            toastr_show('Provincia', ajaxMessage, 'error',{});
        }
    });
};
$.fn.cargar_canton=function(cod_hacienda){
    var canton=$(this);
    $.ajax({
        url: global_const._routes.uri_ajax+global_const._routes.canton+"/"+encodeURI(cod_hacienda),
        method: "GET",
        beforeSend: function(){
            var form=canton.closest('form');
            canton.html('<option value="">Cargando Cantones ...</option>');
            form.find('#distrito').html('<option value="">Debe seleccionar un cantón</option>');
            form.find('#barrio').html('<option value="">Debe seleccionar un distrito</option>');
        },
        data: {
            "_token": global_const._token,
        },
        success: function(resp, status, xhr){
            if(typeof xhr.responseJSON==='object' && xhr.responseJSON!==null){
                var json=xhr.responseJSON;
                if(typeof json.data=='object' && json.data!=null){
                    canton.html($('<option>', {
                        text: 'Seleccione un cantón',
                        selected: true,
                        disabled: true
                    }));
                    $.each(json.data, function(index, v){
                        $('<option>', {
                            text: v.nombre,
                            value: v.cod_facpro
                        }).appendTo(canton);
                    });
                    global_const._ubicacion.canton=set_value_if_exist(canton, global_const._ubicacion.canton);
                }
            }
        },
        error: function(xhr, status, error){
            var ajaxMessage='Ha ocurrido un error al cargar los cantones.';
            if(xhr.status==0){
                ajaxMessage+=' (Sin conexión)';
            }else if(xhr.status==404){
                ajaxMessage+=' (No encontrado)';
            }
            toastr_show('Cantón', ajaxMessage, 'error',{});
        }
    });
};
$.fn.cargar_distrito=function(cod_hacienda){
    var distrito=$(this);
    $.ajax({
        url: global_const._routes.uri_ajax+global_const._routes.distrito+"/"+encodeURI(cod_hacienda),
        method: "GET",
        beforeSend: function(){
            var form=distrito.closest('form');
            distrito.html('<option value="">Cargando Distritos ...</option>');
            form.find('#barrio').html('<option value="">Debe seleccionar un distrito</option>');
        },
        data: {
            "_token": global_const._token,
        },
        success: function(resp, status, xhr){
            if(typeof xhr.responseJSON==='object' && xhr.responseJSON!==null){
                var json=xhr.responseJSON;
                if(typeof json.data=='object' && json.data!=null){
                    distrito.html($('<option>', {
                        text: 'Seleccione un distrito',
                        selected: true,
                        disabled: true
                    }));
                    $.each(json.data, function(index, v){
                        $('<option>', {
                            text: v.nombre,
                            value: v.cod_facpro
                        }).appendTo(distrito);
                    });
                    global_const._ubicacion.distrito=set_value_if_exist(distrito, global_const._ubicacion.distrito);
                }
            }
        },
        error: function(xhr, status, error){
            var ajaxMessage='Ha ocurrido un error al cargar los distritos.';
            if(xhr.status==0){
                ajaxMessage+=' (Sin conexión)';
            }else if(xhr.status==404){
                ajaxMessage+=' (No encontrado)';
            }
            toastr_show('Distrito', ajaxMessage, 'error',{});
        }
    });
};
$.fn.cargar_barrio=function(cod_hacienda){
    var barrio=$(this);
    $.ajax({
        url: global_const._routes.uri_ajax+global_const._routes.barrio+"/"+encodeURI(cod_hacienda),
        method: "GET",
        beforeSend: function(){
            barrio.html('<option value="">Cargando Barrios ...</option>');
        },
        data: {
            "_token": global_const._token,
        },
        success: function(resp, status, xhr){
            if(typeof xhr.responseJSON==='object' && xhr.responseJSON!==null){
                var json=xhr.responseJSON;
                if(typeof json.data=='object' && json.data!=null){
                    barrio.html($('<option>', {
                        text: 'Seleccione un barrio',
                        selected: true,
                        disabled: true
                    }));
                    $.each(json.data, function(index, v){
                        $('<option>', {
                            text: v.nombre,
                            value: v.cod_facpro
                        }).appendTo(barrio);
                    });
                    global_const._ubicacion.barrio=set_value_if_exist(barrio, global_const._ubicacion.barrio);
                }
            }
        },
        error: function(xhr, status, error){
            var ajaxMessage='Ha ocurrido un error al cargar los barrios.';
            if(xhr.status==0){
                ajaxMessage+=' (Sin conexión)';
            }else if(xhr.status==404){
                ajaxMessage+=' (No encontrado)';
            }
            toastr_show('Barrio', ajaxMessage, 'error',{});
        }
    });
};
$.fn.cargar_metrica=function(){
    var metrica=$(this);
    $.ajax({
        url: global_const._routes.uri_ajax+global_const._routes.pauta,
        method: "GET",
        beforeSend: function(){
            metrica.html('<option value="">Cargando Pautas ...</option>');
        },
        data: {
            "_token": global_const._token,
        },
        success: function(resp, status, xhr){
            if(typeof xhr.responseJSON==='object' && xhr.responseJSON!==null){
                var json=xhr.responseJSON;
                if(typeof json.html=='string'){
                    metrica.html($('<option>', {
                        text: 'Seleccione un opción',
                        selected: true,
                        disabled: true
                    }));
                    $(json.html).appendTo(metrica);
                    global_const._metrica=set_value_if_exist(metrica, global_const._metrica);
                }
            }
        },
        error: function(xhr, status, error){
            var ajaxMessage='Ha ocurrido un error al cargar las pautas.';
            if(xhr.status==0){
                ajaxMessage+=' (Sin conexión)';
            }else if(xhr.status==404){
                ajaxMessage+=' (No encontrado)';
            }
            toastr_show('Pauta', ajaxMessage, 'error',{});
        }
    });
};
$.fn.verificar_descuento=function(){
    var btn=$(this);
    var html_btn=btn.html();
    var form=btn.closest('form');
    var codigo_descuento=$.trim(form.find('#codigo_descuento').val());
    var tipo_descuento=$.trim(form.find('#tipo_descuento').val());
    if(codigo_descuento.trim()!='' && tipo_descuento!=''){
        $.ajax({
            url: global_const._routes.uri_ajax+global_const._routes.validar_descuento+"/"+encodeURI(tipo_descuento)+"/"+encodeURI(codigo_descuento),
            method: "GET",
            data: {
                "_token": global_const._token,
            },
            beforeSend: function(){
                btn.attr('disabled', true);
                btn.html('Verificando código de descuento ...');
            },
            success: function(resp, status, xhr){
                if(typeof xhr.responseJSON==='object' && xhr.responseJSON!==null){
                    var json=xhr.responseJSON;
                    var fn_set_values=function(){
                        btn.attr('disabled', true);
                        form.find('#codigo_descuento').prop('readonly', true);
                        form.find('#tipoDescuento').val(form.find('#tipo_descuento').val())
                        form.find('#tipo_descuento').attr('disabled', true);
                    };
                    if(json.descuento=='1' && (json.proceso=='1' || json.proceso=='2')){
                        form.find('#descuento').val(json.porcentaje);
                        form.find('#proceso').val(json.proceso);
                        form.find('#plan_2').val(json.plan);
                        fn_set_values();
                    }else if(json.aplica_invitacion==1 && json.descuento==1){
                        form.find('#descuento').val(json.porcentaje);
                        form.find('#proceso').val(1);
                        form.find('#plan_2').val(1);
                        form.find('#aplica_invitacion').val(json.aplica_invitacion);
                        fn_set_values();
                    }else{
                        form.find('#descuento').val(0);
                        toastr_show('Descuento', 'El código de descuento no existe.', 'error',{});
                    }
                }
            },
            error: function(xhr, status, error){
                var ajaxMessage='Ha ocurrido un error al verificar el descuento.';
                if(xhr.status==0){
                    ajaxMessage+=' (Sin conexión)';
                }else if(xhr.status==404){
                    ajaxMessage+=' (No encontrado)';
                }
                form.find('#descuento').val(0);
                toastr_show('Descuento', ajaxMessage, 'error',{});
            },
            complete: function(){
                btn.removeAttr('disabled');
                btn.html(html_btn);
                form.aplicar_descuento();
            }
        });
    }else{
        form.find('#descuento').val(0);
        form.aplicar_descuento();
        var str_warning=[];
        if(tipo_descuento.trim()===''){
            str_warning.push('seleccione el tipo de descuento');
        }
        if(codigo_descuento.trim()===''){
            str_warning.push('ingrese el código de descuento');
        }
        toastr_show('Descuento', 'Por favor, '+str_warning.join(' y ')+'.', 'warning',{});
    }
};
$.fn.aplicar_descuento=function(){
    var form=$(this);
    if(form.find('#plan').val()===global_const._plan_pago_uso){
        return;
    }
    var descuento=parseFloat(form.find('#descuento').val());
    var input_costo=form.find('#costo');
    var costo=parseFloat(input_costo.data('costo'));
    var periodo=form.find('#periodo').val();
    var plan_descuento=form.find('#plan_2').val();
    var tipo_descuento=form.find('#proceso').val();
    var aplica_descuento_invitacion=form.find('#aplica_invitacion').val();
    var eliminar_desc=form.find('#eliminar_descuento');
    eliminar_desc.hide();
    if(costo>0 && descuento>0){
        var aplica=false;
        var str_no_aplica='';
        if(aplica_descuento_invitacion=='1'){
            //Aplica para ambos periodos
            aplica=true;
        }else if($.inArray(tipo_descuento, ['1','2']) !== -1 ){
            if(plan_descuento=='1'){
                //Aplica para ambos periodos
                aplica=true;
            }
            else if(plan_descuento== '2'){
                //Aplica únicamente para el periodo Anual
                if(periodo==global_const._codigo_concepto_anual){
                    aplica=true;
                }
                str_no_aplica=' (Descuento '+descuento.toFixed(2)+'% en Período Anual)';
            }
            else if(plan_descuento=='3'){
                //Aplica únicamente para el periodo Mensual
                if(periodo==global_const._codigo_concepto_mensual){
                    aplica=true;
                }
                str_no_aplica=' (Descuento '+descuento.toFixed(2)+'% en Período Mensual)';
            }
        }
        if(aplica){
            var descuento_USD=(costo*descuento)/100;
            var costo_USD=costo-descuento_USD;
            input_costo.val('$'+costo.toFixed(2)+ ' (Descuento '+descuento+'% $'+descuento_USD.toFixed(2)+') $'+costo_USD.toFixed(2));
        }
        else{
            input_costo.val('$'+costo.toFixed(2)+ str_no_aplica);
        }
    }
    /**
     * Mostar Link de eliminar descuento cuando:
     * - Tiene un descuento pero no ha seleccionado un periodo.
     * - Tiene un descuento y ha seleccionado un periodo.
     */
    if(descuento>0){
        eliminar_desc.show();
    }
};
$.fn.copiar_datos=function(){
    var t=$(this);
    var form=t.closest('form');
    var nombrecontacto='';
    var correoContacto='';
    if($(this).is(':checked')===true){
        nombrecontacto=form.find('#nombre').val();
        correoContacto=form.find('#email').val();
    }
    form.find('#nombre_contacto').val(nombrecontacto);
    form.find('#email_contacto').val(correoContacto);
};
var set_value_if_exist=function(element, value){
    element=$(element);
    if(element.find('option[value="'+value+'"]').length>0){
        element.val(value).change();
    }
    return "";
};
$.fn.eliminar_descuento=function(){
    var btn=$(this);
    var form=btn.closest('form');
    form.find('#codigo_descuento').prop('readonly', false).val('');
    form.find('#tipoDescuento').val('')
    form.find('#tipo_descuento').attr('disabled', false).val('');
    form.find('#descuento').val(0);
    form.find('#proceso').val(0);
    form.find('#plan_2').val(0);
    form.find('#periodo').change();
    btn.hide();
};
/**
 *
 * @param {String} title
 * @param {String} message
 * @param {String} type
 * @param {object} options
 * @returns {*}
 */
function toastr_show(title, message, type, options){
    var cfg=$.extend({}, global_const._toastr.options, options);
    if(type==='info'){
        return toastr.info(message, title, cfg);
    }else if(type==='success'){
        return toastr.success(message, title, cfg);
    }else if(type==='warning'){
        return toastr.warning(message, title, cfg);
    }else if(type==='error'){
        return toastr.error(message, title, cfg);
    }
}
