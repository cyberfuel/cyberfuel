
$("input[type='number']").inputSpinner()
$(".buttons-only").inputSpinner({ buttonsOnly: true, autoInterval: undefined })

var myCollapsRegistrar = document.getElementById('collapseRegistrar')
myCollapsRegistrar.addEventListener('show.bs.collapse', function () {
  var $inputNet = $("#inputNet")
  var $inputGross = $("#inputGross")
  $inputNet.on("input", function (event) {
    $inputGross.val($(event.target).val() * 15.99)
  })
  $inputGross.on("input", function (event) {
    $inputNet.val($(event.target).val() / 15.99)
  })
  $inputGross.val($inputNet.val() * 15.99)




})


var myCollapsTrans = document.getElementById('collapseTrans')
myCollapsTrans.addEventListener('show.bs.collapse', function () {

  var $inputNet2 = $("#inputNet2")
  var $inputGross2 = $("#inputGross2")
  $inputNet2.on("input", function (event) {
    $inputGross2.val($(event.target).val() * 15.99)
  })
  $inputGross2.on("input", function (event) {
    $inputNet2.val($(event.target).val() / 15.99)
  })
  $inputGross2.val($inputNet2.val() * 15.99)





})


var myCollapsUtilizar = document.getElementById('collapseUtilizar')
myCollapsUtilizar.addEventListener('show.bs.collapse', function () {

  var $inputNet2 = $("#inputNet2")
  var $inputGross2 = $("#inputGross2")
  $inputNet2.on("input", function (event) {
    $inputGross2.val($(event.target).val() * 15.99)
  })
  $inputGross2.on("input", function (event) {
    $inputNet2.val($(event.target).val() / 15.99)
  })
  $inputGross2.val($inputNet2.val() * 15.99)




})



//$(function () {
//  cargar_sugerencias_dominios();
//});

// boton de agregar en transferencia
var currentLanguage = $('html').attr('lang');
$(".boton_toggle_agregar, .boton_toggle_registrar").on("click", function () {


  if (currentLanguage === 'es') {
    $(this).children('.txt-agregado').text("Agregado");
  } else if (currentLanguage === 'en') {
    $(this).children('.txt-agregado').text("Aggregate");
  }
  $(this).removeClass('btn-primary w-100');
  $(this).addClass('btn-success');
  $(this).children('#check').removeClass('fa-shopping-cart');
  $(this).children('#check').addClass('fa-check');
});


$(".boton_toggle_agregar.utili").on("click", function () {


  if (currentLanguage === 'es') {
    $(this).children('.txt-agregado').text("Agregado");
  } else if (currentLanguage === 'en') {
    $(this).children('.txt-agregado').text("Aggregate");
  }
  $(this).removeClass('btn-primary w-100');
  $(this).addClass('btn-success');
  $(this).addClass('disabled');
  $(this).children('#check').removeClass('fa-shopping-cart');
  $(this).children('#check').addClass('fa-check');
  $('.boton_toggleTrans').parent().addClass('d-none');
  $(".cerrarAddUtilizar").parent().removeClass('d-none')

});

$(".cerrarAddUtilizar").on("click", function () {
  $(this).parent().addClass('d-none');
  $('.boton_toggleTrans').parent().removeClass('d-none');

  if (currentLanguage === 'es') {
    $(".boton_toggle_agregar.utili").children('.txt-agregado').text("Utilizar");
  } else if (currentLanguage === 'en') {
    $(".boton_toggle_agregar.utili").children('.txt-agregado').text("Use");
  }
  $(".boton_toggle_agregar.utili").addClass('btn-primary w-100');
  $(".boton_toggle_agregar.utili").removeClass('btn-success');
  $(".boton_toggle_agregar.utili").removeClass('disabled');
  $(".boton_toggle_agregar.utili").children('#check').addClass('fa-shopping-cart');
  $(".boton_toggle_agregar.utili").children('#check').removeClass('fa-check');
  $("#collapseUtilizar").collapse("toggle");


});

$(".cerrarAddRegistrar").click(function () {
  var cerrar_registrar = $(this).closest(".bot-registrar").find(".boton_toggle_registrar");
  // Realiza la acción que desees en el elemento "clase-cercana"
  cerrar_registrar.removeClass('disabled');
  $(this).addClass('d-none');
  $("#collapseRegistrar").collapse("toggle");

  if (currentLanguage === 'es') {
    cerrar_registrar.children('.txt-agregado').text("Registrar");
  } else if (currentLanguage === 'en') {
    cerrar_registrar.children('.txt-agregado').text("To register");
  }
  cerrar_registrar.children('#check').addClass('fa-shopping-cart');
  cerrar_registrar.children('#check').removeClass('fa-check');
});

$(".boton_toggle_registrar").click(function () {
  // Encuentra el elemento más cercano con la clase "clase-cercana" dentro del mismo contenedor
  var abrir_registrar = $(this).closest(".bot-registrar").find(".cerrarAddRegistrar");

  // Realiza la acción que desees en el elemento "clase-cercana"
  abrir_registrar.removeClass('d-none');
  $(this).addClass('disabled');
});

// boton de transferencia

$(".boton_toggleTrans").on("click", function () {


  if (currentLanguage === 'es') {
    $(this).children('.txt-agregado').text("Tranferido");
  } else if (currentLanguage === 'en') {
    $(this).children('.txt-agregado').text("Transfer");
  }
  $(this).removeClass('btn-primary w-100');
  $(this).addClass('btn-success');
  $(this).addClass('disabled');
  $(this).children('#check').removeClass('fa-exchange-alt');
  $(this).children('#check').addClass('fa-check');
  $('.boton_toggle_agregar.utili').parent().addClass('d-none');
  $(".cerrarAddTrans").parent().removeClass('d-none');
});

$(".cerrarAddTrans").on("click", function () {
  $(this).parent().addClass('d-none');
  $('.boton_toggle_agregar').parent().removeClass('d-none');



  if (currentLanguage === 'es') {
    $(".boton_toggleTrans").children('.txt-agregado').text("Transferir");
  } else if (currentLanguage === 'en') {
    $(".boton_toggleTrans").children('.txt-agregado').text("Transfer");
  }
  $(".boton_toggleTrans").addClass('btn-primary w-100');
  $(".boton_toggleTrans").removeClass('btn-success');
  $(".boton_toggleTrans").removeClass('disabled');
  $(".boton_toggleTrans").children('#check').addClass('fa-exchange-alt');
  $(".boton_toggleTrans").children('#check').removeClass('fa-check');
  $("#collapseTrans").collapse("toggle");
});





function verAddOnTrans() {

  $('.ocultar_textoTrans').addClass('d-none');
  $('.boton_toggleTrans').removeClass('btn-primary w-100');
  $('.boton_toggleTrans').addClass('btn-success');
  $('#checkTrans').removeClass('fa-exchange-alt');
  $('#checkTrans').addClass('fa-check');
  $('.no-disponible').addClass('d-none');
  $('.disponible').removeClass('d-none');
}


// aqui se esconden los hospedajes que no se ocupan
$(document).ready(function () {

 function escogerHost(){
    
    $(".mostrar").addClass('d-none'); 
    $(".mostrarCheck").removeClass('d-none'); 
    $(".btn-cerrar-host").removeClass('d-none'); 
    //$(".ocultar-precio").addClass('d-none');
    $(".mostrar").closest('.row').find('.mostrar-precio.hospedajes').removeClass('d-none')
    $(".quitar_fill").removeClass('flex-fill col-auto'); 
    $(".quitar_fill").addClass('col-md-auto col');
   }
    
  

  $(".mostrar").click(function () {

      
    // $(".cerrarFiltro").addClass('d-none');
    var target = $(this).data("target");
    $(".esconder-hospedaje").hide();
    $(".esconder-hospedaje[data-container='" + target + "']").show();

    var dataTargetValue = $(this).attr('data-target');

    if (dataTargetValue === 'container1') {
      $("#textTransfer").val(1.00);
      $("#textRegistrar").val(1.00);
      $("#textUtilizar").val(1.00);
      $(".selectorSpam").addClass('d-none');
    } else if (dataTargetValue === 'container2') {
      $("#textTransfer").val(10.00);
      $("#textRegistrar").val(10.00);
      $("#textUtilizar").val(10.00);
      $(".selectorSpam").addClass('d-none');
    } else if (dataTargetValue === 'container3') {
      $("#textTransfer").val(60);
      $("#textRegistrar").val(60);
      $("#textUtilizar").val(60);
      $(".selectorSpam").addClass('d-none');
    } else if (dataTargetValue === 'container4') {
      $("#textTransfer").val(120);
      $("#textRegistrar").val(120);
      $("#textUtilizar").val(120);
      $(".selectorSpam").addClass('d-none');
    } else if (dataTargetValue === 'container5') {
      $("#textTransfer").val(20);
      $("#textRegistrar").val(20);
      $("#textUtilizar").val(20);
      $(".selectorSpam").addClass('d-none');
    } else {
      // Acción por defecto si no coincide con ninguna opción
      alert('Opción no reconocida');
    }

      escogerHost();
      
      
  });
    
    


  $(".btn-cerrar-host").click(function () {
    $('.btn-cerrar-host').addClass('d-none');
    $(".esconder-hospedaje").show();
    $('.mostrar').removeClass('d-none');
    $(".mostrarCheck").addClass('d-none');
    $(".ocultar-precio").removeClass('d-none');
    //$(".mostrar-precio").addClass('d-none');
    $(".btn-cerrar-host").closest('.row').find('.mostrar-precio.hospedajes').addClass('d-none');
    $(".mostrar").addClass('flex-fill col-auto');
    $(".mostrar").removeClass('col-md-1 col');
    $(".quitar_fill").addClass('flex-fill col-auto');
    $(".quitar_fill").removeClass('col-md-1 col');
    $(".selectorSpam").removeClass('d-none');
    $("#textTransfer").val(1);

    // Seleccionar el elemento con la clase "selectorSpam"
    $(".selectorSpam input").val(1);
    $(".cerrarFiltro, .quitar-mes").removeClass('d-none');

  });


  $(".cerrarRegistro").click(function () {

    $(this).toggleClass('btn-quaternary btn-outline btn-primary');
    $(this).children().toggleClass('fa-cart-plus fa-2x');
    $(this).children().toggleClass('fa-times');
    var claseHermana = $(".mostrar-precio-filtro");
    claseHermana.toggleClass('d-none');
  });
    
    
   

  $(".toggleButton").click(function () {
     // alert('sirve');
  
    $(this).toggleClass('btn-quaternary btn-outline btn-primary');
    $(this).children().toggleClass('fa-cart-plus fa-2x');
    $(this).children().toggleClass('fa-times');
    $(this).closest('.row').find('.mostrar-precio').toggleClass('d-none');


  });



  $('.agregarFiltro').click(function () {
    //alert('sirve');
    $(this).addClass('d-none');
    $('.cerrarFiltro').removeClass('d-none');
  });

  $('.cerrar').click(function () {
    //alert('sirve');
    $('.cerrarFiltro').addClass('d-none');
    $('.agregarFiltro').removeClass('d-none');
  });


});




$(document).ready(function () {
  // Selecciona el input de tipo número
  $("#inputUtilizar").on("input", function () {
    // Obtiene el valor del input de tipo número
    var valor = $(this).val();
    // Imprime el valor en el campo de texto
    $("#textUtilizar").val(valor);
  });

  // Selecciona el input de tipo número
  $("#inputTransfer").on("input", function () {
    // Obtiene el valor del input de tipo número
    var valor = $(this).val();
    // Imprime el valor en el campo de texto
    $("#textTransfer").val(valor);
  });

  // Selecciona el input de tipo número
  $("#inputRegistrar").on("input", function () {
    // Obtiene el valor del input de tipo número
    var valor = $(this).val();
    // Imprime el valor en el campo de texto
    $("#textRegistrar").val(valor);
  });



});





