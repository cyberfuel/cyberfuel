
function consultar_dominio(url_redireccionar, dispositivo) {
    var dominio = $("#nombre_dominio_" + dispositivo).val();
    var extension = $("#extension_dominio_" + dispositivo).val();

    var dominio_completo = "";
    if ((dominio.length > 0) && (extension.length > 0)) {
        var dominio_completo = dominio + "." + extension;
    }

    if (dominio_completo.toString().length == 0) {
        $("#nombre_dominio_" + dispositivo).css("background-color", "#ffd5d5");
    } else {
        $.ajax({
            type: "POST",
            url: "consultar_dominio",
            headers: {
                "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
            },
            data: "dominio=" + dominio_completo,
            success: function () {
                window.location = base_path + "/" + url_redireccionar;
            }
        });
    }
}

//----------------------------------------------------------------------------------------------------------------------

function cargar_sugerencias_dominios() {
    $.ajax({
        type: "POST",
        async: true,
        url: "cargar_sugerencias_dominios",
        headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function () {
            $("#div_cargando_dominios").show();
        },
        success: function (html_sugerencias_dominios) {
            $("#div_cargando_dominios").hide();
            $("#div_sugerencias_dominios").html(html_sugerencias_dominios);
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function agregar_registro_dominio(dominio, cantidad) {
    var codigo_servicio = $("#codigo_registro_dominio").val();    //Codigo Servicio
    var periodo = "anual";                                        //Periodo
    var familia = $("#familia_registro_dominio").val();           //Familia del servicio
    var titulo = dominio;                                        //Titulo (nombre del dominio)
    var precio = $("#precio_registro_dominio").val();            //Precio

    carrito_compras_agregar_producto(codigo_servicio, periodo, cantidad, familia, titulo, precio);
}

//----------------------------------------------------------------------------------------------------------------------

function agregar_registro_dominio_sugerido(id_campo) {
    var codigo_servicio = $("#codigo_dominio_sugerido" + id_campo).val();     //Codigo Servicio
    var periodo = "anual";                                                   //Periodo
    var familia = $("#familia_dominio_sugerido" + id_campo).val();           //Familia del servicio
    var titulo = $("#dominio_sugerido" + id_campo).val();                   //Titulo (nombre del dominio)
    var precio = $("#precio_dominio_sugerido" + id_campo).val();            //Precio
    var cantidad = $("#anios" + id_campo).val();                              //Cantidad de anios

    changeColor($("#btn_dominio_sugerido" + id_campo));

    carrito_compras_agregar_producto(codigo_servicio, periodo, cantidad, familia, titulo, precio);
}

//----------------------------------------------------------------------------------------------------------------------

function agregar_complemento(id_complemento) {
    var codigo_servicio = $("#codigo_complemento" + id_complemento).val();   //Codigo Servicio
    var periodo = "mensual";                                                //Periodo
    var familia = $("#familia_complemento" + id_complemento).val();         //Familia del servicio
    var titulo = $("#titulo_complemento" + id_complemento).val();          //Titulo (nombre del dominio)
    var precio = $("#precio_complemento" + id_complemento).val();          //Precio
    var cantidad = 1;                                                        //Cantidad de meses

    //changeColor($("#btn_complemento"+id_complemento));

    carrito_compras_agregar_producto(codigo_servicio, periodo, cantidad, familia, titulo, precio);
}

//----------------------------------------------------------------------------------------------------------------------

function mostrar_datos_hospedaje(servicio, tipo_hospedaje) {
    //Linux (planes personal/pymes/empresarial)
    if ((servicio == "WH-LPPE") || (servicio == "WH-LPPY") || (servicio == "WH-LPEM")) {
        $("#span_" + tipo_hospedaje + "_windows_tarifa_mensual").hide();
        $("#span_" + tipo_hospedaje + "_windows_tarifa_anual").hide();
        $("#select_" + tipo_hospedaje + "_windows").hide();

        $("#span_" + tipo_hospedaje + "_linux_tarifa_mensual").show();
        $("#span_" + tipo_hospedaje + "_linux_tarifa_anual").show();
        $("#select_" + tipo_hospedaje + "_linux").show();

        $("#servidor_" + tipo_hospedaje).val("linux");
    }

    //Windows (planes personal/pymes/empresarial)
    if ((servicio == "WH-WPPE") || (servicio == "WH-WPPY") || (servicio == "WH-WPEM")) {
        $("#span_" + tipo_hospedaje + "_linux_tarifa_mensual").hide();
        $("#span_" + tipo_hospedaje + "_linux_tarifa_anual").hide();
        $("#select_" + tipo_hospedaje + "_linux").hide();

        $("#span_" + tipo_hospedaje + "_windows_tarifa_mensual").show();
        $("#span_" + tipo_hospedaje + "_windows_tarifa_anual").show();
        $("#select_" + tipo_hospedaje + "_windows").show();

        $("#servidor_" + tipo_hospedaje).val("windows");
    }
}

//----------------------------------------------------------------------------------------------------------------------

function agregar_plan_hospedaje(tipo_plan) {
    var sistema_operativo = $("#servidor_" + tipo_plan).val();                         //Sistema Operativo (linux o windows)
    var codigo_servicio = $("input:radio[name=rad_" + tipo_plan + "]:checked").val();  //Codigo del servicio
    var periodo = $("#select_" + tipo_plan + "_" + sistema_operativo).val();           //Periodo (mensual o anual)
    var familia = $("#familia_" + tipo_plan + "_" + sistema_operativo).val();          //Familia del servicio
    var titulo = $("#titulo_" + tipo_plan + "_" + sistema_operativo).val();           //Titulo
    var precio = $("#precio_" + tipo_plan + "_" + sistema_operativo).val();           //Precio

    switch (periodo) {
        case "mensual": var cantidad = 1; break;
        case "anual": var cantidad = 1; break;
    }

    carrito_compras_agregar_producto(codigo_servicio, periodo, cantidad, familia, titulo, precio);
}

//----------------------------------------------------------------------------------------------------------------------

function agregar_hospedaje_comercio_electronico() {
    var codigo_servicio = $("#codigo_comercio_electronico").val();    //Codigo Servicio
    var periodo = "mensual";                                          //Periodo
    var familia = $("#familia_comercio_electronico").val();           //Familia del servicio
    var titulo = $("#titulo_comercio_electronico").val();            //Titulo
    var precio = $("#precio_comercio_electronico").val();            //Precio
    var cantidad = 1;

    carrito_compras_agregar_producto(codigo_servicio, periodo, cantidad, familia, titulo, precio);
}

//----------------------------------------------------------------------------------------------------------------------

function carrito_compras_agregar_producto(codigo_servicio, periodo, cantidad, familia, titulo, precio) {
    $.ajax({
        type: "POST",
        url: "carrito_compras_agregar_producto",
        headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
        },
        data: "codigo_servicio=" + codigo_servicio + "&periodo=" + periodo + "&cantidad=" + cantidad + "&familia=" + familia + "&titulo=" + titulo + "&precio=" + precio,
        success: function () {
            //Actualiza total de productos en el carrito de compras
            carrito_compras_obtener_total_productos();

            //Actualiza html del carrito de compras
            carrito_compras_actualizar_lista_productos();

            //Muestra modal del carrito de compras
            toggleCart();
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function carrito_compras_eliminar_producto(index_carrito) {
    $.ajax({
        type: "POST",
        url: "carrito_compras_eliminar_producto",
        headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
        },
        data: "index_carrito=" + index_carrito,
        success: function (html_carrito_compras) {
            //Actualiza total de productos en el carrito de compras
            carrito_compras_obtener_total_productos();

            //Actualiza html del carrito de compras
            carrito_compras_actualizar_lista_productos();
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function carrito_compras_eliminar_producto_resumen(index_carrito) {
    $.ajax({
        type: "POST",
        url: "carrito_compras_eliminar_producto",
        headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
        },
        data: "index_carrito=" + index_carrito,
        success: function () {
            location.reload();
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function carrito_compras_obtener_total_productos() {
    $.ajax({
        type: "POST",
        url: "carrito_compras_obtener_total_productos",
        headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
        },
        success: function (total_productos_carrito_compras) {
            if (total_productos_carrito_compras > 0) {
                $("#span_total_carrito_compras").html(total_productos_carrito_compras);
            } else {
                $("#span_total_carrito_compras").html("");
            }
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function carrito_compras_actualizar_lista_productos() {
    $.ajax({
        type: "POST",
        url: "carrito_compras_actualizar_lista_productos",
        headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
        },
        success: function (html_carrito_compras) {
            $("#div_carrito_compras").html(html_carrito_compras);
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function datos_admin_igual_tecnico() {
    if ($("#chk_tecnico").is(":checked")) {
        $("#nombre_tecnico").val($("#nombre_admin").val());
        $("#apellido_tecnico").val($("#apellido_admin").val());
        $("#correo_tecnico").val($("#correo_admin").val());
        $("#tel_tecnico").val($("#tel_admin").val());
    } else {
        $("#nombre_tecnico").val("");
        $("#apellido_tecnico").val("");
        $("#correo_tecnico").val("");
        $("#tel_tecnico").val("");
    }
}

//----------------------------------------------------------------------------------------------------------------------

function datos_admin_igual_cobros() {
    if ($("#chk_cobro").is(":checked")) {
        $("#nombre_cobro").val($("#nombre_admin").val());
        $("#apellido_cobro").val($("#apellido_admin").val());
        $("#correo_cobro").val($("#correo_admin").val());
        $("#tel_cobro").val($("#tel_admin").val());
    } else {
        $("#nombre_cobro").val("");
        $("#apellido_cobro").val("");
        $("#correo_cobro").val("");
        $("#tel_cobro").val("");
    }
}

//----------------------------------------------------------------------------------------------------------------------

function aplicar_cupon_descuento(url_redireccionar) {
    var cupon_descuento = $.trim($("#cupon_descuento").val());
    if (cupon_descuento.length > 0) {
        $.ajax({
            type: "POST",
            url: "aplicar_cupon_descuento",
            headers: {
                "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
            },
            data: "cupon_descuento=" + cupon_descuento,
            success: function (cupon_aplicado) {
                if (cupon_aplicado == "ok") {
                    location.reload();
                } else {
                    $("#cupon_descuento").css("background-color", "#ffd5d5");
                    $("#div_error_cupon_descuento").show();
                }
            }
        });
    } else {
        $("#cupon_descuento").css("background-color", "#ffd5d5");
    }
}

//----------------------------------------------------------------------------------------------------------------------

function eliminar_cupon_descuento() {
    $.ajax({
        type: "POST",
        url: "eliminar_cupon_descuento",
        headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
        },
        data: "eliminar_cupon_descuento=eliminar",
        success: function () {
            location.reload();
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function redireccionar_pago(url_redireccionar) {
    var valida_formulario = true;

    //Validar campos requeridos
    $(".requerido").each(function (e) {
        if ($(this).val().length == 0) {
            $(this).css("background-color", "#ffd5d5");
            valida_formulario = false;
        } else {
            $(this).css("background-color", "white");
        }
    });

    if (valida_formulario == true) {
        $.ajax({
            type: "POST",
            url: "guardar_datos_temporales_cliente",
            headers: {
                "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
            },
            data: $("#form_registro").serialize(),
            success: function (resultado) {
                var obj_resultado = JSON.parse(resultado);

                if (obj_resultado.estado == "ok") {
                    window.location = base_path + "/" + url_redireccionar;
                } else {
                    mostrar_mensaje_sweet_alert_aceptar(obj_resultado.titulo, obj_resultado.mensaje, "error", obj_resultado.boton_aceptar);
                }
            }
        });
    }
}

//----------------------------------------------------------------------------------------------------------------------

function procesar_registro_con_compra() {
    if (idioma_global == "es") {
        var titulo_registro = "Registro Cliente";
        var mensaje_registro = "Desea realmente proceder con el registro?"
        var texto_boton_confirmar = "Aceptar";
        var texto_boton_cancelar = "Cerrar";
    } else {
        var titulo_registro = "Customer Registration";
        var mensaje_registro = "Do you really want to proceed with the registration?";
        var texto_boton_confirmar = "Accept";
        var texto_boton_cancelar = "Close";
    }

    Swal.fire({
        width: '500px',
        title: titulo_registro,
        text: mensaje_registro,
        icon: "question",
        showCancelButton: true,
        confirmButtonColor: '#0088cc',
        cancelButtonColor: '#dc3545',
        confirmButtonText: texto_boton_confirmar,
        cancelButtonText: texto_boton_cancelar
    }).then((result) => {
        if (result.isConfirmed) {

            $.ajax({
                type: "POST",
                url: "procesar_registro_con_compra",
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                data: "registro=cliente",
                beforeSend: function () {
                    $("#div_cargando_sitio").show();
                },
                complete: function () {
                    $("#div_cargando_sitio").hide();
                },
                success: function (resultado_registro) {
                    var datos_resultado = JSON.parse(resultado_registro);

                    if (datos_resultado.estado == "ok") {
                        mostrar_mensaje_sweet_alert_aceptar(datos_resultado.titulo, datos_resultado.mensaje, "success", datos_resultado.boton_aceptar);
                    } else {
                        mostrar_mensaje_sweet_alert_aceptar(datos_resultado.titulo, datos_resultado.mensaje, "error", datos_resultado.boton_aceptar);
                    }
                }
            });

        }
    });
}

//----------------------------------------------------------------------------------------------------------------------

function permitir_solo_numeros(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

//----------------------------------------------------------------------------------------------------------------------

function mostrar_mensaje_sweet_alert_aceptar(titulo, mensaje, tipo_mensaje, texto_boton) {
    Swal.fire({
        width: '500px',
        title: titulo,
        text: mensaje,
        icon: tipo_mensaje,
        confirmButtonText: texto_boton
    });
}
