
$(".contact-form").each(function () {
  $(this).validate({
    errorPlacement: function (e, o) {
      "radio" == o.attr("type") || "checkbox" == o.attr("type") ? e.appendTo(o.closest(".form-group")) : o.is("select") && o.closest(".custom-select-1") ? e.appendTo(o.closest(".form-group")) : o.closest(".form-group").length ? e.appendTo(o.closest(".form-group")) : e.insertAfter(o)
    },
    submitHandler: function (e) {
      var o = $(e),
        s = o.find(".contact-form-success"),
        r = o.find(".contact-form-error"),
        a = $(this.submitButton),
        t = o.find(".mail-error-message"),
        n = a.val();
      a.val(a.data("loading-text") ? a.data("loading-text") : "Loading...").attr("disabled", !0);
      var l = o.serializeArray(),
        c = {};
      $(l).each(function (e, o) {
        c[o.name] = o.value
      }), void 0 != c["g-recaptcha-response"] && (c["g-recaptcha-response"] = o.find("#g-recaptcha-response").val()), $.ajax({
        type: "POST",
        url: o.attr("action"),
        data: c
      }).always(function (e, l, c) {
        if (t.empty().hide(), "success" == e.response) return s.removeClass("d-none"), r.addClass("d-none"), o.find(".form-control").val("").blur().parent().removeClass("has-success").removeClass("has-danger").find("label.error").remove(), s.offset().top - 80 < $(window).scrollTop() && $("html, body").animate({
          scrollTop: s.offset().top - 80
        }, 300), o.find(".form-control").removeClass("error"), void a.val(n).attr("disabled", !1);
        "error" == e.response && void 0 !== e.errorMessage ? t.html(e.errorMessage).show() : t.html(e.responseText).show(), r.removeClass("d-none"), s.addClass("d-none"), r.offset().top - 80 < $(window).scrollTop() && $("html, body").animate({
          scrollTop: r.offset().top - 80
        }, 300), o.find(".has-success").removeClass("has-success"), a.val(n).attr("disabled", !1)
      })
    }
  })
})


$(".infoToggler").click(function () {
  $(this).find('img').toggle();
});

$(".recupera").click(function () {
  $("#login").fadeToggle();
});

$(".regresa").click(function () {
  $("#login").fadeToggle();
});

//side cart
function toggleCart() {
  document.querySelector('.sidecart').classList.toggle('open-cart');

}

function changeColor(elemento) {
  $(elemento).addClass("btn-success").find("i").addClass("fas fa-check");

}

/*necesita dominio*/

$("#btnTengo").click(function () {

  $("#ya-tengo").show();
});

function mostrar_cd() {
  $("#opcion_filtro").removeClass("d-none");
}

function mostrar_dominio() {
  $("#opcion_dominio").removeClass("d-none");
}


$(document).ready(function () {
  $('img[alt=""]').prop('alt', 'Cyberfuel, su sitio web en manos de expertos');
  $('img[title=""]').prop('alt', 'Cyberfuel, los mejores en hospedajes y sitios web');
  $('img').attr('title', 'Cyberfuel, su sitio web en manos de expertos');


  $('img').not('.carousel-item .w-100').attr('loading', 'lazy');
  $('a').each(function () {
    var textolabel = $(this).text();
    $(this).attr('aria-label', textolabel);
    if ($(this).attr('aria-label', '')) {
      $(this).attr('aria-label', textolabel);
    }
  });




});


$('.chat-overlay img').attr('width', '50');
$('.chat-overlay img').attr('height', '50');

$('.whatsapp').attr('width', '50');
$('.whatsapp').attr('aria-label', 'whatsapp');

$('.card-img, .card-img-bottom, .card-img-top.carru').attr('width', '50');

$('.featured-box').hover(
  function () {
    $(this).addClass('sombra-servicios');
  }, function () {
    $(this).removeClass('sombra-servicios');
  }
);
//////////////////////

/*alert momentareos*/
document.addEventListener('DOMContentLoaded', function () {
  document.getElementById('conRegistros').addEventListener('click', function () {
    Swal.fire({
      position: "top-end",
      icon: "success",
      title: "Registro exitoso",
      showConfirmButton: false,
      position: "center",
      timer: 1500
    }).then((result) => {
      window.location.href = "/auth"
    });
  });
});

//script para que se abra en contactenos la opcion de select bass
$(document).ready(function () {
  $('#redireccionarBtn').click(function () {
    // Redirigir a la página de contacto después de hacer clic en el botón
    window.location.href = '/contactenos';
  });

  $(window).on('load', function () {
    // Obtener el elemento select después de que se haya cargado completamente la página de contacto
    var selectElement = $('#servicio');

    // Obtener el valor de la opción deseada ('Baas' en este caso)
    var opcionDeseada = 'Baas';

    // Recorrer todas las opciones del select
    selectElement.find('option').each(function () {
      // Verificar si el valor de la opción es el deseado
      if ($(this).val() === opcionDeseada) {
        // Seleccionar la opción deseada
        $(this).prop('selected', true);
        // Salir del bucle una vez encontrada la opción deseada
        return false;
      }
    });
  });
});



