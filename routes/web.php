<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Livewire\Select2Dropdown;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/ver_version_laravel', 'MainController@ver_version_laravel');

/*  ruta para idioma  */
Route::get('/lang/{language}', function ($language) {
    session()->put('language', $language);
    app()->setLocale(session()->get('language'));
    return back();
})->middleware('translate')->name('language');

Route::get('/', 'MainController@index')->name('main')->middleware('translate');

Route::get('contactenos', 'ContactFormController@form')->name('contactenos.form');
Route::get('contact-us', 'ContactFormController@form')->name('contactenos.form');

Route::post('contactenos', 'ContactFormController@send')->name('contactenos.send');;

Route::get('/{slug}', 'HospedajeController@index')->name('hospedaje-web')->middleware('translate');

/*Route::post('orden' function(){
	return redirect()->route('/');
})->name ('orden');
*/

Route::get('auth', function () {
    return view('auth');
});




//Cliente
Route::post('/guardar_datos_temporales_cliente', 'HospedajeController@guardar_datos_temporales_cliente');
Route::post('/procesar_registro_con_compra', 'HospedajeController@procesar_registro_con_compra');

//Descuento
Route::post('/aplicar_cupon_descuento', 'HospedajeController@aplicar_cupon_descuento');
Route::post('/eliminar_cupon_descuento', 'HospedajeController@eliminar_cupon_descuento');

//Dominios
Route::post('/consultar_dominio', 'HospedajeController@consultar_dominio');
Route::post('/cargar_sugerencias_dominios', 'HospedajeController@cargar_sugerencias_dominios');

//Carrito Compras
Route::post('/carrito_compras_agregar_producto', 'HospedajeController@carrito_compras_agregar_producto');
Route::post('/carrito_compras_eliminar_producto', 'HospedajeController@carrito_compras_eliminar_producto');
Route::post('/carrito_compras_actualizar_lista_productos', 'HospedajeController@carrito_compras_actualizar_lista_productos');
Route::post('/carrito_compras_obtener_total_productos', 'HospedajeController@carrito_compras_obtener_total_productos');
//Route::post('contactenos', [
//    ContactenosController::class,
//    'enviar'
//])->name('contactenos');

Route::get('/test', [Select2Dropdown::class, '__invoke'])->name('test');


//Route::get('contactenos', 'ContactFormController@form')->name('contactenos.form')->middleware('translate');
//Route::post('contactenos', 'ContactFormController@send')->name('contactenos.send')->middleware('translate');

Route::prefix('landings')->group(function () {
    Route::get('servicios-productos', function () {
        return view('landing/servicios-productos');
    })->name('servicios-productos');
});
