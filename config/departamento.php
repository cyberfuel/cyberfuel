<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Departamentos - Formulario de contáctenos
    |--------------------------------------------------------------------------
    |
    | Lista de departamentos disponibles para el formulario de contáctenos.
    | Cada departamento podrá tener 1 a n correos electrónicos.
    */

    /*
    'general'=>[
        'str'=>'General',
        'email'=>(env('APP_ENV', 'local')==='local'?[
            'gsoto@cyberfuel.com',
            'pcarranza@cyberfuel.com',
            'sac@cyberfuel.com'
        ]:[
            'sac@cyberfuel.com'
        ])
    ],
    */
    'ventas'=>[
        'str'=>'Ventas',
        'email'=>(env('APP_ENV', 'local')==='local'?[
            //'gsoto@cyberfuel.com',
            'ventas@cyberfuel.com'
        ]:[
            'ventas@cyberfuel.com',
            'ventaspsc@cyberfuel.com'
        ])
    ],
    'soporte'=>[
        'str'=>'Soporte Técnico',
        'email'=>(env('APP_ENV', 'local')==='local'?[
           // 'gil1382@gmail.com',
            'soporte@cyberfuel.com',
        ]:[
            'soporte@cyberfuel.com',
        ])
    ]
];
